<?php require('template/_header.php'); ?>
<?php require('template/_navbar.php'); ?>

<style>
  .media{
	  padding:10px;
	  border-bottom:1px #eee solid;
	  margin-bottom: -20px;
  }
  .media:hover {
	  background:#fbfbfb;
  }

  .bounty{
	  padding:10px;
	  text-align:center;
	  background:#eee;
  }

    /* RESPONSIVE CSS
    -------------------------------------------------- */
    @media (max-width: 767px) {
      .marketing .span4 + .span4 {
        margin-top: 40px;
      }

    }
    </style>

    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container">
    <?php require('template/_flashdata_show.php'); ?>
      <!-- Three columns of text below the carousel -->
      <!--<h2>他們都用 Citytasker ！</h2>
      <p>Citytasker 的目標讓"整個城市都有我的好幫手"！我們連結上千位值得信任的台北人，擁有不同的技能、人脈

      每個人的人脈有限、專長有限、時間更是有限，因此我們串聯上千位值得信任的台北人，來幫助你解決生活中重要的大小事，在你需要的時候與地點

      可以在這裡輕鬆找到城市中值得信任的好幫手，幫你處理生活中的大小事，也可以從周遭找到幫助別人的機會，運用閒餘時間還能賺點外快！</p>
      <br/>-->
      <h2>如何讓任務完成？</h2>
      <hr>
      <div class="row-fluid marketing">
        <div class="span4">
          <img src="<?= base_url("img/web/hiw-1.png"); ?>">
          <h4>步驟1：輕鬆發布任務</h4>
          <p>描述任務、時間、地點等資訊，以及你願意支付多少酬勞讓任務達成。你可以直接用現金或是匯款來交易，但別忘了只有在<strong>任務完成後才需要支付</strong>！</p>
          <p>幫手對任務產生疑問，請多提供資訊來確保達成你的期待。為保護<strong>個人隱私</strong>資料，可以選擇哪些資訊公開或只給出任務的幫手知道。</p>
        </div><!-- /.span4 -->
        <div class="span4">
          <img src="<?= base_url("img/web/hiw-2.png"); ?>">
          <h4>步驟2：找對的幫手做對的任務</h4>
          <p>每一位 Citytasker 成員都必須完成<strong>手機驗證</strong>，透過過去工作紀錄以及評價，可以讓你找出最適合解決任務的幫手。只需要把任務交辦出去，就可以等著驗收成果。</p>
          <p>有任何關於任務的進展，Citytasker 會以信件讓你隨時掌控。</p>
        </div><!-- /.span4 -->
        <div class="span4">
          <img src="<?= base_url("img/web/hiw-3.png"); ?>">
          <h4>步驟3：完成後付款</h4>
          <p>一旦有幫手接下任務，會收到一份關於他的完整機密檔案以<strong>防止詐騙</strong>事件發生，同樣你的私密訊息與個人資料也只有他會收到。</p>
          <p>任務完成後，可以直接用現金或匯款來支付你的幫手，如果他做的很棒也別忘了給他一些鼓勵與評價！</p>
          <p>有甚麼任務需要完成呢？ <strong><a href="<?= base_url("task/post"); ?>">發布任務</a></strong></p>
        </div><!-- /.span4 -->
      </div><!-- /.row -->
      <br/>
      <br/>
      <h2>為什麼要在 Citytasker 發布任務？</h2>
      <hr>
      <div class="row-fluid marketing">
         <div class="span1">
         </div><!-- /.span4 -->
         <div class="span4">
            <h4>成為會員是免費的</h4>
            <p>只有當你發布的任務被完成後才需要付款，除此之外在平台裡進行任何事都是免費的。</p>
         </div><!-- /.span4 -->
         <div class="span2">
         </div><!-- /.span4 -->
         <div class="span4">
            <h4>便利的在地化服務</h4>
      		<p>我們在北北基建立了一個由上萬人串聯的服務網路，可以讓你更輕鬆找到合適的幫手，更棒的是他們可能就在你的辦公室或居住的社區附近。</p>
         </div><!-- /.span4 -->
      </div><!-- /.row -->
      <div class="row-fluid marketing">
         <div class="span1">
         </div><!-- /.span4 -->
         <div class="span4">
            <h4>快速發布並輕鬆管理</h4>
            <p>只需要30秒的時間就能完成任務發布，關於任務的進度、報名、甚至是相關討論，都會同時呈現在專屬頁面中。</p>
         </div><!-- /.span4 -->
         <div class="span2">
         </div><!-- /.span4 -->
         <div class="span4">
            <h4>一切由你定義</h4>
            <p>你可以天馬行空指定想被完成的任務，制定你想支付的價錢、地點、時間，還可以選擇你想讓誰來完成，全都由你做主！</p>
         </div><!-- /.span4 -->
      </div><!-- /.row -->
      <div class="row-fluid marketing">
         <div class="span1">
         </div><!-- /.span4 -->
         <div class="span4">
            <h4>連結多元化的資源共享網路</h4>
            <p>我們的成員來自社會中各行各業不同年齡的人，擁有多元的專業能力、人脈網路、興趣、想法或是各種器材，樂於透過平台互相協助彼此。</p>
         </div><!-- /.span4 -->
         <div class="span2">
         </div><!-- /.span4 -->
         <div class="span4">
            <h4>完全的網路環境</h4>
            <p>每一位成員都必須完成 facebook 帳號、電子郵件以及手機認證，以確保真實身分以及正當性，透並過透明化的個人履歷來了解每個人的專長、經歷以及過去的評價。同時您的個資也會受到保護不用攤在陽光下，只有當你確定雇用對象時雙方才會取得對方的聯絡方式。</p>
         </div><!-- /.span4 -->
      </div><!-- /.row -->
      <div class="row-fluid marketing">
         <div class="span1">
         </div><!-- /.span4 -->
         <div class="span4">
            <h4>智慧的網路系統</h4>
      		<p>根據任務發布的所在地區，系統會分析出鄰近且有能力的人來發送出任務邀請。任務中發生的任何事，都會透過電子郵件自動傳遞，讓雙方都能清楚掌握目前的進度。</p>
         </div><!-- /.span4 -->
         <div class="span2">
         </div><!-- /.span4 -->
         <div class="span4">
            <h4>創造正向的在地就業機會</h4>
      		<p>讓每一分錢都能回到你的城市，刺激在地經濟，還能協助更多社會中弱勢的朋友創造新型態的工作機會。</p>
         </div><!-- /.span4 -->
      </div><!-- /.row -->
      <br/>
      <br/>
      <h2>何時使用 Citytasker？</h2>
      <hr>
      <div class="row-fluid marketing">
         <div class="span1">
         </div><!-- /.span4 -->
         <div class="span5">
            <h4>當你忙不過來時</h4>
      		<p>生活中有很多不重要卻又必須做的事，佔用我們不少寶貴時間。</p>
         </div><!-- /.span4 -->
         <div class="span1">
         </div><!-- /.span4 -->
         <div class="span5">
            <h4><i class="icon-leaf"></i> 輕鬆生活</h4>
            <p>你可以把時間留在真正重要或真正想做的部分，生命本該浪費在美好事物
         </div><!-- /.span4 -->
      </div><!-- /.row -->
      
      <h4>當你需要一個團隊時</h4>
      <h4>當你不知道找誰幫忙時</h4>
      <h4>當你行動不便時</h4>
      <h4>當你人在外地時</h4>
      <h4>當你需要特殊能力時</h4>
      <h4>當你想要好好休息時</h4>
      <h4>當你不確定員工是否合適時</h4>
      
      <br/>
      <br/>
      <h2>如何賺外快？</h2>
      <hr>
      <div class="row-fluid marketing">
        <div class="span4">
          <img src="<?= base_url("img/web/hiw-4.png"); ?>">
          <h4>步驟1：搜尋在你身邊的任務</h4>
          <p><strong>打開任務地圖</strong>，找到你能夠完成的任務，按下<strong>我要出任務</strong>按鈕後 Citytasker 便會通知案主你已經準備好出任務。</p>
          <p><strong>請注意！</strong>當雇主同意之後，就代表已經被指派任務，可以開始執行任務或是與案主直接進行進一步聯繫。</p>
        </div><!-- /.span4 -->
        <div class="span4">
          <img src="<?= base_url("img/web/hiw-5.png"); ?>">
          <h4>步驟2：確定任務內容並完成</h4>
          <p>為了<strong>保護你的權益</strong>，可以透過 Citytasker 提供的留言功能與案主溝通任務內容與細節，降低任務失敗的風險。</p>
          <p>所有討論內容都將會被記錄並生成一份具有法律效力的文件，若任務結束出現爭議將成為依據。</p>
        </div><!-- /.span4 -->
        <div class="span4">
          <img src="<?= base_url("img/web/hiw-6.png"); ?>">
          <h4>步驟3：賺外快，得聲譽</h4>
          <p>利用你的閒暇時間來完成任務，完成後就能直接拿到現金賺外快。累積任務經驗與評價，可以讓未來更有機會得到更多任務的指派！</p>
          <p>準備好出任務了嗎？ <strong><a href="<?= base_url("task/map"); ?>">搜尋任務</a></strong></p>
        </div><!-- /.span4 -->
      </div><!-- /.row -->
		<br/>
      <br/>
      <h2>為什麼要在 Citytasker 發布任務？</h2>
      <hr>
      <div class="row-fluid marketing">
         <div class="span1">

         </div><!-- /.span4 -->
         <div class="span5">
            <h4><i class="icon-thumbs-up"></i> 成為會員是免費的</h4>
            <p>只有當你發布的任務被完成後才需要付款，除此之外在平台裡進行任何事都是免費的。</p>
         </div><!-- /.span4 -->
         <div class="span1">
         </div><!-- /.span4 -->
         <div class="span5">
            <h4>方便的在地服務</h4>
      		<p>我們在北北基建立了一個由上萬人串聯的服務網，可以讓你更輕鬆找到合適的幫手，而且可能就在你的辦公室附近或居住的社區。</p>
         </div><!-- /.span4 -->
      </div><!-- /.row -->
      <div class="row-fluid marketing">
         <div class="span1">

         </div><!-- /.span4 -->
         <div class="span5">
            <h4><i class=" icon-random"></i> 快速發布</h4>
            <p>直接透過表單填寫相關資訊，不怕遺漏、格式乾淨，轉貼任務只需要複製網址即可，更方便使用。</p>
         </div><!-- /.span4 -->
         <div class="span1">
         </div><!-- /.span4 -->
         <div class="span5">
            <h4><i class="icon-th-list"></i> 輕鬆管理</h4>
            <p>透過特別設計的系統功能，可以讓你輕鬆從報名者名單中，選擇想雇用的人手。</p>
         </div><!-- /.span4 -->
      </div><!-- /.row -->
      <div class="row-fluid marketing">
         <div class="span1">

         </div><!-- /.span4 -->
         <div class="span5">
            <h4><i class="icon-envelope"></i> 自動通知</h4>
            <p>任務的每個狀態、有人詢問、報名或出任務，系統都會自動寄發信件通知，隨時掌握最新資訊。</p>
         </div><!-- /.span4 -->
         <div class="span1">
         </div><!-- /.span4 -->
         <div class="span5">
            <h4><i class="icon-flag"></i> 緊急動員</h4>
            <p>透過平台中的資料比對，可以搜尋到任務附近的 Citytasker 成員，並發出任務通知來吸引他們注意。</p>
         </div><!-- /.span4 -->
      </div><!-- /.row -->
      <div class="row-fluid marketing">
         <div class="span1">
         </div><!-- /.span4 -->
         <div class="span5">
            <h4>創造正向的在地就業機會</h4>
      		<p>讓每一分錢都能回到你的城市，刺激在地經濟。</p>
         </div><!-- /.span4 -->
         <div class="span1">
         </div><!-- /.span4 -->
         <div class="span5">
            <h4><i class="icon-leaf"></i> 輕鬆生活</h4>
            <p>你可以把時間留在真正重要或真正想做的部分，生命本該浪費在美好事物
         </div><!-- /.span4 -->
      </div><!-- /.row -->
      <br/>
      <br/>
      <h2>你應該來 Citytasker 出任務，因為...</h2>
      <hr>
      <div class="row-fluid marketing">
         <div class="span1">
         </div><!-- /.span4 -->
         <div class="span5">
            <h4>創造正向的在地就業機會</h4>
      		<p>讓每一分錢都能回到你的城市，刺激在地經濟。</p>
         </div><!-- /.span4 -->
         <div class="span1">
         </div><!-- /.span4 -->
         <div class="span5">
            <h4><i class="icon-leaf"></i> 輕鬆生活</h4>
            <p>你可以把時間留在真正重要或真正想做的部分，生命本該浪費在美好事物
         </div><!-- /.span4 -->
      </div><!-- /.row -->
      <h4>你有很多空嫌時間</h4>
      <h4>你急需要一筆錢</h4>
      <h4>你想體驗不同工作</h4>
      <h4>你想在進入職場前先實習</h4>
      <h4>你想在進入職場前先實習</h4>
      <br/>
      <br/>
      <div class="row-fluid">
      	<div class="span6">
            <h3>介紹影片</h3>
      		<hr>
         	<iframe width="100%" height="320" src="https://www.youtube-nocookie.com/embed/36iGwrzIMbM" frameborder="0" allowfullscreen></iframe>
         </div>
      	<div class="span6">
            <h3>成為粉絲</h3>
      		<hr>
         	<iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fcitytasker&amp;width=450&amp;height=350&amp;show_faces=true&amp;colorscheme=light&amp;stream=false&amp;show_border=false&amp;header=false&amp;appId=258248100903520" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100%; height:320px;" allowTransparency="true"></iframe>
         </div>
      </div>
    </div>

<?php require('template/_copyright.php'); ?>
<?php require('template/_footer.php'); ?>
