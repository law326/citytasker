<?php require('template/_header.php'); ?>
<?php require('template/_navbar.php'); ?>

<style>
@media (min-width: 767px) {
	#mobi{
		display:none;
	}
}
</style>

<div id="mobi" style="text-align:center; width:100%; padding-top:20px; margin-bottom:-20px;">
	<img class="img-brand" src="<?= base_url("img/web/logo.png"); ?>" />
</div>

<div class="container">
	<?php require('template/_flashdata_show.php'); ?>

   <form id="contact-form" class="narrow contact-form" action="<?= base_url("user/signup_ing");?>" method="post" accept-charset="utf-8">
   <fieldset>
      <h2 class="form-signup-heading">註冊<span id="mobi"> / <a href="<?= base_url("user/login"); ?>">登入</a></span></h2>
      <input type="text" class="input-block-level" placeholder="使用者名稱"
      name="username" value="<?=$this->input->post('username')?>">

      <input type="text" class="input-block-level" placeholder="Email"
      id="email" name="email" value="<?=$this->input->post('email')?>">

      <input type="text" class="input-block-level" maxlength="3" placeholder="郵遞區號 (3碼)"
      name="zip" value="<?=$this->input->post('zip')?>">

      <input type="password" class="input-block-level" placeholder="密碼 (至少6字元)" name="password">
      <input class="btn btn-large btn-block" title="Citytasker 保證不會主動寄發垃圾郵件，並且可以隨時終止你的帳戶或停用本服務，請安心註冊。"
      type="submit" name="name" value="免費註冊" onclick="this.disabled=true;this.value='提交中...';this.form.submit();" />
      <hr>
      <button class="btn btn-large btn-block btn-primary" type="button">連結 facebook</button>
      <hr>
      <p>按下<strong>"免費註冊"</strong>後，即表示你已經同以本服務之<strong><a href="<?= base_url("service/terms"); ?>" target="_blank">使用條款</a></strong>，並將遵守規定。</p>

      <?php //因為有開啟CSRF
      $CI =& get_instance();
      $csrf_name = $CI->security->get_csrf_token_name();
      $csrf_value = $CI->security->get_csrf_hash();
      ?>
      <input type="hidden" name="<?= $csrf_name ?>" value="<?= $csrf_value ?>" />

      <?php //避免重複提送表單
      $resubmit_code = 'not yet';
      $_SESSION['resubmit_code'] = $resubmit_code;
      ?>
      <input type="hidden" name="resubmit_code" value="<?=$resubmit_code?>">
   </fieldset>
   </form>

</div><!-- /container -->

<?php require('template/_copyright.php'); ?>
<script src="<?= base_url("js/jquery.validate.js"); ?>"></script>
<script>
$(document).ready(function(){

 $('#contact-form').validate(
 {
  rules: {
    name: {
      minlength: 2,
      required: true
    },
    email: {
      required: true,
      email: true
    },
    subject: {
      minlength: 2,
      required: true
    },
    message: {
      minlength: 2,
      required: true
    }
  },
  highlight: function(element) {
    $(element).closest('.control-group').removeClass('success').addClass('error');
  },
  success: function(element) {
    element
    .text('OK!').addClass('valid')
    .closest('.control-group').removeClass('error').addClass('success');
  }
 });
}); // end document.ready
</script>
<?php require('template/_footer.php'); ?>