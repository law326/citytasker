<?php require('template/_header.php'); ?>
<?php require('template/_navbar.php'); ?>
<?php require('template/_flashdata_show.php'); ?>

<!-- Facebook login
================================================== -->
<div class="row-fluid">
   <button id="facebook" class="btn btn-facebook btn-large">使用 facebook 快速登入</button>
   <span class="help-inline">馬上就能使用更多完整功能喔！</span>
   <hr>
   <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcitytasker&amp;send=false&amp;layout=standard&amp;width=450&amp;show_faces=true&amp;font&amp;colorscheme=light&amp;action=like&amp;height=80&amp;appId=258248100903520" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:80px;" allowTransparency="true"></iframe>
</div>

<!--
<form action="<?= base_url("service/email_sending/");?>" method="post" accept-charset="utf-8" AUTOCOMPLETE="off">
    <div id="contact"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
         <h3 id="myModalLabel">意見回饋</h3>
      </div>
      <div class="modal-body">
         <p>使用Facebook註冊發生異常，造成您的困擾實感抱歉，無法登入屬於特殊情形，目前技術人員正在處理中
請留下您的資料，我們在您的帳號啟用後會與您連絡，謝謝。</p>
         <input type="text" class="input-block-level" maxlength="20" value="我使用Facebook註冊發生異常"
         name="sending_title" value="<?= html_entity_decode($this->input->post('sending_title'))?>">
         <br />
         <textarea rows="5" style="width:96%;" id='ct1' placeholder="請輸入您的facebook帳號(數字或數字加英文組合)"
         name="sending_content" ><?= html_entity_decode($this->input->post('sending_content'))?></textarea>
         <br />
         <input type="text" class="input-block-level" maxlength="50" placeholder="你的Email"
         name="sending_email" value="<?= html_entity_decode($this->input->post('sending_email'))?>">

         <input type="hidden" name="user_agent" value="<?= $this->input->user_agent(); ?>" />
         <input type="hidden" name="former_URL" value="<?= $this->uri->uri_string(); //所在頁面?>" />

         <?php //因為有開啟CSRF
         $CI =& get_instance();
         $csrf_name = $CI->security->get_csrf_token_name();
         $csrf_value = $CI->security->get_csrf_hash();
         ?>
         <input type="hidden" name="<?= $csrf_name ?>" value="<?= $csrf_value ?>" />
      </div>
      <div class="modal-footer">
         <input class="btn btn-large" type="submit" name="name" value="送出" onclick="this.disabled=true;this.value='提交中...';this.form.submit();" />
      </div>
    </div>
</form> -->

<?php require('template/_copyright.php'); ?>

<style>
@media (min-width: 767px) {
  #mobi_menu{
      display:none;
  }
}
@media (max-width: 767px) {
    .container{
        padding-bottom:100px;
    }
    #mobi_menu{
        position:fixed;
        bottom:0px;
        left:-1%;
        width:102%;
        z-index:999;
    }
    #mobi_menu .btn-group{
        text-align:center;
        width:100%;
    }
    #mobi_menu .btn{
        width:25%;
        padding:8px 0px;
    }
}
</style>

<div id="mobi_menu">
   <div class="btn-group">
    <?php if (is_logged_in() == TRUE) { ?>
          <a class="btn" href="<?= site_url("user/settings"); ?>"><img src="http://cdn1.iconfinder.com/data/icons/windows8_icons/26/services.png" /><br />帳戶設定</a>
      <?php }else{ ?>
        <a class="btn" href="#user_info" data-toggle="modal"><img src="http://cdn1.iconfinder.com/data/icons/windows8_icons/26/my_topic.png" /><br />登入/註冊</a>
      <?php } ?>
      <a class="btn" href="<?= site_url("task/post"); ?>"><img src="http://cdn1.iconfinder.com/data/icons/windows8_icons_iconpharm/26/advertising.png" /><br /> 發布任務</a>
      <a class="btn" href="<?= site_url("task/map"); ?>"><img src="http://cdn1.iconfinder.com/data/icons/windows8_icons_iconpharm/26/map_marker.png" /><br/>尋找任務</a>
      <a class="btn" href="#contact" data-toggle="modal"><img src="http://cdn1.iconfinder.com/data/icons/windows8_icons_iconpharm/26/online_support.png" /><br />意見回饋</a>
   </div>
</div>

<div style="position:fixed; bottom:0; right:8%; z-index:99;">
    <a class="btn btn-warning" href="<?= base_url("task/post"); ?>"
    style="-webkit-border-radius: 4px 4px 0 0; -moz-border-radius: 4px 4px 0 0; border-radius: 4px 4px 0 0;">
      <i class="icon-bullhorn icon-white"></i> 發布任務
   </a>
   <a class="btn btn-inverse" href="#contact" data-toggle="modal"
      style="-webkit-border-radius: 4px 4px 0 0; -moz-border-radius: 4px 4px 0 0; border-radius: 4px 4px 0 0;">
      <i class="icon-user icon-white"></i> 聯絡 Citytasker
   </a>
</div>

<!-- Modal -->




<div id="fb-root"></div>
<script type="text/javascript">
  window.fbAsyncInit = function() {
     //Initiallize the facebook using the facebook javascript sdk
      FB.init({
        appId: '<?php echo $this->config->item('appID'); ?>', // App ID
        cookie:true, // enable cookies to allow the server to access the session
        status:true, // check login status
        xfbml:true, // parse XFBML
        oauth : true //enable Oauth
       });
  };

   (function(d){
           var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
           if (d.getElementById(id)) {return;}
           js = d.createElement('script'); js.id = id; js.async = true;
           js.src = "//connect.facebook.net/zh_TW/all.js";
           ref.parentNode.insertBefore(js, ref);
         }(document));

   //Onclick for fb login
 $('#facebook').click(function(e) {
    FB.login(function(response) {
     if(response.authResponse) {
        var url = '<?= $this->uri->uri_string();?>'; //取得當前網址
        parent.location = '<?= base_url('fbci/fblogin'); ?>'+'/'+url;
      }else{
         alert('請安心授權給Citytasker，我們保證不會在非經你的同意下回傳訊息至Facebook');
      }
 },{scope: 'email,read_stream,publish_stream,user_birthday,user_location,user_work_history,user_photos,user_activities,user_likes'}); //permissions
});

</script>

<!-- Analytics
================================================== -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-42632316-1', 'citytasker.tw');
  ga('send', 'pageview');

</script>

    </body>
</html>