<?php require('template/_header.php'); ?>
<?php require('template/_navbar.php'); ?>

<?php if (isset($_COOKIE['poster_task_id'])) { ?>
<?php require('template/_news_alerts.php');//案主提示關連任務 ?>
<?php } ?>

<div id="container" class="container">
	<div class="row-fluid">
      <div class="span8">
      <?php require('template/_flashdata_show.php'); ?>
         <div id="header" class"row-fluid">
            <div class=" page-header span12">
               <ul class="media-list">
                  <li class="media">
                     <a class="pull-left" href="#">
	                     <img class="media-object img-rounded" data-src="holder.js/150x150" src="<?= base_url("uploads/head_img/$result_vitae->img")?>">
                     </a>
                     <div class="media-body">
   	                  <h1 class="media-heading">
									<?= $result_vitae->name; ?>

                        </h1>
                        <p>
                        	<code><i class="icon-ok"></i> facebook 認證</code>
                           <code><i class="icon-ok"></i> Mail 認證</code>
                           <?php if ( ! empty($result_vitae->phone)): ?>
                           <code><i class="icon-ok"></i> 手機認證</code>
                           <?php endif ?>
                        </p>
                        <?php $read_id = $result_vitae->member_id;
                              $star_num = ( ! empty($count_credit->avg)) ?  $count_credit->avg: '0' ;
                              require('template/_star.php'); ?>
                        <span id="readOnly<?= $read_id; ?>"></span> <?= $star_num; ?>/5 (<?=
(isset($count_credit->total)) ? $count_credit->total : '0' ;?>筆評價)
                        <h5><i class="icon-map-marker"></i> <?= $zip; ?>地區</h5>
                        <h5><i class="icon-briefcase"></i> 接案方式
								<?= $jobway;?>
                        </h5>
                     </div>
                  </li>
               </ul>
            </div>
         </div>
			<div id="info" class="row-fluid">
            <div id="about">
               <h4><i class="icon-info-sign"></i> 基本資料</h4>
               <div style="padding:2px 0 0 20px;">
               	<h5>生日：<?php echo "28歲(1985/09/17)"; ?> </h5>
                  <h5>身高：<?php echo "171 cm"; ?> </h5>
                  <h5>體重：<?php echo "65 kg"; ?> </h5>
                  <h5>最高學歷：<?php if(empty($result_vitae->education)){ echo "尚未填寫"; }else{ echo $result_vitae->education; } ?> </h5>
                  <h5>主修科系：<?php echo"電子工程"; ?> </h5>
                  <h5>語言：<?php if(empty($result_vitae->language)){ echo "尚未填寫"; }else{ echo $result_vitae->language; } ?> </h5>
                  <h5>專長：<?php echo "設計、單車"; ?> </h5>
               </div>
               <hr>
               <h4><i class="icon-info-sign"></i> 自我介紹</h4>
               <div style="padding:2px 0 0 20px;">
                  <h5><?php echo "很高興能跟大家分享我們努力了四個多月所完成的網站，從小大我就一直有很多夢想想去實現，很幸運的我也一直有機會能真正去完成並實踐，Cityt"; ?></h5>
               </div>
               <hr>
               <h4><i class="icon-info-sign"></i> 社團經歷</h4>
               <div style="padding:2px 0 0 20px;">
                  <h5><?php if(empty($result_vitae->work)){ echo "尚未填寫"; }else{ echo $result_vitae->work; } ?></h5>
               </div>
               <hr>
               <h4><i class="icon-info-sign"></i> 工作經歷</h4>
               <div style="padding:2px 0 0 20px;">
                  <h5><?php echo "2008 第一次十天單車環島<br>
2008 HP贊助十五天單車環島<br>
2010 九天單車環島<br>
2010 五天中央山脈大縱走<br>
2011 永不放棄挑戰巔峰完賽<br>
2012 完成從北京騎單車到羅馬七個月13國16332.6km"; ?></h5>
               </div>
            </div>
            <!--
            <ul id="picture" class="media-list">
               <li class="media">
               <?php
               $member_picture=0; //暫時不用這個
               $img_num = count($member_picture);
               if($img_num!=0){
               ?>
               <h4><i class="icon-picture"></i> 照片與作品</h4>
               <?php };
               for($i=0;$i<=$img_num-1;$i++){
               ?>
               <a class="pull-left" href="#">
               <img class="img-rounded" data-src="holder.js/112x112" src="<?php //echo $member_picture[$i]; ?>">
               </a>
               <?php }; ?>
               </li>
            </ul><!-- end picture -->
            <br />
            <div id="feedback">
            	<h4><i class="icon-thumbs-up"></i> 評價紀錄</h4>
               <hr>
            	<div id="info">
                  <ul class="media-list">
                  	<small><?php if(($count_credit->total) == 0) echo '還沒有任何紀錄！' ;?></small>
                  <?php if ($credit_results): ?>
                     <?php foreach ($credit_results as $credit): ; //所有評價?>
                     <li class="media">
                        <a class="pull-left" href="#">
                           <img class="media-object img-rounded" style="width:40px;"
                           src="<?php echo base_url("uploads/head_img/$credit->img")?> ">
                        </a>
                        <div class="media-body">
                           <h5 class="media-heading">
                              <strong>
                                 <a href="<?= base_url("user/$credit->giver_id");?>"><?= $credit->name; ?></a> 評論：
                                 <a href="<?= base_url("task/$credit->task_id");?>"><?= $credit->title; ?></a>
                              </strong>
                           </h5>
                           <?php $read_id = $credit->id;
                                 $star_num = ( ! empty($credit->score)) ?  $credit->score: '0' ;
                                 require('template/_star.php'); ?>
                           評價：<span id="readOnly<?= $read_id; ?>"></span>  <?= $star_num; ?>/5
                           <p><?= $credit->comment; ?></p>
                           <h6><small><?= $credit->created_at; ?></small></h6>
                        </div>
                     </li>
                     <?php endforeach; //評價?>
                  <?php endif; ?>
                  </ul>
               </div><!-- end info -->
            </div>
         </div><!-- end info -->
		</div><!-- end span8 -->
		<div class="span4">
			<div id="paid" class="box">
        	<ul class="thumbnails">
         <strong>
         	<li class="span4"><h2><?= $count_posted;?></h2><small>發布任務</small></li>
         	<li class="span4"><h2><?= (isset($count_credit->total)) ? $count_credit->total : '0' ;?></h2><small>獲得評價</small></li>
         	<li class="span4"><h2><?= $count_finished;?></h2><small>完成任務</small></li>
         </strong>
         </ul>
         <?php if (is_logged_in() == TRUE) { ?>
               <?php if ($_SESSION['member_id']==$exe_name){ ?>
               <a class="btn btn-inverse" style='padding:10px 25px;'  href="<?= base_url("user/vitae"); ?>">個人檔案設定</a>
               <?php }} ?>
        </div>
			<div class="row-fluid">
				<div id="experience" class="span12">
               <h5 class="text-header">接任務紀錄</h5>
               <small><?php if(($count_credit->total) == 0) echo '還沒有任何紀錄！' ;?></small>
            	<?php if ($credit_catalog_results): ?>
               <?php foreach ($credit_catalog_results as $catalog_count) :?>
               <p>
                  <strong><a href="<?= base_url("task/map/catalog/$catalog_count->catalog_id");?>"><?= $catalog_count->catalog; ?></a>
                  </strong> <?php $read_id = $catalog_count->task_id;
                        $star_num = ( ! empty($catalog_count->avg)) ?  $catalog_count->avg: '0' ;
                        require('template/_star.php'); ?>
                  <span id="readOnly<?= $read_id; ?>"></span> <?= $star_num; ?>/5 (<?= (isset($count_credit->total)) ? $count_credit->total : '0' ;?>筆評價)
               </p>
               <?php endforeach;?>
            	<?php endif; ?>
         	</div>
			<?php require('template/_share.php'); ?>
			</div>
		</div><!-- end span4 -->
	</div>
</div><!-- end container -->

<?php require('template/_copyright.php'); ?>
<?php require('template/_footer.php'); ?>