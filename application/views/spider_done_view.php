<?php require('template/_header.php'); ?>
<?php require('template/_navbar.php'); ?>
<script type="text/javascript" src="<?= base_url("js/masonry.pkgd.min.js")?>"></script>
<script type="text/javascript" src="<?= base_url("js/jquery.infinitescroll.min.js")?>"></script>
<style type="text/css">
body .modal {
  width: 80%; /* desired relative width */
  height: 80%; /* desired relative width */
  left: 6%; /* (100%-width)/2 */
  /* place center */
  margin-left:auto;
  margin-right:auto;
}
</style>

<style>
.item{
  display: inline-block;
  border: 1px dotted #4F4F4F;
  padding: 10px;
  margin: 5px 5px 5px 0;
  overflow:hidden;
  width:337px;
}
</style>

<div class="container-fluid">
  <div class="row-fluid">
    <div class="span12">
      <ul class="nav nav-tabs">
        <li>
          <a href="<?= base_url('spider/');?>">FB爬蟲</a>
        </li>
        <li>
          <a href="<?= base_url('spider/load/999');?>" target="_blank">載入全部社團</a>
        </li>
        <li>
          <a href="<?= base_url('spider/load/192414784133675');?>" target="_blank">載入 OUR WORK</a>
        </li>
        <li class="active">
          <a href="<?= base_url('spider/done_show');?>">所有已發布(<?= $count_del?>)</a>
        </li>
        <li>
          <a href="<?= base_url('spider/delete_show');?>">所有已刪除</a>
        </li>
      </ul>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12">
      <?= $pageLinks ;?>
      <table class="table">
        <thead>
          <tr>
            <th> 任務名稱 </th>
            <th> 發佈時間 </th>
            <th> 案主回訪時間 </th>
            <th> 回訪時間 </th>
            <th> 回訪次數 </th>
            <th> 完成關連 </th>
          </tr>
        </thead>
        <tbody>
<? foreach ($all_results as $feed):
$bg_color = ($feed->connect == 1) ? 'success' : 'warning' ;
?>
          <tr class="<?= $bg_color;?>">
            <td><a href="<?= base_url("task/$feed->id"); ?>" target="_blank"> <?= $feed->id?>_<?= $feed->title?></a></td>
            <td><?= $feed->created_at; ?></td>
            <td><?= $feed->deadline; ?></td>
            <td><?= $feed->hit_time; ?></td>
            <td><?= $feed->hit_count; ?></td>
            <td><?= $feed->connect; ?></td>
          </tr>

<? endforeach ;?>

        </tbody>
      </table>
    </div>
  </div>
</div>




<?php require('template/_copyright.php'); ?>
<?php require('template/_footer.php'); ?>
