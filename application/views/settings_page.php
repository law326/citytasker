<?php require('template/_header.php'); ?>
<?php require('template/_navbar.php'); ?>

<?php if (isset($_COOKIE['poster_task_id'])) { ?>
<?php require('template/_news_alerts.php');//案主提示關連任務 ?>
<?php } ?>

<div id="container" class="container">
   <div class="row-fluid">
      <div class="span8">
      	<?php require('template/_flashdata_show.php'); ?>
         <div id="header" class"row-fluid">
            <div class="page-header span12">
	            <h1>帳戶設定</h1>
            </div>
         </div>
         <div>
            <form action="<?= base_url("upload/do_upload_settings") ?>" method="post" enctype="multipart/form-data" >
               <h4><strong>更新照片</strong></h4>
               <div>請上傳張關於你的照片，這張照片也將會影響大家對於你的第一印象</div>
               <br/>
               <div class="fileupload fileupload-new media" data-provides="fileupload">
                  <div class="pull-left">
                     <div class="fileupload-preview thumbnail" style="width: 150px; height: 150px;">
                        <img class="img-rounded" src="<?= base_url("uploads/head_img/$img");?>">
                     </div>
                  </div>
                  <div class="media-body">
                     <!-- Nested media object -->
                     <p>從電腦中選取圖檔 (最小像素 150x150 )<br/>檔案格式：JPG, GIF, PNG</p>
                     <p>
                        <span class="btn btn-file">
                           <span class="fileupload-new">選擇照片</span>
                           <span class="fileupload-exists">更換</span>
                           <input type="file" name="userfile" size="20" />
                        </span>
                        <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">刪除</a>
                        <input class="btn" type="submit" value="上傳" />
                     </p>
                  </div>
               </div>

               <?php //因為有開啟CSRF
               $CI =& get_instance();
               $csrf_name = $CI->security->get_csrf_token_name();
               $csrf_value = $CI->security->get_csrf_hash();
               ?>

               <input type="hidden" name="<?= $csrf_name ?>" value="<?= $csrf_value ?>" />
            </form>

            <hr />

            <form action="<?= base_url("user/settings_update") ?>" method="post" >

               <h4><strong>詳細資料</strong></h4>
               <p>你的詳細資料只有在確定接任務之後交易雙方會知道，不會公開在網路上</p>
               <br/>
               <div class="row-fluid">
                  <div class="span2 text-right">使用者名稱</div>
                  <div class="span6">
                     <input type="text" class="input-block-level" value="<?= $name;?>" name="name">
                  </div>
               </div>
               <div class="row-fluid">
                  <div class="span2 text-right">真實姓名</div>
                  <div class="span6">
                     <input type="text" class="input-block-level" value="<?= $realname?>" name="realname">
                  </div>
               </div>
               <div class="row-fluid">
                  <div class="span2 text-right">登入帳號</div>
                  <div class="span6">
                     <input type="text" class="input-block-level" value="<?= $email;?>" name="email" readonly="readonly">
                  </div>
               </div>
               <div class="row-fluid">
                  <div class="span2 text-right">聯絡信箱</div>
                  <div class="span6">
                     <input type="text" class="input-block-level" value="<?= $email2;?>" name="email2">
                  </div>
               </div>
               <div class="row-fluid">
                  <div class="span2 text-right">手機</div>
                  <div class="span6">
                     <input type="text" id="phone1" class="input-block-level" value="<?= $phone;?>" name="phone" >
                  </div>
               </div>
               <hr/>

               <h4><strong>出沒地區</strong></h4>
               <p>設定你目前所在的位置 ，系統會自動顯示身邊的相關任務</p>
               <br/>
               <div id="zip" class="row-fluid">
                  <div class="span2 text-right">郵遞區號</div>
                  <div class="span6">
                     <input type="text" class="input-block-level" value="<?= set_value('zip', $zip)?>" name="zip" maxlength="3" placeholder="郵遞區號 (3碼)">
                  </div>
               </div>

               <hr />
               <!--
               <h4><strong>重設密碼</strong></h4>
               <br/>
               <div class="row-fluid">
                  <div class="span2 text-right">密碼</div>
                  <div class="span6">
                     <input type="password" class="input-block-level" value="" name="password">
                  </div>
               </div>
               <div class="row-fluid">
                  <div class="span2 text-right">新密碼</div>
                  <div class="span6">
                     <input type="password" class="input-block-level" value="" name="newpassword">
                  </div>
               </div>

               <hr />

               <h4><strong>社群連結</strong></h4>
               <p>連結你的社群，分享Citytasker給你的朋友</p>
               <br/>
               <div class="row-fluid">
                  <a class="btn btn-large btn-primary offset2 span6 disabled" href="#">連結 facebook</a>
               </div>

               <hr />
               -->
               <?php //因為有開啟CSRF
               $CI =& get_instance();
               $csrf_name = $CI->security->get_csrf_token_name();
               $csrf_value = $CI->security->get_csrf_hash();
               ?>
               <div class="row-fluid">
                  <input class="btn btn-large" type="submit" value="存檔" />
               </div>
               <input type="hidden" name="<?= $csrf_name ?>" value="<?= $csrf_value ?>" />
            </form>
         </div><!-- end header -->
      </div><!-- end span9 -->
      <div class="span4">
      	<?php require('template/_setting_menu.php'); ?>
      </div>
   </div>
</div>

<?php require('template/_copyright.php'); ?>
<?php require('template/_footer.php'); ?>
