<?php require('template/_header.php'); ?>
<?php require('template/_navbar.php'); ?>

<div id="container" class="container">
   <div class="row-fluid">
      <div class="span8">
      <?php require('template/_flashdata_show.php'); ?>
         <div id="header" class"row-fluid">
            <div class="page-header span12">
               <h1>接案紀錄</h1>
            </div>
         </div>
<? if (isset($posted_results)) { ?>
         <?php foreach ($posted_results as $task): //所有任務記錄(包含索取)?>
				<table class="table table-striped table-bordered" style="font-size:14px;">
            	<thead>
               	<tr>
                  	<th>
                     	<a href="<?= base_url("task/$task->id");?>"><?= $task->title; ?></a>
                      <small style="font-weight:normal;">(報名截止：<?= $task->deadline;?>)</small>
                     </th>
                	</tr>
					</thead>
              	<tbody>
                  <tr>
                     <td class="media">
                        <a class="pull-left" href="#">
                           <img class="media-object img-rounded" style="width:48px" src="
                           <?php echo base_url("uploads/head_img/$task->img")?>">
                        </a>
                        <span class="pull-right">$<?= $task->price;?>
                        <?php if ($task->recipient_id != null){ ?>
                           已評價<?php }else{ ?>
                        <?php }?>
                        </span>
                        <div class="media-body">
                           <div class="media">
                              <p>
                                 <a href="<?= $task->member_id; ?>"><?= $task->name; ?></a>　
                                 <div><i class="icon-ok"></i>
                                  <?if ( ! empty($task->phone)) {
                                  echo $task->phone;
                                  }else{
                                  echo $task->p_phone;
                                  } ?>　
                                 <i class="icon-envelope"></i>
                                 <?php if ( ! empty($task->email)) {
                                  $mail = $task->email;
                                  }else{
                                  $mail = $task->p_email;
                                  } ?>
                                  <?=$mail;?> <a class="btn btn-mini btn-primary" target="_blank" href="mailto:<?=$mail;?>?subject=[Citytasker]<?= $task->title; ?>">寄送履歷</a></div>
                              </p>
                              <?php

                              ?>
                              <p><i class="icon-map-marker"></i> 任務位置：<?= address_is_online($task->address); ?></p>
                              <?php if (!empty($task->content)){ ?>
                              <p><i class="icon-lock"></i> 私密內容：<code><?= $task->content; ?></code></p>
                              <?php }; ?>
                           </div>
                        </div>
                     </td>
                  </tr>
                  <!-- Modal -->
                  <form id="contact-form" class="contact-form" action="<?= base_url("credit/give_ing");?>"
                     method="post" accept-charset="utf-8" onSubmit="return checkForm<?= $task->id . '_' . $task->member_id; ?>(this)">
                     <div id="feedback<?= $task->id . '_' . $task->member_id; ?>"
                        class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                           <h3 id="myModalLabel">評價</h3>
                        </div>
                        <div class="modal-body">
                           <h4>評價案主：<?= $task->name; ?></h4>
                           <input type="hidden" name="task_id" value="<?= $task->id ?>" />
                           <input type="hidden" name="task_name" value="<?= $task->title ?>" />
                           <input type="hidden" name="giver_id" value="<?= $_SESSION['member_id'] ?>" />
                           <input type="hidden" name="recipient_id" value="<?= $task->member_id ?>" />
                           <input type="hidden" name="recipient_identity" value="<?= 'poster' ?>" />
                           <p>任務：<?= $task->title; ?></p>
                           <hr>
                           <strong>滿意指數</strong>
                           <p>
                              <span id="function<?= $task->id . '_' . $task->member_id; ?>"></span>
                              <span id="function-hint<?= $task->id . '_' . $task->member_id; ?>" class="hint">0</span>
                              <input type="hidden" name="score" id="score<?= $task->id . '_' . $task->member_id; ?>" value="">
                           </p>
                           <!-- 星星 -->
                           <script type="text/javascript">
                           $(function() {
                              $('#function<?= $task->id . '_' . $task->member_id; ?>').raty({
                                 path      : '<?= base_url("js/star/img"); ?>',
                                 size      : 24,
                                 half      : true,
                                 starHalf  : 'star-half-big.png',
                                 starOff   : 'star-off-big.png',
                                 starOn    : 'star-on-big.png',
                                 target    : '#function-hint<?= $task->id . '_' . $task->member_id; ?>',
                                 targetKeep: true,
                                 targetType: 'number'
                              });
                           });
                           </script>
                           <strong>評價意見</strong>
                           <textarea rows="3" style="width:97%;" name="comment"
                              placeholder="評價將成為公開紀錄，讓其他會員可以依據參考。"><?=$this->input->post('comment')?></textarea>
                        </div>
                        <div class="modal-footer">
                           <span class="help-inline"><small>*一旦送出便無法修改，請謹慎給予！</small></span>
                           <input class="btn btn-large" type="submit" value="完成" />
                            <script type="text/javascript">
                            function checkForm<?= $task->id . '_' . $task->member_id; ?>() {
                               var cd = $("#function-hint<?= $task->id . '_' . $task->member_id; ?>").text();
                               if (cd == '') cd='0';
                                if (confirm('確定給'+cd+'分?')) {
                                   $("#score<?= $task->id . '_' . $task->member_id; ?>").val(cd);
                                   return true;
                                } else{
                                   return false;
                                };
                            }
                            </script>
                        </div>
                     </div>

                   <input type="hidden" name="former_URL" value="<?= $this->uri->uri_string(); //所在頁面?>" />
                     <?php //因為有開啟CSRF
                     $CI =& get_instance();
                     $csrf_name = $CI->security->get_csrf_token_name();
                     $csrf_value = $CI->security->get_csrf_hash();
                     ?>
                     <input type="hidden" name="<?= $csrf_name ?>" value="<?= $csrf_value ?>" />
                  </form>
                  <!-- */ Modal -->
					</tbody>
				</table>
			<?php endforeach; //任務紀錄 ?>
<? }else{ ?>
      你還沒有接過任何任務，趕快點選<a href="<?= base_url("task/map"); ?>">搜尋任務</a>來解任務吧！
<? } ?>
      </div>
      <div class="span4">
         <?php require('template/_setting_menu.php'); ?>
      </div>
   </div>
</div>

<? if ($new_offer == 'new_offer'):?>
  <script type="text/javascript">
  $(function(){
    $('table:first').hide().fadeIn(3000);
    $('table:first').css("border", "4px solid #93CE37");
  });
  </script>
<? endif;?>
<?php require('template/_copyright.php'); ?>
<?php require('template/_footer.php'); ?>
