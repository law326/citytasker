<?php require('template/_header.php'); ?>
<?php require('template/_navbar.php'); ?>

<div id="container" class="container">
   <div class="row-fluid">
      <div class="span8">
      <?php require('template/_flashdata_show.php'); ?>
         <div id="header" class"row-fluid">
            <div class="page-header span12">
               <h1>發佈任務紀錄</h1>
            </div>
         </div>
			<?php
         $date = Array("3/4","3/12","3/14","3/24","3/27");
         $task = Array("手機或網路手機或網123234235路手機或網路手機或網路手機或網路","手機或網路手機或網路","手機或網路手機或網路手機或網路手機或網路","手機或網路手機或網路手機或網路","手機或網路手機或網路手機或網路");
         $task_id = Array("123456","123456","123456","123456","123456");
			$runner = Array(Array("手機1"),Array("手機2"),Array("手機或3"),Array("手機或41","手機或42"),Array("或網路5"));
			$runner_id = Array("123456","123456","123456",Array("123456","sdfs"),"123456");
         $status = Array(Array("0"),Array("1"),Array("0"),Array("1","0"),Array("0"));
         $money = Array(Array("12"),Array("345"),Array("555"),Array("20","123"),Array("38"));
         
         for ($i=0;$i<=4;$i++){
         ?>
         	<table class="table table-striped table-bordered" style="font-size:14px;">
              <thead>
                <tr>
                  <th><a href="<?= $task_id[$i]; ?>"><?= $task[$i]; ?></a> <small style="font-weight:normal;">(2012/12/12)</small></th>
                </tr>
              </thead>
              <tbody>
              	<?php for($j=0;$j<=count($runner_id[$i])-1;$j++){ ?>
                <tr>
                  <td class="media">
                     <a class="pull-left" href="#">
                        <img class="media-object img-rounded" data-src="holder.js/24x24">
                     </a>
                     <span class="pull-right">$<?=$money[$i][$j]; ?> | 
                     <?php if ($status[$i][$j]==1){ ?>
                     	結案<?php }else{ ?><a  href="#feedback<?=$i."_".$j;?>" data-toggle="modal">評價</a>
							<?php } ?>
                     </span>
                     <!-- Modal -->
                     <div id="feedback<?=$i."_".$j;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                           <h3 id="myModalLabel">評價</h3>
                        </div>
                        <div class="modal-body">
                        	<h4>評價幫手：<?= $runner[$i][$j]; ?></h4>
                           <p>任務：<?= $task[$i]; ?></p>
                           <hr>
                           <strong>滿意指數</strong>
                           <p>
                           	<span id="function<?=$j;?>"></span> 
                              <span id="function-hint<?=$j;?>" class="hint">0</span>
                           </p>
                           <!-- 星星 -->
									<script type="text/javascript">
                            $(function() {
                              $('#function<?=$j;?>').raty({
                                path      : '<?= base_url("js/star/img"); ?>',
                                size      : 24,
                                starHalf  : 'star-half-big.png',
                                starOff   : 'star-off-big.png',
                                starOn    : 'star-on-big.png',
                                target    : '#function-hint<?=$j;?>',
                                targetKeep: true,
                                precision : true
                              });
                            });
                           </script>
                           <strong>評價意見</strong>
                           <textarea rows="3" style="width:100%;" name="content_private" 
                           placeholder="評價將成為公開紀錄，讓其他會員可以依據參考。"></textarea>
                        </div>
                        <div class="modal-footer">
                           <span class="help-inline"><small>*一旦送出便無法修改，請謹慎給予！</small></span>
                           <a href="#" class="btn btn-large">完成</a>
                        </div>
                     </div>
                     <!-- */ Modal -->
                     <div class="media-body">
                        <div class="media">
                           <span><a href="<?= $runner[$i][$j]; ?>"><?= $runner[$i][$j]; ?></a>　</span>
                           <span><i class="icon-ok"></i> 0916666666　</span>
                           <span><i class="icon-envelope"></i> dfhskdldfnlaskflsdfjl@sdfhsdfjdslfjsdfj.com</span>
                        </div>
                     </div>
                	</td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
         <?php } ?>
      </div>
      <div class="span4">
         <?php require('template/_setting_menu.php'); ?>
      </div>
   </div>
</div>

<?php require('template/_copyright.php'); ?>
<?php require('template/_footer.php'); ?>
