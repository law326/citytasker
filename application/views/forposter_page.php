<?php require('template/_header.php'); ?>
<?php require('template/_navbar.php'); ?>

<style>
	h4{
		color:#F15922;
	}
	.marketing .span1{
	  text-align:center;
	  padding-top:15px;
  }
  .media{
	  padding:10px;
	  border-bottom:1px #eee solid;
	  margin-bottom: -20px;
  }
  .media:hover {
	  background:#fbfbfb;
  }

  .bounty{
	  padding:10px;
	  text-align:center;
	  background:#eee;
  }

    /* RESPONSIVE CSS
    -------------------------------------------------- */
    @media (max-width: 767px) {
      .marketing .span4 + .span4 {
        margin-top: 40px;
      }

    }
</style>

    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container">
    <?php require('template/_flashdata_show.php'); ?>
      <h2>如何讓任務完成？</h2>
      <hr>
      <div class="row-fluid marketing">
        <div class="span4">
          <img src="<?= base_url("img/web/hiw-1.png"); ?>">
          <h4>步驟1：輕鬆發布任務</h4>
          <p>描述任務、時間、地點等資訊，以及你願意支付多少酬勞讓任務達成。你可以直接用現金或是匯款來交易，但別忘了只有在<strong>任務完成後才需要支付</strong>！</p>
          <p>幫手對任務產生疑問，請多提供資訊來確保達成你的期待。為保護<strong>個人隱私</strong>資料，可以選擇哪些資訊公開或只給出任務的幫手知道。</p>
        </div><!-- /.span4 -->
        <div class="span4">
          <img src="<?= base_url("img/web/hiw-2.png"); ?>">
          <h4>步驟2：找對的幫手做對的任務</h4>
          <p>每一位 Citytasker 成員都必須完成<strong>手機驗證</strong>，透過過去工作紀錄以及評價，可以讓你找出最適合解決任務的幫手。只需要把任務交辦出去，就可以等著驗收成果。</p>
          <p>有任何關於任務的進展，Citytasker 會以信件讓你隨時掌控。</p>
        </div><!-- /.span4 -->
        <div class="span4">
          <img src="<?= base_url("img/web/hiw-3.png"); ?>">
          <h4>步驟3：完成後付款</h4>
          <p>一旦有幫手接下任務，會收到一份關於他的完整機密檔案以<strong>防止詐騙</strong>事件發生，同樣你的私密訊息與個人資料也只有他會收到。</p>
          <p>任務完成後，可以直接用現金或匯款來支付你的幫手，如果他做的很棒也別忘了給他一些鼓勵與評價！</p>
          <p>有甚麼任務需要完成呢？ <strong><a href="<?= base_url("task/post"); ?>">免費發布任務</a></strong></p>
        </div><!-- /.span4 -->
      </div><!-- /.row -->
      <br/>
      <br/>
      <h2>為什麼要在 Citytasker 發布任務？</h2>
      <hr>
      <div class="row-fluid marketing">
         <div class="span1">
         	<img src="https://cdn0.iconfinder.com/data/icons/windows8_icons/26/one_free.png"/>
         </div><!-- /.span4 -->
         <div class="span5">
            <h4>成為會員是免費的</h4>
            <p>只有當你發布的任務被完成後才需要付款，除此之外在平台裡進行任何事都是免費的。</p>
         </div><!-- /.span4 -->
         <div class="span1">
         	<img src="https://cdn1.iconfinder.com/data/icons/windows8_icons_iconpharm/26/map_marker.png"/>
         </div>
         <div class="span5">
            <h4>便利的在地化服務</h4>
      		<p>我們在北北基建立了一個由上萬人串聯的服務網路，可以讓你更輕鬆找到合適的幫手，更棒的是他們可能就在你的辦公室或居住的社區附近。</p>
         </div><!-- /.span4 -->
      </div><!-- /.row -->
      <div class="row-fluid marketing">
         <div class="span1">
         	<img src="https://cdn1.iconfinder.com/data/icons/windows8_icons/26/snow_storm.png"/>
         </div><!-- /.span4 -->
         <div class="span5">
            <h4>快速發布並輕鬆管理</h4>
            <p>只需要30秒的時間就能完成任務發布，關於任務的進度、報名、甚至是相關討論，都會同時呈現在專屬頁面中。</p>
         </div><!-- /.span4 -->
         <div class="span1">
         	<img src="https://cdn1.iconfinder.com/data/icons/windows8_icons/26/talk.png"/>
         </div><!-- /.span4 -->
         <div class="span5">
            <h4>一切由你定義</h4>
            <p>你可以天馬行空指定想被完成的任務，制定你想支付的價錢、地點、時間，還可以選擇你想讓誰來完成，全都由你做主！</p>
         </div><!-- /.span4 -->
      </div><!-- /.row -->
      <div class="row-fluid marketing">
         <div class="span1">
         	<img src="https://cdn1.iconfinder.com/data/icons/windows8_icons/26/shuffle.png"/>
         </div><!-- /.span4 -->
         <div class="span5">
            <h4>連結多元化的資源共享網路</h4>
            <p>我們的成員來自社會中各行各業不同年齡的人們，擁有多元的專業能力、人脈網路、興趣、想法或是各種器材，樂於透過平台互相協助彼此。</p>
         </div><!-- /.span4 -->
         <div class="span1">
         	<img src="https://cdn1.iconfinder.com/data/icons/windows8_icons_iconpharm/26/checked_checkbox.png"/>
         </div><!-- /.span4 -->
         <div class="span5">
            <h4>安全的網路環境</h4>
            <p>每一位成員都必須完成 facebook 帳號、電子郵件以及手機認證，以確保真實身分以及正當性，透並過透明化的個人履歷來了解每個人的專長、經歷以及過去的評價。同時您的個資也會受到保護不用攤在陽光下，只有當你確定雇用對象時雙方才會取得對方的聯絡方式。</p>
         </div><!-- /.span4 -->
      </div><!-- /.row -->
      <div class="row-fluid marketing">
         <div class="span1">
         	<img src="https://cdn1.iconfinder.com/data/icons/windows8_icons/26/solutions.png"/>
         </div><!-- /.span4 -->
         <div class="span5">
            <h4>智慧的網路系統</h4>
      		<p>根據任務發布的所在地區，系統會分析出鄰近且有能力的人來發送出任務邀請。任務中發生的任何事，都會透過電子郵件自動傳遞，讓雙方都能清楚掌握目前的進度。</p>
         </div><!-- /.span4 -->
         <div class="span1">
            <img src="https://cdn1.iconfinder.com/data/icons/windows8_icons_iconpharm/26/workers.png"/>
         </div><!-- /.span4 -->
         <div class="span5">
            <h4>創造正向的在地就業機會</h4>
      		<p>讓每一分錢都能回到你的城市，刺激在地經濟，還能協助更多社會中弱勢的朋友創造新型態的工作機會。</p>
         </div><!-- /.span4 -->
      </div><!-- /.row -->
      <br/>
      <br/>
      <h2>何時使用 Citytasker？</h2>
      <hr>
      <div class="row-fluid marketing">
         <div class="span1">
         	<img src="https://cdn1.iconfinder.com/data/icons/windows8_icons/26/watch.png"/>
         </div><!-- /.span4 -->
         <div class="span5">
            <h4>當你忙不過來時</h4>
      		<p>現代人經常是工作、兩頭燒，太多的事都等著你去完成，大老闆可以請秘書，而任何人都可以在 Citytasker 請個好幫手，來幫你分憂解勞。</p>
         </div><!-- /.span4 -->
         <div class="span1">
         	<img src="https://cdn1.iconfinder.com/data/icons/windows8_icons_iconpharm/26/question.png"/>
         </div><!-- /.span4 -->
         <div class="span5">
            <h4>當你不知道該如何解決問題時</h4>
            <p>當你問天天不應，問地地不靈時，把問題跟需求放上我們已經串連好的人脈網路，讓有能立協助你的貴人幫助你。</p>
         </div><!-- /.span4 -->
      </div><!-- /.row -->
		<div class="row-fluid marketing">
         <div class="span1">
         	<img src="https://cdn1.iconfinder.com/data/icons/windows8_icons_iconpharm/26/superman.png"/>
         </div><!-- /.span4 -->
         <div class="span5">
            <h4>當你需要專業能力時</h4>
      		<p>每個人的所學與專長都很有限，與其花時間學會還不見得做得好，還不如找個專家幫你把事做好。</p>
         </div><!-- /.span4 -->
         <div class="span1">
         	<img src="https://cdn1.iconfinder.com/data/icons/windows8_icons_iconpharm/26/palm_tree.png"/>
         </div><!-- /.span4 -->
         <div class="span5">
            <h4>當你想要享受人生時</h4>
            <p>人不是鐵打的，需要適時的讓自己休息並享受假期，但這並不代表你的工作或家事就沒辦法完成。</p>
         </div><!-- /.span4 -->
      </div><!-- /.row -->
      <div class="row-fluid marketing">
         <div class="span1">
         	<img src="https://cdn1.iconfinder.com/data/icons/windows8_icons_iconpharm/26/nurse.png"/>
         </div><!-- /.span4 -->
         <div class="span5">
            <h4>當你行動不便時</h4>
      		<p>人生不如意十常八九，金錢上有保險可以理賠，不便之處可以找個人來協助你，不要苦了自己。</p>
         </div><!-- /.span4 -->
         <div class="span1">
         	<img src="https://cdn1.iconfinder.com/data/icons/windows8_icons/26/western.png"/>
         </div><!-- /.span4 -->
         <div class="span5">
            <h4>當你人在外地時</h4>
            <p>如果你出差、遠行或住在外地，人雖然不在北北基，但一樣可以透過網路找個代理人幫你把事完成。</p>
         </div><!-- /.span4 -->
      </div><!-- /.row -->
		<div class="row-fluid marketing">
         <div class="span1">
         	<img src="https://cdn0.iconfinder.com/data/icons/windows8_icons/26/group.png"/>
         </div><!-- /.span4 -->
         <div class="span5">
            <h4>當你需要很多人手時</h4>
      		<p>要把一個活動辦好，會需要更多人手來協助分工合作。</p>
         </div><!-- /.span4 -->
         <div class="span1">
         	<img src="https://cdn1.iconfinder.com/data/icons/windows8_icons_iconpharm/26/manager.png"/>
         </div><!-- /.span4 -->
         <div class="span5">
            <h4>當你不確定員工是否合適時</h4>
            <p>在找到對的人做對的事之前，你可以嘗試透過短期外包來找尋更合適的人選。</p>
         </div><!-- /.span4 -->
      </div><!-- /.row -->
	</div>
<?php require('template/_copyright.php'); ?>
<?php require('template/_footer.php'); ?>
