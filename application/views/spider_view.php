<?php require('template/_header.php'); ?>
<?php require('template/_navbar.php'); ?>
<script type="text/javascript" src="<?= base_url("js/masonry.pkgd.min.js")?>"></script>

<style type="text/css">
body .modal {
  width: 80%; /* desired relative width */
  height: 80%; /* desired relative width */
  left: 6%; /* (100%-width)/2 */
  /* place center */
  margin-left:auto;
  margin-right:auto;
}
</style>

<style>
.item{
  display: inline-block;
  border: 1px dotted #4F4F4F;
  padding: 10px;
  margin: 5px 5px 5px 0;
  overflow:hidden;
  width:337px;
}
</style>
<div class="container-fluid">
  <div class="row-fluid">
    <div class="span12">
      <ul class="nav nav-tabs">
        <li class="active">
          <a href="#">FB爬蟲(<?= $count_todo?>)</a>
        </li>
        <li>
          <a href="<?= base_url('spider/load/999');?>" target="_blank">載入全部社團</a>
        </li>
        <li>
          <a href="<?= base_url('spider/load/192414784133675');?>" target="_blank">載入 OUR WORK</a>
        </li>
        <li>
          <a href="<?= base_url('spider/done_show');?>">所有已發布</a>
        </li>
        <li>
          <a href="<?= base_url('spider/delete_show');?>">所有已刪除</a>
        </li>
      </ul>
    </div>
  </div>
</div>

<?= $pageLinks ;?>
</br>
<div id="container"  style="height:800px; width:728px; overflow:hidden; margin:0 auto;">
<? foreach ($all_results as $feed): ?>
<div id="<?= $feed->tid?>" class="item">
  <?= $feed->id?>
   | <?= $feed->name; ?>
   | <a href="<?= "https://www.facebook.com/$feed->tid";?>" target="_blank"> <?= $feed->from_name; ?></a>

   <h4><small><?= $feed->created_time; ?></small></h4>
   <button type="button" class="btn poster" data-toggle="modal" data-target="#myModal<?= $feed->id;?>">發布</button>
 </br>
   <a  class="pull-left btn btn-danger del">刪除</a>
   <div class="media-body">
      <p class="media-heading"><?= nl2br($feed->message); ?></p>
   </div>
</div>

<!-- Modal -->
<div id="myModal<?= $feed->id;?>" class="modal hide fade">
<iframe width="100%" height="100%" ></iframe>
</div>
<? endforeach ;?>
</div>


<script type="text/javascript">
  $(".del").click(function () {
    var $btn = $(this);
    var tid = $(this).parent().attr('id');
    read_status(tid,'del',$btn);

  });

  function read_status (tid,status,btn,dummy_id) {
    // 修改任務狀態
    var cct = $("input[name=csrf_citytaser_name]").val();
    var AJAXrequest = $.ajax({
          type: "POST",
          cache: false,
          url: "<?= base_url('spider/ajax_read'); ?>",
          data: {
              csrf_citytaser_name: cct,
              tid:tid,
              dummy_id:dummy_id,
              status:status
          }
      });

      AJAXrequest.done(function(msg) {
          if (msg == 'yes') {
              if (status == 'del') { //只有del 才須btn 因ajax同步無法直接return值
                 btn.parent().slideUp(); //btn所在上一層
              }
          } else {
             alert('error');
          };
      });

      AJAXrequest.fail(function(jqXHR, textStatus) {
      });
  }

</script>

<script type="text/javascript">
  $(function(){
      $("div[id^='myModal']").on('hidden', function () {
        if (confirm('Hey~ 確定發佈了嗎? 需要將此案件隱藏嗎?')) {
           var tid = $(this).prev().attr('id'); //抓modal上方的div 就是這整篇任務的div

           var str = $(this).attr('id');
           var reg=/[^\d]+/img;
           var dummy_id = str.replace(reg,""); //將myModal34 只取出34

           read_status(tid,'post',null,dummy_id);
           $("#" + tid).slideUp();
           return true;
        } else{
           return false;
        };
      })

      //當click發佈 modal跳出時再載入iframe
      $("div[id^='myModal']").on('shown', function () {
          var str = $(this).attr('id');
          var reg=/[^\d]+/img;
          var dummy_id = str.replace(reg,""); //將myModal34 只取出34
          var src =
          $(this).children('iframe').attr('src','<?= base_url('/spider/post/') ?>'+'/'+dummy_id);
      })


  });
</script>
<?php require('template/_copyright.php'); ?>
<?php require('template/_footer.php'); ?>
