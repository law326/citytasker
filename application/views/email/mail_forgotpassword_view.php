<!-- 密碼遺忘, 使用者接收 -->
<div style="padding:20px 0px; background:url(<?= base_url("img/web/bg.png"); ?>)">
   <div style="
   max-width: 480px;
   padding: 19px 29px;
   margin: 50px auto;
   background:#fff;
   border: 1px solid #e5e5e5;
   -webkit-border-radius: 5px;
     -moz-border-radius: 5px;
       border-radius: 5px;
   -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
     -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
       box-shadow: 0 1px 2px rgba(0,0,0,.05);
   ">
      <div style="padding-bottom:10px; border-bottom:#eee 1px solid;"><img src="<?= base_url("img/web/logo.png"); ?>" /></div>
      <p>Hi <?= $name ?> 瞧瞧我們幫你找到什麼！</p>
      <p>你的新密碼是 <?= $new_password ?> </p>
      <p>登入後請更新成你好記的密碼喔。</p>
      <?php  $message =(array)"你的新密碼是 $new_password";
             $this->session->set_flashdata('info', $message);?>
      <p>趕快來試看看吧！ <a href ='<?= base_url("user/login/"); ?>' >登入</a></p>
      <br />
      <p>本信件由系統產生，請勿回信。</p>
      <p>©2013 Citytasker Inc.</p>
      </div>
   </div>
</div>
<br />