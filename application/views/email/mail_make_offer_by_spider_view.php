<!-- 報名任務, 案主接收 -->
<div style="padding:20px 0px; background:url(<?= base_url("img/web/bg.png"); ?>)">
   <div style="
   max-width: 480px;
   padding: 19px 29px;
   margin: 50px auto;
   background:#fff;
   border: 1px solid #e5e5e5;
   -webkit-border-radius: 5px;
     -moz-border-radius: 5px;
       border-radius: 5px;
   -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
     -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
       box-shadow: 0 1px 2px rgba(0,0,0,.05);
   ">
      <div style="padding-bottom:10px; border-bottom:#eee 1px solid;"><img src="<?= base_url("img/web/logo.png"); ?>" /></div>
      <table>
        <td style="padding-right: 5px;">
          <img src="<?= base_url("uploads/head_img/".$runner_img); ?>" style="
            width: 60px;
            -webkit-border-radius: 5px;
              -moz-border-radius: 5px;
                border-radius: 5px;
            -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
              -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
            " />
         </td>
         <td>
          <p><?= $runner_name?> 已經透過 <a href="<?= base_url(); ?>" target="_blank">Citytasker</a> 打工刊登網站報名了以下工作：<?= $task_name ?><br /><?= nl2br($task_content)?></p>

         </td>
      </table>
      <p style="padding-left: 70px;">
        <b>我們發現這則工作訊息似乎與您有關聯，</b><b>相關詳情請進入 </b><a href="<?= base_url("spider/poster/$task_id/"); ?>" style="font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;color:rgb(255,255,255);text-decoration:none;display:inline-block;padding:4px 12px;margin-bottom:0px;line-height:20px;text-align:center;vertical-align:middle;background-color:rgb(248,148,6);background-image:linear-gradient(rgb(251,180,80),rgb(248,148,6));border-width:1px;border-style:solid;border-color:rgba(0,0,0,0.0980392) rgba(0,0,0,0.0980392) rgba(0,0,0,0.247059);border-top-left-radius:4px;border-top-right-radius:4px;border-bottom-right-radius:4px;border-bottom-left-radius:4px;outline:0px;background-repeat:repeat no-repeat" target="_blank"><b>工作頁面</b></a><wbr>&nbsp;&nbsp;</div>
      </p>
      <br />
      <p>本信件由系統產生，請勿回信。</p>
      <p>©2013 Citytasker Inc.</p>
      </div>
   </div>
</div>
<br />