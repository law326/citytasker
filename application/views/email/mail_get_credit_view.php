<!-- 得到評價, 雙方接收 -->
<div style="padding:20px 0px; background:url(<?= base_url("img/web/bg.png"); ?>)">
   <div style="
   max-width: 480px;
   padding: 19px 29px;
   margin: 50px auto;
   background:#fff;
   border: 1px solid #e5e5e5;
   -webkit-border-radius: 5px;
     -moz-border-radius: 5px;
       border-radius: 5px;
   -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
     -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
       box-shadow: 0 1px 2px rgba(0,0,0,.05);
   ">
      <div style="padding-bottom:10px; border-bottom:#eee 1px solid;"><img src="<?= base_url("img/web/logo.png"); ?>" /></div>
      <table>
        <td style="padding-right: 5px;">
          <img src="<?= base_url("uploads/head_img/".$giver_img); ?>" style="
            width: 60px;
            -webkit-border-radius: 5px;
              -moz-border-radius: 5px;
                border-radius: 5px;
            -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
              -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
            " />
         </td>
         <td>
          <p><?= $giver_name?> 已經給你評價，關於任務：<br />"<a href="<?= base_url("task/$task_id"); ?>"><?= $task_name ;?></a>"</p>
         </td>
      </table>
      <p style="padding-left: 70px;"><strong>"<?= $comment; ?>"</strong></p>
      <?php if($recipient_identity=="poster"){$who = "posted";}else{$who = "running";} ?>
      <p style="padding-left: 70px;">你有給他評價了嗎？ <a href="<?= base_url("user/$who"); ?>">回復評價</a></p>
      <br />
      <p>本信件由系統產生，請勿回信。</p>
      <p>©2013 Citytasker Inc.</p>
      </div>
   </div>
</div>