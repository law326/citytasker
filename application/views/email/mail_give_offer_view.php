<!-- 雇用通知, 幫手接收 -->
<div style="padding:20px 0px; background:url(<?= base_url("img/web/bg.png"); ?>)">
   <div style="
   max-width: 480px;
   padding: 19px 29px;
   margin: 50px auto;
   background:#fff;
   border: 1px solid #e5e5e5;
   -webkit-border-radius: 5px;
     -moz-border-radius: 5px;
       border-radius: 5px;
   -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
     -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
       box-shadow: 0 1px 2px rgba(0,0,0,.05);
   ">
      <div style="padding-bottom:10px; border-bottom:#eee 1px solid;"><img src="<?= base_url("img/web/logo.png"); ?>" /></div>
      <table>
        <td style="padding-right: 5px;">
          <img src="<?= base_url("uploads/head_img/$poster_img"); ?>" style="
            width: 60px;
            -webkit-border-radius: 5px;
              -moz-border-radius: 5px;
                border-radius: 5px;
            -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
              -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
            " />
         </td>
         <td>
          <p><?= $poster_name ;?> 確定指派你出任務：<br />"<a href="<?= base_url("task/$task_id"); ?>"><?= $task_name ?></a>"</p>
         </td>
      </table>
      <p style="padding-left: 70px;">任務是有時間性的，請記得務必在時間內完成喔！ <a href="<?= base_url("user/running"); ?>">主動聯繫案主</a></p>
      <br />
      <p>本信件由系統產生，請勿回信。</p>
      <p>©2013 Citytasker Inc.</p>
      </div>
   </div>
</div>