<?php require('template/_header.php'); ?>
<?php require('template/_navbar.php'); ?>
<link href="<?= base_url("css/doc.css"); ?>" rel="stylesheet">

  <style>
    .menu{
		 margin-top:30px;
	 }
  </style>

  <div id="container" class="container">
    <div class="row-fluid">
      <div class="span9">
        <div id="header" class"row-fluid">
          <div class="noline page-header span12">
          	<h1>歷史紀錄</h1>
          </div>
          <ul class="nav-menu nav nav-tabs">
            <li class="active">
              <a href="#feed" data-toggle="tab">全部</a>
            </li>
            <li>
              <a href="#pay" data-toggle="tab">已付款</a>
            </li>
            <li>
              <a href="#storage" data-toggle="tab">保證金</a>
            </li>
            <li>
              <a href="#income" data-toggle="tab">可提領</a>
            </li>
            <li>
              <a href="#remove" data-toggle="tab">已提領</a>
            </li>
          </ul>
          <div class="tab-content">
             <div class="tab-pane active" id="feed">
               <?php require('template/_history_feed.php'); ?>
             </div>
             <div class="tab-pane" id="pay">
               <?php require('template/_history_pay.php'); ?>
             </div>
             <div class="tab-pane" id="storage">
               <?php require('template/_history_storage.php'); ?>
             </div>
             <div class="tab-pane" id="income">
               <?php require('template/_history_income.php'); ?>
             </div>
             <div class="tab-pane" id="remove">
               <?php require('template/_history_remove.php'); ?>
             </div>
          </div>
        </div>
      </div>
      <div class="span3">
        <?php require('template/_setting_menu.php'); ?>
      </div>
    </div>
  </div>

<?php require('template/_copyright.php'); ?>
<?php require('template/_footer.php'); ?>
