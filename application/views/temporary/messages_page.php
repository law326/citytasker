<?php require('template/_header.php'); ?>
<?php require('template/_navbar.php'); ?>
<link href="<?= base_url("css/doc.css"); ?>" rel="stylesheet">

  <style>
    .menu{
		 margin-top:30px;
	 }
  </style>

  <div id="container" class="container">
    <div class="row-fluid">
      <div class="span9">
        <div id="header" class"row-fluid">
          <div class="page-header">
            <h1>訊息</h1>
          </div>
          <div id="info">
             <ul class="media-list">
               <li class="media">
                 <a class="pull-left" href="#">
                   <img class="media-object img-rounded" data-src="holder.js/50x50">
                 </a>
                 <div class="media-body">
                   <h5 class="media-heading"><strong><a>John C.</a> 歡迎加入</strong></h5>
                   <h6><small>2012年5月31</small></h6>
                 </div>
               </li>
               <li class="media">
                 <a class="pull-left" href="#">
                   <img class="media-object img-rounded" data-src="holder.js/50x50">
                 </a>
                 <div class="media-body">
                   <h5 class="media-heading"><strong><a>John C.</a> 已經成為 "Citytasker"</strong></h5>
                   <h6><small>2012年5月31</small></h6>
                 </div>
               </li>
               <li class="media">
                 <a class="pull-left" href="#">
                   <img class="media-object img-rounded" data-src="holder.js/50x50">
                 </a>
                 <div class="media-body">
                   <h5 class="media-heading"><strong><a>John C.</a> 發布新任務 "<a href="<?= base_url("user"); ?>">我需要人幫我買小禮物</a>"</strong></h5>
                   <h6><small>2012年5月31</small></h6>
                 </div>
               </li>
               <li class="media">
                 <a class="pull-left" href="#">
                   <img class="media-object img-rounded" data-src="holder.js/50x50">
                 </a>
                 <a class="pull-right btn btn-primary" href="#">指派給他</a>
                 <div class="media-body">
                   <h5 class="media-heading"><strong><a>John C.</a> 願意接下任務 "<a href="<?= base_url("user"); ?>">我需要人幫我買小禮物</a>"</strong></h5>
                   <h6><small>2012年5月31</small></h6>
                 </div>
               </li>
               <li class="media">
                 <a class="pull-left" href="#">
                   <img class="media-object img-rounded" data-src="holder.js/50x50">
                 </a>
                 <div class="media-body">
                   <h5 class="media-heading"><strong><a>John C.</a> 留言在任務 "<a href="<?= base_url("user"); ?>">我需要人幫我買小禮物</a>"</strong></h5>
                   <p>Think your friends might find this task interesting? Send them a link! Think your friends might find this task interesting? Send them a link! Think your friends might find this task interesting? Send them a link!</p>
                   <h6><small>2012年5月31</small></h6>
                 </div>
               </li>
               <li class="media">
                 <a class="pull-left" href="#">
                   <img class="media-object img-rounded" data-src="holder.js/50x50">
                 </a>
                 <div class="media-body">
                   <h5 class="media-heading"><strong><a>John C.</a> 回復留言在任務 "<a href="<?= base_url("user"); ?>">我需要人幫我買小禮物</a>"</strong></h5>
                   <p>Think your friends might find this task interesting? Send them a link! Think your friends might find this task interesting? Send them a link! Think your friends might find this task interesting? Send them a link!</p>
                   <h6><small>2012年5月31</small></h6>
                 </div>
               </li>
               <li class="media">
                 <a class="pull-left" href="#">
                   <img class="media-object img-rounded" data-src="holder.js/50x50">
                 </a>
                 <div class="media-body">
                   <h5 class="media-heading"><strong><a>John C.</a> 接下任務 "<a href="<?= base_url("user"); ?>">我需要人幫我買小禮物</a>"</strong></h5>
                   <p>任務報酬已經成功扣款並暫時存放在 Citytasker</p>
                   <h6><small>2012年5月31</small></h6>
                 </div>
               </li>
                    <li class="media">
                 <a class="pull-left" href="#">
                   <img class="media-object img-rounded" data-src="holder.js/50x50">
                 </a>
                 <a class="pull-right btn btn-primary" href="#">給他評價</a>
                 <div class="media-body">
                   <h5 class="media-heading"><strong><a>John C.</a> 已經完成任務 "<a href="<?= base_url("user"); ?>">我需要人幫我買小禮物</a>"</strong></h5>
                   <h6><small>2012年5月31</small></h6>
                 </div>
               </li>
               <li class="media">
                 <a class="pull-left" href="#">
                   <img class="media-object img-rounded" data-src="holder.js/50x50">
                 </a>
                 <div class="media-body">
                   <h5 class="media-heading"><strong><a>John C.</a> 評論任務 "<a href="<?= base_url("user"); ?>">我需要人幫我買小禮物</a>"</strong></h5>
                   <p>Think your friends might find this task interesting? Send them a link! Think your friends might find this task interesting? Send them a link! Think your friends might find this task interesting? Send them a link!</p>
                   <h6><small>2012年5月31</small></h6>
                 </div>
               </li>
             </ul>
           </div><!-- end info -->
           <div class="pagination pagination-right">
              <ul>
                <li><a href="#">Prev</a></li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">Next</a></li>
              </ul>
            </div>
        </div>
      </div>
      <div class="span3">
        <?php require('template/_setting_menu.php'); ?>
      </div>
    </div>
  </div>

<?php require('template/_copyright.php'); ?>
<?php require('template/_footer.php'); ?>
