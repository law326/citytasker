<?php require('template/_header.php'); ?>
<?php require('template/_navbar.php'); ?>
<link href="<?= base_url("css/doc.css"); ?>" rel="stylesheet">

  <style>
    .menu{
		 margin-top:30px;
	 }
  </style>

  <div id="container" class="container">
    <div class="row-fluid">
      <div class="span9">
        <div id="header" class"row-fluid">
          <div class="noline page-header span12">
            <?php require('template/_flashdata_show.php'); ?>

            <h1>設定</h1>
          </div>
          <ul class="nav-menu nav nav-tabs">
            <li class="active">
              <a href="#info" data-toggle="tab">個人資料</a>
            </li>
            <li>
              <a href="#alerts" data-toggle="tab">通知設定</a>
            </li>
          </ul>
          <div class="tab-content">
             <div class="tab-pane active" id="info">
               <?php require('template/_settings_info.php'); ?>
             </div>
             <div class="tab-pane" id="alerts">
               <?php require('template/_settings_alerts.php'); ?>
             </div>
          </div>
        </div><!-- end header -->
      </div><!-- end span9 -->
      <div class="span3">
        <?php require('template/_setting_menu.php'); ?>
      </div>
    </div>
  </div>

<?php require('template/_copyright.php'); ?>
<?php require('template/_footer.php'); ?>
