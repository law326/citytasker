<?php require('template/_header.php'); ?>
<?php require('template/_navbar.php'); ?>
<link href="<?= base_url("css/doc.css"); ?>" rel="stylesheet">

  <div id="container" class="container">
    <div class="row-fluid">
      <div class="span8 offset2">
        <div id="header" class"row-fluid">
          <div class="page-header span12">
          	<ul class="media-list">
              <li class="media">
                <a class="pull-left" href="<?= base_url("user"); ?>">
                  <img class="media-object img-rounded" data-src="holder.js/70x70" src="<?php //echo base_url("img/user").$poster_id.".jpg"; ?>">
                </a>
                <div class="media-body">
                  <h1 class="media-heading"><?= $runner_name; ?> 出任務</h1>
                  <h5>
                  	<i class="icon-ok"></i> 認證Citytasker /
                     <i class="icon-briefcase"></i> 完成任務<?= $run; ?>件 /
                     <i class="icon-star"></i> 評價<?= $fraction; ?>(5)
                  </h5>
                </div>
              </li>
            </ul>
          </div>
        </div><!-- end header -->
        <h4>出任務：<a href="<?php //echo $task_id; ?>"><?= $title; ?></a></h4>
        <br/>
        <h4>手機號碼 <small>(必填項目)</small></h4>
        <input type="text" class="input-block-level">
        <span class="help-block"><i class="icon-lock"></i> <small>方便接案者與你進一步聯繫使用，不會公開在網站上</small></span>
        <hr>
        <div class="row-fluid">
          <div class="span12" data-toggle="buttons-radio">
          <h4>付款方式</h4>
          	<div class="row-fluid" style="text-align:center;">
            	<div class="span4">
               	<button type="button" class="btn">
                  	<img src="http://cdn1.iconfinder.com/data/icons/payment-icons/64/CashU.png">
                  	<strong>網路ATM</strong>
                  </button><br><br>
                  <small>系統自動增加10元手續費</small>
               </div>
            	<div class="span4">
               	<button type="button" class="btn">
                  	<img src="http://cdn1.iconfinder.com/data/icons/payment-icons/64/Cash.png">
                     <strong>超商付款</strong>
                  </button><br><br>
               	<small>系統自動增加30元手續費</small>
               </div>
            	<div class="span4">
               	<button type="button" class="btn">
                  	<img src="http://cdn1.iconfinder.com/data/icons/payment-icons/64/PayPal.png">
                     <strong>信用卡</strong>
                  </button><br><br><small>適用國內外發卡銀行</small>
               </div>
            </div>
          </div>
        </div>
        <hr>
        <h4>支付帳款</h4>
        <h2>NT$ <?= $bounty; ?> <small>(含<?= "超商付款"; ?> NT$ 30 手續費，這筆費用恕無法退還)</small></h2>
        <hr/>
        <a class="btn btn-large btn-primary" href="#">付款</a>
        <span class="help-inline"><small>你完成付款後，Citytasker 就會執行的任務。</small></span>
      </div><!-- end span8 -->
    </div>
  </div><!-- end container -->

<?php require('template/_copyright.php'); ?>
<?php require('template/_footer.php'); ?>
