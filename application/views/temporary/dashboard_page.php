<?php require('template/_header.php'); ?>
<?php require('template/_navbar.php'); ?>
<link href="<?= base_url("css/doc.css"); ?>" rel="stylesheet">

  <style>
    #dashboard .text-header{
		 text-align:center;
		 margin-bottom:-20px;
	 }
  </style>

  <div id="container" class="container">
    <div class="row-fluid">
      <div class="span8">
        <div id="header" class"row-fluid">
          <div class="noline page-header span12">
          	<ul class="media-list">
              <li class="media">
                <a class="pull-right" href="<?= base_url("user"); ?>">
                  <img class="media-object img-rounded" data-src="holder.js/150x150">
                </a>
                <div class="media-body">
                  <h1 class="media-heading">于美人 <small>16級</small></h1>
                  <h5><i class="icon-map-marker"></i> 生涯收入：$999,999 <small>(平均月薪：$999)</small></h5>
                  <h5><i class="icon-map-marker"></i> 累積聲譽：999,999</h5>
                  <h5><i class="icon-flag"></i> 完成任務：100</h5>
                </div>
              </li>
            </ul>
            <ul class="nav-menu nav nav-tabs">
              <li class="active">
                <a href="#dashboard" data-toggle="tab">生涯進度</a>
              </li>
              <li>
                <a href="#target" data-toggle="tab">勳章任務</a>
              </li>
            </ul>
            <div class="tab-content">
               <div class="tab-pane active" id="dashboard">
                 <?php require('template/_dashboard_career.php'); ?>
               </div>
               <div class="tab-pane" id="target">
                 <?php require('template/_dashboard_target.php'); ?>
               </div>
            </div>
          </div>
        </div><!-- end header -->
      </div><!-- end span8 -->
      <div class="span4">
        <div id="paid" class="box">
          <h4>準備好了嗎？</h4>
          <p>破解任務、累積聲譽，成為城市中的高手</p>
          <button class="btn btn-inverse">任務地圖</button>
        </div>

          <ul id="experience" class="media-list">
            <li class="media">
              <h5 class="text-header">勳章任務 <small>6/52 (<a href="#">解任務</a>)</small></h5>
              <a class="pull-left" href="#">
                <img class="img-circle" data-src="holder.js/50x50">
              </a>
              <a class="pull-left" href="#">
                <img class="img-circle" data-src="holder.js/50x50">
              </a>
              <a class="pull-left" href="#">
                <img class="img-circle" data-src="holder.js/50x50">
              </a>
              <a class="pull-left" href="#">
                <img class="img-circle" data-src="holder.js/50x50">
              </a>
              <a class="pull-left" href="#">
                <img class="img-circle" data-src="holder.js/50x50">
              </a>
            </li>
            <li class="media">
              <a class="pull-left" href="#">
                <img class="img-circle" data-src="holder.js/50x50">
              </a>
              <a class="pull-left" href="#">
                <img class="img-circle" data-src="holder.js/50x50">
              </a>
              <a class="pull-left" href="#">
                <img class="img-circle" data-src="holder.js/50x50">
              </a>
              <a class="pull-left" href="#">
                <img class="img-circle" data-src="holder.js/50x50">
              </a>
              <a class="pull-left" href="#">
                <img class="img-circle" data-src="holder.js/50x50">
              </a>
            </li>
          </ul><!-- end share -->
          <?php require('template/_feedback.php'); ?>

      </div><!-- end span4 -->
    </div>
  </div><!-- end container -->

<?php require('template/_copyright.php'); ?>
<?php require('template/_footer.php'); ?>
