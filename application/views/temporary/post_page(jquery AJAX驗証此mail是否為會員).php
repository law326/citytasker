
$('#user_email').bind('change', function() {
    check_email($('#user_email').val(), function(exists) {
        if (exists){
         $('#user_password').css({"display":"block"});//會員需驗證密碼
         $('#user_name').css({"display":"none"});
         $('#user_phone').css({"display":"none"});
         $('#user_zip').css({"display":"none"});

       }else{
         $('#user_password').css({"display":"none"});
         $('#user_name').css({"display":"block"});
         $('#user_phone').css({"display":"block"});
         $('#user_zip').css({"display":"block"});
       }
    });
});

function check_email(email, callback) {
    var cct = $("input[name=csrf_citytaser_name]").val();
    $.ajax({
        url: '<?= base_url('ajax/check_email'); ?>',
        data: {email: email, 'csrf_citytaser_name': cct},
        type:"POST",
        dataType:"json",
        cache: false,
        beforeSend:function(){
           $('#loadingIMG').show();
          },
        complete:function(){
           $('#loadingIMG').hide();
         },
         success: function(data) {
             callback(data);
         }
    });
}