<?php require('template/_header.php'); ?>
<?php require('template/_navbar.php'); ?>
<style>
@media (max-width: 767px) {
	.media .pull-left {
		float:left;
	}
	.media .pull-right {
		float:right;
	}
	.span4{
		float:left;
	}
}
</style>

<?php if ($task->address  != '線上或手機'):?>

<?php endif; ?>
<?php
$count_runner = count($runner_results); // 多少人願意來完成任務
$count_status1 = 0; //計算已經雇用幾名跑者
?>

<?php if (isset($_COOKIE['poster_task_id'])) { ?>
<?php require('template/_news_alerts.php');//案主提示關連任務 ?>
<?php } ?>

<div id="container" class="container">
	<div class="row-fluid">
    <div class="span8">
      <?php require('template/_flashdata_show.php'); ?>
        <div id="header" class"row-fluid">
          <div class="page-header span12">
            <h1 class="media-heading"><?= $title; ?></h1>
            <div class="media">
                <a class="pull-left" href="<?= base_url("user/$task->member_id"); ?>">
                  <img class="media-object img-rounded" style="width:80px; margin-top:10px;" src="<?php echo base_url("uploads/head_img/$task->img")?>">
                </a>
                <div class="media-body">
                  <h5>
                    <i class="icon-map-marker"></i> <?= $task->address;?>　
                    <i class="icon-tags"></i> <a href="<?= base_url("task/map/$task->area/catalog/$task->catalog_id")?> "><?= $task->catalog; ?></a>　
                    <i class="icon-random"></i> <a href="<?= base_url("task/copy/$task->task_id");?>">複製任務</a>
                  </h5>
                <h5><i class="icon-calendar"></i> 報名截止 <?= $task->deadline; ?></h5>
                <h5><a href="<?= base_url("user/$task->member_id");?>"><?= $task->name; ?></a></h5>
              </div>
            </div>
          </div>
        </div><!-- end header -->

        <div id="info" class="row-fluid">

            <h4><i class="icon-info-sign"></i> 任務內容</h4>

            <div>
               <p id="task_content" style="padding:2px 0 0 20px;"><?= nl2br($task->content); ?></p>
               <hr/>
               <?php //按鈕的狀態
            switch ($user_task_relation) {
               case 'poster':
                  // 發布任務狀態, 且無人索取offer才能修改
                  echo "<a class='btn btn-inverse' style='padding:10px 25px;' href='" . base_url("task/edit/$task->task_id") . "'><strong>修改任務</strong></a>";
                  break;
               case 'runner':
                  echo "<button class='btn btn-inverse disabled' style='padding:10px 25px;'><strong>已經報名</strong></button>";
                  break;
               default:
                  // TODO:通過手機認證才可以接任務 請先進行手機認證
                  if (isset($_SESSION['member_id'])) {
                     if ($task_status_id < 2) {
                        echo "<button id='offer_btn' class='btn btn-inverse' style='padding:10px 25px;' href='#offer' data-toggle='modal'><strong>我要報名</strong></button>";
                     }else{
                      echo "<button id='offer_btn' class='btn btn-inverse disabled' style='padding:10px 25px;'><strong>報名截止</strong></button>";
                     }

                  }else{
                    // 尚未登入
                     echo "<a class='btn btn-inverse' href='#user_info' data-toggle='modal' style='padding:10px 25px;'><strong>我要報名</strong></a>";
                  }
               break;
            }
            ?>　
            <a class="btn" style='padding:10px 25px;' href="#contact" data-toggle="modal"><strong>不當內容</strong></a>

            <!--
               <h6 style="padding-top:15px;">需求人數：<?= $task->people; ?>人</h6>
               <h6>任務分類：<a href="<?= base_url("task/map/$task->area/catalog/$task->catalog_id")?> "><?= $task->catalog; ?></a></h6>
                     <h6>任務位置：<?= $task->address;?></h6>
                     -->
               <hr/>
               <!-- AddThis Button BEGIN -->
               <div class="addthis_toolbox addthis_default_style ">
               <a class="addthis_button_facebook_like" fb:like:layout="button_count" style="margin-right:30px;"></a>
               <a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
               <a class="addthis_button_tweet"></a>

               </div>
               <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-51c2adee76671144"></script>
               <!-- AddThis Button END -->
            </div>
         </div><!-- end info -->
         <div id="qa" class="row-fluid">
         	<div class="span12">
               <ul class="media-list">
                  <h4 class="text-header"><i class="icon-question-sign"></i> 討論區</h4>
                  <!-- <?php //if ($task_status_id < 2){ //任務完成後不顯示?> -->
                  <li class="media">
                  	<?php if (is_logged_in() == TRUE) { ?>
                     <form action="<?= base_url("discuss/post_ing");?>" method="post" accept-charset="utf-8">

                     <div class="media-body">
                     	<p><textarea rows="3" style="width:96%;" placeholder="對於任務不清楚、提出討論或是介紹你自己來爭取錄取機會(250個中文字以內)"name="content"></textarea></p>
                     </div>
                     <input type="hidden" name="task_id" value="<?= $task->task_id ?>" />
                     <input type="hidden" name="task_name" value="<?= $title ?>" />
                     <input type="hidden" name="poster_id" value="<?= $task->member_id ?>" />

                     <input type="hidden" name="user_task_relation" value="<?= $user_task_relation ?>" />
	                  <span class="help-inline"><small>*為避免個人資料外流，請勿在此填寫個人相關資料，如:手機、銀行帳戶..等。</small></span>
                     <input class="btn pull-right" type="submit" value="送出" onClick="this.disabled=true;this.value='提交中...';this.form.submit();" />
                     <?php //因為有開啟CSRF
                           $CI =& get_instance();
                           $csrf_name = $CI->security->get_csrf_token_name();
                           $csrf_value = $CI->security->get_csrf_hash();
                     ?>
                   <input type="hidden" name="<?= $csrf_name ?>" value="<?= $csrf_value ?>" />
                   <input type="hidden" name="former_URL" value="<?= $this->uri->uri_string(); //所在頁面?>" />

                  </form>
                     <?php }else{ ?>
                     <p>
                     	只要先完成<a href="#user_info" data-toggle="modal">登入/ 註冊</a>就可以參與討論！
                     </p>
                     <?php } ?>
                  </li>
                  <hr>
                  <!-- <?php// } //完成後不開放討論 ?> -->
                  <?php if (isset($discuss_results)){ ?>
                  <?php foreach ($discuss_results as $discuss): ?>
                  <li class="media">
                     <a class="pull-left" href="<?= base_url("user/$discuss->member_id");?>">
                        <img class="media-object img-rounded" style="width:40px;" src="<?php echo base_url("uploads/head_img/$discuss->img")?>">
                     </a>
                     <div class="media-body">
                      <div>
                        <a href="<?= base_url("user/$discuss->member_id");?>"><?= $discuss->name; ?></a>
                         <small><?= $discuss->created_at; ?></small>
                      </div>
                        <p class="media-heading"><?= nl2br($discuss->content); ?></p>
                     </div>
                  </li>
                  <?php endforeach; //討論區 ?>
                  <?php }else{ ?>
                  <small><?php echo "還沒有任何紀錄！"; ?></small>
                  <?php } ?>
               </ul>
            </div>
         </div><!-- end qa -->
      </div><!-- end span8 -->
      <div class="span4">
         <div id="paid" class="box">
            <h2><small><?php if ($task->payment == 0){ echo "現領"; }else{ echo "匯款";}?></small> $<?= $task->price ?>

            </h2>
            <p>準備好出任務了嗎？<br/>對任務有任何疑問可以<a href="#qa">提問討論</a>。</p>
         </div>
         <!--
         <ul id="ul2" class="nav nav-list box">
         <?php
         $status_img = array( "comment", "user", "thumbs-up");
         $status_text = array("發布任務", "執行中", "任務完成");
         for($i=0; $i<=2; $i++){
             if($task_status_id == $i){ $active = "class=\"active\""; }else{ $active = ""; }
            echo "<li ".$active."><a><i class=\"icon-".$status_img[$i]."\"></i>　".$status_text[$i]."<span>　".$status_time[$i]."</span></a></li>";
         }
         ?>
         </ul>
         -->
         <div class="row-fluid">
         	<?php if ($task->address  != '線上或手機'):?>
                <div id="map-canvas" style="width:100%; height:250px; margin-top:20px;"></div>
            <?php endif; ?>

         <!--
            <div id="feedback" class="span12">
               <ul class="media-list">
                  <h5 class="text-header">給幫手的評價</h5>
                  	<?php if ($task_status_id == 2 AND isset($credit_results)){?>
							<?php foreach ($credit_results as $credit): //所有評價?>
                     <li class="media">
                        <a class="pull-left" href="<?= base_url("user/$credit->recipient_id");?>">
                           <img class="media-object img-rounded" style="width:40px;"
                           src="<?php echo base_url("uploads/head_img/$credit->img")?> ">
                        </a>
                        <div class="media-body">
                           <h5 class="media-heading">
                           	<a href="<?= base_url("user/$credit->recipient_id");?>"><?= $credit->name; ?></a>
                           </h5>
                           <?php $read_id = $credit->recipient_id;
                                 $star_num = ( ! empty($credit->score)) ?  $credit->score: '0' ;
                                 require('template/_star.php'); ?>
									<span id="readOnly<?= $read_id; ?>"></span>  <?= $star_num; ?>/5
                           <p><?= $credit->comment; ?></p>
                           <h6><small><?= $credit->created_at; ?></small></h6>
                        </div>
                     </li>
                     <?php endforeach; //評價?>
                     <?php }else{ ?>
                     <small><?php if(isset($count_credit) AND $count_credit->total == 0) echo '還沒有任何紀錄！' ;?></small>
                     <?php }; ?>
               </ul>
            </div><!-- end feedback -->
            <div id="runner" class="span12">
               <ul class="media-list">
                  <h5 class="text-header">有 <?= $count_runner; ?> 人報名任務
                      <?php if ($user_task_relation == 'poster' && $count_runner > 0): ?>
                      <a class="btn btn-mini pull-right" href="<?= base_url("user/posted");?>">聯繫報名者</a>
                      <?php endif; ?>
                  </h5>
                  <?php if ($count_runner > 0){ ?>
                  <?php foreach ($runner_results as $runner): $i++; ?>


                     <a class="pull-left" href="<?= base_url("user/$runner->id");?>">
                        <img alt="<?= $runner->name; ?>" title="<?= $runner->name; ?>" class="media-object img-rounded" style="width:40px; margin:0 10px 10px 0;" src="
                        <?= base_url("uploads/head_img/$runner->img")?>">
                     </a>

                  <?php endforeach ?>
                  <?php }else{ ?>
                     <small>還沒有任何紀錄！</small>
                     <?php }; ?>
               </ul>
            </div><!-- end runner -->

         <?php require('template/_share.php'); ?>
         </div>
      </div><!-- end span4 -->
   </div>
</div><!-- end container -->
<!-- Modal -->
<div id="offer" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" >×</button>
      <h3 id="myModalLabel">免費出任務，只需要按讚加入粉絲團~</h3>
   </div>
   <div class="modal-body">
   	<p><strong>注意！</strong> Citytasker 的<strong>個人檔案與照片都是履歷的一部份</strong>，建議將資料述完整並換上清晰近照，將有助於任務被錄取的機會！<a href="<?= base_url("user/vitae"); ?>" target="_blank">設定個人檔案</a></p>
      <!--<p>
      	當你完成這項任務後可以得到 <strong>$<?= $task->price?></strong> 的報酬，Citytasker 將會在<strong>任務完成後</strong>向你收取其中 <strong>
      	$<?= ceil($task->price * 0.15); ?></strong> 作為網站營運與維護的費用！
      </p>
      <p style="color:#F15922">台北隊長最近出任務<strong>不在總部</strong>，所以只要完成以下條件就可以<strong>免費出任務</strong>喔！</p>-->
      <hr>
      <h4>按讚加入粉絲團：</h4>
      <div id="isFans"><i class="icon-refresh"></i> 判斷中...</div>
      <div class="socialiframetrack_facebook" id="iframe_facebook_like">
			<iframe style="display:none; border:none; overflow:hidden; width:450px; height:60px;" id="fb-like" src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcitytasker&amp;send=false&amp;layout=standard&amp;width=450&amp;show_faces=true&amp;font&amp;colorscheme=light&amp;action=like&amp;height=60&amp;appId=258248100903520" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
      </div>

      <input type="hidden" id="Fans_status" />
      <!-- <hr> -->
      <!-- <h4>步驟二：發佈推薦 Citytasker 訊息到自己牆上</h4> -->
      <div id="isPost" class="text-right"></div>
      <!-- <input type="hidden" id="Post_status" /> -->
   </div>


</div>

<?php if ($task->address  != '線上或手機'):?>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script>
$(function() {
  initialize(); //map
});

function initialize() {
  var mapOptions = {
  zoom: 14,
  center: new google.maps.LatLng(<?= $task->lat_lng; ?>),
  disableDefaultUI: true,
  scrollwheel: false,
  mapTypeId: google.maps.MapTypeId.ROADMAP
}
  var map = new google.maps.Map(document.getElementById('map-canvas'),mapOptions);
  var image = '<?= base_url("img/web/task.png"); ?>';
  var myLatLng = new google.maps.LatLng(<?= $task->lat_lng; ?>);
  var beachMarker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    icon: image
  });
}
</script>
<?php endif; ?>


<?php
// 當有runner時 才載入此JS 避免錯誤
if (isset($runner_results)):
?>
<script type="text/javascript">
/*
function checkForm(name,id) {
  // if (<?= $task->people?> == <?= $count_status1?>) {
  //   alert("已達需求人數上限");
  //   return ;
  // }
    if (confirm('確定指派 '+name,id+' 執行此任務?')) {
        var cct = $("input[name=csrf_citytaser_name]").val();

        var AJAXrequest = $.ajax({
              type: "POST",
              cache: false,
              url: "<?= base_url('ajax/give_offer'); ?>",
              data: {csrf_citytaser_name: cct,
                     runner_id: id,
                     task_id: <?= $task->task_id;?>,
                     task_name: '<?= $title;?>'  }
          });

          AJAXrequest.done(function(msg) {
              alert(msg);
              location.reload();
          });

          AJAXrequest.fail(function(jqXHR, textStatus) {
              // alert("some error: (" + textStatus + ").");
          });
          return true;
    } else{
       return false;
    };
}
*/
</script>
<? endif ;?>

<?php
// 有登入時 才載入以下JS
if (isset($_SESSION['member_id'])):
?>

<script src="<?= base_url("js/jquery.iframetracker.js"); ?>"></script>
<script type="text/javascript">
$(function() {
  //判斷是否有按讚加入粉絲
  $('#iframe_facebook_like').iframeTracker({
      blurCallback: function(){
      setTimeout($('#Fans_status').val('ok'),500);
      setTimeout("check_fans_ajax();",500);
      setTimeout("submit_offer();",500);
      }
  });

 $('#offer_btn').click(function(e) {
     check_fans_ajax();
 });

 $('#isPost').click(function(e) {
  submit_offer();
 });

 function check_fans_ajax() {
   var cct = $("input[name=csrf_citytaser_name]").val();
   var AJAXrequest = $.ajax({
         type: "POST",
         cache: false,
         url: "<?= base_url('ajax/check_fb_fans'); ?>",
         data: {
             csrf_citytaser_name: cct
         }
     });

     AJAXrequest.done(function(msg) {
         if (msg == 'yes') {
             $('#Fans_status').val('ok');
             if ($('#Fans_status').val()=='ok') {
                $('#isFans').html('<h4><i class="icon-ok"></i> 完成</h4>');
                $('#isPost').html('<a class="btn pull-right">送出報名</a>');
                $('#fb-like').hide();
             };
         } else {
             $('#Fans_status').val('no');
             $('#fb-like').show();
				 $('#isFans').html('');

         };
     });

     AJAXrequest.fail(function(jqXHR, textStatus) {
         // alert("some error: (" + textStatus + ").");
     });
 }

function submit_offer() {
   if ($('#Fans_status').val()=='ok') {
      $('#isFans').html('<h4><i class="icon-ok"></i> 完成</h4>');
      $('#fb-like').hide();
      submit_offer_ajax();
   };
}

function submit_offer_ajax (argument) {
  var cct = $("input[name=csrf_citytaser_name]").val();
  var AJAXrequest = $.ajax({
        type: "POST",
        cache: false,
        url: "<?= base_url('ajax/make_offer'); ?>",
        data: {csrf_citytaser_name: cct,
                poster_id: <?= $task->member_id;?>,
                runner_id: <?= $_SESSION['member_id'];?>,
                task_name: '<?= $title;?>',
                task_id: <?= $task->task_id;?>,
                task_content: $('#task_content').text()
                // count_runner: '<?= $count_status1;?>'
                // people: <?= $task->people; ?>,
                 }
    });

    AJAXrequest.done(function(msg) {
        alert(msg);
        location.replace('<?= base_url().'user/running/new_offer'?>');
    });

    AJAXrequest.fail(function(jqXHR, textStatus) {
        // alert("some error: (" + textStatus + ").");
    });
}
});//$.ready
</script>
<? endif ;?>

<?php require('template/_copyright.php'); ?>
<?php require('template/_footer.php'); ?>
