<?php require('template/_header.php'); ?>
<?php require('template/_navbar.php'); ?>
<script type="text/javascript" src="<?= base_url("js/masonry.pkgd.min.js")?>"></script>
<script type="text/javascript" src="<?= base_url("js/jquery.infinitescroll.min.js")?>"></script>
<style type="text/css">
body .modal {
  width: 80%; /* desired relative width */
  height: 80%; /* desired relative width */
  left: 6%; /* (100%-width)/2 */
  /* place center */
  margin-left:auto;
  margin-right:auto;
}
</style>

<style>
.item{
  display: inline-block;
  border: 1px dotted #4F4F4F;
  padding: 10px;
  margin: 5px 5px 5px 0;
  overflow:hidden;
  width:337px;
}
</style>
<div class="container-fluid">
  <div class="row-fluid">
    <div class="span12">
      <ul class="nav nav-tabs">
        <li>
          <a href="<?= base_url('spider/');?>">FB爬蟲</a>
        </li>
        <li>
          <a href="<?= base_url('spider/load/999');?>" target="_blank">載入全部社團</a>
        </li>
        <li>
          <a href="<?= base_url('spider/load/192414784133675');?>" target="_blank">載入 OUR WORK</a>
        </li>
        <li>
          <a href="<?= base_url('spider/done_show');?>">所有已發布</a>
        </li>
        <li class="active">
          <a href="<?= base_url('spider/delete_show');?>">所有已刪除(<?= $count_del?>)</a>
        </li>
      </ul>
    </div>
  </div>
</div>

<?= $pageLinks ;?>
</br>

<div id="container"  style="height:800px; width:728px; overflow:hidden; margin:0 auto;">
<? foreach ($all_results as $feed): ?>
<div id="<?= $feed->tid?>" class="item">
  <?= $feed->id?>
   | <?= $feed->name; ?>
   | <a href="<?= "https://www.facebook.com/$feed->tid";?>" target="_blank"> <?= $feed->from_name; ?></a>

   <h4><small><?= $feed->created_time; ?></small></h4>
 </br>
   <div class="media-body">
      <p class="media-heading"><?= nl2br($feed->message); ?></p>
   </div>
</div>
<? endforeach ;?>
<?php require('template/_copyright.php'); ?>
<?php require('template/_footer.php'); ?>
