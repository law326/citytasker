<?php require('template/_header.php'); ?>
<?php require('template/_navbar.php'); ?>

<div class="container">
<?php require('template/_flashdata_show.php'); ?>
	<div class="narrow_long">
      <h2>為你的城市連署！</h2>
      <hr />
      <p>希望你所在的城市也能享有 Citytasker 的服務嗎？只要呼朋引伴來完成 <strong>2000</strong> 人的連署目標，Citytasker 的服務就會開放到你所在的城市！</p>
      <p>如果還不了解什麼是 Citytasker ！可以從<a href="<?= base_url("service/forposter"); ?>">了解更多</a>開始。</p>
      <br/>
      <?php if(empty($_SESSION['member_zip'])){ ?>
      <ul style="line-height:24px;">
      	<li>桃園已經有 <strong><?= $taoyuan; ?></strong> 人完成連署</li>
         <li>新竹已經有 <strong><?= $hsinchu; ?></strong> 人完成連署</li>
         <li>苗栗已經有 <strong><?= $miaoli; ?></strong> 人完成連署</li>
         <li>台中已經有 <strong><?= $taichung; ?></strong> 人完成連署</li>
         <li>南投已經有 <strong><?= $nantou; ?></strong> 人完成連署</li>
         <li>彰化已經有 <strong><?= $changhua; ?></strong> 人完成連署</li>
         <li>雲林已經有 <strong><?= $yunlin; ?></strong> 人完成連署</li>
         <li>嘉義已經有 <strong><?= $chiayi; ?></strong> 人完成連署</li>
         <li>台南已經有 <strong><?= $tainan; ?></strong> 人完成連署</li>
         <li>高雄已經有 <strong><?= $kaohsiung; ?></strong> 人完成連署</li>
         <li>屏東已經有 <strong><?= $pingtung; ?></strong> 人完成連署</li>
         <li>宜蘭已經有 <strong><?= $yilan; ?></strong> 人完成連署</li>
         <li>花蓮已經有 <strong><?= $hualien; ?></strong> 人完成連署</li>
         <li>台東已經有 <strong><?= $taitung; ?></strong> 人完成連署</li>
         <li>外島已經有 <strong><?= $islands; ?></strong> 人完成連署</li>
      </ul>
			<?php if (is_logged_in() == TRUE) { ?>
         <h3>請至<a href="<?= base_url("user/settings"); ?>">帳戶設定</a>完成你出沒地區的郵遞區號設定，才能完成連署！</h3>
         <?php }else{ ?>
         <br/>
         <button class="btn btn-large" href="#user_info" data-toggle="modal">馬上加入連署</button>
         <?php } ?>
      <?php }else{ ?>
      <h3><?= $city_name; ?>已經有 <strong><?= $total; ?></strong> 人完成連署</h3>
      <br/>
      <p>你已經完成連署了，但距離完成目標還需要更多身邊朋友一起來努力喔！</p>
      <!-- AddThis Button BEGIN -->
      <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
      <a class="addthis_button_preferred_1"></a>
      <a class="addthis_button_preferred_2"></a>
      <a class="addthis_button_preferred_3"></a>
      <a class="addthis_button_compact"></a>
      <a class="addthis_counter addthis_bubble_style"></a>
      </div>
      <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-51c2adee76671144"></script>
      <?php } ?>
      <hr />
      <h4>成為粉絲，開放新城市的最新訊息就能即時掌握！</h4>
      <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcitytasker&amp;send=false&amp;layout=standard&amp;width=450&amp;show_faces=true&amp;font&amp;colorscheme=light&amp;action=like&amp;height=80&amp;appId=258248100903520" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:80px;" allowTransparency="true"></iframe>
	</div>
</div>

<?php require('template/_copyright.php'); ?>
<?php require('template/_footer.php'); ?>
