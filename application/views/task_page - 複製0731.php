<?php require('template/_header.php'); ?>
<?php require('template/_navbar.php'); ?>
<style>
@media (max-width: 767px) {
	.media .pull-left {
		float:left;
	}
	.media .pull-right {
		float:right;
	}
	.span4{
		float:left;
	}
}
</style>

<?php if ($task->address  != '線上或手機'):?>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script>
$(document).ready(function() {
  initialize();
});
</script>
<script>
function initialize() {
	var mapOptions = {
	zoom: 15,
	center: new google.maps.LatLng(<?= $task->lat_lng; ?>),
	disableDefaultUI: true,
	mapTypeId: google.maps.MapTypeId.ROADMAP
}
	var map = new google.maps.Map(document.getElementById('map-canvas'),mapOptions);
	var image = '<?= base_url("img/web/task.png"); ?>';
	var myLatLng = new google.maps.LatLng(<?= $task->lat_lng; ?>);
	var beachMarker = new google.maps.Marker({
		position: myLatLng,
		map: map,
		icon: image
	});
}
</script>
<?php endif; ?>

<div id="container" class="container">
	<div class="row-fluid">
      <div class="span8">
      <?php require('template/_flashdata_show.php'); ?>
         <div id="header" class"row-fluid">
            <div class="page-header span12">
               <ul class="media-list">
                  <li class="media">
                     <a class="pull-left" href="<?= base_url("user/$task->member_id"); ?>">
                        <img class="media-object img-rounded" style="width:70px;"
                        src="<?php echo base_url("uploads/head_img/$task->img")?>">
                     </a>
                     <div class="media-body">
                        <h1 class="media-heading"><?= $title; ?></h1>
                        <h5>
                           <a href="<?= base_url("user/$task->member_id");?>"><?= $task->name; ?></a>　
                           <i class="icon-calendar"></i> 完成期限 <?= $task->deadline; ?>　
                           <i class="icon-random"></i> <a href="<?= base_url("task/copy/$task->task_id");?>">複製任務</a>
                        </h5>
                    </div>
                  </li>
               </ul>
            </div>
         </div><!-- end header -->
         <div id="info" class="row-fluid">
            <h4><i class="icon-info-sign"></i> 任務內容</h4>
            <div style="padding:2px 0 0 20px;">
               <p><?= $content; ?></p>
               <h6 style="padding-top:15px;">需求人數：<?= $task->people; ?>人</h6>
               <h6>任務分類：<a href="<?= base_url("task/map/catalog/$task->catalog_id")?> "><?= $task->catalog; ?></a></h6>
                     <h6>任務位置：<?= $task->address;?></h6>
                     <?php if ($task->address  != '線上或手機'):?>
                         <div id="map-canvas" style="width:100%; height:250px;"></div>
                     <?php endif; ?>
               <br/>
               <!-- AddThis Button BEGIN -->
               <div class="addthis_toolbox addthis_default_style ">
               <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
               <a class="addthis_button_tweet"></a>
               <a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
               </div>
               <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-51c2adee76671144"></script>
               <!-- AddThis Button END -->
            </div>
         </div><!-- end info -->
         <div id="qa" class="row-fluid">
         	<div class="span12">
               <ul class="media-list">
                  <h4 class="text-header"><i class="icon-question-sign"></i> 討論區</h4>
                  <li class="media">
                  	<?php if (is_logged_in() == TRUE) { ?>
                     <form action="<?= base_url("discuss/post_ing");?>" method="post" accept-charset="utf-8">

                     <div class="media-body">
                     	<p><textarea rows="3" style="width:96%;" placeholder="對於任務不清楚、提出討論或是介紹你自己來爭取錄取機會(250個中文字以內)"name="content"></textarea></p>
                     </div>
                     <input type="hidden" name="task_id" value="<?= $task->task_id ?>" />
	                  <span class="help-inline"><small>*為避免個人資料外流，請勿在此填寫個人相關資料，如:手機、銀行帳戶..等。</small></span>
                     <input class="btn pull-right" type="submit" value="送出" onClick="this.disabled=true;this.value='提交中...';this.form.submit();" />
                     <?php //因為有開啟CSRF
                           $CI =& get_instance();
                           $csrf_name = $CI->security->get_csrf_token_name();
                           $csrf_value = $CI->security->get_csrf_hash();
                     ?>
                   <input type="hidden" name="<?= $csrf_name ?>" value="<?= $csrf_value ?>" />
                   <input type="hidden" name="former_URL" value="<?= $this->uri->uri_string(); //所在頁面?>" />

                  </form>
                     <?php }else{ ?>
                     <p>
                     	只要先完成<a href="#user_info" data-toggle="modal">登入/ 註冊</a>就可以參與討論！
                     </p>
                     <?php } ?>
                  </li>
                  <hr>
                  <?if (isset($discuss_results)): ?>
                  <?php foreach ($discuss_results as $discuss): ?>
                  <li class="media">
                     <a class="pull-left" href="<?= base_url("user/$discuss->member_id");?>">
                        <img class="media-object img-rounded" style="width:40px;" src="<?php echo base_url("uploads/head_img/$discuss->img")?>">
                     </a>
                     <div class="media-body">
                        <h4 class="media-heading">
                        	<a href="<?= base_url("user/$discuss->member_id");?>"><?= $discuss->name; ?></a>
                           <small><?= $discuss->created_at; ?></small>
                        </h4>
                        <p><?= nl2br($discuss->content); ?></p>
                     </div>
                  </li>
                  <?php endforeach; //討論區 ?>
                  <?php endif;?>
               </ul>
            </div>
         </div><!-- end qa -->
      </div><!-- end span8 -->
      <div class="span4">
         <div id="paid" class="box">
            <h2>$<?= $task->price ?>
               <small>
               <?php if($task->payment == 0){
                  echo '(現金)';
               } else {
                  echo '(匯款)';
                }?>
               </small>
            </h2>
            <p>準備好出任務了嗎？<br/>對任務有任何疑問可以<a href="#qa">提問討論</a>。</p>
            <?php //按鈕的狀態
            switch ($user_task_relation) {
               case 'poster':
                  // 發布任務狀態,才能修改
                  if ($task_status_id == 0) {
                  echo "<a class='btn btn-inverse' style='padding:10px 25px;' href='" . base_url("task/edit/$task->task_id") . "'><strong>修改任務</strong></a>";
                   }
                  break;
               case 'runner':
                  echo "<button class='btn btn-inverse disabled' style='padding:10px 25px;'><strong>已經報名</strong></button>";
                  break;
               default:

                  if (isset($_SESSION['member_id'])) {
                     if ($task_status_id != 2) {
                        echo "<button id='offer_btn' class='btn btn-inverse' style='padding:10px 25px;' href='#offer' data-toggle='modal'><strong>我要出任務</strong></button>";
                     }
                  }else{
                     echo "<a class='btn btn-inverse' href='#user_info' data-toggle='modal' style='padding:10px 25px;'><strong>我要出任務</strong></a>";
                  }
               break;
            }
            ?>
         </div>
         <ul id="ul2" class="nav nav-list box">
         <?php
         $status_img = array( "comment", "user", "thumbs-up");
         $status_text = array("發布任務", "執行中", "任務完成");
         for($i=0; $i<=2; $i++){
            if($task_status_id == $i){ $active = "class=\"active\""; }else{ $active = ""; }
            echo "<li ".$active."><a><i class=\"icon-".$status_img[$i]."\"></i>　".$status_text[$i]."<span>　".$status_time[$i]."</span></a></li>";
         }
         ?>
         </ul>
         <div class="row-fluid">
            <div id="feedback" class="span12">
               <ul class="media-list">
                  <h5 class="text-header">任務評價</h5>
                  	<?php if ($task_status_id == 2 AND isset($credit_results)){?>
							<?php foreach ($credit_results as $credit): //所有評價?>
                     <li class="media">
                        <a class="pull-left" href="<?= base_url("user/$task->member_id");?>">
                           <img class="media-object img-rounded" style="width:40px;"
                           src="<?php echo base_url("uploads/head_img/$credit->img")?> ">
                        </a>
                        <div class="media-body">
                           <h5 class="media-heading">
                           	<strong>
                                 <a href="<?= base_url("user/$credit->recipient_id");?>"><?= $credit->name; ?></a>
                              </strong>
                           </h5>
                           <?php $read_id = $credit->recipient_id;
                                 $star_num = ( ! empty($credit->score)) ?  $credit->score: '0' ;
                                 require('template/_star.php'); ?>
									評價：<span id="readOnly<?= $read_id; ?>"></span>  <?= $star_num; ?>/5
                           <p><?= $credit->comment; ?></p>
                           <h6><small><?= $credit->created_at; ?></small></h6>
                        </div>
                     </li>
                     <?php endforeach; //評價?>
                     <?php }else{ ?>
                     <small><?php if(isset($count_credit) AND $count_credit->total == 0) echo '還沒有任何紀錄！' ;?></small>
                     <?php }; ?>
               </ul>
            </div><!-- end feedback -->
            <div id="runner" class="span12">
               <ul class="media-list">
                  <h5 class="text-header">有 <?= count($runner_results); ?> 人願意來完成任務</h5>
                  <?php if ($task_status_id == 1){ ?>
                  <?php foreach ($runner_results as $runner): $i++; ?>

                  <li class="media">
                     <a class="pull-left" href="<?= base_url("user/$runner->id");?>">
                        <img class="media-object img-rounded" style="width:40px" src="
                        <?php echo base_url("uploads/head_img/$runner->img")?>">
                     </a>
                     <?php //發案者才有雇用按鈕 ?>

                     <?php if ($user_task_relation == 'poster' AND $runner->status == 0): ?>
                     <a class="pull-right btn btn-mini" href=""><strong>雇用</strong></a>
                     <?php endif; ?>

                     <?php if ($runner->status == 1): ?>
                     <a class="pull-right btn btn-mini btn-warning disabled"><strong>已錄取</strong></a>
                     <?php endif; ?>

                     <div class="media-body">
                        <h4 class="media-heading"><a href="<?= base_url("user/$runner->id");?>"><?= $runner->name;?></a></h4>
                        <p>
                           <?php $read_id = $runner->id;
                                 $star_num = ( ! empty($runner->avg)) ?  $runner->avg: '0' ;
                                 require('template/_star.php'); ?>
                           <span id="readOnly<?= $read_id; ?>"></span>  <?= $star_num; ?>/5 (<?= $star_num;?>筆評價)
                        </p>
                     </div>
                  </li>
                  <?php endforeach ?>
                  <?php }else{ ?>
                     <small><?php if(isset($count_credit) AND $count_credit->total == 0) echo '還沒有任何紀錄！' ;?></small>
                     <?php }; ?>
               </ul>
            </div><!-- end runner -->
         <?php require('template/_share.php'); ?>
         </div>
      </div><!-- end span4 -->
   </div>
</div><!-- end container -->

<!-- Modal -->
<!-- <div id="offer" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> -->
<div id="offer"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" >×</button>
      <h3 id="myModalLabel">服務費結算</h3>
   </div>
   <div class="modal-body">
      <p>
      	當你完成這項任務後可以得到 <strong>$<?= $task->price?></strong> 的報酬，Citytasker 將會在<strong>任務完成後</strong>向你收取其中 <strong>
      	$<?= ceil($task->price * 0.15); ?></strong> 作為網站營運與維護的費用！
      </p>
      <p style="color:#F15922">台北隊長最近出任務<strong>不在總部</strong>，所以只要完成以下條件就可以<strong>免費出任務</strong>喔！</p>
      <hr>
      <h4>步驟一：按讚加入粉絲團</h4>
      <div id="isFans"></div>
      <div id="fb-like" class="fb-like" data-href="http://www.facebook.com/citytasker" data-send="false" data-width="450" data-show-faces="true"></div>
      <input type="hidden" id="Fans_status" />
      <hr>
      <h4>步驟二：發佈推薦 Citytasker 訊息到自己牆上</h4>
      <div ><a class="btn" id="isPost">發布訊息</a></div>
      <input type="hidden" id="Post_status" />

   </div>
<!--    <div class="modal-footer">
   	<a href="#" class="btn btn-large">送出</a>
   </div> -->
</div>

<script type="text/javascript">
 // $('div').hasClass("pluginConnectButton").toString("bb");
//  $("pluginConnectButton").load(function() {
//   $('#offer').addClass("modal hide fade");
// });
 // (function(e) {
 //  alert('ul2');
 //  $('#offer').addClass("modal hide fade");
 // }
 // $(".div").mouseover(function(){
 //      //確認like button載入後才賦與modal hide fade 解決FFX,IE相同性問題
 //     if ($('iframe').hasClass('fb_ltr') ) {
 //      $('#offer').addClass("modal hide fade");
 //     };
 //     // $('<iframe />').load(function(){
 //     //  alert('iframe done');
 //     // }
 // });

 $('iframe.fb_ltr').load(function() {
   alert('iframe') ;
 });

 $('#offer_btn').mouseover(function(e) {
      //確認like button載入後才賦與modal hide fade 解決FFX,IE相同性問題
      if ($('iframe').hasClass('fb_ltr')) {
       $('#offer').addClass("modal hide fade");
      };
});

 $('#offer_btn').click(function(e) {
      //確認like button載入後才賦與modal hide fade 解決FFX,IE相同性問題
      // if ($('div').hasClass('pluginButtonPressed')) {
      //  $('#offer').addClass("modal hide fade");
      // };

     var cct = $("input[name=csrf_citytaser_name]").val();
     var AJAXrequest = $.ajax({
         type: "POST",
         cache: false,
         url: "<?= base_url('ajax/check_fb_fans'); ?>",
         data: {
             csrf_citytaser_name: cct
         }
     });

     AJAXrequest.done(function(msg) {
         if (msg == 'yes') {
             $('#Fans_status').val('ok');
             submit_offer();
         } else {
             $('#Fans_status').val('no');
         };
     });

     AJAXrequest.fail(function(jqXHR, textStatus) {
         alert("some error: (" + textStatus + ").");
     });
 });

 $('#isPost').click(function(e) {
         FB.ui({
                 method: 'feed',
                 name: 'Citytasker | 整個城市都有我的好幫手！',
                 link: '<?=current_url();?>',
                 picture: '<?=$meta_image;?>',
                 //caption: '<?= $meta_description;?>',
                 description: '有上萬台北人，利用空閒時間在 Citytasker 跑任務，輕鬆賺現金！什麼都漲就是薪資不漲，現在只要付出一點時間，就可以幫自己微加薪賺現金'
             },
             function(response) {
                 if (response && response.post_id) {
                     //發布成功
                     $('#isPost').html('<h4><i class="icon-ok"></i>完成</h4>');
                     $('#Post_status').val('ok');
                     submit_offer();
                 } else {
                     alert('需要發布訊息才可以出任務喔！');
                 }
             }
         );
});

function submit_offer() {
   if ($('#Fans_status').val()=='ok') {
      $('#isFans').html('<h4><i class="icon-ok"></i>完成</h4>');
      $('#fb-like').hide();
   };

   if ($('#Post_status').val()=='ok') {
      if ($('#Fans_status').val()=='ok') {
         alert('送出!');
      }else{
         alert('只剩下加入粉絲團就完成了喔!');
      };
   }else{
      if ($('#Fans_status').val()=='ok') {
         alert('只剩下加入步驟二就完成了喔!');
      }else{
         alert('submit送出!');
      };
   };
}
</script>
<?php require('template/_copyright.php'); ?>
<?php require('template/_footer.php'); ?>
