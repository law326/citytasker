<style>
@media (min-width: 767px) {
	#mobi{
		display:none;
		visibility:hidden;
	}
}
</style>

<div id="mobi">
   <img class="img-brand" src="<?= base_url("img/web/logo.png"); ?>" style="text-align:center; padding-top:20px; margin-bottom:20px;" />
   <br />
   <p>Citytasker 擁有超過 <strong>上萬</strong> 位幫手分布在大台北地區的各個角落，已經準備好要隨時出任務了！生活中大小事只要想被完成，就讓我們來幫祝你分憂解勞吧！</p>
   <a class="btn btn-large" href="#user_info" data-toggle="modal"><strong>登入 / 註冊</strong></a>
   <a class="btn btn-large btn-inverse" href="<?= base_url("task/post"); ?>"><strong>發布任務</strong></a>
   <hr />
   <embed class="video offset1 span4 img-rounded"
      src="https://www.youtube-nocookie.com/v/36iGwrzIMbM?hl=zh_TW&amp;version=3&amp;rel=0&cc_load_policy=1"
      type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true"></embed>
</div>
<!-- /container -->
