<style type="text/css">
.news-alerts{
  position:absolute;top:0;left:0;width:70%;left:15%;padding:20px;background:#464646;z-index:5050;border:10px solid #222;border-top:0;box-shadow:0 0 40px 5px black}

</style>

<div class="modal">
        <div class="modal-header">
           <h3 id="myModalLabel">您是案主本人嗎？</h3>
        </div>
        <div class="modal-body">
          <p>近期有不少熱心使用者，會透過 Citytasker 轉刊他人工作訊息與其他會員分享工作機會，若您是這則工作的案主，<strong>請透過下方"免費註冊"完成身分驗證，即可取得管理權限</strong>，以及所有報名者的聯繫清單！</p>
           <hr>

          <?php
              echo "可能與您相關的工作：</br>" ;
              $cookie_arr = json_decode($_COOKIE['poster_task_id'],TRUE);
              $cookie_arr_title = json_decode($_COOKIE['poster_task_title'],TRUE);
              $i = 0;
              foreach ($cookie_arr as $key) {
                  echo "<a href=".base_url("task/$key"). " \" target='_blank'>$cookie_arr_title[$i]</a>" . '</br>';
                  $i = $i + 1;
              }
          ?>
          <hr>

           <div >
           <?php
           if ( ! isset($_SESSION['member_id'])) {
              echo "<a class='btn btn-warning' href='#user_info' data-toggle='modal' style='padding:10px 25px;'><strong>免費註冊</strong></a>";
           }else{
              echo "如有問題，請點選下方聯絡Citytasker，將有專人為您處理~";
           }

           ?>
           </div>

        </div>
</div>