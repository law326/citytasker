﻿<!DOCTYPE html>
<html lang="zh" ><head>
      <meta charset="utf-8">
      <?php
      $exe_name = $this->uri->segment(2);
      $exe_name_task = $this->uri->segment(1);
      $segment2_numeric = is_numeric($this->uri->segment(2)) == true ? true : false ;//判斷樹否為數字

		$meta_title = "Citytasker | 整個城市都有我的好幫手！";
		$meta_description = "有上萬台北人，利用空閒時間在 Citytasker 跑任務，輕鬆賺現金！在什麼都漲但薪資不漲的年代，只要付出一點時間，就可以幫自己輕鬆微加薪！";
		$meta_image = base_url('img/web/logo-fb.png');

      if ($exe_name_task == "task" AND $segment2_numeric == true) {
			$fb_share_title = $meta_title;
			$fb_share_text = $meta_description;
			$fb_share_img = $meta_image;

			$meta_title = $title." | 更多有趣好玩的任務都在 Citytasker";

         $content = explode("\n",$task->content); // 替換JS斷行
         $content = implode("",$content);

			$meta_description = $content;
			$meta_image = base_url('uploads/head_img/'.'/'.$head_img);
		}else if ($exe_name_task == "user" AND $segment2_numeric == true) {
			$meta_title = $result_vitae->name." | 更多有趣好玩的任務都在 Citytasker";
			$meta_image = base_url('uploads/head_img/'.'/'.$head_img);
      }
      ?>
      <title><?=$meta_title;?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <meta name="author" content="JEJ & BOBO"/>
      <meta name="description" content="<?=$meta_description;?>"/>
      <meta property="og:title" content="<?=$meta_title;?>"/>
      <meta property="og:type" content="website"/>
      <meta property="og:url" content="<?=current_url();?>"/>
      <meta property="og:image" content="<?=$meta_image;?>"/>
      <meta property="og:site_name" content="Citytasker"/>
      <meta property="og:description" content="<?=$meta_description;?>"/>
      <meta name="keywords" content="
      	errands, tasks, personal assistance, courier, ikea, furniture delivery, donations,
         交通共乘, 照護, 寵物美容, 遛狗, 寫報告, 代購, 派報, 打掃, 翻譯, 快遞, 找工作, 加薪, 義工, 打字, 代排隊, 家庭代工, 定點舉牌, 人行看板, 老人陪伴, 接送, 修電腦, 問卷, 家教, 微工作,
         創業, 服務, 技能, 外包, 打工, 賺錢, 兼職,
         代買, 臨時人力, 閒置人力, 演唱會, 門票,
         台北打工, part time, PT, SG, PG, SB, PB, MODEL, 派遣, 人力銀行, 求職, 跑腿, 幫手, 鐘點
      ">

      <?php if (isset($noRobotIndex)) { echo "<meta name='robots' content='noindex,nofollow'> ";} ?>

      <!-- Le styles -->
      <link href="<?= base_url("css/bootstrap.css"); ?>" rel="stylesheet">
      <link href="<?= base_url("css/bootstrap-responsive.css"); ?>" rel="stylesheet">
      <link href="<?= base_url("css/jasny-bootstrap.css"); ?>" rel="stylesheet">
      <link href="<?= base_url("css/jasny-bootstrap-responsive.css"); ?>" rel="stylesheet">
      <link href="<?= base_url("css/doc.css"); ?>" rel="stylesheet">
      <link href="<?= base_url("css/social-buttons.css"); ?>" rel="stylesheet">
      <link href="<?= base_url("css/ui/jquery.ui.all.css"); ?>" rel="stylesheet">

      <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
      <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js"></script>
      <![endif]-->

      <!-- Le fav and touch icons -->
      <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?= base_url("img/web/icon-144.png"); ?>">
      <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?= base_url("img/web/icon-114.png"); ?>">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?= base_url("img/web/icon-72.png"); ?>">
      <link rel="apple-touch-icon-precomposed" href="<?= base_url("img/web/icon-57.png"); ?>">
      <link rel="shortcut icon" href="<?= base_url("img/web/icon-32.png"); ?>">
   </head>
   <!-- JavaScript 開始放置處 -->
   <script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
   <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.0/jquery.validate.js"></script>

   <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
   <script  src="<?= base_url("js/jquery.maxlength.js"); ?>"></script>
   <script src="<?= base_url("js/bootstrap.min.js"); ?>"></script>
   <script src="<?= base_url("js/jasny-bootstrap.min.js"); ?>"></script>
   <script src="<?= base_url("js/jquery.cookie.js"); ?>"></script>


   <script type="text/javascript" src="<?= base_url("js/star/jquery.raty.min.js"); ?>"></script>



   <body data-spy="scroll" data-target=".bs-docs-sidebar">
