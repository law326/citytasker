<style>
@media (min-width: 768px) and (max-width: 979px) {

  .navbar{
	  margin-bottom:-5px;
  }
}
@media (max-width: 767px) {
  .navbar{
	  display:none;
  }
}
</style>

<div class="navbar navbar-default navbar-fixed-top">
	<div class="navbar-inner">
      <div class="container">
      <strong>
         <ul class="nav pull-right" style="margin-top:10px;">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
            </a>

            <!-- login status -->
				<?php if (is_logged_in() == TRUE) { ?>
               <!--
               <li><a href="<?php //echo base_url("user/messages"); ?>">信息(99)</a></li>
               -->
               <li class="dropdown">
               	<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-briefcase"></i> 我的任務 <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                     <li><a tabindex="-1" href="<?= base_url("user/posted"); ?>">發案紀錄</a></li>
                     <li><a tabindex="-1" href="<?= base_url("user/running"); ?>">接案紀錄</a></li>
                  </ul>
               </li>
               <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Hi <?= $_SESSION['member_name'] ?>
                     <img style="position:absolute; width:28px; top:5px;" class="img-rounded"
                        src="<?= base_url("uploads/head_img/".$_SESSION['member_img'])?>"/>
                     <b class="caret" style="margin-left:35px;"></b>
                  </a>
                  <ul class="dropdown-menu">
                     <!--
                     <li><a tabindex="-1" href="<?php //echo base_url("user/dashboard"); ?>">生涯紀錄</a></li>
                     -->
                     <li><a tabindex="-1" href="<?= base_url("user").'/'.$_SESSION['member_id']; ?>">個人檔案</a></li>
                     <!--
                     <li><a tabindex="-1" href="<?= base_url("user/payments"); ?>">交易紀錄</a></li>
                     <!--
                     <li class="divider"></li>
                     <li><a tabindex="-1" href="#">幫手排行榜</a></li>
                     <li><a tabindex="-1" href="#">勳章任務</a></li>
                     <li class="divider"></li>
                     <li><a tabindex="-1" href="#">邀請朋友</a></li>
                     -->
                     <li class="divider"></li>
                     <li><a tabindex="-1" href="<?= base_url("user/settings"); ?>">帳戶設定</a></li>
                     <li><a tabindex="-1" href="<?= base_url("user/vitae"); ?>">個人檔案設定</a></li>
                     <li class="divider"></li>
                     <?php if ($_SESSION['member_id'] == '1' OR $_SESSION['member_id'] == '2') { ?>
                     <li><a tabindex="-1" href="<?= base_url("admin"); ?>">後台</a></li>
                     <li><a tabindex="-1" href="<?= base_url("admin/alltask"); ?>">任務總覽</a></li>
                     <li><a tabindex="-1" href="<?= base_url("spider"); ?>">FB爬蟲</a></li>
                     <li class="divider"></li>
                     <?php } ?>
                     <li><a tabindex="-1" href="<?= base_url("user/logout") ?>">登出</a></li>
                  </ul>
               </li>
            <?php } else { ?>
               <a class="btn" href="#user_info" data-toggle="modal"><strong>登入 / 免費註冊</strong></a>
            <?php } ?>
         </ul>
         <!-- login status End -->
         <a class="brand" href="<?= base_url(""); ?>"><img id="logogo" alt="台北找幫手解任務" class="img-brand" src="<?= base_url("img/web/logo.png"); ?>" /></a>
         <div class="nav-collapse collapse">
               <ul class="nav">
                  <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">了解更多 <b class="caret"></b></a>
                     <ul class="dropdown-menu">
                        <li><a tabindex="-1" href="<?= base_url("service/forposter"); ?>">我想發布任務</a></li>
                        <li><a tabindex="-1" href="<?= base_url("service/forrunner"); ?>">我想完成任務</a></li>
                     </ul>
                  </li>
                  <?php if (isset($_SESSION['vitae_status']) AND $_SESSION['vitae_status'] == 'TRUE'){ ?>
                  <li><a href="<?= base_url('task/map');?>" data-toggle="modal">搜尋任務</a></li>
                  <?php }else{ ?>
                  <li><a href="<?= base_url("task/map"); ?>">搜尋任務</a></li>
                  <?php } ?>

                  <li><a href="<?= base_url("task/post"); ?>">免費發布任務</a></li>
               </ul>
         </div>
         </strong>
      </div>
	</div>
</div>