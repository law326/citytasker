<div id="info">
   <ul class="media-list">
   <?php
   $page_num = 1;
   $feed_num = 10;
   $page_feed = $page_num * $feed_num;
   
   $feedback_num = count($feedback_comment); //12
   if($page_num==1){ $feed_start = 0; }else{ $feed_start = $page_feed - 11; }//10
   if($page_feed>=$feedback_num){ $feed_end = $feedback_num - 1; }else{ $feed_end = $page_feed - 1; }//19
   if($feedback_num==0)echo $member_name."還沒完成過任務";
   for($i=$feed_start;$i<=$feed_end;$i++){
   ?>
      <li class="media">
         <a class="pull-left" href="#">
         	<img class="media-object img-rounded" data-src="holder.js/50x50" src="<?php //echo $feedback_poster_id[$i]; ?>">
         </a>
         <div class="media-body">
            <h5 class="media-heading">
               <strong>
                  <a href="<?php //echo $feedback_poster_id[$i]; ?>"><?= $feedback_poster_name[$i]; ?></a> 評論： 
                  <a href="<?php  //echo $feedback_task_id[$i]; ?>"><?= $feedback_task_name[$i]; ?></a>
					</strong>
            </h5>
            <span id="readOnly<?=$i;?>"></span> 3/5
            <p><?= $feedback_comment[$i]; ?></p>
            <h6><small><?= $feedback_post_time[$i]; ?></small></h6>
         </div>
      </li>
   <?php } ?>
   </ul>
</div><!-- end info -->
<div class="pagination pagination-right">
   <ul>
   <?php 
   $page_prev = $page_num - 1;
   if($page_prev!=0){ echo "<li><a href=\"".$page_prev."\">Prev</a></li>"; }
   
   $page_sum = ceil($feedback_num / $feed_num);
   for($i=1;$i<=$page_sum;$i++){
   if($page_num == $i){ $active = "class=\"active\""; }else{ $active = ""; }
   echo "<li ".$active."><a href=\""."\">".$i."</a></li>";
   }
   
   $page_next = $page_num + 1;
   if($page_next<=$page_sum){ echo "<li><a href=\""."\">Next</a></li>"; }
   ?>
   </ul>
</div>