<style>
@media (max-width: 767px) {
  .footer{
	  display:none;
  }
}
</style>

<!-- Footer
================================================== -->
<footer class="footer" style="background:url(<?= base_url("img/web/bg.png"); ?>); margin-bottom:-20px;">
   <div class="container">
      <p class="pull-right"></p>
      <p class="pull-left">&copy; 2013 Citytasker, Inc. &middot; <a href="<?= base_url("service/terms"); ?>">使用條款</a> &middot; <a href="<?= base_url("service/privacy"); ?>">隱私政策</a> &middot; <a href="<?= base_url("service/team"); ?>">關於我們</a> &middot; <a href="https://www.facebook.com/citytasker">facebook</a></p>
   </div>
</footer>

<!-- Le javascript
================================================== -->

<!-- 模擬圖片 -->
<!--<script src="http://twitter.github.com/bootstrap/assets/js/holder/holder.js"></script>-->
<!-- 選單卡住 -->
<!--<script src="http://twitter.github.com/bootstrap/assets/js/application.js"></script>-->