<style>
@media (min-width: 767px) {
  #mobi_menu{
	  display:none;
  }
}
@media (max-width: 767px) {
	.container{
		padding-bottom:100px;
	}
	#mobi_menu{
		position:fixed;
		bottom:0px;
		left:-1%;
		width:102%;
		z-index:999;
	}
	#mobi_menu .btn-group{
		text-align:center;
		width:100%;
	}
	#mobi_menu .btn{
		width:25%;
		padding:8px 0px;
	}
}
</style>

<div id="mobi_menu">
   <div class="btn-group">
   	<?php if (is_logged_in() == TRUE) { ?>
	      <a class="btn" href="<?= site_url("user/settings"); ?>"><img src="http://cdn1.iconfinder.com/data/icons/windows8_icons/26/services.png" /><br />帳戶設定</a>
      <?php }else{ ?>
      	<a class="btn" href="#user_info" data-toggle="modal"><img src="http://cdn1.iconfinder.com/data/icons/windows8_icons/26/my_topic.png" /><br />登入/註冊</a>
      <?php } ?>
      <a class="btn" href="<?= base_url("task/post"); ?>"><img src="http://cdn1.iconfinder.com/data/icons/windows8_icons_iconpharm/26/advertising.png" /><br /> 發布任務</a>
      <a class="btn" href="<?= base_url("task/map"); ?>"><img src="http://cdn1.iconfinder.com/data/icons/windows8_icons_iconpharm/26/map_marker.png" /><br/>尋找任務</a>
      <a class="btn" href="#contact" data-toggle="modal"><img src="http://cdn1.iconfinder.com/data/icons/windows8_icons_iconpharm/26/online_support.png" /><br />意見回饋</a>
   </div>
</div>

<div style="position:fixed; bottom:0; right:8%; z-index:99;">
	<a class="btn btn-warning" href="<?= base_url("task/post"); ?>"
   	style="-webkit-border-radius: 4px 4px 0 0; -moz-border-radius: 4px 4px 0 0; border-radius: 4px 4px 0 0;">
      <i class="icon-bullhorn icon-white"></i> 免費發布任務
   </a>
   <a class="btn btn-inverse" href="#contact" data-toggle="modal"
      style="-webkit-border-radius: 4px 4px 0 0; -moz-border-radius: 4px 4px 0 0; border-radius: 4px 4px 0 0;">
      <i class="icon-user icon-white"></i> 聯絡 Citytasker
   </a>
</div>

<!-- Modal -->
<form action="<?= base_url("service/email_sending/");?>" method="post" accept-charset="utf-8" AUTOCOMPLETE="off">
	<div id="contact" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
         <h3 id="myModalLabel">意見回饋</h3>
      </div>
      <div class="modal-body">
         <p>有任何話想對我們說，無論是建議、問題回報、投訴或是合作，都歡迎直接留下你的想法與意見，我們會盡快處理或與你聯繫。</p>
         <input type="text" class="input-block-level" maxlength="20" placeholder="信息標題"
         name="sending_title" value="<?= html_entity_decode($this->input->post('sending_title'))?>">
         <br />
         <textarea rows="5" style="width:96%;" id='ct1' placeholder="我們今天能幫你什麼？"
         name="sending_content" ><?= html_entity_decode($this->input->post('sending_content'))?></textarea>
         <br />
         <input type="text" class="input-block-level" maxlength="50" placeholder="你的Email"
         name="sending_email" value="<?= html_entity_decode($this->input->post('sending_email'))?>">

         <input type="hidden" name="user_agent" value="<?= $this->input->user_agent(); ?>" />
         <input type="hidden" name="former_URL" value="<?= $this->uri->uri_string(); //所在頁面?>" />

         <?php //因為有開啟CSRF
         $CI =& get_instance();
         $csrf_name = $CI->security->get_csrf_token_name();
         $csrf_value = $CI->security->get_csrf_hash();
         ?>
         <input type="hidden" name="<?= $csrf_name ?>" value="<?= $csrf_value ?>" />
      </div>
      <div class="modal-footer">
         <input class="btn btn-large" type="submit" name="name" value="送出" onclick="this.disabled=true;this.value='提交中...';this.form.submit();" />
      </div>
	</div>
</form>

<!-- Task Posted
================================================== -->

<!-- Modal -->
<div id="post_tip" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      <h3 id="myModalLabel">任務發布提示</h3>
   </div>
   <ul class="modal-body">
      <li>為了保護個人隱私，<strong>請勿將Mail以及手機寫入任務內容</strong>，雇用人選確認後聯繫方式會公開在<a href="<?= base_url("user/posted"); ?>">發案紀錄</a>中。</li><br/>
      <li>任務需要履歷相關資料時，可以在<strong>任務內容中要求報名者把工作經驗、學歷、自傳、換上個人近照等完整履</strong>歷寫在<a href="<?= base_url("user/vitae"); ?>">個人檔案</a>中即可。</li>
   </ul>
   <div class="modal-footer">
      <a class="btn btn-large" href="<?= base_url("task/post"); ?>" >我同意</a>
   </div>
</div>

<!-- Go Task
================================================== -->

<!-- Modal -->
<div id="task_tip" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      <h3 id="myModalLabel">出任務提示</h3>
   </div>
   <ul class="modal-body">
      <li>多數任務是需要履歷相關資料，在出任務前可以在<a href="<?= base_url("user/vitae"); ?>">個人檔案</a>中填寫<strong>工作經驗、學歷、自傳、換上個人近照等完整履</strong>，寫得越完整將會影響被錄取的可能性！<br/><strong>(提示：填完所有資料後本提示也將不再出現！)</strong></li><br/>
      <li>為了保護個人隱私，<strong>請勿將Mail以及手機在討論區公開</strong>，發案者聯繫方式將於被雇用後公開在<a href="<?= base_url("user/running"); ?>">接案紀錄</a>中。</li>
   </ul>
   <div class="modal-footer">
      <a class="btn btn-large" href="<?= base_url("task/map"); ?>" >我同意</a>
   </div>
</div>

<!-- Facebook login
================================================== -->

<!-- Modal -->
<div id="user_info" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      <h3 id="myModalLabel">啟用 Citytasker ！</h3>
   </div>
   <div class="modal-body">
      <div class="row-fluid">
         <button id="facebook" class="btn btn-facebook btn-large">使用 facebook 快速登入</button>
         <span class="help-inline">馬上就能使用更多完整功能喔！</span>
         <hr>
         <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcitytasker&amp;send=false&amp;layout=standard&amp;width=450&amp;show_faces=true&amp;font&amp;colorscheme=light&amp;action=like&amp;height=80&amp;appId=258248100903520" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:80px;" allowTransparency="true"></iframe>
      </div>
   </div>
</div>

<div id="fb-root"></div>
<script type="text/javascript">
  window.fbAsyncInit = function() {
     //Initiallize the facebook using the facebook javascript sdk
      FB.init({
        appId: '<?php echo $this->config->item('appID'); ?>', // App ID
        cookie:true, // enable cookies to allow the server to access the session
        status:true, // check login status
        xfbml:true, // parse XFBML
        oauth : true //enable Oauth
       });
  };

   (function(d){
           var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
           if (d.getElementById(id)) {return;}
           js = d.createElement('script'); js.id = id; js.async = true;
           js.src = "//connect.facebook.net/zh_TW/all.js";
           ref.parentNode.insertBefore(js, ref);
         }(document));

   //Onclick for fb login
 $('#facebook').click(function(e) {
    FB.login(function(response) {
     if(response.authResponse) {
        var url = '<?= $this->uri->uri_string();?>'; //取得當前網址
        parent.location = '<?= base_url('fbci/fblogin'); ?>'+'/'+url;
      }else{
         alert('請安心授權給Citytasker，我們保證不會在非經你的同意下回傳訊息至Facebook');
      }
 },{scope: 'user_birthday,email'}); //permissions
});

$('.container').click(function(event) {
  FB.getLoginStatus(function(response) {
    if (response.status === 'connected') {
      // the user is logged in and has authenticated your
      // app, and response.authResponse supplies
      // the user's ID, a valid access token, a signed
      // request, and the time the access token
      // and signed request each expire
      var uid = response.authResponse.userID;
      var accessToken = response.authResponse.accessToken;

         console.log('connect');

    } else if (response.status === 'not_authorized') {
         console.log('not_authorized');
    } else {
         console.log('isnt logged in to Facebook');

      // the user isn't logged in to Facebook.
    }
   });

});

</script>

<!-- Analytics
================================================== -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-42632316-1', 'citytasker.tw');
  ga('send', 'pageview');

</script>

	</body>
</html>