<ul class="nav nav-list">
	<li class="nav-header">選單</li>
	<li class="divider"></li>
   <li>
   	<a href="<?= base_url("user").'/'.$_SESSION['member_id']; ?>"><i class="icon-chevron-left"></i> 個人檔案</a>
   </li>
   <li class="divider"></li>
   <li <?php if ($exe_name == "posted"){ echo "class=\"active\""; } ?>>
   	<a href="<?= base_url("user/posted"); ?>"><i class="icon-chevron-left"></i> 發案紀錄</a>
   </li>
   <li <?php if ($exe_name == "running"){ echo "class=\"active\""; } ?>>
   	<a href="<?= base_url("user/running"); ?>"><i class="icon-chevron-left"></i> 接案紀錄</a>
   </li>
   <li class="divider"></li>
   <!--
   <li <?php if ($exe_name == "payments"){ echo "class=\"active\""; } ?>>
   	<a href="<?= base_url("user/payments"); ?>"><i class="icon-chevron-left"></i> 交易紀錄</a>
   </li>
   -->
   <!-- <li><a href="#labels-badges"><i class="icon-chevron-left"></i> 幫手排行榜</a></li> -->
   <!--
	<?php if ($is_have_phone == FALSE) { ?>
   <li <?php if ($exe_name == "phone_vaild"){ echo "class=\"active\""; } ?>>
   	<a href="<?= base_url("phone_vaild/"); ?>"><i class="icon-chevron-left"></i> 手機認證</a>
   </li>
   <?php  ;} ;?>
   -->
   <li <?php if ($exe_name == "settings"){ echo "class=\"active\""; } ?>>
   	<a href="<?= base_url("user/settings"); ?>"><i class="icon-chevron-left"></i> 帳戶設定</a>
   </li>
   <li <?php if ($exe_name == "vitae"){ echo "class=\"active\""; } ?>>
   	<a href="<?= base_url("user/vitae"); ?>"><i class="icon-chevron-left"></i> 個人檔案設定</a>
   </li>
</ul>

<!--
<div id="paid" class="box">
  <button class="btn btn-inverse">發布任務</button>
</div>
<ul class="menu nav nav-tabs nav-stacked">
  <li><a href="<?= base_url("user/messages"); ?>"><i class="icon-chevron-left"></i> 訊息</a></li>
  <li><a href="<?= base_url("user/history"); ?>"><i class="icon-chevron-left"></i> 歷史紀錄</a></li>
  <li><a href="#labels-badges"><i class="icon-chevron-left"></i> 幫手排行榜</a></li>
  <li><a href="#typography"><i class="icon-chevron-left"></i> 邀請朋友</a></li>

  <li><a href="<?= base_url("user/dashboard"); ?>"><i class="icon-chevron-left"></i> 勳章任務</a></li>
<?php if ($phone =='請至右側手機驗證頁面,進行驗證.') { ?>
  <li><a href="<?= base_url("phone_vaild/"); ?>"><i class="icon-chevron-left"></i> 手機認證</a></li>
<?php  ;} ;?>
  <li class="active"><a href="<?= base_url("user/settings"); ?>"><i class="icon-chevron-left"></i> 設定</a></li>
</ul>
-->
