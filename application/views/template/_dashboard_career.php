               <div id="career" class="row-fluid">
                 <div class="span12">
                   <div class="row-fluid">
                     <div class="span6">
                       <div class="text-header">目標達成金額</div>
                       <div class="box"> 
                         <h1>50%</h1>
                         <div>目前累計金額5,000<br/>再完成20個任務即可達成</div>
                       </div>
                     </div>
                     <div class="span6">
                       <div class="text-header">目標累積聲譽</div>
                       <div class="box"> 
                         <h1>75%</h1>
                         <div>目前累計聲譽1,400點<br/>再完成20個任務即可達成</div>
                       </div>
                     </div>
                   </div>
                   <br/>
                   <div class="row-fluid">
                     <div class="span6">
                       <div class="text-header">分紅獎金</div>
                       <div class="box"> 
                         <h3>須滿17級</h3>
                         <div>滿1000元分紅4.5%<br/>2個月內再完成20個任務即可達成</div>
                       </div>
                     </div>
                     <div class="span6">
                       <div class="text-header">年終獎金</div>
                       <div class="box"> 
                         <h3>須滿17級</h3>
                         <div>目前累計業績14,000<br/>再完成20個任務即可達成</div>
                       </div>
                     </div>
                  </div>
                 </div>
               </div><!-- end career -->