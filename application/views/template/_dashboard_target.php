<div id="target" class="row-fluid">
  <div class="span12">
    <div class="row-fluid">
      <div class="span4">
        <img class="img-rounded" style="width:120px;" src="https://playfoursquare.s3.amazonaws.com/badge/300/bender.png"/>
        <h5><a href="#tip1" role="button" data-toggle="modal">夜行者</a></h5>
      </div> 
      <div class="span4">
        <img class="img-rounded" style="width:120px;" src="https://playfoursquare.s3.amazonaws.com/badge/300/crunked.png"/>
        <h5>聲譽加速器</h5>
      </div>
      <div class="span4">
        <img class="img-rounded" style="width:120px;" src="https://playfoursquare.s3.amazonaws.com/badge/300/overshare.png"/>
        <h5>話題王</h5>
      </div>             
    </div><!-- end row-fluid -->
    <div class="row-fluid">
      <div class="span4">
        <img class="img-rounded" style="width:120px;" src="https://playfoursquare.s3.amazonaws.com/badge/300/bender.png"/>
        <h5>夜行者</h5>
      </div> 
      <div class="span4">
        <img class="img-rounded" style="width:120px;" src="https://playfoursquare.s3.amazonaws.com/badge/300/crunked.png"/>
        <h5>聲譽加速器</h5>
      </div>
      <div class="span4">
        <img class="img-rounded" style="width:120px;" src="https://playfoursquare.s3.amazonaws.com/badge/300/overshare.png"/>
        <h5>話題王</h5>
      </div>             
    </div><!-- end row-fluid -->
    <div class="row-fluid">
      <div class="span4">
        <img class="img-rounded" style="width:120px;" src="https://playfoursquare.s3.amazonaws.com/badge/300/bender.png"/>
        <h5>夜行者</h5>
      </div> 
      <div class="span4">
        <img class="img-rounded" style="width:120px;" src="https://playfoursquare.s3.amazonaws.com/badge/300/crunked.png"/>
        <h5>聲譽加速器</h5>
      </div>
      <div class="span4">
        <img class="img-rounded" style="width:120px;" src="https://playfoursquare.s3.amazonaws.com/badge/300/overshare.png"/>
        <h5>話題王</h5>
      </div>             
    </div><!-- end row-fluid -->
  </div>
</div><!-- end target -->

<!-- Modal -->
<div id="tip1" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">夜行者 <small>攻略</small></h3>
  </div>
  <div class="modal-body">
    <img class="img-rounded" style="width:120px;" src="https://playfoursquare.s3.amazonaws.com/badge/300/bender.png"/>
    <p>連續四天晚間執行任務者</p>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">關閉</button>
    <button class="btn btn-primary">出任務</button>
  </div>
</div>