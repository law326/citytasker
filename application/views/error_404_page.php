<?php require('template/_header.php'); ?>
<?php require('template/_navbar.php'); ?>

<div class="container">
<?php require('template/_flashdata_show.php'); ?>
	<div class="narrow_long">
      <h2>ERROR 404！</h2>
      <hr />
      <p>你尋找的頁面可能已被移除或不存在，<a href="<?= base_url(""); ?>">回到首頁</a>繼續瀏覽其它頁面。</p>
      <p>有任何問題也歡迎<a href="mailto:info@citytasker.tw">寄信給 Citytasker</a></p>
      <hr />
      <h4>給你的回覆滿意嗎？</h4>
      <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcitytasker&amp;send=false&amp;layout=standard&amp;width=450&amp;show_faces=true&amp;font&amp;colorscheme=light&amp;action=like&amp;height=80&amp;appId=258248100903520" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:80px;" allowTransparency="true"></iframe>
	</div>
</div>

<?php require('template/_copyright.php'); ?>
<?php require('template/_footer.php'); ?>
