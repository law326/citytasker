<?php require('template/_header.php'); ?>
<?php require('template/_navbar.php'); ?>

<div id="container" class="container">
   <div class="row-fluid">
      <div class="span8">
      	<?php require('template/_flashdata_show.php'); ?>
         <div id="header" class"row-fluid">
            <div class="page-header span12">
	            <h1>個人檔案設定</h1>
            </div>
         </div>
         <div>
         	<form action="<?= base_url("upload/do_upload_settings") ?>" method="post" enctype="multipart/form-data" >
               <h4><strong>更新照片</strong></h4>
               <div>請上傳張關於你的照片，這張照片也將會影響大家對於你的第一印象</div>
               <br/>
               <div class="fileupload fileupload-new media" data-provides="fileupload">
                  <div class="pull-left">
                     <div class="fileupload-preview thumbnail" style="width: 150px; height: 150px;">
                        <img class="img-rounded" src="<?= base_url("uploads/head_img/$img");?>">
                     </div>
                  </div>
                  <div class="media-body">
                     <!-- Nested media object -->
                     <p>從電腦中選取圖檔 (最小像素 150x150 )<br/>檔案格式：JPG, GIF, PNG</p>
                     <p>
                        <span class="btn btn-file">
                           <span class="fileupload-new">選擇照片</span>
                           <span class="fileupload-exists">更換</span>
                           <input type="file" name="userfile" size="20" />
                        </span>
                        <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">刪除</a>
                        <input class="btn" type="submit" value="上傳" />
                     </p>
                  </div>
               </div>

               <?php //因為有開啟CSRF
               $CI =& get_instance();
               $csrf_name = $CI->security->get_csrf_token_name();
               $csrf_value = $CI->security->get_csrf_hash();
               ?>

               <input type="hidden" name="<?= $csrf_name ?>" value="<?= $csrf_value ?>" />
            </form>
            <hr />
            <form action="<?= base_url("user/vitae_ing") ?>" method="post" >
            	<div class="row-fluid">
                  <div class="span2 text-right">電子郵件</div>
                  <div class="span10">
                     <input type="text" class="input-block-level" name="language" value="">
                  </div>
               </div>
               <div class="row-fluid">
                  <div class="span2 text-right">真實姓名</div>
                  <div class="span10">
                     <input type="text" class="input-block-level" name="language" value="">
                  </div>
               </div>
             	<h4><strong>聯繫方式</strong></h4>
               <p>請填妥相關資訊讓案主可以方便與您聯繫</p>
               <br/>
               <div class="row-fluid">
                  <div class="span2 text-right">電子郵件</div>
                  <div class="span10">
                     <input type="text" class="input-block-level" name="language" value="">
                  </div>
               </div>
               <div class="row-fluid">
                  <div class="span2 text-right">手機號碼</div>
                  <div class="span10">
                     <input type="text" class="input-block-level" name="language" value="">
                  </div>
               </div>
               <hr>
               <h4><strong>基本資料</strong></h4>
               <p>填寫越詳細也將有助於大家更認識你</p>
               <br/>
               <div class="row-fluid">
                  <div class="span2 text-right">生日</div>
                  <div class="span10">
                     <input type="text" class="input-block-level" name="language" value="">
                  </div>
               </div>
               <div class="row-fluid">
                  <div class="span2 text-right">性別</div>
                  <div class="span10">
                  <select>
                    <option>男</option>
                    <option>女</option>
                  </select>
                  </div>
               </div>
               <div class="row-fluid">
                  <div class="span2 text-right">身高</div>
                  <div class="span10">
                     <input type="text" class="input-block-level" name="language" value="">
                  </div>
               </div>
               <div class="row-fluid">
                  <div class="span2 text-right">體重</div>
                  <div class="span10">
                     <input type="text" class="input-block-level" name="language" value="">
                  </div>
               </div>
               <div class="row-fluid">
                  <div class="span2 text-right">語言</div>
                  <div class="span10">
                     <input type="text" class="input-block-level" placeholder="EX: 國語	、台語..." name="language" value="<?=set_value('language', $result->language)?>">
                  </div>
               </div>
               <div class="row-fluid">
                  <div class="span2 text-right">最高學歷</div>
                  <div class="span10">
                     <input type="text" class="input-block-level" placeholder="EX: 國立台北科技大學碩士(電腦與通訊研究所)" name="education" value="<?=set_value('education', $result->education)?>">
                  </div>
               </div>
               <div class="row-fluid">
                  <div class="span2 text-right">科系</div>
                  <div class="span10">
                     <input type="text" class="input-block-level" placeholder="EX: 電腦與通訊研究所" name="education" value="<?=set_value('education', $result->education)?>">
                  </div>
               </div>

               <hr />

               <h4><strong>自我推薦</strong></h4>
               <p>向 Citytasker 的朋友們介紹自己，如果你有工作經歷，請多描述任何能幫助大家認識並信任你喔！</p>
               <br/>
               <div class="row-fluid">
                  <textarea rows="3" id='ct1' class="span12" placeholder="" name="about" ><?=set_value('about', $result->about)?></textarea>
               </div>

               <hr>

                <h4><strong>經歷</strong></h4>
               <p>向 Citytasker 的朋友們介紹自己，如果你有工作經歷，請多描述任何能幫助大家認識並信任你喔！</p>
               <br/>
               <div class="row-fluid">
                  <textarea rows="3" id='ct1' class="span12" placeholder="" name="about" ><?=set_value('work', $result->work)?></textarea>
               </div>

               <hr>
               
                <h4><strong>接案方式</strong></h4>
               <p>你能夠用哪些方式來完成任務</p>
               <br/>
               <div class="row-fluid">
                  <label class="checkbox offset2 span5">
                  	<input type="checkbox" name="jobway[]" value="1" <?= set_checkbox('jobway', '1'); ?> <? if ($result->jobway1 == 1) echo "checked"; ?> >手機或電腦(線上任務)
                  </label>
                  <label class="checkbox span4">
                  	<input type="checkbox" name="jobway[]" value="2" <?= set_checkbox('jobway', '2'); ?> <? if ($result->jobway2 == 1) echo "checked"; ?> >步行
                  </label>
               </div>
               <div class="row-fluid">
                  <label class="checkbox offset2 span5">
                  	<input type="checkbox" name="jobway[]" value="3" <?= set_checkbox('jobway', '3'); ?> <? if ($result->jobway3 == 1) echo "checked"; ?> >自行車
                  </label>
                  <label class="checkbox span4">
                  	<input type="checkbox" name="jobway[]" value="4" <?= set_checkbox('jobway', '4'); ?> <? if ($result->jobway4 == 1) echo "checked"; ?> >摩托車
                  </label>
               </div>
               <div class="row-fluid">
                  <label class="checkbox offset2 span5">
                  	<input type="checkbox" name="jobway[]" value="5" <?= set_checkbox('jobway', '5'); ?> <? if ($result->jobway5 == 1) echo "checked"; ?> >轎車
                  </label>
                  <label class="checkbox span4">
                  	<input type="checkbox" name="jobway[]" value="6" <?= set_checkbox('jobway', '6'); ?> <? if ($result->jobway6 == 1) echo "checked"; ?> >卡車
                  </label>
               </div>

               <hr />

               <div class="row-fluid">
                  <input class="btn btn-large" type="submit" value="存檔" />
               </div>

               <?php //因為有開啟CSRF
                  $CI =& get_instance();
                  $csrf_name = $CI->security->get_csrf_token_name();
                  $csrf_value = $CI->security->get_csrf_hash();
               ?>
               <input type="hidden" name="<?= $csrf_name ?>" value="<?= $csrf_value ?>" />
               <input type="hidden" name="former_URL" value="<?= $this->uri->uri_string();?>" />

            </form>
         </div><!-- end header -->
      </div><!-- end span9 -->
      <div class="span4">
      	<?php require('template/_setting_menu.php'); ?>
      </div>
   </div>
</div>

<?php require('template/_copyright.php'); ?>
<?php require('template/_footer.php'); ?>
