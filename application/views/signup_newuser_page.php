<?php require('template/_header.php'); ?>
<?php require('template/_navbar.php'); ?>

<div class="container">
<?php require('template/_flashdata_show.php'); ?>

	<div class="narrow_long">
      <h2>歡迎你的註冊</h2>
      <hr>
      <p>Hi <?= $username ;?>，你的帳號還<strong>尚未啟用</strong>，請立即至註冊使用的
      <a href="<?= $mail_site?>" target="view_window"><?= $mail_name ?></a> 信箱來啟用確認信！</p>
      <p>完成帳號啟用，就可以使用Citytasker的服務。</p>
      <hr>
      <p>沒有收到相關信件，請檢查是否被分類至垃圾郵件，或是
      <a href="mailto:info@citytasker.tw">寄信給 Citytasker</a>，讓我們由專人來協助你！</p>
   </div>
</div><!-- end container -->

<?php require('template/_copyright.php'); ?>
<?php require('template/_footer.php'); ?>
