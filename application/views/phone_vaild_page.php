<?php require('template/_header.php'); ?>
<?php require('template/_navbar.php'); ?>

<div class="container">
<?php require('template/_flashdata_show.php'); ?>

<?php //避免重複提送表單
$resubmit_code = 'not yet';
$_SESSION['resubmit_code'] = $resubmit_code;
?>

<!-- 進行驗證頁面 start -->
<?php if (isset($nexmo_status)) { ?>
<form class="narrow" action="<?= base_url("phone_vaild/phone_vaild_ing/");?>" method="post" accept-charset="utf-8" >
<h2 class="form-login-heading">手機驗證</h2>
<hr>
<?php echo "你已於 $date 要求進行手機認證，系統已寄發認證簡訊至門號：$phone 。申請認證者IP位置：$ip_address 。" ;?>
<hr>
<input type="text" class="input-block-level" placeholder="請輸入你的驗證碼(共6碼數字)" data-mask="999999"
name="input_phone" value="<?=$this->input->post('input_phone')?>">

<input class="btn btn-large btn-block" type="submit" id="submit" value="進行驗證" maxlength="6" onclick="this.form.submit();this.disabled=true;this.value='提交中...';"/>
<hr>
<small><a href="<?= base_url("user/settings");?>">回到設定頁</a> | <a href="#tips_post" data-toggle="modal">需要協助驗證嗎？</a></small>

<?php //因為有開啟CSRF
$CI =& get_instance();
$csrf_name = $CI->security->get_csrf_token_name();
$csrf_value = $CI->security->get_csrf_hash();
?>
<input type="hidden" name="<?= $csrf_name ?>" value="<?= $csrf_value ?>" />
</form>
<?php }else {?>
<!-- 進行驗證頁面 end -->


<!-- 申請註冊碼頁面 start -->
<form class="narrow" action="<?= base_url("phone_vaild/phone_vaild_apply");?>" method="post" accept-charset="utf-8" >

<h2 class="form-login-heading">申請手機驗證碼</h2>
<hr>
<input type="text" class="input-block-level" placeholder="請輸入你的手機號碼"
name="input_phone" value="<?=$this->input->post('input_phone')?>" maxlength="10">

<input class="btn btn-large btn-block" type="submit" id="submit" value="申請驗證" onclick="this.form.submit();this.disabled=true;this.value='提交中...';" />
<hr>
<small><a href="<?= base_url("user/settings");?>">回到設定頁</a> | <a href="#tips_apply" data-toggle="modal">為何我需要進行手機驗證？</a></small>

<?php //因為有開啟CSRF
$CI =& get_instance();
$csrf_name = $CI->security->get_csrf_token_name();
$csrf_value = $CI->security->get_csrf_hash();
?>
<input type="hidden" name="<?= $csrf_name ?>" value="<?= $csrf_value ?>" />
<input type="hidden" name="resubmit_code" value="<?=$resubmit_code?>">

</form>

<?php } ?>
<!-- 申請註冊碼頁面 end -->
</div> <!-- /container -->

<!-- Modal -->
<div id="tips_post" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      <h3 id="myModalLabel">無法順利進行驗證嗎？</h3>
   </div>
   <div class="modal-body">
      <h4>其它驗證方式：</h4>
      <p>若無法順利進行驗證，請來信告知，將有專人與你進行驗證，謝謝！</p>
      <p><a href="#contact" data-toggle="modal">聯絡 Citytasker</a></p>
   </div>
</div>

<!-- Modal -->
<div id="tips_apply" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      <h3 id="myModalLabel">為何我需要進行手機驗證？</h3>
   </div>
   <div class="modal-body">
      <h4>確認你的帳號身份</h4>
      <p>為保障你的 Citytasker 帳號權益，及避免日後相關糾紛，任何 Citytasker 有效會員均需通過手機驗證，且通過手機驗證才能發布或出任務。</p>
      <br/>
      <h4>驗證簡訊是免費的</h4>
      <p>手機驗證是<strong>免費</strong>的，Citytasker 不會收取接收通知的費用。<!--如果是在美國或加拿大，則會採用行動服務供應商的標準費率來接收簡訊。
      如果有任何關於費率的問題，請與的服務供應商聯絡以取得詳細資訊，謝謝！--></p>
      <p>有任何問題也歡迎<a href="mailto:info@citytasker.tw">寄信給 Citytasker</a></p>
   </div>
</div>

<?php require('template/_copyright.php'); ?>
<script type="text/javascript">
// submit送出後失效
  function dosubmit(){
   var input = document.getElementById("submit");
   input.disabled = 'disabled';
   return true;
  }

</script>
<?php require('template/_footer.php'); ?>
