<?php require('template/_header.php') ?>
<?php require('template/_navbar.php') ?>

<style>
	.alert{
		margin-bottom:20px;
	}
@media (min-width: 1313px) {
	#mobi{
		display:none;
	}
}
@media (max-width: 1313px) {
	#dest{
		display:none;
	}
}
@media (min-width: 767px) {
	#sideContainer img{
		width:65px;
		height:65px;
	}
}
@media (max-width: 767px) {
	#sideMenu{
		position:fixed;
		width:100%;
		top:0;
		left:0;
	}
	.media .pull-left {
		float:left;
	}
	.media .pull-right {
		float:right;
	}
	#sideContainer{

	}
	#sideContainer img{
		width:45px;
		height:45px;
	}
}
</style>

<div id="generateLink"></div>
<div id="mapContainer"></div>
<div id="sideMenu">
<?php require('template/_flashdata_show.php'); ?>
	<div style="height:45px; background:#eee; padding:9px;">
   	<div class="btn-group">
      <button class="btn dropdown-toggle btn-large" data-toggle="dropdown" href="#">
          	<?= $here ;?>
      		<span class="caret"></span>
   	   </button>
	      <ul class="dropdown-menu">
          <?php foreach ($other_city as $key => $value): ?>
            <li><a tabindex="-1" href="<?= base_url("$value")?>"> <?= $key;?></a></li>
          <?php endforeach ?>
          	<li><a tabindex="-1" href="<?= base_url('task/map/online') ?>">線上任務</a></li>
            <li><a tabindex="-1" href="<?= base_url("petition"); ?>">外縣市</a></li>
			</ul>
      </div>
      <div id="dest" class="btn-group">
         <a class="btn btn-large <?php if ($sort_type == 'newest')echo 'disabled'; ?>" href="<?= base_url("task/map/$area/newest") ?>">最新任務</a>
         <a class="btn btn-large <?php if ($sort_type == 'price')echo 'disabled';?> " href="<?= base_url("task/map/$area/price") ?>">高報酬</a>
         <a class="btn btn-large <?php if ($sort_type == 'deadline')echo 'disabled';?> " href="<?= base_url("task/map/$area/deadline") ?>">即將截止</a>
      </div>
      <div id="mobi" class="btn-group">
         <button class="btn dropdown-toggle btn-large" data-toggle="dropdown" href="#">
          	排序
      		<span class="caret"></span>
   	   </button>
	      <ul class="dropdown-menu">
            <li><a tabindex="-1" href="<?= base_url("task/map/$area/newest") ?>">最新任務</a></li>
            <li><a tabindex="-1" href="<?= base_url("task/map/$area/price") ?>">高報酬</a></li>
				<li><a tabindex="-1" href="<?= base_url("task/map/$area/deadline") ?>">即將截止</a></li>
			</ul>
      </div>
      <div class="btn-group pull-right">
        <!--<a class="btn btn-large <?php if ($sort_type == 'online')echo 'disabled';?> " href="<?= base_url('task/map/online') ?>">線上任務</a>-->
         <button class="btn dropdown-toggle btn-large" data-toggle="dropdown" href="#">
          	分類
      		<span class="caret"></span>
   	   </button>
	      <ul class="dropdown-menu">
          <li><a tabindex="-1" href="<?= base_url("task/map/$area/catalog/1") ?>">活動展覽</a></li>
          <li><a tabindex="-1" href="<?= base_url("task/map/$area/catalog/2") ?>">百貨賣場</a></li>
          <li><a tabindex="-1" href="<?= base_url("task/map/$area/catalog/3") ?>">研究調查</a></li>
          <li><a tabindex="-1" href="<?= base_url("task/map/$area/catalog/4") ?>">影音支援</a></li>
          <li><a tabindex="-1" href="<?= base_url("task/map/$area/catalog/5") ?>">辦公室類</a></li>
          <li><a tabindex="-1" href="<?= base_url("task/map/$area/catalog/12") ?>">其他任務</a></li>
            <li class="divider"></li>
            <li class="disabled"><a tabindex="-1" href="#">電腦網路</a></li>
            <li class="disabled"><a tabindex="-1" href="#">交通運輸</a></li>
            <li class="disabled"><a tabindex="-1" href="#">美食外送</a></li>
            <li class="disabled"><a tabindex="-1" href="#">代買代排</a></li>
      	</ul>
      </div>
   </div>
	<ul id="sideContainer"></ul>
</div>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
var Demo = {
  map: null,
  mapContainer: document.getElementById('mapContainer'),
  sideContainer: document.getElementById('sideContainer'),
  generateLink: document.getElementById('generateLink'),
  numMarkers: <?= $numMarkers; ?>,
  markers: [],
  visibleInfoWindow: null,

  generateTriggerCallback: function(object, eventType) {
    return function() {
      google.maps.event.trigger(object, eventType);
    };
  },

  openInfoWindow: function(infoWindow, marker) {
    return function() {
      // Close the last selected marker before opening this one.
      if (Demo.visibleInfoWindow) {
        Demo.visibleInfoWindow.close();
      }

      infoWindow.open(Demo.map, marker);
      Demo.visibleInfoWindow = infoWindow;
    };
  },

  clearMarkers: function() {
    for (var n = 0, marker; marker = Demo.markers[n]; n++) {
      marker.setVisible(false);
    }
  },

  generateRandomMarkers: function(center) {
    // Populate side bar.
    var avg = {
      lat: 0,
      lng: 0
    };

	 var navbar = 68;

	 var div = Demo.mapContainer;
	 div.style.height = window.innerHeight - navbar + 'px';

	 // Clear list and map markers.
	 div.innerHTML = '';
    Demo.clearMarkers();

    var ul = Demo.sideContainer;
    ul.style.width = 100 + '%';
    ul.style.height = window.innerHeight - navbar - 65 + 'px';

    // Clear list and map markers.
    ul.innerHTML = '';
    Demo.clearMarkers();

	<?php
if ( isset($task_results)) {

  // FIXME:此處與welcome_page相同 要重構
  foreach ($task_results as $task):
        switch ($task->status){
          case 0:
            $status_text = "報名";
				    $btn_status = "btn-inverse";
            break;

          case 1:
            $status_text = "報名";
				    $btn_status = "btn-warning";
            break;

          case 2:
            $status_text = "截止";
				    $btn_status = "btn-inverse disabled";
            break;

          case 4:
            $status_text = "截止";
            $btn_status = "btn-inverse disabled";
            break;

          default:
            $status_text = "default任務狀態";
				    $btn_status = "btn-danger";
            break; }
  ?>

  <?php if ($task->lat_lng !== null): ?>
      //有經緯度的，準備進行插點
      var html = 'Opening marker #' + '<?= $task->id ?>';
      var randLatLng = new google.maps.LatLng(<?= $task->lat_lng ?>);
      var image = '<?= base_url("img/web/task.png"); ?>';

      var marker = new google.maps.Marker({
        map: Demo.map,
        position: randLatLng,
        draggable: false,
        icon: image
      });
      Demo.markers.push(marker);

      // Create marker info window.
      var infoWindow = new google.maps.InfoWindow({
        content: [
       '<div class="media">',
       '<div class="media-body">',
       '<h3 class="pull-right bounty img-rounded"><small><?php if($task->payment == 0){echo '現金';} else {echo '匯款';}?></small> $',
		 '<?= $task->price ?>', '</h3>','<h5><strong><a class="media-heading" href="<?= base_url('task/')."/".$task->id;?>">',
		 '<?= $task->title ?>','</a></strong></h5>','<p><i class="icon-calendar"></i> 報名截止 ','<?= $task->deadline ?>','</p>',
       '<p>','<?= escape_php_to_js($task->content) ;?>','</p>',
          '</div></div>'
        ].join(''),
        size: new google.maps.Size(200, 80)
      });

      // Add marker click event listener.
      google.maps.event.addListener(
        marker, 'click', Demo.openInfoWindow(infoWindow, marker));
      <?php endif; ?>

      <?php $img = base_url("uploads/head_img/$task->img")?>
      // Generate list elements for each marker.
      var li = document.createElement('li');
      var aSel = document.createElement('div');
      aSel.href = 'javascript:void(0);';
      aSel.innerHTML =
      '<div class="media">' +
      '<a class="pull-left" href="<?= base_url("user/$task->member_id");?>">' +
      '<img class="media-object img-rounded" src="<?= $img ?>" />' +
      '</a><h3 id="dest" class="pull-right">' +
        '<small><?php if($task->payment == 0){echo '現金';} else {echo '匯款';}?></small> $' + '<?= $task->price ?>' +
        ' <a class="btn <?=$btn_status;?>" href="<?= base_url("task/$task->id");?>">' + '<?= $status_text ?>' + '</a></h3>' +
		  '<h4 id="mobi" class="pull-right">' +
        '<small><?php if($task->payment == 0){echo '現金';} else {echo '匯款';}?></small> $' + '<?= $task->price ?>' +
        ' <br/><a class="btn <?=$btn_status;?> pull-right" href="<?= base_url("task/$task->id");?>">' + '<?= $status_text ?>' + '</a></h4>' +
      '<div class="media-body">' +
        '<h5 class="media-heading" style="overflow:hidden; height:20px;"><strong><a href="<?= base_url('task/')?>/' + '<?= $task->id ?>' + '">' + '<?= $task->title ?>' +'</a></strong></h5>' +
        '<small><i class="icon-calendar"></i> 報名截止 ' + '<?= $task->deadline ?>' + '<br/>' +
        <?php if($task->lat_lng !== null){
            $task->address = address_format1($task->address);
            echo "'<i class=\"icon-map-marker\"></i> ' + '$task->address' ";
        }else{
            echo "'<i class=\"icon-globe\"></i> 線上或手機'";
        }?> +'</small></div></div>';

      <?php if ($task->lat_lng !== null): ?>
          aSel.onclick = Demo.generateTriggerCallback(marker, 'click');
      <?php endif?>

      li.appendChild(aSel);
      ul.appendChild(li);

    <?php endforeach ?>

<?php }else{
      // echo "ul.innerHTML = '尚無任務'";
      // echo ("var li = document.createElement('li')");
      // echo ("var aSel = document.createElement('div')");
      // // echo ("aSel.href = 'javascript:void(0)'");
      // echo ("aSel.innerHTML = 'hi'");
      // echo ("li.appendChild(aSel)");

} ?>
    // Center map.
    Demo.map.setCenter(new google.maps.LatLng(<?= $latlng ;?>));
  },

  init: function() {
    //var firstLatLng = new google.maps.LatLng(37.4419, -122.1419);
    Demo.map = new google.maps.Map(Demo.mapContainer, {
      zoom: 12,
      //center: firstLatLng,
		mapTypeControl: false,
		streetViewControl: false,
		mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    // Show generate link only after map tiles finish loading.
    google.maps.event.addListener(Demo.map, 'tilesloaded', function() {
      Demo.generateLink.style.display = 'block';
    });

    // Add onclick handler to generate link.
    google.maps.event.addDomListener(Demo.generateLink, 'click', function() {
      Demo.generateRandomMarkers(Demo.map.getCenter());
    });

    // Generate markers against map center.
    google.maps.event.trigger(Demo.generateLink, 'click');
  }
};

google.maps.event.addDomListener(window, 'load', Demo.init, Demo);
</script>


<?php require('template/_footer.php'); ?>