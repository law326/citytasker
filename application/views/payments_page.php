<?php require('template/_header.php'); ?>
<?php require('template/_navbar.php'); ?>

<div id="container" class="container">
   <div class="row-fluid">
      <div class="span8">
      <?php require('template/_flashdata_show.php'); ?>
         <div id="header" class"row-fluid">
            <div class="page-header span12">
               <h1>交易紀錄</h1>
            </div>
         </div>
         <table class="table table-striped table-bordered" style="font-size:14px;">
            <thead>
               <tr>
						<th>日期</th>
                  <th>任務</th>
                  <th>類型</th>
                  <th>結算</th>
                  <!--<th>服務費</th>-->
               </tr>
            </thead>
            <tbody>
            	<?php
					$date = Array("3/4","3/12","3/14","3/24","3/27");
					$task = Array("手機或網路手機或網123234235路手機或網路手機或網路手機或網路","手機或網路手機或網路","手機或網路手機或網路手機或網路手機或網路","手機或網路手機或網路手機或網路","手機或網路手機或網路手機或網路");
					$task_id = Array("123456","123456","123456","123456","123456");
					$status = Array("0","1","1","0","1");
					$money = Array("12","345","555","20","38");
					
					for ($i=0;$i<=4;$i++){
					?>
               <tr>
                  <td><?= $date[$i]; ?></td>
                  <td class="span6"><a href="<?= $task_id[$i]; ?>"><?= $task[$i]; ?></a></td>
                  <td><?php if ($status[$i]==1){ ?><code>收入</code><?php }else{ ?><code>支出</code><?php } ?></td>
                  <td><?= $money[$i]; ?></td>
                  <!--<td><?php if ($status[$i]==1){ echo ceil($money[$i]*0.15); }else{ echo "-"; } ?></td>-->
               </tr>
               <?php } ?>
            </tbody>
         </table>
      </div>
      <div class="span4">
         <?php require('template/_setting_menu.php'); ?>
      </div>
   </div>
</div>

<?php require('template/_copyright.php'); ?>
<?php require('template/_footer.php'); ?>
