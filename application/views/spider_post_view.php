<?php require('template/_header.php'); ?>
<style>
#map{
	width:100%;
	height:250px;
	margin-top:0px;
}
@media (max-width: 767px) {
	.row-fluid .span2 {
		display: block;
		width: 31.914893617021278%;
  		*width: 31.861702127659576%;
		float: left;
		margin-left: 2.127659574468085%;
		*margin-left: 2.074468085106383%;
		margin-bottom:5px;
	}
	.row-fluid .id4{
		margin-left:0;
	}
}
.btn-catalog, .btn-click{
    border: 4px solid #75B9F0;
}
</style>

<div id="container" class="container">
	<div class="row-fluid">
      <div class="span8 offset2">
	      <div id="header">
   		   <div class="page-header">
				   <?php require('template/_flashdata_show.php'); ?>
					<h1>由<?= $dummy_id . $dummy_name;?>身分發布任務</h1>

          <?php
          if (is_logged_in() == FALSE){
            echo "<div class='alert alert-error after'>";
            echo "請先登入才可發布任務喔！";
            echo "</div>";
          }
          ?>
				</div>
			</div><!-- end header -->
         <form action="<?= base_url("spider/post_ing") ?>" method="post" accept-charset="utf-8" >
          <input type="hidden" name="member_id" id="member_id" value="<?= $dummy_id?>" />

            <div class="row-fluid">
               <div class="span12" data-toggle="buttons-radio">
                  <h4>任務種類</h4>
                  <input type="hidden" name="catalog_hidden" id="status" value="" />
                  <div id="jobMenu" class="row-fluid">
							<button type="button" class="btn span2" id="1"><img src="<?= base_url("img/web/job/event.png"); ?>"><strong>活動展覽</strong></button>
                     <button type="button" class="btn span2" id="2"><img src="<?= base_url("img/web/job/shopping.png"); ?>"><strong>百貨賣場</strong></button>
                     <button type="button" class="btn span2" id="3"><img src="<?= base_url("img/web/job/research.png"); ?>"><strong>研究調查</strong></button>
                     <button type="button" class="btn span2 id4" id="4"><img src="<?= base_url("img/web/job/show.png"); ?>"><strong>影音支援</strong></button>
                     <button type="button" class="btn span2" id="5"><img src="<?= base_url("img/web/job/office.png"); ?>"><strong>辦公室類</strong></button>
                     <button type="button" class="btn span2" id="12"><img src="<?= base_url("img/web/job/other.png"); ?>"><strong>其他任務</strong></button>
                  </div>
               </div>
	         </div>
         	<hr/>
            <div class="row-fluid">
               <h4>任務標題 <small></small></h4>
               <input type="text" class="input-block-level" maxlength="30" placeholder="EX: 幫我清理房間" name="title" value="<?= html_entity_decode(set_value('title'))?>">
               <br/>
               <h4>任務內容</h4>
               <textarea rows="20" id='ct1' class="span12" placeholder=""name="content" >
<?= html_entity_decode(set_value('content',@$task_result->message)); ?></textarea>
               <br/>
               <!--
               <h4>私密內容</h4>
               <textarea rows="3" class="span12" name="content_private" ><?= html_entity_decode(set_value('content_private', @$task_result->private_content))?></textarea>
               <span class="help-block"><i class="icon-lock"></i> <small>私密內容只有任務執行者可以看見</small></span>
               <hr/>
               -->
               <h4>聯絡資訊</h4>
               <input  type="text" class="input-block-level" placeholder="email"
                name="email" value="<?= $task_result->email; ?>">
               <input  type="text" class="input-block-level" placeholder="電話、手機 (選填)"
                name="phone" value="<?= $task_result->phone; ?>">
               <h4>任務位置</h4>
               <label class="radio inline">
                  <input type="radio" id="optionsRadios1" value="0" name="onlinetask" <?= set_radio('onlinetask', '0')?> <? if ($task_result->onlinetask == 0) echo "checked"; ?> >必須到指定位置完成任務
                                 </label>
                 <label class="radio inline">
                  <input type="radio" id="optionsRadios2" value="1" name="onlinetask" <?= set_radio('onlinetask', '1')?> <? if ($task_result->onlinetask == 1) echo "checked"; ?> >線上或手機即可完成任務
               </label>
               <br/><br/>
               <input  type="text" class="input-block-level" placeholder="EX: 台北市南港區研究院路二段..."
               id="addresspicker_map" name="address" value="<?= html_entity_decode(set_value('address')); ?>">
               <span class="help-block">
                  <small><strong>提示：</strong>不知道詳細地址時可透過地圖直接拖曳轉換出地址</small>
               </span>
               <input type="hidden" id="temp_address" />
               <div id="map"></div>

               <div id="address_error"></div>
<div style="display:none;">
               <label>Locality: </label>
               <input id="locality" disabled=disabled>
               <br/>
               <label>District: </label>
               <input id="administrative_area_level_2" disabled=disabled>
               <br/>
               <label>Country: </label>
               <input id="country" disabled=disabled>
               <br/>
               <label>Postal Code: </label>
               <input id="postal_code" disabled=disabled>
               <br/>
               <?php
               if (isset($task_result->lat_lng)) {
                 $lat = strstr($task_result->lat_lng, ',',TRUE);
                 $lng = str_replace(',', '', strstr($task_result->lat_lng, ','));
               }
               ?>
               <label>Lat: </label>
               <input id="lat" name="lat" value="<?= html_entity_decode(set_value('lat', @$lat))?>">
               <br/>
               <label>Lng: </label>
               <input id="lng" name="lng" value="<?= html_entity_decode(set_value('lng', @$lng))?>">
               <br/>
               <label>是否點選: </label>
               <input id="geoclick" name="geoclick" value="<?= html_entity_decode(set_value('geoclick')); ?> <? if (isset($task_result->address)) echo "yes"; ?>  ">
</div>
            </div>
            <hr/>
            <h4>報名截止</h4>
            <div class="row-fluid">
               <select class="span6" name="date">
                  <option value="">日期</option>
                  <option value="<?=date("Y-m-d",time())?>" <?php echo set_select('date', date("Y-m-d",time())); ?>>今天</option>
                  <?php
                  $weekarray=array("日","一","二","三","四","五","六");

                  if (isset($task_result->deadline)) { //複製或修改任務時
                    $selectDay = strtotime($task_result->deadline);
                    echo "<option value='" . date("Y-m-d",$selectDay) . "'" . set_select('date', date("Y-m-d",$selectDay)) . "selected >";
                    echo date("m / d" , $selectDay )." (星期".$weekarray[date("w",$selectDay)].")</option>";
                  }

                  // option的value要設定成日期格式
                  for ($i = 1; $i <= 30; $i++) {
                     $selectDay = mktime(0,0,0,date("m"),date("d")+ $i,date("Y"));
                     echo "<option value='" . date("Y-m-d",$selectDay) . "'" . set_select('date', date("Y-m-d",$selectDay)) . ">";
                     echo date("m / d" , $selectDay )." (星期".$weekarray[date("w",$selectDay)].")</option>";
                  }

                  ?>
               </select>
               <select class="span6" name="time">
                  <option value="">時間</option>
                  <?php
                  if (isset($task_result->deadline)) { //複製或修改任務時
                    $time_value = date("H:i",strtotime($task_result->deadline));
                    echo "<option value=$time_value" . set_select('time', $time_value) ." selected>" . $time_value. "</option>";
                  }

                    for ($i = 0; $i <= 23; $i++) {
                      $time_value = str_pad($i,2,'0',STR_PAD_LEFT) .':00';
                      echo "<option value=$time_value" . set_select('time', $time_value) .">" . $time_value. "</option>";
                    }
                  ?>
               </select>
            </div>
            <span class="help-block">
               <small><strong>提醒：</strong>時間截止之後，任務的報名功能會自動關閉</small>
            </span>
            <hr/>
           <!--  <h4>需求人數</h4>
            <div class="row-fluid">
               <div class="input-append">
                  <input id="reward" type="text" name="people" maxlength="2" value="1" style="width:100px;"
                  value="<?= html_entity_decode(set_value('people', @$task_result->people)); ?>" />
                  <span id="nt" class="add-on">人</span>
               </div>
               <span class="help-inline">
                  <small><strong>提醒：</strong>實際雇用人數只要小於或等於這個數字即可，所以建議若是有很多人<br/>數需求的狀況時可以估高一些</small>
               </span>
            </div>
            <hr/> -->
            <h4>任務酬勞</h4>
            <label class="radio inline">
               <input type="radio" id="optionsPayment1" value="0" name="payment" <?= set_radio('payment', '0'); ?> <? if ($task_result->payment == 0) echo "checked"; ?> />現金
            </label>
            <label class="radio inline">
               <input type="radio" id="optionsPayment2" value="1" name="payment" <?= set_radio('payment', '1'); ?> <? if ($task_result->payment == 1) echo "checked"; ?> />匯款
            </label>
            <br/><br/>
            <div class="row-fluid">
               <div class="input-prepend">
                  <span id="nt" class="add-on">NT</span>
                  <input id="reward" type="text" name="price" style="width:100px;" value="<?= html_entity_decode(set_value('price', $task_result->price))?>">
               </div>
               <span class="help-inline">
                  <small>
                     <strong>提醒：</strong>請將時薪與時數換算成總金額，金額大將越吸引關注<!--任務酬勞並不包含執行任務時所需之額外花費--><br/>
                     <a href="#tips" data-toggle="modal">出價參考</a>
                  </small>
               </span>
            </div>
            <hr/>
            <?php if (is_logged_in() == TRUE) { ?>
            	<input class="btn btn-large" type="submit" name="submit" value="發布" />
            <?php }else{ ?>
               <a class="btn btn-large" href="#user_info" data-toggle="modal">請先登入再發布</a>
            <?php } ?>
            <span class="help-inline">
            	<small>
                  *按下發布後，即表示你同意遵守<a href="<?= base_url("service/terms"); ?>" target="_blank">使用條款</a>
               </small>
            </span>

            <?php //因為有開啟CSRF
            $CI =& get_instance();
            $csrf_name = $CI->security->get_csrf_token_name();
            $csrf_value = $CI->security->get_csrf_hash();
            ?>
            <input type="hidden" name="<?= $csrf_name ?>" value="<?= $csrf_value ?>" />
            <input type="hidden" name="former_URL" value="<?= $this->uri->uri_string();?>" />

         </form>
		</div>
	</div>
</div>
<!-- 地址 -->
<script src="http://maps.google.com/maps/api/js?sensor=false&region=zh-TW&language=zh-TW"></script>
<script src="<?= base_url("js/addresspicker/jquery.ui.addresspicker.js"); ?>"></script>
<script>
$(function() {
  var addresspicker = $( "#addresspicker" ).addresspicker();
  var addresspickerMap = $( "#addresspicker_map" ).addresspicker({
    regionBias: "fr",
  updateCallback: showCallback,
    elements: {
     map:      "#map",
     lat:      "#lat",
     lng:      "#lng",
     street_number: '#street_number',
     route: '#route',
     locality: '#locality',
     administrative_area_level_2: '#administrative_area_level_2',
     administrative_area_level_1: '#administrative_area_level_1',
     country:  '#country',
     postal_code: '#postal_code',
    type:    '#type'
    }
  });

  var gmarker = addresspickerMap.addresspicker( "marker");
  gmarker.setVisible(true);
  addresspickerMap.addresspicker( "updatePosition");

 $('#reverseGeocode').change(function(){
  $("#addresspicker_map").addresspicker("option", "reverseGeocode", ($(this).val() === 'true'));
 });

 function showCallback(geocodeResult, parsedGeocodeResult){
      $('#callback_result').text(JSON.stringify(parsedGeocodeResult, null, 4));
      $('#geoclick').attr("value", "yes");
      $("#address_error").html("");
   }
<!-- 地址 End -->

// 紀錄點選的任務總類
$('div.span12 button').click(function() {
    $("#status").attr('value', $(this).attr('id'));
});

// 任務總類 btn變色
$('.span2').hover(
  function() {
    $( this ).addClass("btn-catalog");
  }, function() {
    $( this ).removeClass("btn-catalog");
  }
)
.click(function () {
  $( this ).addClass("btn-click").siblings().removeClass('btn-click');
});

$('#addresspicker_map').focus(function() {
  $('#geoclick').attr("value","");
})
  .blur(function() {
    if ($('#geoclick').val() !== "yes") {
      $(this).val("");
      $("#address_error").html("<p>輸入地址後，請由下拉選單中挑選正確地址!若還是沒有正確選項，請再將地址描述完整些。</p>");
    } else {
      $("#address_error").html("");
    };
  });

//線上任務
$('#optionsRadios2').click(function() {
    $('#temp_address').val($('#addresspicker_map').val());
    disableMap();
});
// 實體任務
$('#optionsRadios1').click(function() {
    $("#addresspicker_map").removeAttr("readonly")
                           .val($('#temp_address').val())
                           .slideDown();
    $('#map').slideDown();
});
if ($('#optionsRadios2:checked').val() == 1) {
  disableMap();
}

function disableMap () {
    $("#addresspicker_map").attr("readonly","readonly")
                           .val("")
                           .slideUp();
    $('#map').slideUp();
}

<?php if (is_logged_in() == FALSE) { ?>
$('input, textarea, button, select').attr('disabled', 'disabled');
$('input, textarea, button, select').attr('title', '請先登入才可發布任務喔！');
<?php } ;?>

});
</script>