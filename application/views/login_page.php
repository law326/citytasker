<?php require('template/_header.php'); ?>
<?php require('template/_navbar.php'); ?>
<style>
@media (min-width: 767px) {
	#mobi{
		display:none;
	}
}
@media (max-width: 767px) {
	#dest{
		display:none;
	}
}
</style>

<div id="mobi">
	<?php require('template/_login.php'); ?>
</div>

<div id="dest" class="container">
<?php require('template/_flashdata_show.php'); ?>

   <form class="narrow" action="<?= base_url("user/login_ing");?>" method="post" accept-charset="utf-8">
      <h2 class="form-login-heading">登入<span id="mobi"> / <a href="<?= base_url("user/signup"); ?>">註冊</a></span></h2>
      <input type="text" class="input-block-level" placeholder="Email"
      name="email" value="<?=$this->input->post('email')?>">
      <input type="password" class="input-block-level" placeholder="密碼"
      name="password" >
      <label class="checkbox">
      	<input type="checkbox" value="remember-me"> 記住密碼 |
         <a href="#tips" data-toggle="modal">忘記密碼？</a>
      </label>
      <hr>
      <input class="btn btn-large btn-block" type="submit" name="name" value="登入" onclick="this.disabled=true;this.value='提交中...';this.form.submit();" />
      <img id="facebook" src="<?= base_url("img/web/facebook_login.png")?>" style="cursor:pointer"/>
		<?php //因為有開啟CSRF
      $CI =& get_instance();
      $csrf_name = $CI->security->get_csrf_token_name();
      $csrf_value = $CI->security->get_csrf_hash();
      ?>
      <input type="hidden" name="<?= $csrf_name ?>" value="<?= $csrf_value ?>" />

	</form>

</div>
<!-- /container -->

<!-- Modal -->
<form action="<?= base_url("user/forgot_password");?>" method="post" accept-charset="utf-8" onsubmit="submit.disabled=1">
	<div id="tips" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
         <h3 id="myModalLabel">忘記密碼</h3>
      </div>
      <div class="modal-body">
         <input type="text" name="email_forgot" class="input-block-level" placeholder="Email">
         <p>填入申請會員時的Email，我們將會寄送一組新密碼給你！</p>
      </div>
      <div class="modal-footer">
         <input class="btn btn-large" type="submit" name="name" value="寄送"
         onclick="this.disabled=true;this.value='提交中...';this.form.submit();"  />
         <?php //因為有開啟CSRF
         $CI =& get_instance();
         $csrf_name = $CI->security->get_csrf_token_name();
         $csrf_value = $CI->security->get_csrf_hash();
         ?>
         <input type="hidden" name="<?= $csrf_name ?>" value="<?= $csrf_value ?>" />
	   </div>
	</div>
</form>
<!-- Modal -->
   <script type="text/javascript">
  window.fbAsyncInit = function() {
     //Initiallize the facebook using the facebook javascript sdk
  FB.init({
    appId: '<?php echo $this->config->item('appID'); ?>', // App ID
      cookie:true, // enable cookies to allow the server to access the session
     status:true, // check login status
      xfbml:true, // parse XFBML
      oauth : true //enable Oauth
     });
   };
   //Read the baseurl from the config.php file
   (function(d){
           var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
           if (d.getElementById(id)) {return;}
           js = d.createElement('script'); js.id = id; js.async = true;
           js.src = "//connect.facebook.net/zh_TW/all.js";
           ref.parentNode.insertBefore(js, ref);
         }(document));
   //Onclick for fb login
 $('#facebook').click(function(e) {
    FB.login(function(response) {
     if(response.authResponse) {
        parent.location ='<?= base_url('fbci/fblogin'); ?>';
      }else{
         alert('請安心授權給Citytasker，我們保證不會在非經你的同意下回傳訊息至Facebook');
      }
 },{scope: 'email,read_stream,publish_stream,user_birthday,user_location,user_work_history,user_hometown,user_photos,read_friendlists,user_activities,user_likes'}); //permissions
});
   </script>
<?php require('template/_copyright.php'); ?>
<?php require('template/_footer.php'); ?>