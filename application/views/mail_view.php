<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>無標題文件</title>
</head>

<body>
<!-- 新註冊, 註冊人接收 OK -->
<table>
<div style="padding:20px 0px; background:url(<?= base_url("img/web/bg.png"); ?>)">
   <div style="
   max-width: 480px;
   padding: 19px 29px;
   margin: 50px auto;
   background:#fff;
   border: 1px solid #e5e5e5;
   -webkit-border-radius: 5px;
     -moz-border-radius: 5px;
       border-radius: 5px;
   -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
     -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
       box-shadow: 0 1px 2px rgba(0,0,0,.05);
   ">
      <div style="padding-bottom:10px; border-bottom:#eee 1px solid;"><img src="<?= base_url("img/web/logo.png"); ?>" /></div>
      <p>Hi John 歡迎加入Citytasker！</p>
      <p>只需要按下<a href="#">驗證郵件</a>之後註冊程序就完成了。</p>
      <br />
      <p>本信件由系統產生，請勿回信。</p>
      <p>©2013 Citytasker Inc.</p>
      </div>
   </div>
</div>
</table>

<br/>

<!-- 新註冊, 註冊人接收 OK -->
<div style="padding:20px 0px; background:url(<?= base_url("img/web/bg.png"); ?>)">
   <div style="
   max-width: 480px;
   padding: 19px 29px;
   margin: 50px auto;
   background:#fff;
   border: 1px solid #e5e5e5;
   -webkit-border-radius: 5px;
     -moz-border-radius: 5px;
       border-radius: 5px;
   -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
     -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
       box-shadow: 0 1px 2px rgba(0,0,0,.05);
   ">
      <div style="padding-bottom:10px; border-bottom:#eee 1px solid;"><img src="<?= base_url("img/web/logo.png"); ?>" /></div>
      <p>Hi John 歡迎加入Citytasker！</p>
      <p>只需要按下<a href="#">驗證郵件</a>之後註冊程序就完成了。</p>
      <br />
      <p>本信件由系統產生，請勿回信。</p>
      <p>©2013 Citytasker Inc.</p>
      </div>
   </div>
</div>

<!-- 密碼遺忘, 使用者接收 OK -->
<div style="padding:20px 0px; background:url(<?= base_url("img/web/bg.png"); ?>)">
   <div style="
   max-width: 480px;
   padding: 19px 29px;
   margin: 50px auto;
   background:#fff;
   border: 1px solid #e5e5e5;
   -webkit-border-radius: 5px;
     -moz-border-radius: 5px;
       border-radius: 5px;
   -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
     -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
       box-shadow: 0 1px 2px rgba(0,0,0,.05);
   ">
      <div style="padding-bottom:10px; border-bottom:#eee 1px solid;"><img src="<?= base_url("img/web/logo.png"); ?>" /></div>
      <p>Hi John 瞧瞧我們幫你找到什麼！</p>
      <p>你註冊的密碼是 "123456" 。</p>
      <p>趕快來試看看吧！ <a href="#">登入</a></p>
      <br />
      <p>本信件由系統產生，請勿回信。</p>
      <p>©2013 Citytasker Inc.</p>
      </div>
   </div>
</div>
<br/>
<!-- 任務中留言, 跑者留言案主接收 案主留言所有留言者接收 -->
<div style="padding:20px 0px; background:url(<?= base_url("img/web/bg.png"); ?>)">
   <div style="
   max-width: 480px;
   padding: 19px 29px;
   margin: 50px auto;
   background:#fff;
   border: 1px solid #e5e5e5;
   -webkit-border-radius: 5px;
     -moz-border-radius: 5px;
       border-radius: 5px;
   -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
     -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
       box-shadow: 0 1px 2px rgba(0,0,0,.05);
   ">
      <div style="padding-bottom:10px; border-bottom:#eee 1px solid;"><img src="<?= base_url("img/web/logo.png"); ?>" /></div>
      <table>
      	<td style="padding-right: 5px;">
         	<img src="../../uploads/head_img/default_pic.png" style="
            width: 60px;
            -webkit-border-radius: 5px;
              -moz-border-radius: 5px;
                border-radius: 5px;
            -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
              -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
            " />
         </td>
         <td>
         	<p>JEJ 在任務 "<a href="#">我是傳奇演唱會我是傳奇演唱會我是傳奇演唱會我是傳</a>" 中留言：</p>
         </td>
      </table>
      <p style="padding-left: 70px;">我是留言我是留言我是留言我是留言我是留言我是留言我是留言我是留言我是留言我是留言</p>
      <p style="padding-left: 70px;">想參與討論嗎？ <a href="#">回復</a></p>
      <br />
      <p>本信件由系統產生，請勿回信。</p>
      <p>©2013 Citytasker Inc.</p>
      </div>
   </div>
</div>
<br />
<!-- 報名任務, 案主接收 OK-->
<div style="padding:20px 0px; background:url(<?= base_url("img/web/bg.png"); ?>)">
   <div style="
   max-width: 480px;
   padding: 19px 29px;
   margin: 50px auto;
   background:#fff;
   border: 1px solid #e5e5e5;
   -webkit-border-radius: 5px;
     -moz-border-radius: 5px;
       border-radius: 5px;
   -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
     -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
       box-shadow: 0 1px 2px rgba(0,0,0,.05);
   ">
      <div style="padding-bottom:10px; border-bottom:#eee 1px solid;"><img src="<?= base_url("img/web/logo.png"); ?>" /></div>
      <table>
      	<td style="padding-right: 5px;">
         	<img src="../../uploads/head_img/default_pic.png" style="
            width: 60px;
            -webkit-border-radius: 5px;
              -moz-border-radius: 5px;
                border-radius: 5px;
            -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
              -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
            " />
         </td>
         <td>
         	<p>Citytasker JEJ 已經報名任務：<br />"<a href="#">我是傳奇演唱會我是傳奇演唱會我是傳奇演唱會我是傳</a>"</p>
         </td>
      </table>
      <p style="padding-left: 70px;">是否要讓他出任務呢？</p>
      <br />
      <p>本信件由系統產生，請勿回信。</p>
      <p>©2013 Citytasker Inc.</p>
      </div>
   </div>
</div>
<br />
<!-- 雇用通知, 幫手接收 OK-->
<div style="padding:20px 0px; background:url(<?= base_url("img/web/bg.png"); ?>)">
   <div style="
   max-width: 480px;
   padding: 19px 29px;
   margin: 50px auto;
   background:#fff;
   border: 1px solid #e5e5e5;
   -webkit-border-radius: 5px;
     -moz-border-radius: 5px;
       border-radius: 5px;
   -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
     -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
       box-shadow: 0 1px 2px rgba(0,0,0,.05);
   ">
      <div style="padding-bottom:10px; border-bottom:#eee 1px solid;"><img src="<?= base_url("img/web/logo.png"); ?>" /></div>
      <table>
      	<td style="padding-right: 5px;">
         	<img src="../../uploads/head_img/default_pic.png" style="
            width: 60px;
            -webkit-border-radius: 5px;
              -moz-border-radius: 5px;
                border-radius: 5px;
            -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
              -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
            " />
         </td>
         <td>
         	<p>JEJ 確定僱用你出任務：<br />"<a href="#">我是傳奇演唱會我是傳奇演唱會我是傳奇演唱會我是傳</a>"</p>
         </td>
      </table>
      <p style="padding-left: 70px;">任務是有時間性的，請記得務必在時間內完成！</p>
      <br />
      <p>本信件由系統產生，請勿回信。</p>
      <p>©2013 Citytasker Inc.</p>
      </div>
   </div>
</div>
<br />
<!-- 任務到期(沒人完成), 案主接收 -->
<div style="padding:20px 0px; background:url(<?= base_url("img/web/bg.png"); ?>)">
   <div style="
   max-width: 480px;
   padding: 19px 29px;
   margin: 50px auto;
   background:#fff;
   border: 1px solid #e5e5e5;
   -webkit-border-radius: 5px;
     -moz-border-radius: 5px;
       border-radius: 5px;
   -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
     -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
       box-shadow: 0 1px 2px rgba(0,0,0,.05);
   ">
      <div style="padding-bottom:10px; border-bottom:#eee 1px solid;"><img src="<?= base_url("img/web/logo.png"); ?>" /></div>
     	<p>你發佈的任務 "<a href="#">我是傳奇演唱會我是傳奇演唱會我是傳奇演唱會我是傳</a>" 宣告失敗！</p>
      <p>一個成功完成的任務有以下因素：</p>
      <ul>
      	<li>增加的個人介紹，讓大家更認識你</li>
         <li>詳盡的任為內容，並且要詳細說明完成任務的條件</li>
         <li>任務報酬給的合理，畢竟時間就是金錢</li>
      </ul>
      <p>想重新發布任務嗎？ <a href="#">發佈</a></p>
      <br />
      <p>本信件由系統產生，請勿回信。</p>
      <p>©2013 Citytasker Inc.</p>
      </div>
   </div>
</div>
<br />
<!-- 任務已雇滿或任務到期(沒被指派到), 幫手接收 -->
<div style="padding:20px 0px; background:url(<?= base_url("img/web/bg.png"); ?>)">
   <div style="
   max-width: 480px;
   padding: 19px 29px;
   margin: 50px auto;
   background:#fff;
   border: 1px solid #e5e5e5;
   -webkit-border-radius: 5px;
     -moz-border-radius: 5px;
       border-radius: 5px;
   -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
     -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
       box-shadow: 0 1px 2px rgba(0,0,0,.05);
   ">
      <div style="padding-bottom:10px; border-bottom:#eee 1px solid;"><img src="<?= base_url("img/web/logo.png"); ?>" /></div>
     	<p>你沒被指派任務 "<a href="#">我是傳奇演唱會我是傳奇演唱會我是傳奇演唱會我是傳</a>" ！</p>
      <p>想成功出任務有以下因素：</p>
      <ul>
      	<li>增加的個人介紹，讓大家更認識你</li>
         <li>將使用者名稱設定為真實姓名，選用能看清楚真面目的照片</li>
         <li>累積更多任務經驗與評價</li>
         <li>任務可以挑選更符合自己的開始，不一定高薪就是好任務</li>
      </ul>
      <p>準備好再接再厲了嗎？ <a href="#">出任務</a></p>
      <br />
      <p>本信件由系統產生，請勿回信。</p>
      <p>©2013 Citytasker Inc.</p>
      </div>
   </div>
</div>
<br />
<!-- 任務到期(有人出任務而且還沒有得到筆評價), 幫手接收 -->
<div style="padding:20px 0px; background:url(<?= base_url("img/web/bg.png"); ?>)">
   <div style="
   max-width: 480px;
   padding: 19px 29px;
   margin: 50px auto;
   background:#fff;
   border: 1px solid #e5e5e5;
   -webkit-border-radius: 5px;
     -moz-border-radius: 5px;
       border-radius: 5px;
   -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
     -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
       box-shadow: 0 1px 2px rgba(0,0,0,.05);
   ">
      <div style="padding-bottom:10px; border-bottom:#eee 1px solid;"><img src="<?= base_url("img/web/logo.png"); ?>" /></div>
     	<p>今天是任務 "<a href="#">我是傳奇演唱會我是傳奇演唱會我是傳奇演唱會我是傳</a>" 的到期日，請記得把任務完成！</p>
      <p>祝你好運！</p>
      <br />
      <p>本信件由系統產生，請勿回信。</p>
      <p>©2013 Citytasker Inc.</p>
      </div>
   </div>
</div>
<br />
<!-- 任務到期(有人出任務而且還沒有得到筆評價), 案主接收 -->
<div style="padding:20px 0px; background:url(<?= base_url("img/web/bg.png"); ?>)">
   <div style="
   max-width: 480px;
   padding: 19px 29px;
   margin: 50px auto;
   background:#fff;
   border: 1px solid #e5e5e5;
   -webkit-border-radius: 5px;
     -moz-border-radius: 5px;
       border-radius: 5px;
   -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
     -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
       box-shadow: 0 1px 2px rgba(0,0,0,.05);
   ">
      <div style="padding-bottom:10px; border-bottom:#eee 1px solid;"><img src="<?= base_url("img/web/logo.png"); ?>" /></div>
     	<p>今天是任務 "<a href="#">我是傳奇演唱會我是傳奇演唱會我是傳奇演唱會我是傳</a>" 的到期日，提醒你當任務被完成後給幫手們評價是種鼓勵喔！</p>
      <p>祝你有個輕鬆的一天！</p>
      <br />
      <p>本信件由系統產生，請勿回信。</p>
      <p>©2013 Citytasker Inc.</p>
      </div>
   </div>
</div>
<br />
<!-- 提醒給予評價, 案主接收 -->
<div style="padding:20px 0px; background:url(<?= base_url("img/web/bg.png"); ?>)">
   <div style="
   max-width: 480px;
   padding: 19px 29px;
   margin: 50px auto;
   background:#fff;
   border: 1px solid #e5e5e5;
   -webkit-border-radius: 5px;
     -moz-border-radius: 5px;
       border-radius: 5px;
   -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
     -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
       box-shadow: 0 1px 2px rgba(0,0,0,.05);
   ">
      <div style="padding-bottom:10px; border-bottom:#eee 1px solid;"><img src="<?= base_url("img/web/logo.png"); ?>" /></div>
      <p>關於任務 "<a href="#">我是傳奇演唱會我是傳奇演唱會我是傳奇演唱會我是傳</a>" 你是否已經完成，需要來給予回報喔！</p>
      <p>現在就回報！ <a href="#">回報</a></p>
      <br />
      <p>本信件由系統產生，請勿回信。</p>
      <p>©2013 Citytasker Inc.</p>
      </div>
   </div>
</div>
<br />
<!-- 得到評價, 雙方接收 -->
<div style="padding:20px 0px; background:url(<?= base_url("img/web/bg.png"); ?>)">
   <div style="
   max-width: 480px;
   padding: 19px 29px;
   margin: 50px auto;
   background:#fff;
   border: 1px solid #e5e5e5;
   -webkit-border-radius: 5px;
     -moz-border-radius: 5px;
       border-radius: 5px;
   -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
     -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
       box-shadow: 0 1px 2px rgba(0,0,0,.05);
   ">
      <div style="padding-bottom:10px; border-bottom:#eee 1px solid;"><img src="<?= base_url("img/web/logo.png"); ?>" /></div>
      <table>
      	<td style="padding-right: 5px;">
         	<img src="../../uploads/head_img/default_pic.png" style="
            width: 60px;
            -webkit-border-radius: 5px;
              -moz-border-radius: 5px;
                border-radius: 5px;
            -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
              -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
            " />
         </td>
         <td>
         	<p>JEJ 已經給你評價，關於任務：<br />"<a href="#">我是傳奇演唱會我是傳奇演唱會我是傳奇演唱會我是傳</a>"</p>
         </td>
      </table>
      <p style="padding-left: 70px;">想了解他說些什麼嗎？ <a href="#">看評價</a></p>
      <br />
      <p>本信件由系統產生，請勿回信。</p>
      <p>©2013 Citytasker Inc.</p>
      </div>
   </div>
</div>
</body>
</html>
