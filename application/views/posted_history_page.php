<?php require('template/_header.php'); ?>
<?php require('template/_navbar.php'); ?>

<div id="container" class="container">
   <div class="row-fluid">
      <div class="span8">
      <?php require('template/_flashdata_show.php'); ?>
         <div id="header" class"row-fluid">
            <div class="page-header span12">
               <h1>發案紀錄</h1>
            </div>
         </div>
<? if (isset($posted_results)) { ?>
         <?php foreach ($posted_results as $task): //所有任務記錄?>

				<table class="table table-striped table-bordered" style="font-size:14px;">
            	<thead>
               	<tr>
                  	<th>
                     	  <a href="<?= base_url("task/$task->id");?>"><?= $task->title; ?></a>
                          <small style="font-weight:normal;">(完成期限：<?= $task->deadline;?>)</small>
                                    </small>
                        <? if ($task->status == 0): ?>
                          <span class="pull-right"><a href="#" onclick="change_deadline<?= $task->id;?>()">提前截止</a></span>
                          <script type="text/javascript">
                          function change_deadline<?= $task->id;?>() {
                              if (confirm('[<?= $task->title; ?>] 確定提前截止?')) {
                                 window.location.href = '<?= base_url("credit/ahead_deadline/$task->id")?>';
                                 return true;
                              } else{
                                 return false;
                              };
                          }
                          </script>
                        <? endif ; ?>
                     </th>
                	</tr>
					</thead>
              	<tbody>
					<?php
               if (isset($runner_results[$task->id])) foreach ($runner_results[$task->id] as $runner):;
               // foreach ($runner_results as $runner): //該任務跑者
               ?>
                  <tr>
                     <td class="media">
                        <a class="pull-left" href="#">
                           <img class="media-object img-rounded" style="width:48px" src="
                           <?php echo base_url("uploads/head_img/$runner->img")?>">
                        </a>
                        <span class="pull-right">
                        $<?= $task->price;?> |
                        <?php if ($runner->recipient_id != null){ ?>
                           已評價<?php }else{ ?>
                           <a  href="#feedback<?= $task->id . '_' . $runner->id; ?>" data-toggle="modal">給予評價</a>
                        <?php } ?>
                        </span>
                        <div class="media-body">
                           <div class="media">
                           	<p>
                                 <a href="<?= $runner->id; ?>"><?= $runner->name; ?></a>
                                 <div><i class="icon-ok"></i> <?= $runner->phone; ?>　
                                 <i class="icon-envelope"></i> <?= $runner->email; ?> 
                                 <a class="btn btn-mini btn-primary" target="_blank" href="mailto:<?= $runner->email; ?> ?subject=[Citytasker]<?= $task->title; ?>">直接聯繫</a>
                                 </div>
                              </p>
                           </div>
                        </div>
                     </td>
                  </tr>
                  <!-- Modal -->
                  <form id="contact-form" class="contact-form" action="<?= base_url("credit/give_ing");?>"
                     method="post" accept-charset="utf-8" onSubmit="return checkForm<?= $task->id . '_' . $runner->id; ?>(this)">
                     <div id="feedback<?= $task->id . '_' . $runner->id; ?>"
                        class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                           <h3 id="myModalLabel">評價</h3>
                        </div>
                        <div class="modal-body">
                           <h4>評價幫手：<?= $runner->name; ?></h4>

                           <input type="hidden" name="task_id" value="<?= $task->id ?>" />
                           <input type="hidden" name="task_name" value="<?= $task->title ?>" />
                           <input type="hidden" name="giver_id" value="<?= $_SESSION['member_id'] ?>" />
                           <input type="hidden" name="recipient_id" value="<?= $runner->id ?>" />
                           <input type="hidden" name="recipient_identity" value="<?= 'runner' ?>" />
                           <p>任務：<?= $task->title; ?></p>
                           <hr>
                           <strong>滿意指數</strong>
                           <p>
                              <span id="function<?= $task->id . '_' . $runner->id; ?>"></span>
                              <span id="function-hint<?= $task->id . '_' . $runner->id; ?>" class="hint">0</span>
                              <input type="hidden" name="score" id="score<?= $task->id . '_' . $runner->id; ?>" value="">
                           </p>
                           <!-- 星星 -->
                           <script type="text/javascript">
                           $(function() {
                              $('#function<?= $task->id . '_' . $runner->id; ?>').raty({
                                 path      : '<?= base_url("js/star/img"); ?>',
                                 size      : 24,
                                 half      : true,
                                 starHalf  : 'star-half-big.png',
                                 starOff   : 'star-off-big.png',
                                 starOn    : 'star-on-big.png',
                                 target    : '#function-hint<?= $task->id . '_' . $runner->id; ?>',
                                 targetKeep: true,
                                 targetType: 'number'
                              });
                           });
                           </script>
                           <strong>評價意見</strong>
                           <textarea rows="3" style="width:97%;" name="comment"
                              placeholder="評價為公開紀錄，發布前請再次確認。"><?=$this->input->post('comment')?></textarea>
                        </div>
                        <div class="modal-footer">
                           <span class="help-inline"><small>*一旦送出便無法修改，請謹慎給予！</small></span>
                           <input class="btn btn-large" type="submit" value="完成" />
                           <script type="text/javascript">
                           function checkForm<?= $task->id . '_' . $runner->id; ?>() {
                              var cd = $("#function-hint<?= $task->id . '_' . $runner->id; ?>").text();
                              if (cd == '') cd='0';
                               if (confirm('確定給'+cd+'分?')) {
                                  $("#score<?= $task->id . '_' . $runner->id; ?>").val(cd);
                                  return true;
                               } else{
                                  return false;
                               };
                           }
                           </script>
                        </div>
                     </div>
                   <input type="hidden" name="former_URL" value="<?= $this->uri->uri_string(); //所在頁面?>" />
                     <?php //因為有開啟CSRF
                     $CI =& get_instance();
                     $csrf_name = $CI->security->get_csrf_token_name();
                     $csrf_value = $CI->security->get_csrf_hash();
                     ?>
                     <input type="hidden" name="<?= $csrf_name ?>" value="<?= $csrf_value ?>" />
                  </form>
                  <!-- */ Modal -->
               <?php endforeach; //跑者 ?>
					</tbody>
				</table>
			<?php endforeach; //任務紀錄 ?>
<? }else{ ?>
      無發案紀錄，趕快點選<a href="<?= base_url("task/post"); ?>">免費發布任務</a>吧！
<? } ?>
      </div>
      <div class="span4">
         <?php require('template/_setting_menu.php'); ?>
      </div>
   </div>
</div>

<?php require('template/_copyright.php'); ?>
<?php require('template/_footer.php'); ?>
