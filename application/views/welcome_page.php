<?php require('template/_header.php'); ?>
<?php require('template/_navbar.php'); ?>

<style>
@media (min-width: 767px) {
	#mobi{
		display:none;
	}
}
@media (max-width: 767px) {
	#dest{
		display:none;
	}
}
</style>

<div id="dest">
   <!-- Carousel
   ================================================== -->
   <div class="carousel">
      <div class="item">
         <div class="container">
         <?php require('template/_flashdata_show.php'); ?>
            <div class="carousel-caption row-fluid">
               <div class="span7">
                  <h1><i class="iconic-cloud"></i>整個城市都有我的好幫手！</h1>
                  <p>Citytasker 擁有超過 <strong>上萬</strong> 位幫手分布在大台北地區的各個角落，已經準備好要隨時出任務了！生活中大小事只要想被完成，就讓我們來幫助你分憂解勞吧！</p>
                  <br/>
                  <p>發布任務是<strong>免費</strong>的，趕快來發布你的任務吧！</p>
                  <br/>
                  <div class="row-fluid">
                     <a class="btn btn-large btn-inverse span4" href="<?= base_url("task/post"); ?>"><i class="icon-bullhorn icon-white"></i> 免費發布任務</a>
                     <iframe class="span8" src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcitytasker&amp;send=false&amp;layout=standard&amp;width=450&amp;show_faces=false&amp;font&amp;colorscheme=light&amp;action=like&amp;height=40&amp;appId=258248100903520" scrolling="no" frameborder="0" style="padding-top:20px; border:none; overflow:hidden;" allowTransparency="true"></iframe>
                  </div>
               </div>
               <embed class="video offset1 span4 img-rounded"
               src="https://www.youtube-nocookie.com/v/36iGwrzIMbM?hl=zh_TW&amp;version=3&amp;rel=0&cc_load_policy=1"
               type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true"></embed>
            </div>
         </div>
      </div>
   </div><!-- /.carousel -->

   <!-- Marketing messaging and featurettes
   ================================================== -->
   <!-- Wrap the rest of the page in another container to center all the content. -->
   <div id="container" class="container">
      <div class="row-fluid">
         <div class="span8">
         	<div>
<div class="dropdown">
  <!-- Link or button to toggle dropdown -->
  <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
    <li><a tabindex="-1" href="#">Action</a></li>
    <li><a tabindex="-1" href="#">Another action</a></li>
    <li><a tabindex="-1" href="#">Something else here</a></li>
    <li class="divider"></li>
    <li><a tabindex="-1" href="#">Separated link</a></li>
  </ul>
</div>

<!-- Button to trigger modal -->
<a href="#myModal" role="button" class="btn" data-toggle="modal">Launch demo modal</a>

<!-- Modal -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Modal header</h3>
  </div>
  <div class="modal-body">
    <p>One fine body…</p>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <button class="btn btn-primary">Save changes</button>
  </div>
</div>

            	<h4>最新任務動態</h4>
            </div>
            <div id="task_wall">

         <?php foreach ($task_result as $task):
         switch ($task->status){
          case 0:
            $status_text = "報名";
            $btn_status = "btn-inverse";
            break;
          case 1:
            $status_text = "報名";
				$btn_status = "btn-warning";
            break;
          case 2:
            $status_text = "截止";
				$btn_status = "btn-inverse disabled";
            break;

          case 4:
            $status_text = "截止";
            $btn_status = "btn-inverse disabled";
            break;

          default:
            $status_text = "default任務狀態";
				$btn_status = "btn-danger";
            break; }

         $img = base_url("uploads/head_img/$task->img");
         ?>
            <div class="media">
               <a class="pull-left" href="<?= base_url("user/$task->member_id");?>">
                  <img class="media-object img-rounded" style="width:65px; height:65px;" src="<?= $img; ?>" />
               </a>
               <h3 class="pull-right">
                  <small>
                     <?php if ($task->payment == 0){ echo "現領"; }else{ echo "匯款";}?></small>
                  </small> $<?= $task->price; ?>
                  <a class="btn <?=$btn_status;?>" href="<?= base_url("task/$task->id");?>"><?= $status_text; ?></a>
               </h3>
               <div class="media-body">
                  <h5 class="media-heading" style="overflow:hidden; height:20px;">
                     <strong><a href="<?= base_url('task/')."/".$task->id;?>"><?= $task->title; ?></a></strong>
                  </h5>
                  <small><i class="icon-calendar"></i> 完成期限 <?= $task->deadline; ?><br/>
                 <?php if($task->lat_lng !== null){
                     $this->load->helper('address_format1_helper'); //地址只顯示到區
                     $task->address = address_format1($task->address);
                     echo "<i class=\"icon-map-marker\"></i>" . nl2br(htmlspecialchars($task->address));
                 }else{
                     echo "<i class=\"icon-globe\"></i> 線上或手機";
                 }?>
                  </small>
               </div>
            </div>
        <?php endforeach ?>
        </div>
         <div class="row-fluid text-center"><a class="span12 btn btn-large" href="<?= base_url("task/map"); ?>">瀏覽更多任務</a></div>
         </div>
         <div class="span4">
            <h4>更優質的行動體驗，即將上線！</h4>
            <img width="100%" src="<?= base_url("img/web/app.png"); ?>" />
            <hr>
            <iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fcitytasker&amp;width=300&amp;height=300&amp;show_faces=true&amp;colorscheme=light&amp;stream=false&amp;show_border=false&amp;header=false&amp;appId=258248100903520" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100%; height:300px;" allowTransparency="true"></iframe>
         </div>
      </div><!-- /.row-fluid -->
    </div>
</div>
<div id="mobi">
	<?php require('template/_login.php'); ?>
</div>

<?php require('template/_copyright.php'); ?>
<?php require('template/_footer.php'); ?>