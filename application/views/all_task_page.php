<?php require('template/_header.php'); ?>
<?php require('template/_navbar.php'); ?>

<?= '任務總數:' . $count_all_task .'筆' ;?>
<?php  foreach ($result as $result): ?>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <div class="span2">
                        <a href="<?= base_url("task/$result->id");?>" target="_blank">
                            <img class="media-object img-rounded" style="width:150px" src="<?php echo base_url("uploads/head_img/$result->img")?>">
                        </a>
                    </div>
                    <div class="span6">
                        <?= $result->id .' ' ;?>
                        <?= $result->title .' ' ;?>

                        <strong><?= $result->area .' ' ;?></strong>
                    </div>
                    <div class="span4">
                        <?= '截止:'.$result->deadline .' ' ;?>
                        <?= '建立:'.$result->created_at .' ' ;?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<? endforeach; ?>
<?php require('template/_copyright.php'); ?>
<?php require('template/_footer.php'); ?>
