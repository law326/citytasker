<?php require('template/_header.php'); ?>
<?php require('template/_navbar.php'); ?>

<style>
	h4{
		color:#F15922;
	}
	.marketing .span1{
	  text-align:center;
	  padding-top:15px;
  }
  .media{
	  padding:10px;
	  border-bottom:1px #eee solid;
	  margin-bottom: -20px;
  }
  .media:hover {
	  background:#fbfbfb;
  }

  .bounty{
	  padding:10px;
	  text-align:center;
	  background:#eee;
  }

    /* RESPONSIVE CSS
    -------------------------------------------------- */
    @media (max-width: 767px) {
      .marketing .span4 + .span4 {
        margin-top: 40px;
      }

    }
</style>

    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container">
    <?php require('template/_flashdata_show.php'); ?>
      <h2>如何賺外快？</h2>
      <hr>
      <div class="row-fluid marketing">
        <div class="span4">
          <img src="<?= base_url("img/web/hiw-4.png"); ?>">
          <h4>步驟1：搜尋在你身邊的任務</h4>
          <p><strong>打開任務地圖</strong>，找到你能夠完成的任務，按下<strong>我要出任務</strong>按鈕後 Citytasker 便會通知案主你已經準備好出任務。</p>
          <p><strong>請注意！</strong>當雇主同意之後，就代表已經被指派任務，可以開始執行任務或是與案主直接進行進一步聯繫。</p>
        </div><!-- /.span4 -->
        <div class="span4">
          <img src="<?= base_url("img/web/hiw-5.png"); ?>">
          <h4>步驟2：確定任務內容並完成</h4>
          <p>為了<strong>保護你的權益</strong>，可以透過 Citytasker 提供的留言功能與案主溝通任務內容與細節，降低任務失敗的風險。</p>
          <p>所有討論內容都將會被記錄並生成一份具有法律效力的文件，若任務結束出現爭議將成為依據。</p>
        </div><!-- /.span4 -->
        <div class="span4">
          <img src="<?= base_url("img/web/hiw-6.png"); ?>">
          <h4>步驟3：賺外快，得聲譽</h4>
          <p>利用你的閒暇時間來完成任務，完成後就能直接拿到現金賺外快。累積任務經驗與評價，可以讓未來更有機會得到更多任務的指派！</p>
          <p>準備好出任務了嗎？ <strong><a href="<?= base_url("task/map"); ?>">搜尋任務</a></strong></p>
        </div><!-- /.span4 -->
      </div><!-- /.row -->
		<br/>
      <br/>
      <h2>為什麼要在 Citytasker 接任務？</h2>
      <hr>
      <div class="row-fluid marketing">
         <div class="span1">
         	<img src="https://cdn0.iconfinder.com/data/icons/windows8_icons/26/one_free.png"/>
         </div><!-- /.span4 -->
         <div class="span5">
            <h4>成為會員是免費的</h4>
            <p>只有當你發布的任務被完成後才需要付款，除此之外在平台裡進行任何事都是免費的。</p>
         </div><!-- /.span4 -->
         <div class="span1">
         	<img src="https://cdn1.iconfinder.com/data/icons/windows8_icons_iconpharm/26/zoom_in.png"/>
         </div>
         <div class="span5">
            <h4>便利的在地化搜尋</h4>
      		<p>透過我們建立的在地化系統，可以讓你更輕鬆發覺生活周遭的各種機會。</p>
         </div><!-- /.span4 -->
      </div><!-- /.row -->
      <div class="row-fluid marketing">
         <div class="span1">
         	<img src="https://cdn1.iconfinder.com/data/icons/windows8_icons_iconpharm/26/nui.png"/>
         </div><!-- /.span4 -->
         <div class="span5">
            <h4>全新的使用者體驗</h4>
            <p>使用創新的地圖化介面探索城市中每一個需要幫忙的角落，透過任務分類以及各種方便的排序，快速找到自己想要的任務，不用再大海撈針了。</p>
         </div><!-- /.span4 -->
         <div class="span1">
         	<img src="https://cdn1.iconfinder.com/data/icons/windows8_icons_iconpharm/26/advertising.png"/>
         </div>
         <div class="span5">
            <h4>公開透明化錄取名單</h4>
      		<p>改變幕後作業流程，讓錄取與否一目了然，不用需耗時間在無謂的等待。</p>
         </div><!-- /.span4 -->
      </div><!-- /.row -->
      <div class="row-fluid marketing">
         <div class="span1">
         	<img src="https://cdn1.iconfinder.com/data/icons/windows8_icons_iconpharm/26/moneybox.png"/>
         </div><!-- /.span4 -->
         <div class="span5">
            <h4>現領機會多</h4>
            <p>因為平台的經營特色，凝聚來自北北基有臨時性或一次性人力需求的人們或廠商，有高達80%以上的任務都可以在完成後直接拿到現金！</p>
         </div><!-- /.span4 -->
         <div class="span1">
         	<img src="https://cdn0.iconfinder.com/data/icons/windows8_icons/26/business_contact.png"/>
         </div>
         <div class="span5">
            <h4>最貼近你的人生履歷</h4>
      		<p>完成任務後會被詳細記錄在個人履歷中，其中也包含了每一次獲得的任務評價，保持優良的紀錄將提升你出任務的機會。</p>
         </div><!-- /.span4 -->
      </div><!-- /.row -->
      <div class="row-fluid marketing">
         <div class="span1">
         	<img src="https://cdn1.iconfinder.com/data/icons/windows8_icons_iconpharm/26/joystick.png"/>
         </div><!-- /.span4 -->
         <div class="span5">
            <h4>讓工作像遊戲一樣多元有趣</h4>
            <p>我們的成員來自社會中各行各業不同地區的人們，除了有實體任務、也有虛擬的線上任務，每天有各式各樣的新需求產生，等待你去發掘破關。</p>
         </div><!-- /.span4 -->
         <div class="span1">
         	<img src="https://cdn1.iconfinder.com/data/icons/windows8_icons/26/solutions.png"/>
         </div><!-- /.span4 -->
         <div class="span5">
            <h4>智慧的網路系統</h4>
            <p>根據你的所在地區，系統會分析出鄰近適合你的任務，並自動通知。任務中發生的任何事，也會透過電子郵件自動傳遞，讓雙方都能清楚掌握目前的進度。</p>
         </div><!-- /.span4 -->
      </div><!-- /.row -->
      <br/>
      <br/>
      <h2>你應該來 Citytasker 出任務，因為...</h2>
      <hr>
      <div class="row-fluid marketing">
         <div class="span1">
         	<img src="https://cdn1.iconfinder.com/data/icons/windows8_icons_iconpharm/26/fish.png"/>
         </div><!-- /.span4 -->
         <div class="span5">
            <h4>你常在家中發閒</h4>
      		<p>每天都有五花八門的任務被發出，有的充滿趣味，有的非常有挑戰，不但能幫助人、打發時間還有錢賺，絕對是一舉數得！</p>
         </div><!-- /.span4 -->
         <div class="span1">
         	<img src="https://cdn0.iconfinder.com/data/icons/windows8_icons/26/money_bag.png"/>
         </div><!-- /.span4 -->
         <div class="span5">
            <h4>你急需要一筆錢</h4>
            <p>還在等打工或上班的薪水嗎？在 Citytasker 有80%以上的任務可以讓你在完成後直接現領！</p>
         </div><!-- /.span4 -->
      </div><!-- /.row -->
      <div class="row-fluid marketing">
         <div class="span1">
         	<img src="https://cdn0.iconfinder.com/data/icons/windows8_icons/26/calendar.png"/>
         </div><!-- /.span4 -->
         <div class="span5">
            <h4>你正處於空窗期</h4>
      		<p>像當兵前或待業中這樣不長不短的尷尬時期，想找到合適的工作很不容易，因此 Citytasker 中自由度高的工作模式將會是最好的選擇！</p>
         </div><!-- /.span4 -->
         <div class="span1">
         	<img src="https://cdn1.iconfinder.com/data/icons/windows8_icons_iconpharm/26/superman.png"/>
         </div><!-- /.span4 -->
         <div class="span5">
            <h4>你是位自由工作者</h4>
            <p>不想過著朝九晚五生活的你，可以自由規劃在你想要的時間出任務，當自己時間的主人。</p>
         </div><!-- /.span4 -->
      </div><!-- /.row -->
      <div class="row-fluid marketing">
         <div class="span1">
         	<img src="https://cdn1.iconfinder.com/data/icons/windows8_icons_iconpharm/26/sci_fi.png"/>
         </div><!-- /.span4 -->
         <div class="span5">
            <h4>你想體驗不同工作</h4>
      		<p>人生本該多采多姿，而不是像機器人一樣反覆無聊工作，有了來自各行各業的不同機會，你可以體驗不一樣的人生。</p>
         </div><!-- /.span4 -->
         <div class="span1">
         	<img src="https://cdn1.iconfinder.com/data/icons/windows8_icons_iconpharm/26/wink.png"/>
         </div><!-- /.span4 -->
         <div class="span5">
            <h4>你喜歡幫助別人</h4>
            <p>你心中有無比的熱忱卻不知道該如何發揮，找個苦主幫助他度過難關，施比受更有福。</p>
         </div><!-- /.span4 -->
      </div><!-- /.row -->
      <div class="row-fluid marketing">
         <div class="span1">
         	<img src="https://cdn0.iconfinder.com/data/icons/windows8_icons/26/positive_dynamic.png"/>
         </div><!-- /.span4 -->
         <div class="span5">
            <h4>你想拓展人脈和商機</h4>
      		<p>用心經營你的專業，讓大家看見你的表現，說不定還會交到新朋友！</p>
         </div><!-- /.span4 -->
         <div class="span1">
         	<img src="https://cdn0.iconfinder.com/data/icons/windows8_icons/26/administrator.png"/>
         </div><!-- /.span4 -->
         <div class="span5">
            <h4>你想在進入職場前先實習</h4>
            <p>來自各行各業的機會，可以讓你在短時間內嘗試不同的機會，找到真正屬於你的理想工作。</p>
         </div><!-- /.span4 -->
      </div><!-- /.row -->
    </div>

<?php require('template/_copyright.php'); ?>
<?php require('template/_footer.php'); ?>
