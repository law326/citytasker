<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 索取、發放Offer
 */
class Offer extends MY_Controller{

function __construct(){
    parent::__construct();
}

function give_ing(){
    // TODO:索取、發放Offer 目前寫在AJAX Controller 暫時沒有其它一般POST頁面用到
    $this->load->library('form_validation');
    $this->_checkFormRules($this->input->post('former_URL')); //進行表單驗證

    $this->load->model('credit_model');
    if ($this->credit_model->add_credit($this->input->post()) == FALSE) {
        $this->_toRedirectBack('/user/posted',(array)'你已經給過評價');
    };
    redirect(base_url($this->input->post('former_URL')));
}

}
/* End of file offer.php */
/* Location: ./application/controllers/offer.php */