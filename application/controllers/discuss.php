<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 每則任務的討論區
 */
class Discuss extends MY_Controller{

function __construct(){
    parent::__construct();
}
/**
 * [新增一筆討論]
 * @param  [type] $id [description]
 * @return [type]     [description]
 */
function post_ing(){
    if (is_logged_in() == FALSE) {
        $this->_toRedirectBack('/phone_vaild',(array)'請先登才可參與討論喔！');
    }

    $this->load->library('form_validation');
    $this->_checkFormRules($this->input->post('former_URL')); //進行表單驗證

    $this->load->model('discuss_model');
    if ($this->discuss_model->add_discuss($this->input->post()) == FALSE) {
        $this->_toRedirectBack($this->input->post('former_URL'),(array)'留言發生錯誤');
    };

    $giver_name = $_SESSION['member_name'];
    $giver_img = $_SESSION['member_img'];
    $discuss_text = $this->input->post('content');
    $task_name = $this->input->post('task_name');
    $task_id = $this->input->post('task_id');

    $user_task_relation = $this->input->post('user_task_relation');
    if ($user_task_relation == 'poster') {
        // 案主留言所有留言者接收
        $this->load->model('discuss_model');
        $mail_list = $this->discuss_model->get_alluser_mail($this->input->post('task_id'));
        $poster_mail = $_SESSION['member_email'];
        foreach ($mail_list as $to_mail) {
            // 寄信加入mail queue
            if ($to_mail['email'] == $poster_mail) continue; //跳過發案者本身
            $data = array('to_mail' => $to_mail['email'],
                           'giver_name' => $giver_name,
                           'giver_img' => $giver_img,
                           'discuss_text' => $discuss_text,
                           'task_name' => $task_name,
                           'task_id' => $task_id
             );
            $this->load->model('mailqueue_model');
            $this->mailqueue_model->add_mailqueue('task_discuss', $data);
        }
    }else{
        // 跑者或會員留言 案主接收
        $this->load->model('user_model');
        $poster_id = $this->input->post('poster_id');
        $to_mail = $this->user_model->get_email_by_id($poster_id);
        // 寄信加入mail queue
        $data = array('to_mail' => $to_mail,
                       'giver_name' => $giver_name,
                       'giver_img' => $giver_img,
                       'discuss_text' => $discuss_text,
                       'task_name' => $task_name,
                       'task_id' => $task_id
         );
        $this->load->model('mailqueue_model');
        $this->mailqueue_model->add_mailqueue('task_discuss', $data);
    }

    redirect(base_url($this->input->post('former_URL')));
}

}
/* End of file discuss.php */
/* Location: ./application/controllers/discuss.php */