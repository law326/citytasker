<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Service相關操作
 */
class Service extends MY_Controller {
    function terms() {
        $this->load->view('terms_page');
    }

    function privacy() {
        $this->load->view('privacy_page');
    }

    function team() {
        $this->load->view('team_page');
    }

    function forrunner() {
        $this->load->view('forrunner_page');
    }

    function forposter() {
        $this->load->view('forposter_page');
    }
    /**
     * [寄信聯絡Citytasker]
     * @return [type] [description]
     */
    public function email_sending() {
        $this->_checkFormRules($this->input->post('former_URL'));

        // 寄信通知
        $this->load->library('email');
        $this->email->from('info@citytasker.tw', 'Citytasker');
        $this->email->to('info@citytasker.tw');
        $subject = $this->input->post('sending_title');
        if (isset($_SESSION['member_id'])) {
            $subject = $subject . 'mid=' . $_SESSION['member_id'];
        }
        $this->email->subject($subject);

        $view_data = array('email' =>$this->input->post('sending_email') ,
                           'content' =>$this->input->post('sending_content') ,
                           'former_URL' =>$this->input->post('former_URL') ,
                           'user_agent' =>$this->input->post('user_agent') );
        $this->email->message( $this->load->view( 'email/mail_service_view', $view_data, true ) );

        if ($this->email->send()) {
            $this->session->set_flashdata('info', array('感謝你的意見回饋！'));
        }else{
            $this->session->set_flashdata('info', array('寄信錯誤發生錯誤,請稍候再試,謝謝'));
        }
        // 導向原本的URL
        $this->_toRedirectBack($this->input->post('former_URL'));

    }

}

/* End of file service.php */
/* Location: ./application/controllers/service.php */
