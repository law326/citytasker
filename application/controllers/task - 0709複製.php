<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 任務相關操作
 */
class Task extends MY_Controller {
  function index() {
      //此class有設定routes
      redirect(base_url("task/map"));
  }

  function __construct()  {
    parent::__construct();
    if (is_logged_in() == FALSE) {
        $this->session->set_flashdata('info', array('快速註冊，免費發案，你還在等什麼？'));
    }
  }

  function post() {
    if (is_logged_in() == TRUE) {
      //有登入檢查此人是否通過手機驗證
      $this->load->model('user_model');
      if ($this->user_model->is_have_phone($_SESSION['member_id']) == FALSE) {
        $this->_toRedirectBack('/phone_vaild',(array)'請先進行手機驗證，才可發布任務喔！');
      }
    }else{
      //未登入狀態
      //提示"你尚未註冊" 發案存在task_temp表，導向註冊頁面
      //快速註冊，免費發案，你還在等什麼？
    }

    $this->load->view('post_page',Array(
                        "submit" => '發布'));
 }


  function post_ing() {
    // TODO: 發案者登入狀態(檢查email是否為會員):
    // A1.未登入未註冊:發文後暫存在task_temp表，並新增member_temp表，寄發註冊確認信，當帳號啟用後
    //           這篇任務轉到task表，刪除task_temp表。。
    // A2.未登入已註冊:輸入密碼通過驗證才寫入task表
    // B1.已登入
    // B2.按FB登入
//避免輸出中文亂碼
header("Content-Type:text/html; charset=utf-8");
echo '<pre>';
var_dump($this->input->post());
echo '</pre>';
    exit;
   $this->load->library('form_validation');

  //實體任務需要驗證
   if ($this->input->post('onlinetask',TRUE) == FALSE) {
    $config_rule[] = array('field' => 'geoclick', 'label' => '地址請由下拉式選單中挑選',
                             'rules' => 'trim|xss_clean|required') ;
    $config_rule[] = array('field' => 'address', 'label' => '任務地址',
                             'rules' => 'trim|xss_clean|required|max_length[255]') ;
    }


  if (is_logged_in() == FALSE) {
    // 沒登入發文需要驗證這些
    $config_rule[] = array('field' => 'name', 'label' => '姓名',
                             'rules' => 'trim|xss_clean|required') ;
    $config_rule[] = array('field' => 'email', 'label' => 'email',
                             'rules' => 'trim|xss_clean|required') ;
    $config_rule[] = array('field' => 'zip', 'label' => '郵遞區號',
                              'rules' => 'trim|xss_clean|required') ;
    $config_rule[] = array('field' => 'phone', 'label' => '手機',
                              'rules' => 'trim|xss_clean|required') ;

    $this->load->model('user_model');
    $is_member = $this->user_model->get_id_by_email($this->input->post('email'));
    if (isset($is_member)) {
        echo "會員未登入";
        // 修改成只有FB登入,暫時沒有此狀態
        // 進行密碼驗證，登入成功變
        // 任務的資訊要一起post過去
    }else{
        echo "你非會員";
        // 存到task_temp 並導向註冊
    }

    exit;
  }

  if (isset($config_rule)) { //有需要加入額外的驗證rule
      $this->config->load('form_validation'); //載入本頁的驗證config檔
      $new_rule = array_merge($this->config->item('task/post_ing'), $config_rule);
      $this->form_validation->set_rules($new_rule);
  }

  $this->_checkFormRules('/task/post'); //進行表單驗證


  // 寫入DB (B1.發案時 已登入)
  $insert_id = $this->_postMemberLogged();


  redirect(base_url("task/" . $insert_id));// 新增完成,進入任務頁面
}

 function edit() {
   //檢查是否為發案者本人
   $task_id = (int)$this->uri->segment(3);
   $this->load->model('task_model');
   if ($this->task_model->is_poster($task_id) == FALSE) {
     $this->_toRedirectBack("/task/$task_id",(array)'發案者本人才可修改任務喔！');
   }

   // 取得此任務資訊
   $task_results = $this->task_model->get_task($task_id);

   $this->load->view('post_page',Array(
                       "submit" => '修改',
                       "task_results" => $task_results
                        ));
}

/**
 * [發案時 非會員]
 * @return [type] [insert_id]
 */
private function _postMemberLogged(){
    $this->load->model('task_model');
    $insert_id = $this->task_model->add_task($_SESSION['member_id'],$this->input->post());

    if ($insert_id == FALSE) {
      $this->_toRedirectBack('task/post',(array)'新增任務失敗');
    }

    return $insert_id;
}
/**
 * [發案時 會員未登入]
 * @return [type] [insert_id]
 */
private function _postMemberNotLogin(){
    $this->load->model('task_model');
    $insert_id = $this->task_model->add_task($_SESSION['member_id'],$this->input->post());

    if ($insert_id == FALSE) {
      $this->_toRedirectBack('task/post',(array)'新增任務失敗');
    }

    return $insert_id;
}

/**
 * [發案時 已登入]
 * @return [type] [insert_id]
 */
private function _postNotMember(){
    $this->load->model('task_model');
    $insert_id = $this->task_model->add_task($_SESSION['member_id'],$this->input->post());

    if ($insert_id == FALSE) {
      $this->_toRedirectBack('task/post',(array)'新增任務失敗');
    }

    return $insert_id;
}

/**
 * [索取任務offer]
 * @return [type] [description]
 */
function get_offer(){
  //TODO:索取任務offer
}
/**
 * [指派接案者]
 * @return [type] [description]
 */
function assign_offer(){
  //TODO:發案者 指派任務時才需考慮剩餘人數
}

/**
 * 任務詳細內容
 * @param  [type] $task_id [description]
 */
function info($task_id) {
  try { //FIXME:嘗試用try catch方式寫
    if ( ! isset($task_id)) throw new Exception('此任務id不存在!');

    $this->load->model('task_model');

    // 取得任務狀態,發布任務、執行中、任務完成
    $status_results = $this->task_model->get_status_by_id($task_id);
    if ($status_results == null) throw new Exception('此任務不存在!');


  }
  catch (Exception $e)  {
    $this->_toRedirectBack('error_404',(array)$e->getMessage());
  }

  // 取得任務詳細資訊
  $task = $this->task_model->get_task($task_id);

  // 訪客與任務關係
  $user_task_relation = $this->task_model->get_user_task_relation($task_id);

  // 取得跑者詳細資訊
  $runner_results = $this->task_model->get_runner($task_id);

  // 取得任務評價
  $this->load->model('credit_model');
  $credit_results = $this->credit_model->get_credit($task_id);

  // 取得討論內容
  $this->load->model('discuss_model');
  $discuss_results = $this->discuss_model->get_discuss($task_id);


  // 任務各狀態時間格式化
  $this->load->helper('date_format1_helper'); //修改日期格式
  for ($i = 0; $i <= 2; $i++) {
    if ( isset($status_results[$i])) {
      $status_time[$i] = date_format1($status_results[$i]->created_at);
    }else{
      $status_time[$i] ='';
    }
  }

  $this->load->helper(array('address_format1_helper','address_is_online_helper'));
  $task->address = address_format1($task->address); //地址只顯示到區
  $task->address = address_is_online($task->address); //空白地址顯示 線上或手機

  $this->load->view('task_page', Array(
                    "task" => $task,
                    "title" => $task->title, //header用
                    "content" => nl2br($task->content), //header用
                    "user_task_relation" => $user_task_relation, //訪客身份
                    "task_status_id" => $task->status,//印出最新的狀態 query用MAX()
                    "status_time" => $status_time,
                    "runner_results" => $runner_results,
                    "credit_results" => $credit_results,
                    "discuss_results" => $discuss_results
                     )
  );

}

/**
 * 任務地圖
 * TODO:不顯示完成的任務但有負評,任務再加上一個顯示狀態
 * @param  string $sort_type [排序方式]
 * @param  string $catalog [分類類型]
 * @return [type] [description]
 */
function map($sort_type = 'newest',$catalog = '1') {
  $this->load->model('task_model');
  $task_results = $this->_taskSortType($sort_type, $catalog);

  $this->load->helper('address_format1_helper'); //地址只顯示到區

  $numMarkers = $this->task_model->count_task_by_type('0');//實體任務數量

  $this->load->view('map_page', Array(
                        "sort_type" => $sort_type,
                        "numMarkers" => $numMarkers,
                        "task_results" => $task_results //若地圖不顯示,可能$task->content有斷行 JS不支援
                        ));
}

private function _taskSortType($sort_type, $catalog){
  $this->load->model('task_model');

  switch ($sort_type) { //FIXME: 出現switch考慮用Strategy模式重構
    case 'newest':
    // 參數對照表 $order , $desc , $onlinetask , $now,$catalog, $num
    return $this->task_model->get_all_task('task.id','desc');

    case 'price':
    return $this->task_model->get_all_task('task.price','desc');

    case 'deadline':
    $now = date("Y-m-d H:i:s",time());
    return $this->task_model->get_all_task('task.deadline','asc',null,$now);

    case 'onlinetask':
    return $this->task_model->get_all_task('task.price','desc' ,1);

    case 'catalog':
    return $this->task_model->get_all_task('task.id','desc',null,null,$catalog);

    default:
    redirect(base_url("/error_404"));
  }
}
    function checkout() {
      $this->load->view('checkout_page', Array(
                        "task_id" => "123456",
                        "title" => "我需要人幫我買小禮物",
                        "deadline" => "4/10 15:00",
                        "bounty" => "99",
                        "runner_name" => "John C.",
                        "runner_id" => "",
                        "run" => "50",
                        "fraction" => "4.5"
                        )
      );
    }

    /**
     * Run Form validation
     * 若不成功則為導向原本頁面
     * @param  string $redirect [description]
     * @return [type]           [description]
     */
    private function _checkFormRules($redirect = '/user/login'){
        $this->load->library('form_validation');
        if ( ! $this->form_validation->run() ) {
            $this->_toRedirectBack($redirect, (array)validation_errors());
        }
        return TRUE;
    }
    private function _toRedirectBack($redirect = '/user/login', $error_message=null){
        if (isset($error_message)) {
            $this->session->set_flashdata('error', (array)$error_message);
        }
        $post = $this->input->post();
        if (isset($post)) {
            $this->session->set_flashdata('postdata', $this->input->post()); // 表單要重填 資料需保留
        }
        redirect(base_url("$redirect"));
    }
}

/* End of file task.php */
/* Location: ./application/controllers/task.php */
