<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 使用者相關操作
 */
class User extends MY_Controller {
function index() {
    $this->load->view('welcome_page');
}

function test() {
    $this->load->view('case/index');
}

function mail() {
    $data = array('noRobotIndex' => 'noRobotIndex' );
    $this->load->view('mail_view', $data);
}

// function __construct(){
//     parent::__construct(); //加入construct會發生錯誤!
// }
function vitae() {
    $this->_checkLoggedIn();
    $this->load->model('user_model');
    $result = $this->user_model->get_vitae($_SESSION['member_id']);

    $this->load->view('vitae_page',Array(
                      "noRobotIndex" => 'noRobotIndex',//禁止後台被搜尋索引
                      "is_have_phone" => $this->_isHavePhone(),
                      "result" => $result
                       ));
}
function vitae_ing(){
    $this->_checkFormRules('/user/vitae'); //進行表單驗證

    $jobway = array();
    for ($i = 1; $i < 6; $i++) {
        $jobway[$i] = 0; //全部填0
    }

    // if (is_bool($this->input->post('jobway')) == TRUE) {
        foreach ($this->input->post('jobway') as $checked) {
            $jobway[$checked] = 1; //有選到為1
        }
    // }

    $data = array('member_id' => (int)$_SESSION['member_id'],
                  'language' => $this->input->post('language'),
                  'education' => $this->input->post('education'),
                  'work' => $this->input->post('work'),
                  'about' => $this->input->post('about') ,
                  'jobway1' => $jobway[1] ,
                  'jobway2' => $jobway[2] ,
                  'jobway3' => $jobway[3] ,
                  'jobway4' => $jobway[4] ,
                  'jobway5' => $jobway[5] ,
                  'jobway6' => $jobway[6] ,
          );
    $this->load->model('user_model');
    $this->user_model->add_vitae($_SESSION['member_id'], $data); //進行更新
    redirect(base_url("/user/vitae"));
}

/**
 * [個人發案歷史紀錄]
 */
function posted() {
    $this->_checkLoggedIn();
    $this->load->model('task_model');

    // if ($_SESSION['member_id'] == 1) { //方便切換身分
    //     $posted_results = $this->task_model->get_posted_history(614); //本人所有發案紀錄
    // }else{
    $posted_results = $this->task_model->get_posted_history($_SESSION['member_id']); //本人所有發案紀錄
    // }
    // if ( ! isset($posted_results)) {
        // 沒有發案紀錄時
        // $this->_toRedirectBack('user/settings', (array)'尚未發布過任務');
    // }

    if (isset($posted_results)) {
        foreach ($posted_results as $result) {
            $runner = $this->task_model->get_posted_history_runner($result->id);//取得該任務跑者
            if (isset($runner)) { //有跑者才存入
                $runner_results[$result->id] = $runner; //該任務所有跑者，用任務id當索引
            }
        }
    }
    // // 跑者資訊在View可這樣讀出
    // foreach ($posted_results as $task) {
    //     if (isset($runner_results[$task->id])) {
    //         foreach ($runner_results[$task->id] as $result) {
    //         //需要2個迴圈,外圍是任務次數,內圍是該任務跑者數量
    //             echo $result->task_id . $result->name .'</br>';
    //         }
    //     }
    // }

    $this->load->view('posted_history_page',Array(
                      "noRobotIndex" => 'noRobotIndex',//禁止後台被搜尋索引
                      "posted_results" => $posted_results,
                      "runner_results" => $runner_results,
                      "is_have_phone" => $this->_isHavePhone()
                       )
                      );
}
/**
 * [個人接下任務紀錄]
 */
function running() {
    $this->_checkLoggedIn();
    $this->load->model('task_model');
    $posted_results = $this->task_model->get_running_history($_SESSION['member_id']); //本人接案紀錄

    // if ( ! isset($posted_results)) {
        // $this->_toRedirectBack('user/settings', (array)'尚未有接案紀錄');
    // }

    $segment = $this->uri->segment(3);
    if (isset($segment)) $new_offer = $this->uri->segment(3);

    $this->load->helper('address_is_online_helper');

    $this->load->view('running_history_page',Array(
                      "noRobotIndex" => 'noRobotIndex',//禁止後台被搜尋索引
                      "posted_results" => $posted_results,
                      "new_offer" => @$new_offer
                      // "is_have_phone" => $this->_isHavePhone()
                       )
                      );
}

private function _isHavePhone() {
      $this->load->model('user_model');
      return $this->user_model->is_have_phone($_SESSION['member_id']);
}


function settings() {
    $this->_checkLoggedIn();

    //讀取個人資訊
    $this->load->model('user_model');
    $query = $this->user_model->get_member_by_email($_SESSION['member_email']);

    $query_array = (array) $query;
    $query_array['pageTitle'] ='設定頁';
    $query_array['noRobotIndex'] ='noRobotIndex'; //禁止後台被搜尋索引
    $query_array['is_have_phone'] = $this->_isHavePhone();

    $this->load->view('settings_page',$query_array);
}

function settings_update(){
    $this->load->library('form_validation');

    $password = $this->input->post('password');
    $newpassword = $this->input->post('newpassword');
    //有輸入新密碼,就需檢查原有密碼是否輸入正確
    if ( ! empty($newpassword) or ! empty($password)) {
        $config_rule[] = array('field' => 'password', 'label' => '密碼',
                                 'rules' => 'xss_clean|trim|callback_validate_credentials|md5') ;
        $config_rule[] = array('field' => 'newpassword', 'label' => '新密碼',
                                 'rules' => 'trim|xss_clean|min_length[6]|md5') ;
    }
    if (isset($config_rule)) { //有需要加入額外的驗證rule
        $this->config->load('form_validation'); //載入本頁的驗證config檔
        $new_rule = array_merge($this->config->item('user/settings_update'), $config_rule);
        $this->form_validation->set_rules($new_rule);
    }

    $this->_checkFormRules('/user/settings'); //進行表單驗證

    $this->load->model('user_model');
    $id = $this->user_model->get_id_by_email($_SESSION['member_email']);

    $data = array('name' => $this->input->post('name'),
                  'realname' => $this->input->post('realname'),
                  'phone' => $this->input->post('phone'),
                  'zip' => $this->input->post('zip')
                  );

    if ( ! empty($newpassword) or ! empty($password)) { //有要更新密碼
        $data['password'] = $this->input->post('newpassword');
    }
    if ( $this->user_model->update_ByID($id,$data) ) {
        $this->user_model->set_login_session($_SESSION['member_email']);//更新後session重發
        $change[] = TRUE;
    }else{
        $change[] = FALSE;
    }

    $email2 = $this->input->post('email2');
    if (empty($email2)) {
        $change[] = $this->user_model->del_email2($id);
    }else{
        $change[] = $this->user_model->add_email2($id, $this->input->post('email2')); //增加連絡信箱
    }

    if (in_array(TRUE, $change) == TRUE) {
        $message = (array)'更新成功';
        $this->session->set_flashdata('info', $message);
    }else{
        $message = (array)'無更新';
        $this->session->set_flashdata('error', $message);
    }

    redirect(base_url("user/settings"));
}

function info($member_id) {
    try { //FIXME:嘗試用try catch方式寫
        if ( ! isset($member_id)) throw new Exception('此使用者不存在!');

        $this->load->model('user_model');
        $result_vitae = $this->user_model->get_user($member_id);
        if ($result_vitae == null) throw new Exception('此使用者不存在!');
      }
      catch (Exception $e)  {
        $this->_toRedirectBack('error_404',(array)$e->getMessage());
      }

    $this->load->helper('zip_format1_helper');
    $zip = zip_format1($result_vitae->zip);

    $this->load->helper('jobway_format1_helper');
    $jobway = jobway_format1($result_vitae);

    $this->load->model('task_model');
    $count_posted = $this->task_model->count_posted($member_id); // 發布任務總數
    $count_finished = $this->task_model->count_runner_finished($member_id); //完成任務總數

    $this->load->model('credit_model');
    $count_credit = $this->credit_model->get_runner_credit_total($member_id); // 獲得評價總數
    $credit_results = $this->credit_model->get_runner_credit($member_id);
    $credit_catalog_results = $this->credit_model->count_runner_catalog($member_id); //各任務分類評價

    $this->load->view('user_page', Array(
                      "result_vitae" => $result_vitae,
                      "zip" => $zip,
                      "jobway" => $jobway,
                      "count_finished" => $count_finished,
                      "count_posted" => $count_posted,
                      "count_credit" => $count_credit,
                      "credit_results" => $credit_results,
                      "credit_catalog_results" => $credit_catalog_results
                      )
     );
}
/*
function login() {
    if (is_logged_in() == TRUE) {
        $this->_toRedirectBack('/user/settings','你已經登入！');
    }
    $this->load->view('login_page');
}
*/
function login_ing(){

    // $time = time()+60*60*24*3650;
    $this->_checkFormRules('/user/login'); //進行表單驗證
    $email = $this->input->post('email');
    // if(!is_kept_logged()){
    //     $remember = (bool) $this->input->post('remember');
    //     if($remember){
    //         setcookie("keep_me", $email, $time, "/", ".".$_SERVER["HTTP_HOST"]);
    //     }
    // }

    $this->user_model->set_login_session($email);
    redirect(base_url("user/settings"));

    //發session 在user_model->check_log_in
    //$email = $this->input->post('email');
    //$this->load->model('user_model');
    //$this->user_model->set_login_session($email);
    //redirect(base_url("user/settings"));
}

function forgot_password(){
    $this->_checkFormRules('/user/login'); //進行表單驗證

    $email_forgot = $this->input->post('email_forgot');
    $this->load->model('user_model');
    $get_items = "id,name,email,password";
    $query = $this->user_model->get_member_by_email($email_forgot,$get_items);

    if (empty($query))  {// 此email不存在
        $this->session->set_flashdata('error', (array)'檢查此email不存在');
        redirect(base_url("/user/login"));
    }

    // 建立一組新密碼
    $new_password = substr( md5(uniqid()) ,1,6); //隨機key取前6碼當作新密碼
    $new_password_md5 = md5($new_password); //md5新密碼

    // 寄信通知
    $this->load->library('email');
    //已使用email.php初始化
    $this->email->from('admin@citytasker.tw', 'Citytasker');
    $this->email->to($this->input->post('email_forgot'));
    $this->email->subject("這是你的 Citytasker 新密碼！");

    $view_data = array('new_password' =>$new_password ,
                       'name' =>$query->name );
    $this->email->message( $this->load->view( 'email/mail_forgotpassword_view', $view_data, true ) );

    // 寄信成功再更新密碼
    if ( ! $this->email->send()) {
        $this->session->set_flashdata('error', (array)'send mail faild');
        redirect(base_url("/user/login"));
    }

    $this->load->model('user_model');
    $data['password'] = $new_password_md5;

    $id = $query->id;
    if ( $this->user_model->update_ByID($id,$data) ) {
        $view_data['status'] = '補寄密碼成功';
    }else{
        $view_data['status'] = '補寄密碼失敗';
    }

    $view_data['email_forgot'] = $email_forgot;
    $this->load->view('forgot_password_page',$view_data);
    // redirect(base_url("/user/login"));

}
/**
 * 驗證帳號密碼是否正確
 * @return [type] [description]
 */
function validate_credentials(){
    $email = $this->input->post('email');
    $password = md5($this->input->post('password'));

    $this->load->model('user_model');
    if ($this->user_model->check_log_in($email,$password)){
        return TRUE;
    }
    else{
        // 自己增加的驗證規則,一定要加上錯誤訊息
        $this->form_validation->set_message('validate_credentials', '帳號/密碼錯誤!');
        return FALSE;
    }
}

/**
 * 檢驗是否為重複提送表單
 * 暫時沒還有全部實作，先從前端防
 * @return [type] [description]
 */
function validate_resubmit() {
    if ( isset( $_POST['resubmit_code']  ) ) {
        // var_dump($_SESSION['resubmit_code']);
        // exit;
        if ( $_POST['resubmit_code'] == $_SESSION['resubmit_code'] ) {
            $_SESSION['resubmit_code'] ='done';
            return TRUE;
        }else {
            // echo " <meta charset='utf-8'> 請勿重複提交表單！";
            $this->form_validation->set_message('validate_resubmit', '請勿重複提交表單！');
            // $this->session->set_flashdata('error', array('請勿重複提交表單！'));
            return FALSE;
        }
    }
}

function logout() {
    session_destroy();
    redirect(base_url("/"));
}

/*
function signup() {
    if (is_logged_in() == TRUE) {
        $this->_toRedirectBack('/user/settings','你已經登入！');
    }
    // else if(is_kept_logged()){
    //     $this->_toRedirectBack('/user/settings','歡迎回來！');
    // }
    $this->load->view('signup_page');
}*/

function signup_ing() {
    $this->_checkFormRules('/user/signup'); //進行表單驗證
    //注意! 經過run後password已被md5加密過了 可直接post('password')

    //寄註冊確認信給會員
    $key = md5(uniqid()); //隨機key 驗證信使用
    $this->load->library('email');
    //已使用email.php初始化
    $this->email->from('admin@citytasker.tw', 'Citytasker');
    $this->email->to($this->input->post('email'));
    $this->email->subject("請盡速啟用你的 Citytasker 帳號！");
    // $this->email->message($email_message);
    $view_data = array('username' =>$this->input->post('username') , 'key' =>$key );
    $this->email->message( $this->load->view( 'email/mail_signup_view', $view_data, true ) );

    $username = $this->input->post('username');
    $zip = $this->input->post('zip');
    $email = $this->input->post('email');
    $password = $this->input->post('password'); //注意這裡不要再md5了

    $this->load->model('user_model');
    if ($this->user_model->add_temp_user($username,$email,$zip,$password,$key)) {
        //寫入DB成功再寄信
        if ( ! $this->email->send()) {
            echo "send mail faild";
            exit;
         }
    }

        //判斷是哪家的email 貼心附上該網站連結 有想到再繼續擴充
    if (preg_match('/@gmail.com/i',$email)) {
        $mail_name = 'Gmail';
        $mail_site = 'https://mail.google.com';

    }elseif (preg_match('/@yahoo.com/i',$email)) {
        $mail_name = 'Yahoo';
        $mail_site = 'http://tw.mail.yahoo.com';

    }elseif (preg_match('/msn.com/i',$email)) {
        $mail_name = 'msn';
        $mail_site = 'https://login.live.com';

    }elseif (preg_match('/hotmail.com/i',$email)) {
        $mail_name = 'hotmail';
        $mail_site = 'https://login.live.com';

    }elseif (preg_match('/outlook.com/i',$email)) {
        $mail_name = 'Outlook';
        $mail_site = 'https://login.live.com';

    }else{
        $mail_name = $email;
        $mail_site = $email;
    }

    $data = array('pageTitle' => '歡迎你的註冊！',
                  'username' => $username,
                  'email' => $email,
                  'mail_name' => $mail_name,
                  'mail_site' => $mail_site
                  );
    $this->load->view('signup_newuser_page',$data);
}


function signup_key($key){
    //啟用帳號
    $this->load->model('user_model');
    if ( ! $this->user_model->is_key_valid($key)) {
        echo "error key.";
        exit;
    }

    if ( ! $this->user_model->add_user($key)) {
        echo "無法新增使用者. 請重試.";
        exit;
    }

    //新增使用者成功發session 在model的add_user() 含name、email、zip

    //初次登入 提醒進行設定 ，之後改成用jQuery
    $data = array('pageTitle' => 'Hi '.$_SESSION['member_name'].' 歡迎你的加入！',
                  'firstUser' => '這是你初次登入，請進行基本資料設定。<br/>提醒你手機還尚未設定，通過手機驗證後才可以發布或出任務喔！'
                  );
    $this->session->set_flashdata('error', $data);
    // $this->load->view('dashboard_page',$data);
    // $this->session->set_flashdata('newuser', $data);
    redirect(base_url("/user/settings/"));
}

function dashboard() {
    $this->load->view('dashboard_page');
}

function messages() {
    $this->load->view('messages_page');
}

function payments() {
    $this->load->view('payments_page');
}

function history() {
    $this->load->view('history_page');
}

}

/* End of file user.php */
/* Location: ./application/controllers/user.php */
