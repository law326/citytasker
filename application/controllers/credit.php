<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 交易評價
 */
class Credit extends MY_Controller{

function __construct(){
    parent::__construct();
}
/**
 * [給評價]
 * @param  [type] $id [description]
 * @return [type]     [description]
 */
function give_ing(){
    $this->load->library('form_validation');

    $this->_checkFormRules($this->input->post('former_URL')); //進行表單驗證
    $this->load->model('credit_model');
    if ($this->credit_model->add_credit($this->input->post()) == FALSE) {
        $this->_toRedirectBack('/user/posted',(array)'你已經給過評價');
    };

    // 寄信加入mail queue
    $this->load->model('user_model');
    $to_mail = $this->user_model->get_email_by_id((int)$this->input->post('recipient_id'));
    $data = array('to_mail' => $to_mail,
                  'giver_name' => $_SESSION['member_name'],
                  'giver_img' => $_SESSION['member_img'],
                  'recipient_id' => (int)$this->input->post('recipient_id'),
                  'task_name' => $this->input->post('task_name'),
                  'task_id' => (int)$this->input->post('task_id')
     );
    $this->load->model('mailqueue_model');
    $this->mailqueue_model->add_mailqueue('get_credit', $data);

    redirect(base_url($this->input->post('former_URL')));
}
// 案件提前截止
function ahead_deadline(){
    //檢查是否為發案者本人
    $task_id = (int)$this->uri->segment(3);
    $this->load->model('task_model');
    if (is_logged_in() == FALSE OR $this->task_model->is_poster($task_id) == FALSE) {
      $this->_toRedirectBack("/user/posted",(array)'發案者本人才可修改喔！');
    }

    // task_status 新增一筆status=4
    $this->task_model->add_status($task_id, 4);
    // 並且修改 完成期限為現在時間
    $data = array('deadline' => date('Y-m-d H:i:s')
                  );
    $this->task_model->update($task_id, $data);
    $this->session->set_flashdata('info', array('任務修改成功!!'));

    redirect(base_url('user/posted'));
}


}
/* End of file credit.php */
/* Location: ./application/controllers/credit.php */