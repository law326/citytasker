<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 連署
 */
class Petition extends MY_Controller{

function __construct(){
    parent::__construct();
}

function index(){
$this->load->model('petition_model');
// FIXME:考慮用array比較
if (isset($_SESSION['member_zip'])) {
    $zip = (int)$_SESSION['member_zip'];
    if ( ($zip >=100 AND $zip<=208) OR ($zip >=213 AND $zip<=253)) {
        $data = array('city_name' => '大台北',
                      'total' =>   $this->petition_model->get_petition(100,253)
                      );
    }elseif($zip >=320 AND $zip<=338){
        $data = array('city_name' => '桃園',
                      'total' =>   $this->petition_model->get_petition(320,338)
                      );
    }elseif($zip >=300 AND $zip<=315){
        $data = array('city_name' => '新竹',
                      'total' =>   $this->petition_model->get_petition(300,315)
                      );
    }elseif($zip >=350 AND $zip<=369){
        $data = array('city_name' => '苗栗',
                      'total' =>   $this->petition_model->get_petition(350,369)
                      );
    }elseif($zip >=400 AND $zip<=439){
        $data = array('city_name' => '台中',
                      'total' =>   $this->petition_model->get_petition(400,439)
                      );
    }elseif($zip >=540 AND $zip<=558){
        $data = array('city_name' => '南投',
                      'total' =>   $this->petition_model->get_petition(540,439)
                      );
    }elseif($zip >=500 AND $zip<=530){
        $data = array('city_name' => '彰化',
                      'total' =>   $this->petition_model->get_petition(500,530)
                      );
    }elseif($zip >=630 AND $zip<=655){
        $data = array('city_name' => '雲林',
                      'total' =>   $this->petition_model->get_petition(630,655)
                      );
    }elseif($zip >=600 AND $zip<=625){
        $data = array('city_name' => '嘉義',
                      'total' =>   $this->petition_model->get_petition(600,625)
                      );
    }elseif($zip >=700 AND $zip<=745){
        $data = array('city_name' => '台南',
                      'total' =>   $this->petition_model->get_petition(700,745)
                      );
    }elseif($zip >=800 AND $zip<=852){
        $data = array('city_name' => '高雄',
                      'total' =>   $this->petition_model->get_petition(800,852)
                      );
    }elseif($zip >=900 AND $zip<=947){
        $data = array('city_name' => '屏東',
                      'total' =>   $this->petition_model->get_petition(900,947)
                      );
    }elseif($zip >=260 AND $zip<=290){
        $data = array('city_name' => '宜蘭',
                      'total' =>   $this->petition_model->get_petition(260,290)
                      );
    }elseif($zip >=970 AND $zip<=983){
        $data = array('city_name' => '花蓮',
                      'total' =>   $this->petition_model->get_petition(970,983)
                      );
    }elseif($zip >=950 AND $zip<=966){
        $data = array('city_name' => '台東',
                      'total' =>   $this->petition_model->get_petition(950,966)
                      );
    }elseif( ($zip >=209 AND $zip<=212) OR  ($zip >=880 AND $zip<=885) OR ($zip >=890 AND $zip<=896) ){
        $data = array('city_name' => '外島',
                      'total' => $this->petition_model->get_petition(209,212)+$this->petition_model->get_petition(880,885)+$this->petition_model->get_petition(890,896)
                      );
    }
}else{
    //未填郵遞區號,全部顯示
    $islands = $this->petition_model->get_petition(209,212)+$this->petition_model->get_petition(880,885)+$this->petition_model->get_petition(890,896);

    $data = array('taipei' =>   $this->petition_model->get_petition(100,116), //台北
                  'keelung' =>  $this->petition_model->get_petition(200,206), //基隆
                  'newtaipei'   =>  $this->petition_model->get_petition(207,253),//新北
                  'yilan'   => $this->petition_model->get_petition(260,290),//宜蘭
                  'taoyuan' => $this->petition_model->get_petition(320,338),//桃園
                  'hsinchu' => $this->petition_model->get_petition(300,315),
                  'miaoli'   => $this->petition_model->get_petition(350,369),
                  'taichung'   => $this->petition_model->get_petition(400,439),//台中
                  'nantou'   => $this->petition_model->get_petition(540,558),
                  'changhua'   => $this->petition_model->get_petition(500,530),
                  'yunlin' => $this->petition_model->get_petition(630,655),
                  'chiayi' =>$this->petition_model->get_petition(600,625),
                  'tainan' =>  $this->petition_model->get_petition(700,745),//台南
                  'kaohsiung' =>  $this->petition_model->get_petition(800,852), //高雄
                  // '連江縣' =>  $this->petition_model->get_petition(209,212,
                  // '澎湖' =>  $this->petition_model->get_petition(880,885),
                  // '金門' =>  $this->petition_model->get_petition(890,896),
                  'pingtung' =>  $this->petition_model->get_petition(900,947),
                  'hualien' =>  $this->petition_model->get_petition(970,983),
                  'taitung' =>  $this->petition_model->get_petition(950,966),
                  'islands' => $islands
                 );
}
    $this->load->view('petition_page',$data);

}
function add()  {
    $this->load->model('petition_model');
    $this->petition_model->add_random_member();
}

}
/* End of file petition.php */
/* Location: ./application/controllers/petition.php */