<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	/*public function index()
	{
		$this->load->view('welcome_page');
	}*/

	public function index() {
	  $this->load->model('task_model');
   	  $task_result = $this->task_model->get_all_task('task.id','desc','taipei',null,null,null,5);

	  $this->load->helper('address_format1_helper'); //地址只顯示到區

	  $numMarkers = $this->task_model->count_task_by_type('0');//實體任務數量

	  $this->load->view('welcome_page', Array(
									"numMarkers" => $numMarkers,
									"task_result" => $task_result
									));
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
