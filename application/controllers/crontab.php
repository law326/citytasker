<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * hosting上的排程任務
 */
class Crontab extends MY_Controller {

    function check_task_deadline(){
        $this->load->model('crontab_model');
        $this->crontab_model->check_task_deadline();
    }

    //測試crontab
    // function mailto_admin() {
    //     $list = array('bobo52310@gmail.com','bobo52310@yahoo.com.tw','ctforfb@gmail.com','ctseefb@gmail.com','jejforct@gmail.com','jejgofb@gmail.com');
    //     foreach ($list as $list) {
    //         $this->load->library('email');
    //         $this->email->from('admin@citytasker.tw', 'Citytasker');
    //         $this->email->to($list);
    //         // $this->email->to($list);
    //         // 'admin@citytasker.tw'
    //         $this->email->subject("Test mailto_admin");

    //         $name = "自己寄給自己,現在時間是" .date("Y-m-d H:i:s",time());
    //         $view_data = array('new_password' =>'做測試' ,
    //                            'name' =>$name );
    //         $this->email->message( $this->load->view( 'email/mail_forgotpassword_view', $view_data, true ) );

    //         if ( ! $this->email->send()) {
    //             error_log('Error email 寄信失敗, mailto=' .$list ,1,"bobo52310@gmail.com","From: admin@citytasker.tw");
    //         }
    //     }
    // }

    function mailqueue(){
        // exit;
        $this->load->model('mailqueue_model');
        $query = $this->mailqueue_model->get();//取出所有待寄信的資料 ,每次取最多10筆

        if ( ! isset($query)) exit;

        $this->load->library('sendemail');
        foreach ($query as $row) {
            $type = $row->type; //信件類型
            $data = unserialize($row->data);

            $mail_status = $this->sendemail->$type($data, $row->id);
            if ($mail_status) {
                $this->mailqueue_model->update_status($row->id, 'true');
            }
        }
    }

    function daily_report(){
        $day =  date("Y-m-d",strtotime('-1 day')); //昨天報告

        $this->load->model('crontab_model');
        $yesterday_mail = $this->crontab_model->count_mail($day);
        $yester_member = $this->crontab_model->count_member($day);

        $this->load->library('email');
        $this->email->from('admin@citytasker.tw', 'Citytasker');
        $this->email->to('admin@citytasker.tw');
        $this->email->subject("Citytasker 統計報告 " . $day);
        $this->email->message("共計發mail:$yesterday_mail , 新會員:$yester_member");

        if ( ! $this->email->send()) {
            error_log('Error email 寄信失敗, mailto=' .$list ,1,"bobo52310@gmail.com","From: admin@citytasker.tw");
        }
    }

}
