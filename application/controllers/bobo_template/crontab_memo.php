<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<?php
查看記體體使用量：ps -u citytasker -o rss,command
查看CPU記體體使用量：cat /proc/loadavg
查看TCP連線數：netstat -ant | grep :80 | wc -l
查詢http連線數：ps auxwww | grep http | wc

由系統寄出通知信,夾帶附件cron.log
mailx -s "Cron output測試" admin@citytasker.tw < $HOME/db_backups/cron.log

每分鐘觸發
*/1 * * * * wget http://www.citytasker.tw/crontab/check_task_deadline

每個小時檢查是否有 到期任務
0 0-23 * * * wget http://www.citytasker.tw/crontab/check_task_deadline


每個小時觸發寄信
0 0-23 * * * wget http://beta.citytasker.tw/crontab/mailto_admin


每天早晚3點(Taipei時間)備份DB 在db_backups目錄
0 7,19 * * * mysqldump --defaults-file=$HOME/db_backups/beta_db.cnf -u citytasker citytasker_db > $HOME/db_backups/beta_db-`date +\%Y\%m\%d-\%H\%M`.sql 2>> $HOME/db_backups/cron.log

每天早晚3點(Taipei時間)備份DB 在beta_dropbox目錄
0 7,19 * * * mysqldump --defaults-file=$HOME/db_backups/beta_db.cnf -u citytasker citytasker_db > $HOME/db_backups/citytasker_db-`date +\%Y\%m\%d-\%H\%M`.sql 2>> $HOME/db_backups/cron.log
0 7,19 * * * mysqldump --defaults-file=$HOME/db_backups/beta_db.cnf -u citytasker citytasker_db > /home/citytasker/Dropbox/beta_dropbox/citytasker_db-`date +\%Y\%m\%d-\%H\%M`.sql 2>> $HOME/webapps/beta/cron.log

SQL Debug用
SELECT * FROM `logs` WHERE time>'2013-07-29 14:50:28' AND time <'2013-07-29 14:54:28'


?>