<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<?php
/**
 * bobo's memo
 * 基本form注意事項
 * <?=set_value('username')?>
 *
 * <input type="hidden" name="<?= $csrf_name ?>" value="<?= $csrf_value ?>" />
 *
 * $this->session->set_flashdata('postdata', $_POST); // 表單要重填 資料需保留
 *
 * Form有哪些欄位
 * id　task_id　giver_id　recipient_id　comment　created_at　score
 * 在 C 驗證流程，表單rule寫在config，若有額外rule需特別處理
 * 避免重複提送表單先不實作，之後JS判斷
 */


?>

<?php require('template/_flashdata_show.php'); ?>

<form id="contact-form" action="<?= base_url("user/signup_ing");?>" method="post" accept-charset="utf-8">
   <fieldset>
      <h2 class="form-signup-heading">註冊</h2>
      <input type="text" class="input-block-level" placeholder="使用者名稱"
      name="username" value="<?= html_entity_decode(set_value('username', $result->username))?>">

      <input type="text" class="input-block-level" placeholder="Email"
      id="email" name="email" value="<?= html_entity_decode(set_value('email', $result->name))?>">

      <input type="text" class="input-block-level" maxlength="3" placeholder="郵遞區號 (3碼)"
      name="zip" value="<?= html_entity_decode(set_value('zip', $result->zip))?>">

      <textarea rows="3" class="span12" name="content_private" ><?= html_entity_decode(set_value('content_private', $result->content_private))?></textarea>

      <input type="checkbox" name="jobway[]" value="1" <?= set_checkbox('jobway', '1');?> <? if ($result->jobway1 == 1) echo "checked"; ?> >步行
      <input type="checkbox" name="jobway[]" value="2" <?= set_checkbox('jobway', '2');?> <? if ($result->jobway2 == 1) echo "checked"; ?> >自行車

      <input type="password" class="input-block-level" placeholder="密碼 (至少6字元)" name="password">
      <input class="btn btn-large btn-block" type="submit" value="送出" onclick="this.disabled=true;this.value='提交中...';this.form.submit();" />
      <hr>
      <button class="btn btn-large btn-block btn-primary" type="button">連結 facebook</button>
      <hr>
      <p>按下<strong>"免費註冊"</strong>後，即表示你已經同以本服務之<strong><a href="<?= site_url("user/terms"); ?>">使用條款</a></strong>，並將遵守規定。</p>

      <?php //因為有開啟CSRF
            $CI =& get_instance();
            $csrf_name = $CI->security->get_csrf_token_name();
            $csrf_value = $CI->security->get_csrf_hash();
      ?>
      <input type="hidden" name="<?= $csrf_name ?>" value="<?= $csrf_value ?>" />
      <input type="hidden" name="former_URL" value="<?= $this->uri->uri_string(); //所在頁面?>" />


      <?php //避免重複提送表單
      $resubmit_code = 'not yet';
      $_SESSION['resubmit_code'] = $resubmit_code;
      ?>
      <input type="hidden" name="resubmit_code" value="<?=$resubmit_code?>">
   </fieldset>
</form>