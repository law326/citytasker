<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//若fbci無法登入時 改用此試試看,差別在於底下這行parse_str()
parse_str($_SERVER['QUERY_STRING'], $_REQUEST); //若getUser()為0可以加入此行
require_once APPPATH.'libraries/facebook/facebook.php';
class Fbci2 extends MY_Controller {
    private $former_URL;
    private $message_info = array();

// function __construct(){
//     parent::__construct();
// }

// function index(){
//   $this->load->view('main');
// }
function welcome(){
    $this->load->view('debug_view');
}
function logout(){
    session_destroy();
    redirect(base_url("/"));
}
//  Admin/debug 用 直接存FB userID 測試
function debug(){
    $this->former_URL = implode('/', func_get_args());
    $base_url = $this->config->item('base_url');//從 config.php 取得 baseurl、appID、appSecret

    //建立facebook物件
     $facebook = new Facebook(array(
                          'appId'   => $this->config->item('appID'),
                          'secret'=> $this->config->item('appSecret'),
                          ));

    $user_id = $facebook->getUser(); // 取得使用者id 如100000090192760 授權過才有

    $me = $facebook->api('/me', array('fields' => 'id, email, name, username,
                             gender, birthday, education, location, languages, work,
                              picture.height(144).width(144)'));

    $data = array('temp' => $user_id,
                  'temp2' => serialize($me),
                  'user_agent' => $this->input->user_agent(),
                  'created_at' => date('Y-m-d H:i:s')
                  );
    $this->db->insert('debug', $data);

      $CI =& get_instance();
       $CI->load->library('email');
       $CI->email->from('admin@citytasker.tw', 'Citytasker');
       $CI->email->to('bobo52310@gmail.com');
       $CI->email->subject('debug頁面');
       $message = array('giver_name' =>$this->input->ip_address(),
                        'giver_img' =>'',
                        'recipient_id' => $this->input->user_agent(),
                        'task_name' =>$data['temp'],
                        'task_id' =>$data['temp2']
                         );
       $CI->email->message( $CI->load->view( '/email/mail_get_credit_view', $message, true ) );

       if ( ! $CI->email->send()) {
           error_log('Error email 寄信失敗, Sendemail_id='.$queue_id,1,"bobo52310@gmail.com","From: admin@citytasker.tw");
              return FALSE;
         }

         //避免輸出中文亂碼
         header("Content-Type:text/html; charset=utf-8");
         echo "已通知Citytaser, 請靜待Citytasker工程師處理~ 謝謝";

}
function fblogin(){
    $this->former_URL = implode('/', func_get_args());
    $base_url = $this->config->item('base_url');//從 config.php 取得 baseurl、appID、appSecret

    //建立facebook物件
     $facebook = new Facebook(array(
                          'appId'	=> $this->config->item('appID'),
                          'secret'=> $this->config->item('appSecret'),
                          ));

	$user_id = $facebook->getUser(); // 取得使用者id 如100000090192760 授權過才有
    if($user_id == 0) redirect(base_url($this->former_URL)); //若未登入

	try{
        $this->load->model('fbci_model');
        if ($this->fbci_model->is_member_by_fbid($user_id) != 1) {
            //新加入會員，是否註冊過是看FB的id是否存在
           $this->_addMemberFromFB($facebook);
        }

        // 取得註冊的email (FB上的email可變動非唯一)
        $fb_email = $this->fbci_model->get_member_by_fbid($user_id, 'fb_email')->fb_email;

        $this->load->model('user_model');
        $this->user_model->set_login_session($fb_email); //取得本站session

        $get_items = "zip,phone"; //檢查是否填寫這些
        $user = $this->user_model->get_member_by_email($fb_email, $get_items);
        $this->_checkZipPhone($user);

        redirect(base_url($this->former_URL));

    }catch(FacebookApiException $e){
        error_log($e.$this->uri->uri_string(),1,"admin@citytasker.tw","From: admin@citytasker.tw");
        $user_id = NULL;
    }
}

private function _addMemberFromFB($facebook){
    $this->message_info = array_merge($this->message_info, (array)'歡迎你的加入！這是你初次登入，請進行基本資料設定。');
    $me = $facebook->api('/me', array('fields' => 'id, email, name, username,
                             gender, birthday, education, location, languages, work,
                              picture.height(144).width(144)'));

    $member_id = $this->fbci_model->add_member($me);
    if ($member_id !== FALSE) {
        $this->load->model('user_model');

        $language = $this->_getMeLanguage($me);
        $education = $this->_getMeEducation($me);
        $work = $this->_getMeWork($me);
        $data = array('member_id' => (int)$member_id,
                      'language' => $language,
                      'education' => $education,
                      'work' => $work
              );
        $this->user_model->add_vitae($member_id, $data);
    }
}
private function _getMeEducation($me){
    if (isset($me['education'])) {
        return (end($me['education'])['school']['name']);
    }
}
private function _getMeLanguage($me){
    if (isset($me['languages'])) {
        foreach ($me['languages'] as $languages) {
            $language = $language .$languages['name'] .',';
        }
        return rtrim($language, ',') ;
    }
    return null;
}
private function _getMeWork($me){
    if (isset($me['work'])) {
        foreach ($me['work'] as $me_work) {
            $work = $work .$me_work['employer']['name'] .',';
        }
        return rtrim($work, ',') ;
    }
    return null;
}

private function _checkZipPhone($user){
    // NOTE:可以寫成Library 方便各個controller使用
    if (empty($user->zip)) {
        $this->message_info = array_merge($this->message_info, (array)'請填寫你的郵遞區號。');
        $this->former_URL = 'user/settings';
    }
    if (empty($user->phone)) {
        $this->message_info = array_merge($this->message_info, (array)'請填寫手機號碼，通過手機驗證後才可以發布或出任務喔。');
        $this->former_URL = 'user/settings';
    }

    $this->session->set_flashdata('info', $this->message_info);
}

private function temp_code(){
/*
        $params = array('next' => $base_url.'fbci/logout');
        $ses_user = array('User' => $me_profile,
                        'logout' => $facebook->getLogoutUrl($params)   //產生 FB logout url
                        );

        $_SESSION['fblogin'] = $ses_user;
*/

/*
            // 取得朋友清單
            $result = $facebook->api('/me/friends');
            exit;
            // 取得朋友清單 End


            // 取得塗鴉牆最近25筆訊息
            $ret_obj = $facebook->api('/me/feed');
            exit;
            // 取得塗鴉牆最近25筆訊息


            // 發布訊息到塗鴉牆
            $ret_obj = $facebook->api('/me/feed', 'POST',
                                        array(
                                          'link' => 'citytasker.tw',
                                          'message' => '好網站Posting with the PHP SDK!'
                                     ));
            echo '<pre>Post ID: ' . $ret_obj['id'] . '</pre>';
            exit;
            // 發布訊息到塗鴉牆 End

        */

}
}

/* End of file fbci2.php */
/* Location: ./application/controllers/fbci2.php */
