<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * spider 爬取FB社團資料
 */
class Spider extends MY_Controller {
    function index($offset = 0) {
        $this->_checkLoggedIn();
        if ( ! ($_SESSION['member_id'] != '1' OR $_SESSION['member_id'] != '2')) $this->_toRedirectBack('', (array)'無權限.');

        $this->load->model('spider_model');

        $pageSize = 50;
        $all_results = $this->spider_model->get_all($offset, $pageSize);

        $this->load->library('pagination');
        $count_all_user = $this->db->count_all('spider');

        $config['base_url'] =  base_url('/spider/');
        $config['total_rows'] = $count_all_user;
        $config['per_page'] = $pageSize;

    // $config['num_links'] = 5;
        $this->pagination->initialize($config);

        $count_del = (int)$this->spider_model->count('del'); //已刪除數量
        $count_post = (int)$this->spider_model->count('post'); //已發布數量
        $count_todo = $this->spider_model->count()-$count_del-$count_post; //待處理

        $data = array('noRobotIndex' => 'noRobotIndex',
                      'all_results' => $all_results,
                      'count_todo' => $count_todo,
                      'pageLinks' => $this->pagination->create_links()
                      );
        $this->load->view('spider_view', $data);
    }

function post(){
 $id = (int)$this->uri->segment(3);
   //讀出該任務資料，並截取出email

 $this->load->model('spider_model');
  $task = $this->spider_model->get_spider($id); // 取得任務詳細資訊

  $dummy_id = $this->config->item('dummy_id');//從 config.php 取得 假帳號id
  $dummy_id = $dummy_id[array_rand($dummy_id)];

   $this->load->model('user_model');
   $dummy_name = $this->user_model->get_user($dummy_id);
$_SESSION['dummy_id'] = $dummy_id;

    $data = array('noRobotIndex' => 'noRobotIndex',
                  'dummy_id' => $dummy_id,
                  'dummy_name' => $dummy_name->name,
                  'task_result' => $task
                      );
    $this->load->view('spider_post_view', $data);
}
function post_ing(){
      $former_URL = $this->input->post('former_URL');
      $this->_checkFormRules($former_URL);

      $this->load->model('task_model');

      $insert_id = $this->task_model->add_task($this->input->post('member_id'));
      if ($insert_id == FALSE) {
        $this->_toRedirectBack('task/post',(array)'新增任務失敗');
      }
      redirect(base_url("task/" . $insert_id));// 新增完成,進入任務頁面

}
//案主從報名信件mail_make_offer_by_spider_view.php點回來
function poster(){
      $task_id = (int)$this->uri->segment(3);
      $this->load->model('task_model');
      $title = $this->task_model->get_task_title($task_id);

      if (isset($task_id)) {
          //寫入cookie 表示為案主
          if (isset($_COOKIE['poster_task_id'])) {
              $cookie_arr = json_decode($_COOKIE['poster_task_id'],TRUE);
              $cookie_arr_title = json_decode($_COOKIE['poster_task_title'],TRUE);

              $is_exist = in_array($task_id, $cookie_arr);
              if ( ! $is_exist) {
                array_push($cookie_arr, $task_id); // 不存在才寫入
                array_push($cookie_arr_title, $title); // 不存在才寫入
                setcookie("poster_task_id",json_encode($cookie_arr), time()+60*60*24*30 ,'/');
                setcookie("poster_task_title",json_encode($cookie_arr_title), time()+60*60*24*30 ,'/');
              }
          }else{
              setcookie("poster_task_id",json_encode((array)$task_id), time()+60*60*24*30 ,'/' );
              setcookie("poster_task_title",json_encode((array)$title), time()+60*60*24*30 ,'/' );
          }

           //更改spider_posted狀態 表示有點回來過
          $task_id = $task_id;//$this->input->post('task_id');

          $this->db->select("hit_count");
          $this->db->from('spider_posted');
          $this->db->where('task_id',$task_id);
          $query = $this->db->get();
          $hit_count = $query->row()->hit_count + 1;

          $data = array('hit_time' => date('Y-m-d H:i:s'),
                        'hit_count' => $hit_count, );
          $this->db->where('task_id',$task_id);
          $this->db->update('spider_posted', $data);
      }
      $this->session->set_flashdata('info', array('您是案主本人嗎?'));
      redirect(base_url("task/" . $task_id));// 進入任務頁面
}

//案主點選關連任務
function poster_connect(){
      $this->_checkLoggedIn();
      // 開始關連任務(修改task memer_id)
      $this->load->model('spider_model');
      $member_id = $_SESSION['member_id'];
      foreach (json_decode($_COOKIE['poster_task_id']) as $task_id) {
          $title = $this->spider_model->poster_connect($task_id, $member_id);
      }

      // 刪掉cookie
      setcookie("poster_task_id",null,time()-60*60,'/');
      setcookie("poster_task_title",null,time()-60*60,'/');

      // 寄信通知管理者
      $this->load->library('email');
      $this->email->from('admin@citytasker.tw', 'Citytasker');
      $this->email->to('admin@citytasker.tw');
      $this->email->subject("原案主回來了! " . $member_id);
      $this->email->message("原案主成為新會員ID=:".$member_id);
      if ( ! $this->email->send()) {
          error_log('Error email 寄信失敗, mailto=' .$list ,1,"bobo52310@gmail.com","From: admin@citytasker.tw");
      }

      $this->session->set_flashdata('info', array('您的案件已經關連~'));
      redirect(base_url("user/posted"));// 發案記錄
}

// 撈FB資料並存在DB
function load(){
  $this->_checkLoggedIn();
  header("Content-Type:text/html; charset=utf-8");

  // if ( ! ($_SESSION['member_id'] != '1' OR $_SESSION['member_id'] != '2')) $this->_toRedirectBack('', (array)'無權限.');

  $page_id = $this->uri->segment(3); //取得社團id

  //載入全部社團
  if ($page_id == 999) {
      $all_club = array('Our work' => '192414784133675',
                        '打工丸' => '162352643817753',
                        '打工狂熱份子' => '315063158519102',
                        '奇創管理' => '194156780626450',
                        '李奧人力' => '180818658634248',
                        '全台最大' => '214666455289762',
                        '大高雄' => '302059333175325',
                        '中部' => '294966743901233',
                        '打工一族' => '179588052178713',
                        '大高屏' => '363744657026806',
                        '打工達人' => '171052553031273',
                        '打工狂' => '409700449076103'
                          );
      foreach ($all_club as $key => $value) {
         $load_count = $this->_feed_load($value);
         echo $key. "共收錄 " . $load_count . " 筆";
         echo "</br>";
      }
      exit; //全部跑完就中止
  }


  if ($page_id == 0) $page_id = '192414784133675'; //沒有社團id時

  if ( ! is_numeric($page_id)) exit; // id不是數字,有問題

  $load_count = $this->_feed_load($page_id);

  echo "共收錄" . $load_count . "筆";
}

/*
開始撈取資料
 */
private function _feed_load($page_id){
  require_once APPPATH.'libraries/facebook/facebook.php';
  $base_url = $this->config->item('base_url');//從 config.php 取得 baseurl、appID、appSecret

  //建立facebook物件
  $facebook = new Facebook(array(
                           'appId'   => $this->config->item('appID'),
                           'secret'=> $this->config->item('appSecret'),
                           ));

  $user_id = $facebook->getUser(); // 取得使用者id 如100000090192760 授權過才有
  if($user_id == 0) redirect(base_url('/service/forrunner'));
  try{
      $permissions = $facebook->api("/me/permissions");
      if( !array_key_exists('user_groups', $permissions['data'][0])) {
          // 若沒有此token 則取得授權
          header( "Location: " . $facebook->getLoginUrl(array("scope" => "user_groups")) );
      }

      $page_info = $facebook->api("$page_id/feed"); //?limit=5

      $this->load->model('spider_model');

      $feed_ok = 0; //符合的有幾筆
      foreach ($page_info[data] as $feed) {
          // 收錄原則:
          // 1.判斷此筆任務是否已在此社團(from_id)：檢查(tid)與內容(message)是否有重複
          $feed_exist = $this->spider_model->is_feed_exist($feed[id], $feed[message]);
          $mid = $feed[from][id];
          // echo "<a href='https://www.facebook.com/$mid'>".$feed[from][name].'</a> ';

          // echo ($feed_exist) ? '曾收錄過 ' : '未收錄過 ' ;


          // 2.比對 string 是否含 email ，只撈有email的
          $this->load->helper('str_preg_match_email_helper');
          $have_email = str_preg_match_email($feed[message]);

          // 3.此任務是否在其它社團發布：依內容(message)來群組(Group by)
          // TODO:未實作

          $feed_time = date("Y-m-d H:i:s",strtotime($feed[created_time]));
          //可使用group by 來對message分類
          if ($feed_exist == FALSE AND $have_email == TRUE) {
              $insert  = array('from_id' => $feed[to][data][0][id],
                                 'from_name' => $feed[to][data][0][name],
                                 'read' => '',
                                 'tid' => $feed[id],//任務Facebook id
                                 'name' => $feed[from][name],
                                 'mid' => $feed[from][id],
                                 'message' => $feed[message],
                                 'email' => $have_email[0][0],
                                 // 'picture' => $status,
                                 // 'link' => $status,
                                 'created_time' => $feed_time,
                                 'created_at' => date('Y-m-d H:i:s')
                                 );

              // 寫入DB
              $this->load->model('spider_model');
              $this->spider_model->add_spider($insert); //回傳加入幾筆
              $feed_ok = $feed_ok + 1;
          }
      } // endforeach
      return $feed_ok;

  }catch(FacebookApiException $e){
      // error_log($e.$this->uri->uri_string().'_feed_load error',1,"bobo@citytasker.tw","From: admin@citytasker.tw");
      $user_id = NULL;
  }
}
function delete_show($offset = 0){
      $this->_checkLoggedIn();
      if ( ! ($_SESSION['member_id'] != '1' OR $_SESSION['member_id'] != '2')) $this->_toRedirectBack('', (array)'無權限.');

      $this->load->model('spider_model');

      $pageSize = 50;
      $all_results = $this->spider_model->get_all($offset, $pageSize, 'del');

      $this->load->library('pagination');

      $config['base_url'] =  base_url('/spider/delete_show');
      $config['total_rows'] = count($all_results);
      $config['per_page'] = $pageSize;

  // $config['num_links'] = 5;
      $this->pagination->initialize($config);

      $count_del = $this->spider_model->count('del');

      $data = array('noRobotIndex' => 'noRobotIndex',
                    'all_results' => $all_results,
                    'count_del' => $count_del,
                    'pageLinks' => $this->pagination->create_links()
                    );
      $this->load->view('spider_del_view', $data);

}
function done_show($offset = 0){
      $this->_checkLoggedIn();
      if ( ! ($_SESSION['member_id'] != '1' OR $_SESSION['member_id'] != '2')) $this->_toRedirectBack('', (array)'無權限.');

      $this->load->model('spider_model');

      $pageSize = 5;
      $all_results = $this->spider_model->get_post($offset, $pageSize);

      $this->load->library('pagination');

      $config['base_url'] =  base_url('/spider/done_show');
      $config['total_rows'] = count($all_results);
      $config['per_page'] = $pageSize;

  // $config['num_links'] = 5;
      $this->pagination->initialize($config);

      $count_del = $this->spider_model->count('post');

      $data = array('noRobotIndex' => 'noRobotIndex',
                    'all_results' => $all_results,
                    'count_del' => $count_del,
                    'pageLinks' => $this->pagination->create_links()
                    );
      $this->load->view('spider_done_view', $data);

}

//測試是否有相同的任務
function same_show($offset = 0){
     $this->_checkLoggedIn();
     if ( ! ($_SESSION['member_id'] != '1' OR $_SESSION['member_id'] != '2')) $this->_toRedirectBack('', (array)'無權限.');

     $this->db->select("*, count(*)");
     $this->db->from('spider');
     // $this->db->where('tid',$tid); // tid存在
     $this->db->group_by("tid");

     // $this->db->where('created_at >', date("Y-m-d H:i:s",time()-2592000)); // 一個月內

     $query = $this->db->get();
    //========== 列出欄位名稱與row值 ========
    header("Content-Type:text/html; charset=utf-8");
    $newObj = new ArrayObject($query->list_fields()); //先複製一份list_fields因讀取一次就消失
    $new_list_fields = $newObj->getArrayCopy();

    foreach ($new_list_fields as $key) { //列出欄位名稱
        echo $key . '　';
    }
    echo "<br/>";

    foreach ($query->result_array() as $row) { //讀出row值
      foreach ($new_list_fields as $key) {
          echo $row[$key] .'　' ;
      }
      echo "<br/>";
      echo "<br/>";

    }

    echo  'row數量' . $query->num_rows() . "<br/>"; //列出查詢筆數
    echo  '欄位數量' . count($new_list_fields);
    exit;
    //========== 列出欄位名稱與row值 End ========

}
//AJAX人工審核 變更任務狀態
function ajax_read(){
    $tid = $this->input->post('tid');
    $status = $this->input->post('status');
    if ( ! isset($tid)) exit;

    $data = array('read' => $status);
    $this->db->where('tid',$tid);
    $this->db->update('spider', $data);

    //新增至 spider_posted
    if ($status == 'post') {

      //搜尋task最上層那一筆 假帳號的id 取得剛發佈的task_id
      $dummy_id = $this->config->item('dummy_id');//從 config.php 取得 假帳號id

      $this->db->select("id, member_id");
      $this->db->from('task');
      $this->db->where_in('member_id',$dummy_id);
      $this->db->order_by("id", "desc"); //由大到小排序

      $query = $this->db->get();
      $task_id = $query->row()->id;

      $insert = array('tid' => $tid,
                      'task_id' => $task_id, //關聯到發佈的任務id
                      'created_at' => date('Y-m-d H:i:s')
                      );
      $this->db->insert('spider_posted', $insert);
    }

    if ($this->db->affected_rows() == 1) echo "yes";
}

}

/* End of file Spider.php */
/* Location: ./application/controllers/Spider.php */
