<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * AJAX對應到此
 */
class Ajax extends MY_Controller{
protected $isAjax;

function __construct(){
    parent::__construct();

    //確認請求來源的確是由AJAX
    if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $this->isAjax = true;
    } else {
        $this->isAjax = false;
        redirect(base_url("/error_404"));
    }
}
/**
 * [判斷此email是否為會員]
 * @param  [type] $id [description]
 * @return [type]     [description]
 */
function check_email(){
    $this->load->model('user_model');
    $is_member = $this->user_model->get_id_by_email($this->input->post('email'));

    if (isset($is_member)) {
        echo "1";
    }else{
        echo "0";
    }
}

/**
 * 檢查此member是否為FB粉絲
 * @return [type] [description]
 */
function check_fb_fans(){
    require_once APPPATH.'libraries/facebook/facebook.php';
    $base_url = $this->config->item('base_url');//從 config.php 取得 baseurl、appID、appSecret
    $facebook = new Facebook(array(
                          'appId'   => $this->config->item('appID'),
                          'secret'=> $this->config->item('appSecret'),
                          ));

    $user_id = $facebook->getUser(); // 取得使用者id 如100000090192760 授權過才有

    if($user_id == 0) redirect(base_url($former_URL)); //若未登入

    try{
        $likes = $facebook->api("/me/likes/633260660022632");
        if( !empty($likes['data']) ){
           echo "yes";
        }
        else {
           echo "no";
        }
    }catch(FacebookApiException $e){
        error_log($e.'ajax_error check_fb_fans',1,"bobo@citytasker.tw","From: admin@citytasker.tw");
        $user_id = NULL;
    }
}

/**
 * 來自 "我要出任務" 跑者索取offer
 * @param  [type] $runner_id [description]
 * @param  [type] $task_id   [description]
 * @return [type]            [description]
 */
function make_offer(){
    $poster_id = $this->input->post('poster_id');
    $task_id = $this->input->post('task_id');

    $this->load->model('task_model');
    $to_mail = $this->task_model->get_contact_email($task_id);
    if ($to_mail == FALSE) {
        // 此案沒有連絡信箱 只好寄給發案者信箱
        $this->load->model('user_model');
        $to_mail = $this->user_model->get_email_by_id($poster_id);
    }

    $runner_id = $this->input->post('runner_id');
    $task_name = $this->input->post('task_name');

    $this->load->model('offer_model');
    if ($this->offer_model->is_offered($runner_id, $task_id)){
        echo "已申請過";
    };
    if ($this->offer_model->add_offer($runner_id, $task_id) == FALSE){
        echo "申請發生錯誤";
    };
    $this->session->set_flashdata('info', (array)'獲得新任務! 系統已通知案主，建議您可主動與案主聯繫~');

    // 指派人數達上限後,該任務仍可報名,但不會寄信通知案主
    // if ((int)$this->input->post('people') > (int)$this->input->post('count_runner') ){
        // 寄信加入mail queue
        $data = array('to_mail' => $to_mail,
                       'runner_name' => $_SESSION['member_name'],
                       'runner_id' => $_SESSION['member_id'],
                       'runner_img' => $_SESSION['member_img'],
                       'task_name' => $task_name,
                       'task_id' => $task_id
         );

        // 爬蟲 判斷此發案者是否為 假帳號id
        $dummy_id = $this->config->item('dummy_id');//從 config.php 取得 假帳號id
        $is_dummy_id = in_array($poster_id, $dummy_id);
        if ($is_dummy_id) {
            // 假帳號發案者
            $this->load->model('mailqueue_model');
            $task_content = $this->input->post('task_content');
            $data['task_content'] = $task_content;
            $this->mailqueue_model->add_mailqueue('make_offer_by_spider', $data);
        } else {
            //一般發案者
            $this->load->model('mailqueue_model');
            $this->mailqueue_model->add_mailqueue('make_offer', $data);
        }

    // }

    echo "已提出申請";
}
/**
 * 雇用 發給跑者offer
 * @param  [type] $runner_id [description]
 * @param  [type] $task_id   [description]
 * @return [type]            [description]
 */
function give_offer(){
    $runner_id = $this->input->post('runner_id');
    $this->load->model('user_model');
    $to_mail = $this->user_model->get_email_by_id($runner_id);
    $task_id = $this->input->post('task_id');
    $task_name = $this->input->post('task_name');

    $this->load->model('offer_model');
    if ($this->offer_model->response_offer($runner_id, $task_id) == FALSE){
        echo "指派發生錯誤";
    };

    // 寄信加入mail queue
    $data = array('to_mail' => $to_mail,
                 'poster_img' => $_SESSION['member_img'],
                 'poster_name' => $_SESSION['member_name'],
                 'task_id' => $task_id,
                 'task_name' => $task_name
    );
    $this->load->model('mailqueue_model');
    $this->mailqueue_model->add_mailqueue('give_offer', $data);
    echo "指派成功";
}

function post_fb_board(){
    // 改用JS回傳到塗鴉牆，為了讓使用者有確認的視窗
}

}
/* End of file ajax.php */
/* Location: ./application/controllers/ajax.php */