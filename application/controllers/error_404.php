<?php
class Error_404 extends CI_Controller{
  public function __construct()  {
    parent::__construct();
  }

  public function index()  {
    $data = array('noRobotIndex' => 'noRobotIndex' );
    header('HTTP/1.1 404 Not Found'); //傳送404碼 避免造成假性404
    $this->load->view('error_404_page', $data);
  }
}

/* End of file error_404.php */
/* Location: ./application/controllers/error_404.php */
