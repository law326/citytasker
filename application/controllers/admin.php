<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 管理者後台
 */
class Admin extends MY_Controller {
    function __construct(){
        parent::__construct();
        $this->_checkLoggedIn();
        if ($_SESSION['member_id'] == '1' OR $_SESSION['member_id'] == '2') {

        }else{
            $this->_toRedirectBack('', (array)'無權限.');
        }
    }
    function alltask(){
            $this->load->model('task_model');

            $result = $this->task_model->get_all_task_admin();
            $count_all_task = $this->db->count_all('task');

            $data = array('noRobotIndex' => 'noRobotIndex',
                          'result' => $result,
                          'count_all_task' => $count_all_task
                          );
            $this->load->view('all_task_page', $data);
    }

    function index($offset = 0) {
            $this->load->model('admin_model');
            $pageSize = 20;
            $all_results = $this->admin_model->get_all_user($offset, $pageSize);
        // $count_all_user = $this->admin_model->count_all_user();
            $count_all_user = $this->db->count_all('member');

            // $this->db->limit($pageSize, $offset);
            // $this->db->order_by("task.created_at", "desc"); //由大到小排序
            // $query = $this->db->get();

            $this->load->library('pagination');
        // $this->load->config('pagination');
        // $config['uri_segment'] = 2;

            $config['base_url'] =  base_url('/admin/');
            $config['total_rows'] = $count_all_user;
            $config['per_page'] = $pageSize;

        // $config['num_links'] = 5;
            $this->pagination->initialize($config);

            $data = array('noRobotIndex' => 'noRobotIndex',
                          'all_results' => $all_results,
                          'count_all_user' => $count_all_user,
                          'pageLinks' => $this->pagination->create_links()
                          );
            $this->load->view('all_view', $data);
    }

/**
 * 切換使用者身分
 */
    function session() {



            $id = $this->uri->segment(3);

            $this->db->select("id, name, email, zip, img");
            $this->db->from('member');
            $this->db->where('id', $id);

            $query = $this->db->get();
            $row = $query->row();

            if ($query->num_rows() == 1) {
              $_SESSION['member_id'] = $row->id;
              $_SESSION['member_name'] = $row->name;
              $_SESSION['member_email'] = $row->email;
              $_SESSION['member_img'] = $row->img;
              $_SESSION['member_zip'] = $row->zip;
            }else{
                echo "error";
            }
            redirect(base_url());

    }

    function debug(){
        $data = array('noRobotIndex' => 'noRobotIndex'
                      );
        $this->load->view('debug_view', $data);
    }

    function zip_geo(){
        $this->load->model('task_model');
        $task_results = $this->task_model->get_all_task('task.id','desc', 0);
    //避免輸出中文亂碼
        header("Content-Type:text/html; charset=utf-8");

        $this->load->helper('address_to_area_helper');
        foreach ($task_results as $task) {
            echo $task->id . ' | ' . $task->title . ' | ' .$task->address . ' | ' .address_to_area($task->address);
            echo '</br>';
        // $this->db->where('id', $task->id);
        // $data = array('area' => address_to_area($task->address) );
        // $this->db->update('task', $data);
        }
    }

    function runner_ranking(){
        $this->load->model('task_model');
        $task_results = $this->task_model->get_all_task('task.id','desc', 0);
    //避免輸出中文亂碼
        header("Content-Type:text/html; charset=utf-8");

        $this->load->helper('address_to_area_helper');
        foreach ($task_results as $task) {
            echo $task->id . ' | ' . $task->title . ' | ' .$task->address . ' | ' .address_to_area($task->address);
            echo '</br>';
        // $this->db->where('id', $task->id);
        // $data = array('area' => address_to_area($task->address) );
        // $this->db->update('task', $data);

        }
    }

    //算出任務總額
    // $this->db->select("id, title , people, price, member_id");
    // $this->db->from('task');
    // $data = array('people' => 0,
    //               'price' => 0     );
    // $this->db->where_not_in($data);
    // $this->db->or_where_not_in('member_id',['1795','614','577']);
    // $query = $this->db->get();
    // $sum;
    // foreach ($query->result() as $key) {
    //     $sum = $sum + (int)$key->people * (int)$key->price;
    //     echo $key->member_id . $key->title."  $key->people x $key->price =" . (int)$key->people * (int)$key->price ;
    //     echo "</br>";
    //     }
    // echo $sum;
    //算出任務總額 End

// 檢查server目前的CPU負載
// function server(){
//     if($fp = @fopen('/proc/loadavg', 'r')) {
//         list($loadaverage) = explode(' ', fread($fp, 6));
//         fclose($fp);

//         if($loadaverage > 1) {
//             header("HTTP/1.0 503 Service Unavailable");
//             echo 'server die oops!';
//         }
//         echo '<pre>';
//         var_dump($loadaverage);
//         echo '</pre>';
//     }
// }

// 以發案者身分,向有留言過的人發信
// function mail_to_discuss(){
//     $giver_name = '蔡宥樂';
//     $giver_img = '614_20130814160855.jpg';
//     $discuss_text = '此任務已修改內容唷~ 請再次確認';
//     $task_name = '廣告素人';
//     $task_id = '62';

//     $this->load->model('discuss_model');
//     $mail_list = $this->discuss_model->get_alluser_mail($task_id);
//     $poster_mail = 'k0603k0603@yahoo.com.tw';
//     foreach ($mail_list as $to_mail) {
//         // 寄信加入mail queue
//         if ($to_mail['email'] == $poster_mail) continue; //跳過發案者本身
//         $data = array('to_mail' => $to_mail['email'],
//                        'giver_name' => $giver_name,
//                        'giver_img' => $giver_img,
//                        'discuss_text' => $discuss_text,
//                        'task_name' => $task_name,
//                        'task_id' => $task_id
//          );
//         $this->load->model('mailqueue_model');
//         $this->mailqueue_model->add_mailqueue('task_discuss', $data);
//     }
// }


}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */
