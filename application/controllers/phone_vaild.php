<?php if ( !defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );
/**
 * 手機簡訊認證程序
 */
class Phone_vaild extends MY_Controller {
    public function __construct() {
        parent::__construct();
        if ( is_logged_in() == FALSE ) {
            $this->session->set_flashdata( 'error', array( '請登入後再進行手機驗證!</br>請至設定->手機認證' ) );
            redirect( base_url( "/service/forposter" ) );
        }
    }
    public function index() {
        // 檢查若還沒有手機號碼,代表尚未驗證過,才允許進入
        $this->load->model( 'user_model' );
        $get_items = "id,phone";
        $query = $this->user_model->get_member_by_email( $_SESSION['member_email'], $get_items );

        if ( ! empty( $query->phone ) ) {
          $this->_toRedirectBack('error_404',(array)'你已經設定過手機號碼');
        }

        // 若有申請過驗證碼,引導到 進行驗證頁面
        //                  若無到 申請註冊碼頁面
        $this->load->model( 'phone_model' );
        $get_items_phone = "*";
        $query_phone = $this->phone_model->get_phone_vaild_by_id( $query->id, $get_items_phone );

        // 檢查id 看是否有申請過
        if ( $query_phone ) {
            // 已經有申請過註冊碼了
            $query_phone = (array) $query_phone;
            $query_phone['pageTitle'] ='手機驗證頁';
            //isset($nexmo_id) = TRUE 到進行驗證頁面
            $this->load->view( 'phone_vaild_page', $query_phone );

        }else {
            // 尚未申請驗證碼 到申請註冊碼頁面
            $query_nocode['pageTitle'] ='申請註冊碼';
            $this->load->view( 'phone_vaild_page', $query_nocode );
        }

    }

    /**
     * 申請驗證碼手機認證
     *
     * @return [type] [description]
     */
    public  function phone_vaild_apply() {
        $this->load->library( 'form_validation' );
        //將rules移到config
        // $this->form_validation->set_rules( 'input_phone', '手機號碼', 'trim|xss_clean|required|min_length[10]|max_length[10]|is_natural|callback_validate_resubmit|is_unique[member.phone]' );

        $this->form_validation->set_message( 'is_unique', '手機已重複,曾經申請過' );
        $this->_checkFormRules('phone_vaild');

        // 準備發送簡訊
        $this->load->library( 'nexmo' );
        $this->nexmo->set_format( 'json' );
        $input_phone = $this->input->get_post( 'input_phone', TRUE );
        $input_phone= substr( $input_phone, 1, 10 ); //去掉號碼開頭的0
        $input_phone = '886' . $input_phone;

        $key_sms ='';//六位數簡訊驗證碼
        for ( $i=0; $i < 6; $i++ ) {
            $key_sms = $key_sms .rand( 0, 9 );
        }

        // *******************Text Message**********************
        $from = 'Citytasker';
        $to = $input_phone;
        $message_sms = array(
                             'text' => "你的Citytasker驗證碼為 $key_sms 共六碼數字。祝你使用愉快!Have a nice day! Citytasker.tw"
                             );

        //正式發送簡訊
        $response = $this->nexmo->send_message( $from, $to, $message_sms );

        // $this->nexmo->d_dump($response);
        // echo "</br>測試抓status和id</br>";
        // var_dump($response->messages[0]->status);

        // echo "<h3>Response Code: " . $this->nexmo->get_http_status() . "</h3>";

        // var_dump($response->messages[0]->status); //抓值ok
        // var_dump($response->messages[0]->to);//抓值ok
        // var_dump($response->messages[0]->message-id); //抓值faild
        // var_dump($response->messages[0]->message-price); //抓值faild

        if ( $response->messages[0]->status == 0 ) {
            // 寄送成功(但不保證為有效門號)，準備寫入DB

            $data = array( 'member_id' => $_SESSION['member_id'],
                          'phone' => $this->input->get_post( 'input_phone', TRUE ),
                          'key' => $key_sms,
                          'created_at' => date('Y-m-d H:i:s'),
                          'ip_address' => $this->input->ip_address(),
                          // 'nexmo_id' => $response->messages[0]->message-id,
                          'nexmo_status' => $response->messages[0]->status
                          );

            $this->load->model('phone_model');
            if ( ! $this->phone_model->add_phone_valid( $data ) ) {
                $this->_toRedirectBack('phone_vaild',(array)'寫入資料庫失敗');
            }

            $message['sedn_ok'] = '驗證碼已寄送至門號：' . (string)$this->input->get_post( 'input_phone', TRUE );
            $this->session->set_flashdata( 'info', $message );

        }else {
            $this->_toRedirectBack('phone_vaild',(array)'驗證碼寄送失敗...');
            // 錯誤碼應該要保存
        }

        redirect( base_url( "phone_vaild/" ) );

    }

    /**
     * 判斷user輸入的認證碼
     *
     * @return [type] [description]
     */
    public  function phone_vaild_ing() {
        $this->_checkFormRules('phone_vaild');

        //取得正確的驗證碼
        $this->load->model('phone_model');
        $get_items_phone = "id,phone,key";
        $query_phone = $this->phone_model->get_phone_vaild_by_id($_SESSION['member_id'], $get_items_phone );

        $input_phone = $this->input->get_post( 'input_phone', TRUE );
        if ($query_phone->key !== $input_phone) {
            $this->_toRedirectBack('phone_vaild',(array)'手機驗證失敗');
        }
         // 驗證成功,phone寫入member表
         $this->load->model('user_model');
         $data = array('phone' =>$query_phone->phone );
         $this->user_model->update_ByID($_SESSION['member_id'],$data);

         // 並刪除phone_vaild表的資料
         $data_delete = array('member_id' =>$_SESSION['member_id']);
         $this->phone_model->delete($data_delete);

         $this->session->set_flashdata( 'info', (array)"手機驗證成功") ;
         redirect( base_url( "user/settings/" ) );
    }

/**
 * 檢查餘額剩多少,給自己看
 * @return [type] [description]
 */
public  function get_balance(){

    $this->load->library( 'nexmo' );
    $this->nexmo->set_format( 'json' );
    $money = $this->nexmo->get_balance();
    //避免輸出中文亂碼
    header("Content-Type:text/html; charset=utf-8");
    echo '<pre>';
    var_dump($money);
    echo '</pre>';

}
    /**
     * 檢驗是否為重複提送表單
     *
     * @return [type] [description]
     */
    public function validate_resubmit() {
        if ( isset( $_POST['resubmit_code'] ) ) {
            if ( $_POST['resubmit_code'] == $_SESSION['resubmit_code'] ) {
                $_SESSION['resubmit_code'] ='done';
                return TRUE;
            }else {
                echo " <meta charset='utf-8'> 請勿重複提交表單！";
                return FALSE;
            }
        }

    }

}

/* End of file phone_vaild.php */
/* Location: ./application/controllers/phone_vaild.php */
