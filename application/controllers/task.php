<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 任務相關操作
 */
class Task extends MY_Controller {
  function index() {
      //此class有設定routes
      redirect(base_url("task/map"));
  }
/*
  function __construct()  {
    parent::__construct();
    if (is_logged_in() == FALSE) {
        $this->session->set_flashdata('info', array('快速註冊，免費發案，你還在等什麼？'));
    }
  }
*/
  function post() {

    /* 填好zip才可接任務
    if (is_logged_in() == TRUE) {
      $this->load->model('user_model');
      $this->check_zip();
    }
    */

    $task_result = new stdClass(); // 要先new 一個空物件出來 才不會發生錯誤
    $task_result->content = "執行日期：
執行時間：

工作薪資：/天
發薪時間：
休息時間：
休息時間是否計薪：是/否

工作地點︰
工作內容︰
需求人數：
面試時間：
受訓時間：

單位名稱：
單位地址：
統一編號：
負責人︰先生/小姐
是否回信給報名者：是/否
錄取者通知方式：電子郵件/手機";

if (isset($_SESSION['member_id'])) {
    $x = $this->_member_contact();
    $task_result->email = $x[email]; //連絡信箱
    $task_result->phone = $x[phone]; //聯絡電話
}

    $this->load->view('post_page',Array(
                        "submit" => '發布',
                        "task_result" => $task_result
                         ));
 }


  private function _post_ing() {
    // 寫入DB (按FB登入 已登入狀態)
    $this->load->model('task_model');

    // 此實體任務是否在開放區域， 全面開放
    // if ($this->input->post('onlinetask') == FALSE) {
    //     if ( ! $this->_check_open_area($this->input->post('address'))){
    //         $this->task_model->add_task_temp($_SESSION['member_id'], $this->input->post());
    //         $this->_toRedirectBack('petition',(array)'此區域尚未開放，趕快來為您的城市連署吧！');
    //     }
    // }

    $insert_id = $this->task_model->add_task($_SESSION['member_id']);
    if ($insert_id == FALSE) {
      $this->_toRedirectBack('task/post',(array)'新增任務失敗');
    }
    redirect(base_url("task/" . $insert_id));// 新增完成,進入任務頁面
  }

 function edit() {
   //檢查是否為發案者本人
   $task_id = (int)$this->uri->segment(3);
   $this->load->model('task_model');
   if (is_logged_in() == FALSE OR $this->task_model->is_poster($task_id) == FALSE) {
     $this->_toRedirectBack("/task/$task_id",(array)'發案者本人才可修改任務喔！');
   }


   // 取得此任務資訊
   $task_result = $this->task_model->get_task($task_id);

   $this->load->view('post_page',Array(
                       "submit" => '修改',
                       "noRobotIndex" => 'noRobotIndex',//禁止後台被搜尋索引
                       "task_result" => $task_result
                        ));
}

private function _edit_ing() {
  if (is_logged_in() == FALSE) redirect(base_url('error_404'));

   $task_id = str_replace('task/edit/', '', $this->input->post('former_URL'));

   $this->load->model('task_model');
   if ( $this->task_model->update_task($task_id)) {
      $this->session->set_flashdata('info', array('任務修改成功!!'));
   }
   redirect(base_url('task/'.$task_id));
}
/**
 * [發布任務、修改任務、複製任務頁面驗證]
 */
 function post_form_validate() {
    $this->load->library('form_validation');
    //實體任務需要驗證
    if ($this->input->post('onlinetask') == FALSE) {
      $config_rule[] = array('field' => 'geoclick', 'label' => '地址請由下拉式選單中挑選',
                               'rules' => 'trim|xss_clean|required') ;
      $config_rule[] = array('field' => 'address', 'label' => '任務地址',
                               'rules' => 'trim|xss_clean|required|max_length[255]') ;
    }

    if (isset($config_rule)) { //有需要加入額外的驗證rule
      $this->config->load('form_validation'); //載入本頁的驗證config檔
      $new_rule = array_merge($this->config->item('task/post_form_validate'), $config_rule);
      $this->form_validation->set_rules($new_rule);
    }

    $former_URL = $this->input->post('former_URL');

    if (substr_count($former_URL, 'post') OR substr_count($former_URL, 'copy')) {
      $this->_checkFormRules("task/post");
      $this->_post_ing(); // 發布任務、複製任務
    }else{
      $this->_checkFormRules($former_URL);
      $this->_edit_ing(); // 修改任務
    }
}

 function copy() {
   // $this->_checkLoggedIn('task/post');

   // 取得此任務資訊
   $task_id = (int)$this->uri->segment(3);
   $this->load->model('task_model');
   $task_result = $this->task_model->get_task($task_id);

   $this->load->helper(array('address_format1_helper','address_is_online_helper'));

   // 非發案者 或還沒登入者，不可複製：私密內容、詳細地址(address,lat_lng)
   if (empty($_SESSION['member_id']) OR $this->task_model->is_poster($task_id) == FALSE) {
      if ($task_result->address != '') {
          $task_result->address = address_format1($task_result->address); //地址只顯示到區
      }
      $task_result->private_content = '';
   }

   if (isset($_SESSION['member_id'])) {
       $x = $this->_member_contact();
       $task_result->email = $x[email]; //連絡信箱
       $task_result->phone = $x[phone]; //聯絡電話
   }

   $this->load->view('post_page',Array(
                       "submit" => '發布此複製任務',
                       "noRobotIndex" => 'noRobotIndex',//禁止後台被搜尋索引
                       "task_result" => $task_result
                        ));
}

function _member_contact(){
  $this->load->model('user_model');
  $x[email] = $this->user_model->get_member_email($_SESSION['member_id']);
  if ($x[email] == FALSE) {
      $x[email] = $_SESSION['member_email'];
  }
  $x[phone] = $this->user_model->get_user($_SESSION['member_id'])->phone;

  return $x;
}
/**
 * 任務詳細內容
 * @param  [type] $task_id [description]
 */
function info($task_id) {
  try { //FIXME:嘗試用try catch方式寫
    if ( ! isset($task_id)) throw new Exception('此任務id不存在!');

    // 第3個參數 表示代po由spider信進來的發案者
    $key = $this->uri->segment(3);
    if (isset($key)) {
        echo $key;
    }

    $this->load->model('task_model');
    // 取得任務狀態,發布任務、執行中、任務完成
    $status_results = $this->task_model->get_status_by_id($task_id);
    if ($status_results == null) throw new Exception('此任務不存在!');

    /* 填好zip才可接任務
    if (is_logged_in() == TRUE) {
      $this->load->model('user_model');
      $this->check_zip();
    }
    */

  }
  catch (Exception $e)  {
    $this->_toRedirectBack('error_404',(array)$e->getMessage());
  }

  $task = $this->task_model->get_task($task_id); // 取得任務詳細資訊

  /*switch ($task->status) {
    case '0': // 任務狀態為發布任務
      $runner_results = $this->task_model->get_runner($task_id); // 跑者詳細資訊
      break;

    case '1': // 執行中
      $runner_results = $this->task_model->get_runner($task_id); // 跑者詳細資訊
      break;

    case '2': // 任務完成
      $this->load->model('credit_model');
      @$credit_results = $this->credit_model->get_finished_credit($task_id); // 所有跑者評價
      break;
  }*/
  $runner_results = $this->task_model->get_runner($task_id); // 跑者詳細資訊

  // 訪客與任務關係
  $user_task_relation = $this->task_model->get_user_task_relation($task_id);

  // 取得討論內容
  $this->load->model('discuss_model');
  $discuss_results = $this->discuss_model->get_discuss($task_id);

  // 任務各狀態時間格式化
  $this->load->helper('date_format1_helper'); //修改日期格式

  for ($i = 0; $i <= 4; $i++) {
    if ( isset($status_results[$i])) {
      $status_time[$i] = date_format1($status_results[$i]->created_at);
    }else{
      $status_time[$i] ='';
    }
  }

  $this->load->helper(array('address_format1_helper','address_is_online_helper'));
  $task->address = address_format1($task->address); //地址只顯示到區
  $task->address = address_is_online($task->address); //空白地址顯示 線上或手機

  $this->load->view('task_page', Array(
                    "task" => $task,
                    "title" => $task->title, //header用
                    "content" => $task->content, //header用 注意JS斷行問題
                    "head_img" => $task->img, //header用
                    "user_task_relation" => $user_task_relation, //訪客身份
                    "task_status_id" => $task->status,//印出最新的狀態 query用MAX()
                    "status_time" => $status_time,
                    // "status_results" => $status_results,
                    "runner_results" => $runner_results,
                    "credit_results" => @$credit_results,
                    "discuss_results" => $discuss_results
                     )
  );

}

/**
 * 任務地圖
 * TODO:不顯示完成的任務但有負評,任務再加上一個顯示狀態
 * @param  string $sort_type [排序方式]
 * @param  string $catalog [分類類型]
 * @return [type] [description]
 */
function map($area='taipei', $sort_type = 'newest', $catalog = '1') {
  $this->load->model('task_model');
  $task_results = $this->_taskSortType($area, $sort_type, $catalog);

// FIXME:改用array寫 更乾淨
  switch ($area) {
    case 'taipei':
      $latlng = '25.061888,121.506458'; //台北
      $here = '台北';
      $other_city = array('桃園' => 'task/map/taoyuan/newest',
                          '新竹' => 'task/map/hsinchu/newest',
                          '台中' => 'task/map/taichung/newest',
                          '台南' => 'task/map/tainan/newest',
                          '高雄' => 'task/map/kaohsiung/newest'
       );
      break;

    case 'taoyuan':
      $latlng = '24.971431,121.25399'; //桃園
      $here = '桃園';
      $other_city = array('台北' => 'task/map/taipei/newest',
                          '新竹' => 'task/map/hsinchu/newest',
                          '台中' => 'task/map/taichung/newest',
                          '台南' => 'task/map/tainan/newest',
                          '高雄' => 'task/map/kaohsiung/newest'
       );
      break;

    case 'hsinchu':
      $latlng = '24.804188,120.987124'; //新竹
      $here = '新竹';
      $other_city = array('台北' => 'task/map/taipei/newest',
                          '桃園' => 'task/map/taoyuan/newest',
                          '台中' => 'task/map/taichung/newest',
                          '台南' => 'task/map/tainan/newest',
                          '高雄' => 'task/map/kaohsiung/newest'
       );
      break;

    case 'taichung':
      $latlng = '24.181523,120.651413'; //台中
      $here = '台中';
      $other_city = array('台北' => 'task/map/taipei/newest',
                          '桃園' => 'task/map/taoyuan/newest',
                          '新竹' => 'task/map/hsinchu/newest',
                          '台南' => 'task/map/tainan/newest',
                          '高雄' => 'task/map/kaohsiung/newest'
       );
      break;

    case 'tainan':
      $latlng = '23.005409,120.206724'; //台南
      $here = '台南';
      $other_city = array('台北' => 'task/map/taipei/newest',
                          '桃園' => 'task/map/taoyuan/newest',
                          '新竹' => 'task/map/hsinchu/newest',
                          '台中' => 'task/map/taichung/newest',
                          '高雄' => 'task/map/kaohsiung/newest'
       );
      break;

    case 'kaohsiung':
      $latlng = '22.648076,120.303184'; //高雄
      $here = '高雄';
      $other_city = array('台北' => 'task/map/taipei/newest',
                          '桃園' => 'task/map/taoyuan/newest',
                          '新竹' => 'task/map/hsinchu/newest',
                          '台中' => 'task/map/taichung/newest',
                          '台南' => 'task/map/tainan/newest'
       );
      break;

    case 'online':
      $here = '線上';
      $other_city = array('台北' => 'task/map/taipei/newest',
                          '桃園' => 'task/map/taoyuan/newest',
                          '新竹' => 'task/map/hsinchu/newest',
                          '台中' => 'task/map/taichung/newest',
                          '台南' => 'task/map/tainan/newest',
                          '高雄' => 'task/map/tainan/kaohsiung'
       );
      break;
  }

  $this->load->helper('address_format1_helper'); //地址只顯示到區

  $numMarkers = $this->task_model->count_task_by_type('0');//實體任務數量
  $this->load->helper('escape_php_to_js_helper'); // 跳脫JS字串,避免斷行與單引號

  $this->load->view('map_page', Array(
                        "sort_type" => $sort_type,
                        "numMarkers" => $numMarkers,
                        "area" => $area,
                        "latlng" => $latlng,
                        "here" => $here,
                        "other_city" => $other_city,
                        "task_results" => $task_results //若地圖不顯示,可能$task->content有斷行 JS不支援
                        ));
}

private function _taskSortType($area, $sort_type, $catalog){
  $this->load->model('task_model');

  switch ($sort_type) { //FIXME: 出現switch考慮用Strategy模式重構
    case 'newest':
    // 參數對照表 $order , $desc ,$area, $onlinetask , $now,$catalog, $num
    return $this->task_model->get_all_task('task.id','desc', $area);

    case 'price':
    return $this->task_model->get_all_task('task.price','desc', $area);

    case 'deadline':
    $now = date("Y-m-d H:i:s",time());
    return $this->task_model->get_all_task('task.deadline','asc', $area, null,$now);

    case 'onlinetask':
    return $this->task_model->get_all_task('task.price','desc',null ,1);

    case 'catalog':
    return $this->task_model->get_all_task('task.id','desc', $area ,null,null,$catalog);

    default:
    redirect(base_url("/error_404"));
  }
}

public  function check_phone(){
  if ($this->user_model->is_have_phone($_SESSION['member_id']) == FALSE) {
    $this->_toRedirectBack('/phone_vaild',(array)'請先進行手機驗證，才可接任務喔！');
  }
}
public  function check_zip(){
  if ($this->user_model->is_have_zip($_SESSION['member_id']) == FALSE) {
    $this->_toRedirectBack('user/settings',(array)'請填寫郵遞區號，才可接任務喔！');
  }
}

//目前開放區域的 白名單
// public  function _check_open_area($area){
//   $array  = array('台北市', '新北市', '基隆市', '桃園縣');
//   $this->load->helper('strpos_array_helper');
//   if (strpos_array($area, $array, 0)) return TRUE;
// }

}
/* End of file task.php */
/* Location: ./application/controllers/task.php */
