<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Upload extends MY_Controller {
  function __construct()  {
    parent::__construct();
    $this->load->helper(array('form'));
  }

  function do_upload_settings()  {
    $config['upload_path'] = './uploads/head_img_temp/'; //先存在temp資料夾內
    $config['allowed_types'] = 'gif|jpg|png';
    $config['max_size'] = '800'; //最大800KB
    // $config['max_width']  = '180'; //限制檔案大小就好,畢竟會縮圖處理
    // $config['max_height']  = '180';
    $config['min_width']  = '150';
    $config['min_height']  = '150';

    $find = array('@','.');
    $file_name = date("YmdHis").'_'.str_replace($find, '', $_SESSION['member_email']);

    $file_name = $_SESSION['member_id'] . '_' . date("YmdHis");
    $config['file_name'] = $file_name;

    $this->load->library('upload',$config);

    if ( ! $this->upload->do_upload()) {
      $flash_data = array('err' => '上傳失敗',
                    'display_error' => $this->upload->display_errors()
                    );

      $this->session->set_flashdata('error', $flash_data);
      redirect(base_url("/user/settings/"));
    }

    //上傳成功
    $file_name = $this->upload->data()['file_name'];
    // $image_width = $this->upload->data()['image_width'];
    // $image_heigth = $this->upload->data()['image_heigth'];

    $flash_data = array('upload_ok' => '上傳照片成功');

    // $cut_status = $this->img_cut($file_name); //進行裁圖
    // //剪裁是否成功資訊 存入flash_data顯示
    // foreach ($cut_status as $key => $value) {
    //   $flash_data[$key] = $value;
    // }

    $resize_status = $this->img_resize($file_name); //進行縮圖
    //縮圖是否成功資訊 存入flash_data顯示
    foreach ($resize_status as $key => $value) {
      $flash_data[$key] = $value;
    }

    $this->load->model('user_model');
    $get_items = "id";
    $query = $this->user_model->get_member_by_email($_SESSION['member_email'], $get_items);

    $this->user_model->update_ByID($query->id, array('img' => $file_name));
    $_SESSION['member_img'] = $file_name;//更新圖示

    $this->session->set_flashdata('info', $flash_data);
    redirect(base_url("/user/settings/"));

  }

  function img_cut($imgName){
    // Citytasker 暫時沒用到
    $config['image_library'] = 'gd2';
    $config['source_image'] = "./uploads/head_img_temp/$imgName"; //注意要有. 才是跟目錄底下
    // $config['new_image'] = "./uploads/head_img/$imgName"; //存到目標資料夾
    $config['create_thumb'] = TRUE;
    $config['thumb_marker'] = '';
    $config['x_axis']  = 50;
    $config['y_axis'] = 50;

    $this->load->library('image_lib',$config);

    $this->image_lib->crop();
    if ( ! $this->image_lib->crop()){
      $flash_data = array('crop_status' => '剪裁失敗',
                          'crop_error' => $this->image_lib->display_errors()
                          );
      return $flash_data;

    }else{
      // 刪除原始檔
      unlink("./uploads/head_img_temp/$imgName");
      $arrayName = array('crop_ok' => '剪裁成功' );
      return $arrayName;
      }
  }

  function img_resize($imgName){
    $config['image_library'] = 'gd2';
    $config['source_image'] = "./uploads/head_img_temp/$imgName"; //注意要有. 才是跟目錄底下
    $config['new_image'] = "./uploads/head_img/$imgName"; //存到目標資料夾
    $config['create_thumb'] = TRUE;
    $config['thumb_marker'] = '';
    $config['maintain_ratio'] = FALSE;
    $config['width']  = 150;
    $config['height'] = 150;

    $this->load->library('image_lib',$config);

    $this->image_lib->resize();
    if ( ! $this->image_lib->resize()){
      $flash_data = array('resize_status' => '縮圖失敗',
                          'resize_error' => $this->image_lib->display_errors()
                          );
      return $flash_data;

    }else{
      // 刪除原始檔
      unlink("./uploads/head_img_temp/$imgName");
      $arrayName = array('resize_ok' => '縮圖成功' );
      return $arrayName;
      }
  }

}
/* End of file upload.php */
/* Location: ./application/controllers/upload.php */
