<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->dealWithIndexPage(); //解決URL上出現index.php的路徑
        $this->_init();
        $this->_check_vitae();
    }

    protected function _init(){
        if ( ! isset($_SESSION)) {
          session_start(); //每個Controller都啟用session,把session判斷放在此似乎是最快的,但有啥缺點嗎?
        }

        $this->config->load('facebook'); //載入FB APP的config檔

        // ini_set('date.timezone','Asia/Taipei');
        // 可加入取得 user IP 之類的共同操作
    }

    // 若履歷尚未完成,按下搜尋任務時會跳出提示訊息
    protected function _check_vitae(){
        if (isset($_SESSION['member_id'])) {
            $this->load->model('user_model');
            if ($this->user_model->is_vitae_ok($_SESSION['member_id'])){
                $_SESSION['vitae_status'] = 'TRUE';
            }else{
                $_SESSION['vitae_status'] = 'FALSE';
            }
        }
    }

    /**
    * 自訂Controller來解決URL上出現index.php的路徑
    * 檢查URL是否包含index.php字串,若有則導向.
    */
    protected  function dealWithIndexPage(){
        $uri = $this->input->server('REQUEST_URI',true);
        // deal with index.php
        $this->input->server();
        if (strpos($uri, 'index.php')!==FALSE) {//是否有出現index
          $this->load->helper('url');
          redirect('/error_404/');
        }
    }

    /**
     * Run Form validation
     * 若不成功則為導向原本頁面
     * @param  string $redirect [description]
     */
    protected function _checkFormRules($redirect = '/service/forrunner'){
        $this->load->library('form_validation');
        if ( ! $this->form_validation->run() ) {
            $this->_toRedirectBack($redirect, (array)validation_errors());
        }
        return TRUE;
    }
    protected function _checkLoggedIn($redirect = '/service/forrunner'){
        if (is_logged_in() == FALSE) {
            $this->session->set_flashdata('error', array('你尚未登入! 請先登入~'));
            redirect(base_url($redirect));
        }
        return TRUE;
    }
    protected function _toRedirectBack($redirect = '/service/forrunner', $error_message=null){
        if (isset($error_message)) {
            $this->session->set_flashdata('error', (array)$error_message);
        }
        $post = $this->input->post();
        if (isset($post)) {
            $this->session->set_flashdata('postdata', $this->input->post()); // 表單要重填 資料需保留
        }
        redirect(base_url("$redirect"));
    }
}
