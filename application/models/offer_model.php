<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Offer_model extends CI_Model{
function __construct(){
    parent::__construct();
}
/**
 * 跑者提出申請
 * @param [type] $runner_id [description]
 * @param [type] $task_id   [description]
 */
function add_offer($runner_id, $task_id){

    $data = array('task_id' => $task_id,
                  'member_id' => $runner_id,
                  'status' => 1,//修改成 申請offer就是錄取1
                  'created_at' => date('Y-m-d H:i:s'),
                  'ip_address' => $this->input->ip_address()
                  );
    $this->db->insert('task_runner',$data);

    if ($this->db->affected_rows() == 0) return FALSE; //新增失敗時

    return TRUE;
}

/**
 * 雇用 發放offer
 * @param [type] $runner_id [description]
 * @param [type] $task_id   [description]
 */
function response_offer($runner_id, $task_id){

    $data = array('task_id' => $task_id,
                  'member_id' => $runner_id,
                  'status' => 1,
                  'created_at' => date('Y-m-d H:i:s'),
                  'ip_address' => $this->input->ip_address()
                  );
    $this->db->insert('task_runner',$data);

    if ($this->db->affected_rows() == 0) return FALSE; //新增失敗時

    //若此跑者是第一位被指派的,更新任務狀態1
    $this->load->model('task_model');
    if ($this->task_model->is_first_runner($task_id)) {
      $this->task_model->add_status($task_id, 1);
    }
    return TRUE;
}
/**
 * [檢查此人是否申請過此任務]
 * @return boolean        [description]
 */
function is_offered($runner_id, $task_id){
    $this->db->select("task_id,created_at");
    $this->db->from('task_runner');
    $where = array('task_id' =>$task_id ,
                    'member_id' =>$runner_id );
    $this->db->where($where);
    $query = $this->db->get();

    if ($query->num_rows() <= 0) return false;
    return true; //給過評價
}

}
/* End of file offer_model.php */
/* Location: ./application/models/offer_model.php */