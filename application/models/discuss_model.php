<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 每則任務內容的討論區
 */
class Discuss_model extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->helper('array_to_object');
    }
    function add_discuss(){
      //TODO:應該要限制3分鐘內不準重複留言
        $data = array('task_id' => $this->input->post('task_id'),
                      'member_id' => $_SESSION['member_id'],//登入才可以參與討論
                      'content' => $this->input->post('content'),
                      'disable' => 0,
                      'created_at' => date('Y-m-d H:i:s'),
                      'ip_address' => $this->input->ip_address()
                      );
        $this->db->insert('task_discuss',$data);

        if ($this->db->affected_rows() == 0) return FALSE; //新增失敗時

        return TRUE;
    }

    function get_discuss($task_id){
        $this->db->select("di.id, di.content, di.created_at,
                          m.id as member_id, m.name, m.img
                       ");
        $this->db->from('task_discuss as di');
        $this->db->join('member as m', "di.member_id = m.id AND di.task_id = $task_id", 'inner');
        // $this->db->join('task_discuss_reply as re', 'di.id = re.discuss_id ', 'left');
        $this->db->order_by("di.created_at", "desc"); //由大到小排序
        $query = $this->db->get();

        if ($query->num_rows() <= 0) return null;
        $row_obj = array_to_object(html_escape($query->result_array())); //過濾htmlspecialchars之後再轉回object
        return $row_obj;
    }

    // 取得該任務所有留言者mail (寄信使用)
    function get_alluser_mail($task_id){
        $this->db->select("m.email
                       ");
        $this->db->from('task_discuss as di');
        $this->db->where('task_id', $task_id);
        $this->db->join('member as m', "di.member_id = m.id ", 'left');
        $this->db->group_by('di.member_id');
        $query = $this->db->get();

        if ($query->num_rows() <= 0) return null;
        return $query->result_array();
    }


    // /* 每則留言暫時不需要單獨回覆 */
    // function get_discuss_reply($discuss_id){
    //     $this->db->select("re.id, re.discuss_id, re.member_id, re.content, re.created_at,
    //                       m.id, m.name, m.img
    //                    ");
    //     $this->db->from('task_discuss_reply as re');
    //     $this->db->join('member as m', "re.member_id = m.id AND re.discuss_id = $discuss_id ", 'inner');
    //     $this->db->order_by("re.created_at", "desc"); //由大到小排序
    //     $query = $this->db->get();

    //     if ($query->num_rows() <= 0) return null;
    //     $row_obj = array_to_object(html_escape($query->result_array())); //過濾htmlspecialchars之後再轉回object
    //     return $row_obj;
    //   }
}
/* End of file discuss_model.php */
/* Location: ./application/models/discuss_model.php */