<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 增:申請簡訊驗證時
 * 刪:通過簡訊驗證
 * 修:
 * 查:
 */
class Phone_model extends CI_Model {
  public  function __construct (){
    parent::__construct();

  }

/**
 * 藉由 member_id 取得 phone_vaild 資料
 * @param  [type] $member_id     [description]
 * @param  [type] $get_items [description]
 * @return [type]            [description]
 */
public  function get_phone_vaild_by_id($member_id,$get_items) {
  $this->db->select("$get_items");
  $query = $this->db->get_where("phone_vaild", Array("member_id" => $member_id));

  if ($query->num_rows() <= 0) { return FALSE;  }
  return $query->row(); //回傳第一筆
}

/**
 * 新增發送簡訊記錄
 * @param [type] $data [description]
 */
public  function add_phone_valid($data){
    $did_add_phone = $this->db->insert('phone_vaild', $data);

    if ($did_add_phone) return TRUE;

    return FALSE;
}

public  function delete($member_id){
  $this->db->where($member_id);
  $this->db->delete('phone_vaild');
}


}

/* End of file phonemodel.php */
/* Location: ./application/models/phonemodel.php */
