<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Spider_model extends CI_Model {
  function __construct() {
    parent::__construct();

 }

 function add_spider($insert){
     $this->db->insert('spider', $insert);
     return $this->db->affected_rows();
 }

 /**
  * 撈任務，比對該任務是否已經存在DB
  */
 function is_feed_exist($tid, $message){
     $this->db->select("tid");
     $this->db->from('spider');
     $this->db->where('tid',$tid); // tid存在
     $this->db->or_where('message', $message); // %message%
     // $this->db->where('created_at >', date("Y-m-d H:i:s",time()-2592000)); // 一個月內

     $query = $this->db->get();

     if ($query->num_rows() <= 0) return FALSE;
     return TRUE;
 }

 function get_all($offset = 0, $pageSize = 30, $read=''){
     $this->db->select("*
                    ");
     $this->db->from('spider');
     $this->db->limit($pageSize, $offset);
     $this->db->where('read',$read); // tid存在

     $this->db->order_by("id", "desc"); //由大到小排序
     $query = $this->db->get();

     if ($query->num_rows() <= 0) return null;
     return $query->result();
 }
 function get_spider($id){
     $this->db->select("*
                    ");
     $this->db->from('spider');
     $this->db->where('id',$id);

     $query = $this->db->get();

     if ($query->num_rows() <= 0) return null;
     return $query->row();
 }
 function get_post($offset = 0, $pageSize = 30){
    $this->db->select("s.* ,
                       t.title
                       ");
    $this->db->from('spider_posted as s');
    $this->db->join('task as t', 's.task_id = t.id', 'left');
    $this->db->limit($pageSize, $offset);

    $this->db->order_by("s.id", "desc"); //由大到小排序
    $query = $this->db->get();

    if ($query->num_rows() <= 0) return null;
    return $query->result();
 }

 function count($read=''){
    $this->db->select("count(id) as total");
    $this->db->from('spider');
    $this->db->where('read', $read);
    $query = $this->db->get();

    return $query->row()->total;
 }

 function poster_connect($task_id, $memeber_id){
     $data_private = array('member_id' => (int)$memeber_id);

     $this->db->where('id',$task_id);
     $this->db->update('task', $data_private);

     return ($this->db->affected_rows() == 0) ? FALSE : TRUE;
 }

}

/* End of file spider_model.php */
/* Location: ./application/models/spider_model.php */
