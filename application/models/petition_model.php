<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Petition_model extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->helper('array_to_object');
    }

    function get_petition($start, $end){
        $this->db->select("count(*) as count");
        $this->db->from('member');
        $this->db->where('zip >=', $start);
        $this->db->where('zip <=', $end);
        $query = $this->db->get();

        return $query->row()->count;
    }

    function add_random_member(){
      // 創造連署假帳號做測試
      $member_new = array();

      // for ($i = 0; $i <10 ; $i++) {
      //     $member_new[] = array('zip' => rand(100,116) );//台北
      // }
      // for ($i = 0; $i <10 ; $i++) {
      //     $member_new[] = array('zip' => rand(200,206) );//基隆
      // }
      // for ($i = 0; $i <1 ; $i++) {
      //     $member_new[] = array('zip' => rand(209,212) );//馬祖
      // }
      // for ($i = 0; $i <10 ; $i++) {
      //     $member_new[] = array('zip' => rand(207,208) );//新北
      // }
      // for ($i = 0; $i <10 ; $i++) {
      //     $member_new[] = array('zip' => rand(220,253) );//新北
      // }
      // for ($i = 0; $i <500 ; $i++) {
      //     $member_new[] = array('zip' => rand(260,290) );//宜蘭
      // }
      // for ($i = 0; $i <1000 ; $i++) {
      //     $member_new[] = array('zip' => rand(300,315) );//新竹
      // }
      // for ($i = 0; $i <4000 ; $i++) {
      //     $member_new[] = array('zip' => rand(320,338) );//桃園
      // }
      // for ($i = 0; $i <200 ; $i++) {
      //     $member_new[] = array('zip' => rand(350,369) );//苗栗
      // }
      // for ($i = 0; $i <3000 ; $i++) {
      //     $member_new[] = array('zip' => rand(400,439) );//台中
      // }
      // for ($i = 0; $i <50 ; $i++) {
      //     $member_new[] = array('zip' => rand(500,530) );//彰化
      // }
      // for ($i = 0; $i <100 ; $i++) {
      //     $member_new[] = array('zip' => rand(540,558) );//南投
      // }
      // for ($i = 0; $i <50 ; $i++) {
      //     $member_new[] = array('zip' => rand(600,625) );//嘉義
      // }
      // for ($i = 0; $i <50 ; $i++) {
      //     $member_new[] = array('zip' => rand(630,655) );//雲林
      // }
      // for ($i = 0; $i <500 ; $i++) {
      //     $member_new[] = array('zip' => rand(700,745) );//台南
      // }
      // for ($i = 0; $i <1500 ; $i++) {
      //     $member_new[] = array('zip' => rand(800,852) );//高雄
      // }
      // for ($i = 0; $i <1 ; $i++) {
      //     $member_new[] = array('zip' => rand(880,885) );//澎湖
      // }
      // for ($i = 0; $i <1 ; $i++) {
      //     $member_new[] = array('zip' => rand(890,896) );//金門
      // }
      // for ($i = 0; $i <10 ; $i++) {
      //     $member_new[] = array('zip' => rand(900,947) );//屏東
      // }
      // for ($i = 0; $i <200 ; $i++) {
      //     $member_new[] = array('zip' => rand(950,966) );//台東
      // }
      // for ($i = 0; $i <300 ; $i++) {
      //     $member_new[] = array('zip' => rand(970,983) );//花蓮
      // }

      // $this->load->model('petition_model');
      // $this->petition_model->add_random_member($member_new);
      //
        $query = $this->db->insert_batch('member', $member_new);
        // echo $query->num_rows();
    }

}
/* End of file petition_model.php */
/* Location: ./application/models/petition_model.php */