<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model {
  function __construct() {
    parent::__construct();

 }
function get_all_user($offset = 0, $pageSize = 50){
    $this->db->select("m.id, m.name, m.img, m.zip, m.created_at,
                      fb.fb_id, fb_gender
                   ");
    $this->db->from('member as m');
    $this->db->join('member_facebook as fb', 'm.id = fb.member_id', 'left');
    // $this->db->where('m.id',$member_id);
    // $this->db->limit($pageSize, $offset);
    $this->db->limit($pageSize, $offset);

    $this->db->order_by("m.id", "desc"); //由大到小排序
    $query = $this->db->get();

    if ($query->num_rows() <= 0) return null;
    return $query->result();
}


}

/* End of file admin_model.php */
/* Location: ./application/models/admin_model.php */
