<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 增:申請加入會員時
 * 刪:null
 * 修:個人資料
 * 查:
 */
class User_model extends CI_Model {
  function __construct() {
   parent::__construct();
   $this->load->helper('array_to_object');

 }

 function add_temp_user($username,$email,$zip,$password,$key){
  $data = array('name' => $username,
                'email' => $email,
                'zip' => $zip,
                'password' => $password,
                'created_at' => date('Y-m-d H:i:s'),
                'key' => $key
                );

  $query = $this->db->insert('member_temp', $data);
  if ( ! $query)  return FALSE;

  return TRUE;
}
function is_key_valid($key){
  $this->db->where('key',$key);
  $query = $this->db->get('member_temp');
  if ($query->num_rows() > 0) return TRUE;

  return FALSE;
}

/**
 * 檢查是否有設定手機號碼
 * @param  [type]  $email [description]
 * @return boolean        [description]
 */
function is_have_phone($member_id){
  $this->db->select('phone')
  ->where('id',$member_id);
  $query = $this->db->get('member');

  if (empty($query->row()->phone)) return FALSE; //未填手機

  return TRUE;
}

function is_have_zip($member_id){
  $this->db->select('zip')
  ->where('id',$member_id);
  $query = $this->db->get('member');

  if (empty($query->row()->zip)) return FALSE;

  return TRUE;
}

function is_vitae_ok($member_id){
  $this->db->select('language, education, work, about')
  ->where('member_id',$member_id);
  $query = $this->db->get('member_vitae');

  foreach ($query->row_array() as $row) {
      if (empty($row)) return FALSE;
  }

  return TRUE;
}

/**
 * 帳號啟用成功 新增使用者
 * @param [type] $key [description]
 */
function add_user($key){
  $this->db->where('key', $key);
  $temp_user = $this->db->get('member_temp');
  if ($temp_user) {
    $row = $temp_user->row();
    $data = array('name' => $row->name,
                  'email' => $row->email,
                  'password' => $row->password,
                  'zip' => $row->zip,
                  // 'access_level' => 1,
                  'img' => 'default_pic.png',
                  'created_at' => date('Y-m-d H:i:s')
                  // 'register_from' => 1
                  );
    $did_add_user = $this->db->insert('member', $data);
  }

  if ($did_add_user) {
    $this->db->where('key', $key);
    $this->db->delete('member_temp');
    $this->set_login_session($row->email);

    return TRUE;
  }
  return FALSE;
}

function get_user($member_id){
    $this->db->select("m.name, m.img, m.phone, m.zip,
                   v.*
                   ");
    $this->db->from('member as m');
    $this->db->join('member_vitae as v', 'm.id = v.member_id', 'left');
    $this->db->where('m.id',$member_id);
    // $this->db->limit($pageSize, $offset);
    // $this->db->order_by("task.created_at", "desc"); //由大到小排序
    $query = $this->db->get();

    if ($query->num_rows() <= 0) return null;
    $row_obj = array_to_object(html_escape($query->row_array())); //過濾htmlspecialchars之後再轉回object
    return $row_obj;
}

function get_member_email($member_id){
    $this->db->select("email");
    $this->db->from('member_email');
    $this->db->where('member_id',$member_id);
    $query = $this->db->get();

    if ($query->num_rows() <= 0) return FALSE;
    return $query->row()->email;
}
function add_vitae($member_id, $data){
    $this->db->where('member_id',$member_id);
    $q = $this->db->get('member_vitae');
    if ( $q->num_rows() > 0 ) {
      //若存在則用update
      $this->db->where('member_id',$member_id);
      $this->db->update('member_vitae', $data);
    }else{
      $this->db->insert('member_vitae', $data);
    }

    return ($this->db->affected_rows() == 0) ? FALSE : TRUE;
}
//加入 連絡信箱
function add_email2($id, $email){
    $this->db->where('member_id',$id);
    $q = $this->db->get('member_email');

    if ( $q->num_rows() > 0 ) {
      //若存在則用update
      $this->db->where('member_id',$id);
      $data = array('email' => $email);
      $this->db->update('member_email', $data);
    }else{
      $data = array('member_id' => $id,
                    'email' => $email);
      $this->db->insert('member_email', $data);
    }

    return ($this->db->affected_rows() == 0) ? FALSE : TRUE;
}
function del_email2($id){
    $this->db->where('member_id',$id);
    $q = $this->db->get('member_email');

    if ( $q->num_rows() > 0 ) { //存在才刪除
      $this->db->delete('member_email', array('member_id' => $id));
    }
    return ($this->db->affected_rows() == 0) ? FALSE : TRUE;
}

function get_vitae($member_id){
    $this->db->select("*");
    $query = $this->db->get_where("member_vitae", Array("member_id" => $member_id));

    if ($query->num_rows() <= 0) return null;
    return $query->row();
}
/**
 * 新增robot來訪紀錄
 */
function  add_robot($data){
  $did_add = $this->db->insert('visitor', $data);
  if ($did_add) return TRUE;

  return FALSE;
}

/**
 * 判斷帳密正確否,正確就呼叫set_login_session
 * @param  [type] $email    [description]
 * @param  [type] $password [description]
 */
function check_log_in($email,$password){
  $this->db->where('email',$email );
  $this->db->where('password',$password);
  $query = $this->db->get('member');

  if ($query->num_rows() == 1) {
    $this->set_login_session($email);
    return TRUE;
  }

  return FALSE;
}

/**
 *  發送session的地方 傳入email設定session
 * @param [type] $email [description]
 */
function set_login_session($email){
  $this->db->select("id, name, email, zip, img");
  $this->db->where('email',$email );
  $query = $this->db->get('member');

  if ($query->num_rows() == 1) {
    if (isset($_SESSION['member_id'])) $is_update = TRUE;//確認是更新

    $row =$query->row();
    // if ($row->id == 55) $row->id=24; //bobo換帳號測試用
    $user = array('member_id' => $row->id,
                  'member_name' => $row->name,
                  'member_email' => $row->email,
                  'member_img' => $row->img,
                  'member_zip' => $row->zip
                  );
    $_SESSION = $user;
    $_SESSION['key'] = md5($_SERVER['HTTP_USER_AGENT'].'citytasker');

    if ($is_update) {
       $this->add_event_log('update');//存下 更新記錄
    }else{
       $this->add_event_log('login');//存下 登入記錄
    }
    return TRUE;
  }

  return FALSE;
}

function add_event_log($type='unknow'){
  $data = array('member_id' => $_SESSION['member_id'],
                'type' => $type,
                'created_at' => date('Y-m-d H:i:s'),
                'ip_address' => $this->input->ip_address()
                );
  $did_add_user = $this->db->insert('event_log', $data);
}
function update_ByID($id,$data){
    $this->db->where('id', $id);
    $this->db->update('member', $data);

    return ($this->db->affected_rows() == 0) ? FALSE : TRUE;
}
function get_id_by_email($email) {
  $this->db->select("id");
  $query = $this->db->get_where("member", Array("email" => $email));

  if ($query->num_rows() <= 0) return null;
  return $query->row()->id;
}

/**
 * 藉由 email 取得使用者資料
 * @param  [type] $email     [description]
 * @param  [type] $get_items [description]
 * @return [type]            [description]
 */
function get_member_by_email($email) {
  $this->db->select("m.id,m.name, m.realname, m.email, m.phone, m.zip, m.img,
                     e.email as email2  ");
  // $query = $this->db->get_where("member", Array("email" => $email));

  //結合連絡信箱
  $this->db->from('member as m');
  $this->db->join('member_email as e', 'm.id = e.member_id', 'left');
  $this->db->where('m.email', $email);
  // $this->db->limit($pageSize, $offset);
  // $this->db->order_by("task.created_at", "desc"); //由大到小排序
  $query = $this->db->get();

  $row_obj = array_to_object(html_escape($query->row_array())); //過濾htmlspecialchars之後再轉回object
  return $row_obj;
}

function get_email_by_id($id) {
  $this->db->select("email");
  $query = $this->db->get_where("member", Array("id" => $id));

  if ($query->num_rows() <= 0) return null;
  return $query->row()->email;
}

}

/* End of file user_model.php */
/* Location: ./application/models/user_model.php */
