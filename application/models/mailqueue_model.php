<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mailqueue_model extends CI_Model{

function __construct(){
    parent::__construct();
}

function add_mailqueue($type, $data){
    $data = array('type' => $type,
                'data' => serialize($data),
                'created_at' => date('Y-m-d H:i:s')
                );

    $query = $this->db->insert('mail_queue', $data);
    if ( ! $query)  return FALSE;

    return TRUE;
}

function get(){
    $this->db->select("*");
    $this->db->limit(10);

    $query = $this->db->get_where("mail_queue", Array("send_status" => null));

    if ($query->num_rows() <= 0) return null;
    return $query->result();
}

/**
 * 更新為true 已發送
 *
 * */
function update_status($id, $status){
  $data = array('send_status' => $status,
                'send_time' => date('Y-m-d H:i:s')
                );

  $this->db->where('id',$id);
  $this->db->update('mail_queue', $data);
}
}
/* End of file mailqueue_model.php */
/* Location: ./application/models/mailqueue_model.php */