<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Credit_model extends CI_Model{
function __construct(){
    parent::__construct();
    $this->load->helper('array_to_object');
}
function add_credit(){
    //新增評價前，先檢查是否已經給過評價
    $task_id = (int)$this->input->post('task_id');
    $giver_id = (int)$this->input->post('giver_id');
    $recipient_id = (int)$this->input->post('recipient_id');
    $recipient_identity = $this->input->post('recipient_identity');
    $is_credited = $this->is_credited($task_id, $giver_id ,$recipient_id, $recipient_identity);
    if ($is_credited == TRUE) return FALSE; //給過評價

    $data = array('task_id' => $task_id,
                  'giver_id' => $giver_id,
                  'recipient_id' => $recipient_id,
                  'recipient_identity' => $recipient_identity,
                  'score' => (int)$this->input->post('score'),
                  'comment' => (string)$this->input->post('comment'),
                  'created_at' => date('Y-m-d H:i:s'),
                  'ip_address' => $this->input->ip_address()
                  );
    $this->db->insert('credit',$data);

    if ($this->db->affected_rows() == 0) return FALSE; //新增失敗時

    //給評價後，判斷發案者是否已經給所有跑者評價，則此任務task_status=2 結案，目前拔掉
    // $this->load->model('task_model');
    // $this->task_model->set_task_finish($task_id);

    return TRUE;
}
/**
 * 當任務完成時 取得此任務的所有跑者評價
 * @param  [type] $task_id [description]
 */
function get_finished_credit($task_id){
    $this->db->select("cd.*,
                      m.name, m.img");
    $this->db->from('credit as cd');
    $this->db->join('member as m', "cd.recipient_id = m.id ", 'left');
    $where = array('task_id' => $task_id ,
                   'cd.recipient_identity' => 'runner'  );
    $this->db->where($where);
    $query = $this->db->get();

    if ($query->num_rows() <= 0) return FALSE;
    $row_obj = array_to_object(html_escape($query->result_array())); //過濾htmlspecialchars之後再轉回object
    return $row_obj;
}

/**
 * 該位跑者評價總數量、平均評價
 * @param  [type]  [total　avg]
 */
function get_runner_credit_total($member_id){
    $query = $this->db->query("
       SELECT count(*) as total,round(AVG(score),1) avg
       FROM `credit` GROUP BY recipient_id Having recipient_id=$member_id"
      );

     return $query->row();
}

/**
 * 該跑者所有評價內容
 */
function get_runner_credit($member_id){
    $this->db->select("cd.id, cd.task_id, cd.giver_id, cd.recipient_identity, cd.comment,
                       cd.created_at, cd.score,
                      m.name, m.img,
                      t.title" );
    $this->db->from('credit as cd');
    $this->db->join('member as m', 'cd.giver_id = m.id ', 'left');
    $this->db->join('task as t', 'cd.task_id = t.id', 'left');
    $where = array('cd.recipient_id' => $member_id);
    $this->db->where($where);
    $query = $this->db->get();

    if ($query->num_rows() <= 0) return FALSE;
    $row_obj = array_to_object(html_escape($query->result_array())); //過濾htmlspecialchars之後再轉回object
    return $row_obj;
}

/**
 * 該跑者各任務分類評分
 */
function count_runner_catalog($member_id){
    $this->db->select("cd.task_id, cd.recipient_id , count(cd.id) total,
                     t.catalog_id, ROUND(AVG(cd.score),1) as avg,
                     tc.catalog",FALSE); //要設定為FALSE才行
    $this->db->from('credit as cd');
    $this->db->join('task as t', "cd.task_id = t.id AND cd.recipient_id=" .$member_id." AND cd.recipient_identity='runner' ", 'left');
    $this->db->join('task_catalog as tc', 't.catalog_id = tc.catalog_id', 'left');
    $this->db->group_by('t.catalog_id');
    $this->db->having('cd.recipient_id',$member_id);

    $query = $this->db->get();

    if ($query->num_rows() <= 0) return FALSE;
    $row_obj = array_to_object(html_escape($query->result_array())); //過濾htmlspecialchars之後再轉回object
    return $row_obj;
}
/**
 * [檢查某任務giver是否給過recipient評價]
 * @param  [type] $recipient_identity被評價者身分 [被評價者身分]
 * @return boolean        [description]
 */
function is_credited($task_id, $giver_id, $recipient_id, $recipient_identity){
    $this->db->select("task_id,created_at");
    $this->db->from('credit');
    $where = array('task_id' =>$task_id ,
                    'giver_id' =>$giver_id ,
                    'recipient_id' =>$recipient_id,
                    'recipient_identity' =>$recipient_identity );
    $this->db->where($where);
    $query = $this->db->get();

    if ($query->num_rows() <= 0) return false;
    return true; //給過評價
}

}
/* End of file credit_model.php */
/* Location: ./application/models/credit_model.php */