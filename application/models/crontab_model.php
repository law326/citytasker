<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Crontab_model extends CI_Model{
function __construct(){
    parent::__construct();
}

/**
 * 昨天寄發多少封mail
 */
function count_mail($day){
    $this->db->select("count(id) count
                   ");
    $this->db->from('mail_queue as m');
    $this->db->like('created_at', $day, 'after');
    $query = $this->db->get();

    return $query->row()->count;
}

/**
 * 昨天新加入多少member
 */
function count_member($day){
    $this->db->select("count(id) count
                   ");
    $this->db->from('member as m');
    $this->db->like('created_at', $day, 'after');
    $query = $this->db->get();

    return $query->row()->count;
}

/**
 * 每分鐘檢查 task_status先以status group取出最大
 * 並挑選狀態為0的(表示沒人接)
 * 取這些任務的deadline來判斷
 * 若deadline小於現在時間 狀態加一筆4失敗
 * 如果沒人報名就 disable 1
 * @return [type] [description]
 */
public  function check_task_deadline(){
    $this->db->select("s.task_id,MAX(s.status) as status,
                      t.deadline
                      ");
    $this->db->from('task_status as s');
    $this->db->join('task as t', "s.task_id = t.id ", 'left');
    $this->db->group_by("s.task_id");
    $this->db->having('status',0);

    $query = $this->db->get();

    //判斷deadline是否小於現在時間
    $now = time();
    $this->load->model('task_model');
    foreach ($query->result() as $task) {
      $deadline = strtotime($task->deadline);
      if ($now > $deadline) {
        //該任務到期了
        $runner = $this->task_model->count_runner_num($task->task_id);
        if ($runner == 0) {
           //沒有人報名
           $this->task_model->add_status($task->task_id, 4, 1); // 標記1任務不顯示
        }else{
           $this->task_model->add_status($task->task_id, 4, 0);
        }

      }
    }
}

}
/* End of file crontab_model.php */
/* Location: ./application/models/crontab_model.php */