<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 任務相關的資料操作
 */
class Task_model extends CI_Model{
  // private $formInput = array(); //表單post過來的所有內容，存在此，方便所有method使用

  public function __construct()  {
    parent::__construct();
    $this->load->helper('array_to_object'); //陣列物件互轉
  }

  /**
   * [新增任務]
   * @param [type] $member_id [description]
   */
  function add_task($member_id){
    // 先新增task表取得insert_id、在新增其它附屬表 如private表
    $deadline = $this->input->post('date') . ' ' . $this->input->post('time');//

    $this->load->helper('address_to_area_helper'); //將地址轉換為area
    if ($this->input->post('address') != "") {
      $area = address_to_area($this->input->post('address'));
    }else{
      $area = 'online';
    }

    $data = array('title' => $this->input->post('title'),
                  'content' => $this->input->post('content'),
                  'onlinetask' => (int)$this->input->post('onlinetask'),
                  'area' => $area,
                  'catalog_id' => (int)$this->input->post('catalog_hidden'),
                  'deadline' => $deadline,
                  'payment' => (int)$this->input->post('payment'),
                  'price' => (int)$this->input->post('price'),
                  'member_id' => (int)$member_id
                  // 'people' => (int)$this->input->post('people'),
                  );
    $this->db->insert('task',$data);
    $insert_id = $this->db->insert_id(); //取得此新任務id

    if ($this->db->affected_rows() == 0) return FALSE; //新增失敗時

    // $content_private = $this->input->post('content_private'); // 新增私密留言
    // if ( !empty($content_private)) $this->add_content_private($insert_id);

    $contact_email = $this->input->post('email'); // 新增連絡信箱
    if ( ! empty($contact_email)) $this->add_contact_email($insert_id, $contact_email);

    $contact_phone = $this->input->post('phone'); // 新增連絡電話
    if ( ! empty($contact_phone)) $this->add_contact_phone($insert_id, $contact_phone);

    // 新增任務地址，實體任務才有
    if ($this->input->post('onlinetask') == FALSE) $this->add_address($insert_id);

    // 新增任務狀態task_status=1
    $this->add_status($insert_id,0);

    $this->add_event_log('task_post');

    return $insert_id;
  }
  /**
   * [新增私密留言，若存在則用update]
   */
  private function add_content_private($task_id){
      $data_private = array('task_id' => (int)$task_id,
                              'content' => $this->input->post('content_private')
                              );

      $this->db->where('task_id',$task_id);
      $q = $this->db->get('task_private');
      if ( $q->num_rows() > 0 ) {
        //若存在則用update
        $this->db->where('task_id',$task_id);
        $this->db->update('task_private', $data_private);
      }else{
        $this->db->insert('task_private',$data_private);
      }

      return ($this->db->affected_rows() == 0) ? FALSE : TRUE;
  }

  /**
   * [新增聯絡資訊]
   */
  private function add_contact_email($task_id, $content){
      $data = array('task_id' => (int)$task_id,
                              'email' => $content
                              );

      $this->db->where('task_id', $task_id);
      $this->db->insert('task_email',$data);
      return ($this->db->affected_rows() == 0) ? FALSE : TRUE;
  }
  private function add_contact_phone($task_id, $content){
      $data = array('task_id' => (int)$task_id,
                              'phone' => $content
                              );

      $this->db->where('task_id', $task_id);
      $this->db->insert('task_phone',$data);
      return ($this->db->affected_rows() == 0) ? FALSE : TRUE;
  }

  function get_contact_email($task_id){
      $this->db->select("email");
      $this->db->from('task_email');
      $where = array('task_id' => $task_id);
      $this->db->where($where);

      $query = $this->db->get();

      if ($query->num_rows() <= 0) return FALSE;
      return $query->row()->email;
  }
  function get_contact_phone($task_id){
      $this->db->select("phone");
      $this->db->from('task_phone');
      $where = array('task_id' => $task_id);
      $this->db->where($where);

      $query = $this->db->get();

      if ($query->num_rows() <= 0) return FALSE;
      return $query->row()->phone;
  }
  /**
   * [新增地址，若存在則用update]
   */
  private function add_address($task_id){
    $data_address = array('task_id' => (int)$task_id,
                          'address' => $this->input->post('address'),
                          'lat_lng' => $this->input->post('lat') .','. $this->input->post('lng')
                          );

    $this->db->where('task_id',$task_id);
    $q = $this->db->get('task_address');
    if ( $q->num_rows() > 0 ) {
      //若存在則用update
      $this->db->where('task_id',$task_id);
      $this->db->update('task_address', $data_address);
    }else{
      $this->db->insert('task_address', $data_address);
    }

    return ($this->db->affected_rows() == 0) ? FALSE : TRUE;
  }

  /**
   * [新增任務狀態]
   */
  function add_status($task_id, $status, $disable){
    $data_status = array('task_id' => (int)$task_id,
                         'status' => $status,
                         'disable' => $disable,
                         'created_at' => date('Y-m-d H:i:s')
                         );
    $this->db->insert('task_status', $data_status);
    return ($this->db->affected_rows() == 0) ? FALSE : TRUE;
  }

  //尚未開放區域的任務 暫存
  function add_task_temp($member_id,$input){
    // 先新增task表取得insert_id、在新增其它附屬表 如private表
    $deadline = $this->input->post('date') . ' ' . $this->input->post('time');//
    $data = array('title' => $this->input->post('title'),
                  'content' => $this->input->post('content'),
                  'onlinetask' => (int)$this->input->post('onlinetask'),
                  'catalog_id' => (int)$this->input->post('catalog_hidden'),
                  'deadline' => $deadline,
                  'payment' => (int)$this->input->post('payment'),
                  'price' => (int)$this->input->post('price'),
                  'member_id' => (int)$member_id,
                  'people' => (int)$this->input->post('people'),
                  'address' => $this->input->post('address'),
                  'ip_address' => $this->input->ip_address()
                  );
    $this->db->insert('task_temp',$data);
  }

  function add_event_log($type){
    $data = array('member_id' => $_SESSION['member_id'],
                  'type' => $type,
                  'created_at' => date('Y-m-d H:i:s'),
                  'ip_address' => $this->input->ip_address()
                  );
    $did_add_user = $this->db->insert('event_log', $data);
  }

/**
 * 取得任務相關資訊
 * @param  [type] $task_id [description]
 * @return [type]          [description]
 */
function get_task($task_id){
    $this->db->select("task.*, task.id as task_id,
                   m.id as mid,m.name,m.img,
                   a.address, a.lat_lng,
                   c.catalog,
                   p.content as private_content,
                   e.email, ph.phone
                   ");
    $this->db->from('task');
    $this->db->join('task_status as s', 'task.id = s.task_id', 'left');
    $this->db->select("MAX(s.status) as status,s.created_at");
    $this->db->group_by("s.task_id");

    $this->db->join('member as m', 'task.member_id = m.id', 'left');
    $this->db->join('task_address as a', 'task.id = a.task_id', 'left');
    $this->db->join('task_catalog as c', 'task.catalog_id = c.catalog_id', 'left');
    $this->db->join('task_private as p', 'task.id = p.task_id', 'left');
    $this->db->join('task_email as e', 'task.id = e.task_id', 'left');
    $this->db->join('task_phone as ph', 'task.id = ph.task_id', 'left');

    $this->db->where('task.id',$task_id);

    // $this->db->limit($pageSize, $offset);
    // $this->db->order_by("task.created_at", "desc"); //由大到小排序
    $query = $this->db->get();

    if ($query->num_rows() <= 0) return null;
    $row_obj = array_to_object(html_escape($query->row_array())); //過濾htmlspecialchars之後再轉回object
    return $row_obj;
}

function get_task_title($task_id){
    $this->db->select("title");
    $this->db->from('task');
    $this->db->where('id',$task_id);
    $query = $this->db->get();
    return $query->row()->title;
}

/**
 * 取得發布任務紀錄
 * 先取出所有任務，再丟到get_posted_history_runner取得該任務跑者
 *  * FIXME:是否有一次下Query的方法
 * @param  [type] $member_id [description]
 * @return [type]          [description]
 */
function get_posted_history($member_id){
    $this->db->select("t.id, t.title, t.onlinetask, t.payment, t.price, t.deadline,
                      s.status, s.created_at,
                      m.phone, m.img,
                      a.address
                      ");
    $this->db->from('task as t');
    $this->db->join('task_status as s', 't.id = s.task_id', 'left'); //只要取最新狀態
    $this->db->select("MAX(s.status) as status,s.created_at");
    $this->db->group_by("s.task_id");
    $this->db->where('t.member_id',$member_id);

    $this->db->join('member as m', 't.member_id = m.id', 'left');
    $this->db->join('task_address as a', 't.id = a.task_id', 'left');
    // $this->db->order_by("t.deadline");
    $this->db->order_by("t.id", 'desc'); //照發案時間

    $query = $this->db->get();

    if ($query->num_rows() <= 0) return null;
    $result_obj = array_to_object(html_escape($query->result_array())); //過濾htmlspecialchars之後再轉回object
    return $result_obj;
}

/**
 * [取得發布任務紀錄的該任務所有跑者]
 * @param  [type] $task_id [description]
 * @return [type] [id　name　img　phone　email　task_id　status　created_at　count]
 */
public  function get_posted_history_runner($task_id){
 $this->db->select("m.id, m.name, m.img, m.phone ,m.email,
                   r.task_id, r.created_at,
                   cd.recipient_id, cd.recipient_identity
                   ");
 $this->db->from('task_runner as r');
 $this->db->join('member as m', "r.member_id = m.id AND r.status=1 AND r.task_id = $task_id", 'inner');
 // $where = array('r.task_id' => $task_id,
 //                'r.status' =>1 );
 // $this->db->where($where);
 $this->db->join('credit as cd', "r.member_id = cd.recipient_id AND cd.recipient_identity = 'runner' AND cd.task_id = $task_id", 'left');

 $query = $this->db->get();

if ($query->num_rows() <= 0) return null;
$result_obj = array_to_object(html_escape($query->result_array())); //過濾htmlspecialchars之後再轉回object
return $result_obj;
}

/**
 * 取得接案紀錄
 * @param  [type] $member_id [description]
 * @return [type]          [description]
 */
function get_running_history($member_id){
    $this->db->select("r.task_id, r.id runner_order,
                       t.id, t.title, t.onlinetask, t.payment, t.price, t.deadline,
                       MAX(r.status) as status,
                       m.id as member_id, m.name, m.img, m.phone p_phone,m.email p_email,
                       a.address,
                       p.content,
                       te.email,
                       tp.phone,
                       cd.recipient_id
                      ");
    $this->db->from('task_runner as r');
    $this->db->join('task as t', "r.task_id = t.id AND r.member_id = $member_id", 'inner');
    $this->db->join('member as m', 't.member_id = m.id', 'left');
    $this->db->join('task_address as a', 'r.task_id = a.task_id', 'left');
    $this->db->join('task_private as p', 'r.task_id = p.task_id', 'left');
    $this->db->join('task_email as te', 'r.task_id = te.task_id', 'left');
    $this->db->join('task_phone as tp', 'r.task_id = tp.task_id', 'left');
    $this->db->join('credit as cd', "t.member_id = cd.recipient_id AND cd.recipient_identity = 'poster' AND cd.task_id = r.task_id", 'left');
    $this->db->group_by("r.task_id");

    // $this->db->order_by("t.deadline");
    $this->db->order_by("runner_order", 'desc'); //照接案時間


    $query = $this->db->get();
    // echo $this->db->last_query(); //最近一筆 SQL 語法

    if ($query->num_rows() <= 0) return null;
    $result_obj = array_to_object(html_escape($query->result_array())); //過濾htmlspecialchars之後再轉回object
    return $result_obj;
}
/**
 * [任務地圖顯示所有任務]
 * @param  string  $order      [排序方式]
 * @param  string  $desc       [降冪]
 * @param  integer $onlinetask [是否為線上任務]
 * @param  [type]  $now        [現在時間]
 * @param  [type]  $catalog    [是否照分類查詢]
 * @param  [type]  $num        [限制筆數]
 * @return [type]              [description]
 */
function get_all_task($order='task.id', $desc='desc', $area='taipei', $onlinetask=null, $now=null, $catalog=null, $num=null){

    $this->db->select("task.*,
                      m.img,
                      MAX(s.disable) as disable,
                      MAX(s.status) as status,s.created_at,
                      a.address,a.lat_lng");
    $this->db->from('task');
    $this->db->join('member as m', 'task.member_id = m.id', 'left');
    $this->db->join('task_status as s', "task.id = s.task_id ",'left');
    $this->db->join('task_address as a', 'task.id = a.task_id', 'left');
    $this->db->group_by("s.task_id");
    $this->db->having('disable', 0);

    if (isset($onlinetask)) $this->db->where('task.onlinetask', $onlinetask);
    $arr_area = array($area, 'online');
    if (isset($area)) $this->db->where_in('area', $arr_area);
    if (isset($now))        $this->db->where('task.deadline >', $now); //日期格式2013-06-20 14:00
    if (isset($catalog))    $this->db->where('task.catalog_id', $catalog);
    if (isset($num))        $this->db->limit($num) ;
    $this->db->order_by($order, $desc); //由大到小排序
    $query = $this->db->get();

    return $query->result();
 }
 //後台看alltask
 function get_all_task_admin(){
     $this->db->select("task.*,
                        m.img,
                        s.created_at
                       ");
     $this->db->from('task');
     $this->db->join('member as m', 'task.member_id = m.id', 'left');
     $this->db->join('task_status as s', 'task.id = s.task_id', 'left');
     $this->db->where('s.status',0);

     // $this->db->join('task_address as a', 'task.id = a.task_id', 'left');
     $this->db->order_by('id','desc'); //由大到小排序
     $query = $this->db->get();

     return $query->result();
  }

/**
 * 任務總數
 * type=0實體任務 type=1線上任務
 */
function count_task_by_type($type) {
        $this->db->select("count(onlinetask) as total");
        $this->db->from('task');
        $this->db->where('onlinetask',$type);
        $query = $this->db->get();

        return $query->row()->total;
}

/**
 * 發案總數量
 */
function count_posted($member_id) {
        $this->db->select("count(id) as total");
        $this->db->from('task');
        $this->db->where('member_id', $member_id);
        $query = $this->db->get();

        return $query->row()->total;
}

/**
 * 計算跑者完成的任務數量,依據得到的評價數量
 */
function count_runner_finished($member_id) {
        $this->db->select("count(recipient_id) as total");
        $this->db->from('credit');
        $where = array('recipient_id' => $member_id,
                'recipient_identity' =>'runner' );
        $this->db->where($where);
        $query = $this->db->get();

        return $query->row()->total;
}

// 該任務跑者數量
function count_runner_num($task_id) {
        $this->db->select("count(id) as total");
        $this->db->from('task_runner');
        $this->db->where('task_id', $task_id);
        $query = $this->db->get();

        return $query->row()->total;
}
function get_status_by_id($id){
    $this->db->select("status,created_at");
    $this->db->from('task_status');
    $this->db->where('task_id',$id);
    $this->db->order_by("created_at"); //由大到小排序
    $query = $this->db->get();

    return $query->result(); //無資料時回傳 null
}

/**
 * 訪客與任務關係
 * 判斷是否為發案者->接案者->一般人
 * @param  [type] $task_id [description]
 * @return [type]            [description]
 */
function get_user_task_relation($task_id){
  if (isset($_SESSION['member_id'])) {
    //檢查是否為發案者
    $this->db->select("id,member_id");
    $this->db->from('task');
    $this->db->where('id', $task_id);
    $this->db->where('member_id', $_SESSION['member_id']);
    $query = $this->db->get();
    if ($query->num_rows() > 0) return 'poster' ;

    //檢查是否為接案者
    $this->db->select("id,member_id");
    $this->db->from('task_runner');
    $this->db->where('task_id', $task_id);
    $this->db->where('member_id', $_SESSION['member_id']);
    $query = $this->db->get();

    if ($query->num_rows() > 0) return 'runner' ;

    return 'stranger';
  }
}

/**
 * 取得此任務跑者個人資訊(含筆評價)
 * 使用 credit  runner  member 三張表
 * @param  [int] $task_id [description]
 * @return [type] [id　name　img　task_id　status　created_at　count　avg　]
 */
function get_runner($task_id){
 $task_id = (int)$task_id; //確保為數值
 $task_id = $this->db->escape($task_id); //直接查詢前進行過濾
 // FIXME:直接下SQL，尚未用成Active record
 $query = $this->db->query("
 SELECT  M.id, M.name, M.img,
         I.task_id, I.status, I.created_at, I.total, I.avg
 FROM (`member` as M)
 INNER JOIN (
             SELECT RN.* , FE.total, FE.avg
             FROM `task_runner` as RN
             LEFT JOIN (
                       SELECT recipient_id,count(*) as total,round(AVG(score),1) avg
                       FROM `credit` GROUP BY recipient_id) as FE
             ON (RN.member_id = FE.recipient_id)
             WHERE `task_id` =  $task_id
             ORDER BY RN.status desc
  ) I
 ON M.id = I.member_id
 GROUP BY M.id
 ORDER BY `created_at` desc LIMIT 100"

  );

 if ($query->num_rows() <= 0) return null;

 return $query->result();
}

/**
 * [檢查此任務是否有跑者，設定此任務狀態為執行中]
 * TODO:檢查此任務是否有跑者,寫好但尚未有地方使用，應該放在確認接案時呼叫檢查
 * @param [type] $task_id [description]
 */
function set_task_running($task_id){
    $this->db->select("status");
    $this->db->from('task_status');
    $this->db->where('task_id', $task_id);
    $query_status = $this->db->get();//當status尚未有狀態1 也就是數量只有1筆

    $this->db->select("count(id) as count");
    $this->db->from('task_runner');
    $where = array('task_id' => $task_id,
                   'status' => 1 );
    $this->db->where($where);
    $query_runner = $this->db->get();//當有跑者

    if ($query_status->num_rows() == 1 AND $query_runner->num_rows() > 0) {
        //新增一筆task_status=1
        $this->add_status($task_id,1);
        return TRUE;
    }
    return FALSE;
}

/**
 * [檢查當此任務所有跑者都有評價，則此任務task_status=2 結案]
 * @param [type] $task_id [description]
 */
function set_task_finish($task_id){
    $this->db->select("people, count(c.id) as count");
    $this->db->from('task as t');
    $this->db->join('credit as c', "c.task_id = t.id AND c.recipient_identity = 'runner' AND c.task_id='$task_id'", 'left');
    $query = $this->db->get();
    if ($query->num_rows() < 0) return false ;

    //新增一筆task_status=2
    if ($query->row()->people == $query->row()->count) {
        $this->add_status($task_id,2);
    }
}

function is_poster($task_id){
  $this->db->select("id,member_id");
  $this->db->from('task as t');
  $where = array('id' => $task_id,
                 'member_id' => $_SESSION['member_id']  );
  $this->db->where($where);
  $query = $this->db->get();
  if ($query->num_rows() > 0) return TRUE;

  return FALSE;
}

function is_first_runner($task_id){
  $this->db->select("task_id, status");
  $this->db->from('task_status');
  $where = array('task_id' => $task_id ,
                 'status' => 1   );
  $this->db->where($where);
  $query = $this->db->get();
  if ($query->num_rows() == 0) return TRUE;

  return FALSE;
}



//任何修改
function update($task_id, $data){
  $this->db->where('id', $task_id);
  $this->db->update('task', $data);

  if ($this->db->affected_rows() == 0) return FALSE; //修改失敗時
}

function update_task($id) {
  $deadline = $this->input->post('date') . ' ' . $this->input->post('time');//
  $data = array('title' => $this->input->post('title'),
                'content' => $this->input->post('content'),
                'onlinetask' => (int)$this->input->post('onlinetask'),
                'catalog_id' => (int)$this->input->post('catalog_hidden'),
                'deadline' => $deadline,
                'payment' => (int)$this->input->post('payment'),
                'price' => (int)$this->input->post('price'),
                'member_id' => (int)$_SESSION['member_id'],
                'people' => (int)$this->input->post('people'),
                'ip_address' => $this->input->ip_address()
                );

  $this->db->where('id',$id);
  $this->db->update('task', $data);

  if ($this->db->affected_rows() == 0) return FALSE; //修改失敗時

  $content_private = $this->input->post('content_private'); // 修改私密留言
  if ( !empty($content_private)) $this->add_content_private($id);

  if ($this->input->post('onlinetask') == FALSE) $this->add_address($id); // 修改任務地址

  return TRUE;
// ******************************


    // $data = array(
    //     'Title' => $title,
    //     'Content' => $content
    // );

    // $this->db->where('ArticleID', $id);
    // $this->db->update('article', $data);
}

}
