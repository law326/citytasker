<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 存取Facebook資料
 */
class Fbci_model extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->helper('array_to_object');
    }
    /**
     * [新增會員]
     * @param [type] $member_id [本站id]
     */
    function add_member($me){
        // 先新增本站會員 取得id
        $data = array('name' => $me['name'],
                      'email' => $me['email'],
                      'zip' => '',
                      'img' => '',
                      'created_at' => date('Y-m-d H:i:s')
                      // 'register_from' => 1
                      );
        $this->db->insert('member', $data);

        if ($this->db->affected_rows() == 0) return FALSE; //新增失敗時

        $member_id = $this->db->insert_id();

        //也新增到 member_facebook 表
        $data = array('fb_id' => $me['id'],
                      'member_id' => $member_id,
                      'fb_email' => $me['email'],
                      'fb_name' => $me['name'],
                      'fb_username' => $me['username'],
                      'fb_gender' => $me['gender'],
                      'fb_birthday' => $me['birthday'],
                      'created_at' => date('Y-m-d H:i:s'),
                      'ip_address' => $this->input->ip_address(),
                      'temp' => serialize($me) // 存下所有FB/me 撈回來的資訊,以備不時之需
                      );
        $this->db->insert('member_facebook',$data);

        //新增個人圖示為FB的
        $file_name = $member_id . '_' . date("YmdHis") .'.jpg';

        $c = curl_init();
        curl_setopt($c, CURLOPT_URL, $me['picture']['data']['url']);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        $image = curl_exec($c);
        curl_close($c);

        // $image = file_get_contents($me[picture][data][url]); //取得個人圖示
        $file_save = file_put_contents("./uploads/head_img/$file_name", $image); // 下載存檔,失敗回傳flase
        $head_img = ($file_save) ? $file_name : 'default_pic.png' ;

        $this->db->where('id',$member_id);
        $this->db->update('member', array('img' =>$head_img ));
        return $member_id;
    }

    function get_member_by_fbid($fbid,$get_items) {
        $this->db->select("$get_items");
        $query = $this->db->get_where("member_facebook", Array("fb_id" => $fbid));

        if ($query->num_rows() <= 0) return null;
        $row_obj = array_to_object(html_escape($query->row_array())); //過濾htmlspecialchars之後再轉回object
        return $row_obj;
    }

    function is_member_by_fbid($fbid) {
      $this->db->select("fb_id");
      $query = $this->db->get_where("member_facebook", Array("fb_id" => $fbid));

      if ($query->num_rows() <= 0) return FALSE;
      return TRUE;
    }

    // function get_credit($task_id){
    //   // TODO:尚未把取得評價 改寫到此
    //     $this->db->select("cd.*,
    //                       m.name, m.img");
    //     $this->db->from('credit as cd');
    //     $this->db->join('member as m', 'cd.recipient_id = m.id', 'left');
    //     $this->db->where('task_id', $task_id);
    //     $query = $this->db->get();

    //     if ($query->num_rows() <= 0) return FALSE;
    //     $row_obj = array_to_object(html_escape($query->result_array())); //過濾htmlspecialchars之後再轉回object
    //     return $row_obj;
    // }
    /**
     * [檢查某任務giver是否給過recipient評價]
     * @param  [type] $recipient_identity被評價者身分 [被評價者身分]
     * @return boolean        [description]
     */
    function is_credited($task_id, $giver_id, $recipient_id, $recipient_identity){
        $this->db->select("task_id,created_at");
        $this->db->from('credit');
        $where = array('task_id' =>$task_id ,
                        'giver_id' =>$giver_id ,
                        'recipient_id' =>$recipient_id,
                        'recipient_identity' =>$recipient_identity );
        $this->db->where($where);
        $query = $this->db->get();

        if ($query->num_rows() <= 0) return false;
        return true; //給過評價
    }

}
/* End of file fbci_model.php */
/* Location: ./application/models/fbci_model.php */