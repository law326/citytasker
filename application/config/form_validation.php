<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$config = array('user/login_ing' => array(
                array('field' => 'email',
                      'label' => 'Email',
                      'rules' => 'trim|xss_clean|required|valid_email|callback_validate_credentials'
                      ),
                array('field' => 'password',
                      'label' => '密碼',
                      'rules' => 'trim|xss_clean|required|callback_validate_resubmit|md5'
                      )
                ),

'user/signup_ing' => array(
                           array('field' => 'username',
                                 'label' => '姓名',
                                 'rules' => 'trim|required|min_length[2]|max_length[12]|xss_clean|callback_validate_resubmit'
                                 ),
                           array('field' => 'email',
                                 'label' => 'Email',
                                 'rules' => 'trim|required|valid_email|xss_clean|is_unique[member.email]'
                                 ),
                           array('field' => 'zip',
                                 'label' => '郵遞區號',
                                 'rules' => 'trim|required|xss_clean|min_length[3]|is_natural'
                                 ),
                           array('field' => 'password',
                                 'label' => '密碼',
                                 'rules' => 'trim|required|xss_clean|min_length[6]|md5'
                                 )
                           ),

'user/settings_update' => array(
                                array('field' => 'name',
                                      'label' => '匿名',
                                      'rules' => 'trim|required|xss_clean|min_length[1]'
                                      ),
                                array('field' => 'realname',
                                      'label' => '真實姓名',
                                      'rules' => 'trim|xss_clean|min_length[2]'
                                      ),
                                array('field' => 'email2',
                                      'label' => '連絡信箱',
                                      'rules' => 'trim|valid_email|xss_clean'
                                      ),
                                array('field' => 'phone',
                                      'label' => '手機號碼',
                                      'rules' => 'trim|xss_clean|required|min_length[10]|max_length[10]|is_natural'
                                      ),
                                array('field' => 'zip',
                                      'label' => '郵遞區號',
                                      'rules' => 'trim|xss_clean|min_length[3]|is_natural'
                                      )
                                ),
'user/forgot_password' => array(
                                array('field' => 'email_forgot',
                                      'label' => 'Email',
                                      'rules' => 'trim|required|valid_email|xss_clean'
                                      )
                                ),

'phone_vaild/phone_vaild_apply' => array(
                                array('field' => 'input_phone',
                                      'label' => '手機號碼',
                                      'rules' => 'trim|xss_clean|required|min_length[10]|max_length[10]|is_natural|callback_validate_resubmit|is_unique[member.phone]'
                                      )
                                ),

'phone_vaild/phone_vaild_ing' => array(
                                array('field' => 'input_phone',
                                      'label' => '驗證碼',
                                      'rules' => 'trim|xss_clean|required|min_length[6]|max_length[6]|is_natural|callback_validate_resubmit'
                                      )
                                ),

'task/post_form_validate' => array(
                                array('field' => 'catalog_hidden',
                                      'label' => '任務種類',
                                      'rules' => 'trim|xss_clean|required|max_length[2]'
                                      ),
                                array('field' => 'title',
                                      'label' => '任務標題',
                                      'rules' => 'trim|xss_clean|required|min_length[2]|max_length[30]'
                                      ),
                                array('field' => 'content',
                                      'label' => '任務內容',
                                      'rules' => 'trim|xss_clean|required|min_length[5]|max_length[2000]'
                                      ),
                                // array('field' => 'content_private',
                                //       'label' => '私密內容',
                                //       'rules' => 'trim|xss_clean|max_length[45]'
                                //       ),
                                array('field' => 'email',
                                      'label' => '聯絡email',
                                      'rules' => 'trim|xss_clean|required|valid_email'
                                      ),
                                array('field' => 'onlinetask',
                                      'label' => '任務位置',
                                      'rules' => 'trim|xss_clean|required|'
                                      ),
                                array('field' => 'date',
                                      'label' => '完成日期',
                                      'rules' => 'trim|xss_clean|required'
                                      ),
                                array('field' => 'time',
                                      'label' => '完成時間',
                                      'rules' => 'trim|xss_clean|required'
                                      ),
                                array('field' => 'people',
                                      'label' => '需求人數',
                                      'rules' => 'trim|xss_clean|is_natural|max_length[99]'
                                      ),
                                array('field' => 'payment',
                                      'label' => '付款方式',
                                      'rules' => 'trim|xss_clean|required'
                                      ),
                                array('field' => 'price',
                                      'label' => '任務酬勞',
                                      'rules' => 'trim|xss_clean|required|is_natural'
                                      )
                                ),
'service/email_sending' => array(
                                array('field' => 'sending_title',
                                      'label' => '標題',
                                      'rules' => 'trim|xss_clean|required|min_length[2]|max_length[45]'
                                      ),
                                array('field' => 'sending_content',
                                      'label' => '內容',
                                      'rules' => 'trim|xss_clean|required|min_length[5]|max_length[2000]'
                                      ),
                                array('field' => 'sending_email',
                                      'label' => 'Email',
                                      'rules' => 'trim|xss_clean|required|valid_email'
                                      )
                                ),
'credit/give_ing' => array(
                                array('field' => 'recipient_id',
                                      'label' => '被評價者id',
                                      'rules' => 'trim|xss_clean|required'
                                      ),
                                array('field' => 'comment',
                                      'label' => '評語',
                                      'rules' => 'trim|xss_clean|required|min_length[2]|max_length[2000]'
                                      ),
                                array('field' => 'score',
                                      'label' => '評分',
                                      'rules' => 'trim|xss_clean|required|numeric'
                                      )
                                ),
'discuss/post_ing' => array(
                                array('field' => 'content',
                                      'label' => '留言內容',
                                      'rules' => 'trim|xss_clean|required|min_length[2]|max_length[250]'
                                      )
                                ),
'user/vitae_ing' => array(
                                array('field' => 'language',
                                      'label' => '語言',
                                      'rules' => 'trim|xss_clean|min_length[2]|max_length[50]'
                                      ),
                                array('field' => 'education',
                                      'label' => '最高學歷',
                                      'rules' => 'trim|xss_clean|min_length[2]|max_length[50]'
                                      ),
                                array('field' => 'job',
                                      'label' => '目前工作',
                                      'rules' => 'trim|xss_clean|min_length[2]|max_length[50]'
                                      ),
                                array('field' => 'about',
                                      'label' => '自我介紹',
                                      'rules' => 'trim|xss_clean|min_length[2]|max_length[2000]'
                                      )
                                ),
'spider/post_ing' => array(
                                array('field' => 'catalog_hidden',
                                      'label' => '任務種類',
                                      'rules' => 'trim|xss_clean|required|max_length[2]'
                                      ),
                                array('field' => 'title',
                                      'label' => '任務標題',
                                      'rules' => 'trim|xss_clean|required|min_length[2]|max_length[30]'
                                      ),
                                array('field' => 'content',
                                      'label' => '任務內容',
                                      'rules' => 'trim|xss_clean|required|min_length[5]|max_length[2000]'
                                      ),
                                // array('field' => 'content_private',
                                //       'label' => '私密內容',
                                //       'rules' => 'trim|xss_clean|max_length[45]'
                                //       ),
                                array('field' => 'email',
                                      'label' => '聯絡email',
                                      'rules' => 'trim|xss_clean|required|valid_email'
                                      ),
                                array('field' => 'onlinetask',
                                      'label' => '任務位置',
                                      'rules' => 'trim|xss_clean|required|'
                                      ),
                                array('field' => 'date',
                                      'label' => '完成日期',
                                      'rules' => 'trim|xss_clean|required'
                                      ),
                                array('field' => 'time',
                                      'label' => '完成時間',
                                      'rules' => 'trim|xss_clean|required'
                                      ),
                                array('field' => 'people',
                                      'label' => '需求人數',
                                      'rules' => 'trim|xss_clean|is_natural|max_length[99]'
                                      ),
                                array('field' => 'payment',
                                      'label' => '付款方式',
                                      'rules' => 'trim|xss_clean|required'
                                      ),
                                array('field' => 'price',
                                      'label' => '任務酬勞',
                                      'rules' => 'trim|xss_clean|required|is_natural'
                                      )
                                )
);
