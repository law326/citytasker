<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "welcome";//"welcome";
$route['404_override'] = 'error_404';//" " 自訂錯誤頁面
$route['task/(:num)'] = "task/info/$1"; //任務頁面URL去掉info/
$route['task/(:num)/(:any)'] = "task/info/$1/$1"; //接收spider的資訊
// $route['task/map/'] = "task/map/$1"; // 地圖頁面URL去掉task/map/ 未完成
$route['user/(:num)'] = "user/info/$1"; //個人簡介頁面URL去掉info/
$route['admin/(:num)'] = "admin/index/$1"; //index
$route['spider/(:num)'] = "spider/index/$1"; //index
// $route['task/info'] = "task";

/* End of file routes.php */
/* Location: ./application/config/routes.php */
