<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 判斷地址所屬的區域
 */
function address_to_area($address){
    $CI =& get_instance();
    $CI->load->helper('strpos_array_helper');

    if (strpos_array($address, array('台北市', '新北市', '基隆市'))) {
        return 'taipei';

    } elseif(strpos_array($address, array('桃園縣'))) {
        return 'taoyuan';

    } elseif(strpos_array($address, array('台中市'))) {
        return 'taichung';

    } elseif(strpos_array($address, array('高雄市'))) {
        return 'kaohsiung';

    } elseif(strpos_array($address, array('新竹市', '新竹縣'))) {
        return 'hsinchu';

    } elseif(strpos_array($address, array('台南市'))) {
        return 'tainan';

    } elseif(strpos_array($address, array('宜蘭縣'))) {
        return 'yilan';

    } elseif(strpos_array($address, array('苗栗縣'))) {
        return 'miaoli';

    } elseif(strpos_array($address, array('南投縣'))) {
        return 'nantou';

    } elseif(strpos_array($address, array('彰化縣'))) {
        return 'changhua';

    } elseif(strpos_array($address, array('雲林縣'))) {
        return 'yunlin';

    } elseif(strpos_array($address, array('嘉義縣'))) {
        return 'chiayi';

    } elseif(strpos_array($address, array('屏東縣'))) {
        return 'pingtung';

    } elseif(strpos_array($address, array('花蓮縣'))) {
        return 'hualien';

    } elseif(strpos_array($address, array('台東縣'))) {
        return 'taitung';

    } elseif(strpos_array($address, array('連江縣', '澎湖縣', '金門縣'))) {
        return 'islands';

    } else{
        return null;
    }
}

/* End of file address_to_area_helper.php */
/* Location: ./application/helpers/address_to_area_helper.php */
