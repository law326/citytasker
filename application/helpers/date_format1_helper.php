<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 轉換日期格式為
 * 將2013-06-04 14:10:27 轉換為 6/04 14:10
 * @return datetime [description]
 */
function date_format1($date){
    if (isset($date)) {
        $time = strtotime($date);
        return date("m/d H:i",$time);
    }
    return FALSE;
}


/* End of file date_format_1_helper.php */
/* Location: ./application/helpers/date_format_1_helper.php */
