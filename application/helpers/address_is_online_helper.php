<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 當地址空白時，判斷此任務為非實體任務
 * 輸出"線上或手機"
 */
function address_is_online($address){
    if (empty($address)) return "線上或手機";
    return $address;
}

/* End of file address_format_1_helper.php */
/* Location: ./application/helpers/address_format_1_helper.php */
