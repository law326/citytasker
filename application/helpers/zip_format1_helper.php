<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 轉換zip格式為地區名稱
 */
function zip_format1($zip){

    if ( empty($zip)) {
        $zip = '尚未填寫郵遞區號';
    } else {
        // 丟到Google geocoding 查詢
        $CI =& get_instance();
        $CI->load->library('geocoding');
        $response = $CI->geocoding->geocode($zip);
        $zip = $response['country_to_zip'];
    }

    return $zip;
}


/* End of file zip_format_1_helper.php */
/* Location: ./application/helpers/zip_format_1_helper.php */
