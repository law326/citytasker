<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 將member_vitae讀出資料轉換格式
 */
function jobway_format1($result){
    if ( ! isset($result)) return FALSE;

    if ($result->jobway1) $jobway = $jobway . '手機或電腦' .'、';
    if ($result->jobway2) $jobway = $jobway . '步行'.'、';
    if ($result->jobway3) $jobway = $jobway . '自行車'.'、';
    if ($result->jobway4) $jobway = $jobway . '摩托車'.'、';
    if ($result->jobway5) $jobway = $jobway . '轎車'.'、';
    if ($result->jobway6) $jobway = $jobway . '卡車'.'、';

    if (empty($jobway)) $jobway = '尚未填寫';
    return rtrim($jobway, '、');
}


/* End of file jobway_format_1_helper.php */
/* Location: ./application/helpers/jobway_format_1_helper.php */
