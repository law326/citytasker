<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 檢查Session判斷使用者是否已經登入
 * 方便View及Controller使用
 * @return boolean [description]
 */
function is_logged_in(){
  $chk = md5($_SERVER['HTTP_USER_AGENT'].'citytasker');

  if (isset($_SESSION['key']) AND $_SESSION['key'] == $chk) {
      return TRUE;
  }
  return FALSE;
}

// Checks whether the user is being remembered
// Returns session or cookie if there is one
// function is_kept_logged(){
//     if($_COOKIE["keep_me"]){
//         $keep_me = $_COOKIE["keep_me"];
//     }elseif($_SESSION["keep_me"]){
//         $keep_me = $_SESSION["keep_me"];
//     }else{
//         return FALSE;
//     }
//     return $keep_me;
// }

/* End of file is_logged_in_helper.php */
/* Location: ./application/helpers/is_logged_in_helper.php */
