<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 比對字串是否 含有email
 */
function str_preg_match_email($str){
    $matches = array();
    $pattern_arr = '/[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i';
    preg_match_all($pattern_arr, $str, $matches);

    if (empty($matches[0]) == 1) {
        return FALSE;
    }else{
        return $matches; //回傳 email array
    }
}

/* End of file str_preg_match_email_helper.php */
/* Location: ./application/helpers/str_preg_match_email_helper.php */
