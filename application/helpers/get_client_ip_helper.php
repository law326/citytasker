<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 較嚴謹的取得clinet的ip
 * 仍有些問題，還是先用CI內建的，暫時先不用這個
 * 來源 http://www.jaceju.net/blog/archives/1913/
 */
function get_client_ip()
{
    foreach (array(
                'HTTP_CLIENT_IP',
                'HTTP_X_FORWARDED_FOR',
                'HTTP_X_FORWARDED',
                'HTTP_X_CLUSTER_CLIENT_IP',
                'HTTP_FORWARDED_FOR',
                'HTTP_FORWARDED',
                'REMOTE_ADDR') as $key) {
        if (array_key_exists($key, $_SERVER)) {
            foreach (explode(',', $_SERVER[$key]) as $ip) {
                $ip = trim($ip);
                if ((bool) filter_var($ip, FILTER_VALIDATE_IP,
                                FILTER_FLAG_IPV4 |
                                FILTER_FLAG_NO_PRIV_RANGE |
                                FILTER_FLAG_NO_RES_RANGE)) {
                    return $ip;
                }
            }
        }
    }
    return null;
}

/* End of file get_client_ip_helper.php */
/* Location: ./application/helpers/get_client_ip_helper.php */
