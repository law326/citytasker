<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
*PHP 與 Javascript 整合常見問題：JS內不可含斷行字串，多行字串需特別處理
*用 '<?= str_replace(chr(10), '\n', $string);?>' 將斷行字元chr(10)替換成\n
*
*另外還有單引號,雙引號等 也需要跳脫處理
*<?= str_replace("'","\\'", $string);?>
* */
function escape_php_to_js($data){
    return str_replace("'","\\'", str_replace(chr(10), '\n', $data)) ;
}

/* End of file escape_php_to_js_helper.php */
/* Location: ./application/helpers/escape_php_to_js_helper.php */
