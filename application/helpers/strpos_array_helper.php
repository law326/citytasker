<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 比對字串是否有出現 某字元
 * 比對的元素為array
 */
function strpos_array($haystack, $needles, $offset=0){
     $chr = array();
     foreach($needles as $needle) {
             $res = strpos($haystack, $needle, $offset);
             if ($res !== false) $chr[$needle] = $res;
     }
     if(empty($chr)) return false;
     return min($chr);
}

/* End of file strpos_array_helper.php */
/* Location: ./application/helpers/strpos_array_helper.php */
