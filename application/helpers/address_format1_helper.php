<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 轉換地址格式為
 * 市等級 '區' 之前的字串才輸出
 * 縣等級 '鄉', '鎮', '市'之前的字串才輸出
 */
function address_format1($address){
    if ( ! isset($address)) return FALSE;
    $is_county = my_strpos( (string)$address, '縣');

    if ($is_county) {
        $pos = my_strpos( (string)$address, array('鄉', '鎮', '市' ));
    }else{
        $pos = my_strpos( (string)$address, array('區', '市' ));
    }

    return substr($address,0,$pos+3); // UTF中文編碼一字為3byte區之前的字串才輸出
}

/**
 * [擴充PHP原生strpos函數，使比對字串可以放入array]
 * @param  [type] $haystack [description]
 * @param  [type] $needle   [description]
 * @return [type]           [description]
 */
function my_strpos($haystack, $needle) {
     if (is_array($needle)) {
         foreach ($needle as $need) {
               if (strpos($haystack, $need) !== false) {
                       return strpos($haystack, $need);
               }
         }
     }else {
          if (strpos($haystack, $needle) !== false) {
                       return strpos($haystack, $needle);
          }
     }

     return false;
}

/* End of file address_format_1_helper.php */
/* Location: ./application/helpers/address_format_1_helper.php */
