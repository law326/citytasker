<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 取得多段URL的GET參數
 * 將參數以 / 分段取出
 */
function get_multi_parameters(){
    return implode('/', func_get_args());
}

/* End of file get_multi_parameters_helper.php */
/* Location: ./application/helpers/get_multi_parameters_helper.php */
