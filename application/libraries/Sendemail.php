<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 寄信的統一中樞
 * 期望最後達到email queue
 * BoBo
 *
 */
class Sendemail {

    function __construct(){

    }

    // FIXME:可以用 動態參數調用 $data['runner_mail'] 目前需要知道參數名稱
    // 報名任務, 案主接收
    function make_offer($data, $queue_id) {
        // $to_mail, $runner_name, $runner_img, $task_name, $task_id
        $CI =& get_instance();
        $CI->load->library('email');
        $CI->email->from('admin@citytasker.tw', 'Citytasker');
        $CI->email->to($data['to_mail']);
        $CI->email->subject('任務:' . $data['task_name'] . '有人報名!');
        $message = array('runner_name' =>$data['runner_name'] ,
                         'runner_id' =>$data['runner_id'],
                         'runner_img' =>$data['runner_img'],
                         'task_name' =>$data['task_name'],
                         'task_id' =>$data['task_id']
                          );
        $CI->email->message( $CI->load->view( '/email/mail_make_offer_view', $message, true ) );

        if ( ! $CI->email->send()) {
            // error_log('Error email 寄信失敗, Sendemail_id='.$queue_id,1,"bobo52310@gmail.com","From: admin@citytasker.tw");
            return FALSE;
         }
         return TRUE;
    }

// 報名任務, 案主接收, 此案件是由spider產生
    function make_offer_by_spider($data, $queue_id) {
        // $to_mail, $runner_name, $runner_img, $task_name, $task_id
        $CI =& get_instance();
        $CI->load->library('email');
        $CI->email->from('admin@citytasker.tw', 'Citytasker');
        $CI->email->to($data['to_mail']);
        $CI->email->subject('任務:' . $data['task_name'] . '有人報名!');
        $message = array('runner_name' =>$data['runner_name'] ,
                         'runner_id' =>$data['runner_id'],
                         'runner_img' =>$data['runner_img'],
                         'task_name' =>$data['task_name'],
                         'task_id' =>$data['task_id'],
                         'task_content' =>$data['task_content']
                          );
        $CI->email->message( $CI->load->view( '/email/mail_make_offer_by_spider_view', $message, true ) );

        if ( ! $CI->email->send()) {
            // error_log('Error email 寄信失敗, Sendemail_id='.$queue_id,1,"bobo52310@gmail.com","From: admin@citytasker.tw");
            return FALSE;
         }
         return TRUE;
    }

    // 雇用通知, 幫手接收
    function give_offer($data, $queue_id){
    // $to_mail, $poster_img, $poster_name, $task_id, $task_name
        $CI =& get_instance();
        $CI->load->library('email');
        $CI->email->from('admin@citytasker.tw', 'Citytasker');
        $CI->email->to($data['to_mail']);
        $CI->email->subject('恭喜獲得任務:' . $data['task_name']);
        $message = array('poster_name' =>$data['poster_name'] ,
                         'poster_img' =>$data['poster_img'],
                         'task_name' =>$data['task_name'],
                         'task_id' =>$data['task_id']
                          );
        $CI->email->message( $CI->load->view( '/email/mail_give_offer_view', $message, true ) );

        if ( ! $CI->email->send()) {
            // error_log('Error email 寄信失敗, Sendemail_id='.$queue_id,1,"bobo52310@gmail.com","From: admin@citytasker.tw");
               return FALSE;
          }
        return TRUE;
    }

// 任務到期(沒人完成), 案主接收

// 任務已雇滿或任務到期(沒被指派到), 幫手接收

// 任務到期(有人出任務而且還沒有得到筆評價), 幫手接收

// 任務到期(有人出任務而且還沒有得到筆評價), 案主接收

// 提醒給予評價, 案主接收


    // 得到評價, 雙方接收
   function get_credit($data, $queue_id){
   // $to_mail, $giver_name, $giver_img,  $recipient_id, $task_id, $task_name
       $CI =& get_instance();
       $CI->load->library('email');
       $CI->email->from('admin@citytasker.tw', 'Citytasker');
       $CI->email->to($data['to_mail']);
       $CI->email->subject($data['giver_name'].' 給您任務評價:');
       $message = array('giver_name' =>$data['giver_name'] ,
                        'giver_img' =>$data['giver_img'],
                        'recipient_id' =>$data['recipient_id'],
								'recipient_identity' =>$data['recipient_identity'],
								'comment' =>$data['comment'],
                        'task_name' =>$data['task_name'],
                        'task_id' =>$data['task_id']
                         );
       $CI->email->message( $CI->load->view( '/email/mail_get_credit_view', $message, true ) );

       if ( ! $CI->email->send()) {
           // error_log('Error email 寄信失敗, Sendemail_id='.$queue_id,1,"bobo52310@gmail.com","From: admin@citytasker.tw");
              return FALSE;
         }
       return TRUE;
   }
    // 任務中留言, 跑者留言案主接收 案主留言所有留言者接收
    function task_discuss($data, $queue_id){
   // $to_mail, $giver_name, $giver_img, $discuss_text, $task_id, $task_name
       $CI =& get_instance();
       $CI->load->library('email');
       $CI->email->from('admin@citytasker.tw', 'Citytasker');
       $CI->email->to($data['to_mail']);
       $CI->email->subject('任務:'.$data['task_name'].' 有人回覆');
       $message = array('giver_name' =>$data['giver_name'] ,
                        'giver_img' =>$data['giver_img'],
                        'discuss_text' =>$data['discuss_text'],
                        'task_name' =>$data['task_name'],
                        'task_id' =>$data['task_id']
                         );
       $CI->email->message( $CI->load->view( '/email/mail_task_discuss_view', $message, true ) );

       if ( ! $CI->email->send()) {
           // error_log('Error email 寄信失敗, Sendemail_id='.$queue_id,1,"bobo52310@gmail.com","From: admin@citytasker.tw");
              return FALSE;
         }
       return TRUE;
   }


}
