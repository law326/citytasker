<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Google Geocoding API 編解碼
 * BoBo
 * https://developers.google.com/maps/documentation/geocoding/?hl=zh-TW#ReverseGeocoding
 *
 */
class Geocoding {

    function __construct(){
        //
    }
    function reverse_geocode($lat, $lng) {
        $latlng = $lat . ',' . $lng;
        $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' . $latlng .'&language=zh-TW&sensor=true';

        $response = $this->makerequest($url);

        if($response['status'] != 'OK') {
            return FALSE;
        } else {
            $response = $response['results'];
            return array(
                         // 'address' => $response['3']['formatted_address'],
                         'address' => $response['0']['formatted_address'],
                         'number' => $response['0']['address_components']['0']['long_name'],
                         'street' => $response['0']['address_components']['1']['long_name'],
                         'li' => $response['0']['address_components']['2']['long_name'],
                         'district' => $response['0']['address_components']['3']['long_name'],
                         'city' => $response['0']['address_components']['4']['long_name'],
                         'county' => $response['0']['address_components']['5']['long_name'],
                         'zip_code' => $response['0']['address_components']['6']['long_name']
                         );
                        /* array(8) {
                          ["address"]=>
                          string(49) "108台灣台北市萬華區東園街66巷7弄8號"
                          ["number"]=>
                          string(1) "8"
                          ["street"]=>
                          string(18) "東園街66巷7弄"
                          ["li"]=>
                          string(9) "忠德里"
                          ["district"]=>
                          string(9) "萬華區"
                          ["city"]=>
                          string(9) "台北市"
                          ["county"]=>
                          string(6) "台灣"
                          ["zip_code"]=>
                          string(3) "108"
                        } */

        }
    }

    function geocode($address) {
       $url = 'http://maps.googleapis.com/maps/api/geocode/json?address='. $address .'&components=country:TW&language=zh-TW&sensor=true';
             $response = $this->makerequest($url);

             if($response['status'] != 'OK') {
                 return FALSE;
             } else {
                 $response = $response['results'];

                 // TODO:把台灣所有的zip都去查詢,DB作一個對照表,之後由此表撈取
                 return array(
                              'lat' => $response['0']['geometry']['location']['lat'],
                              'lng' => $response['0']['geometry']['location']['lng'],
                              'zip' => $response['0']['address_components']['0']['long_name'],
                              'district' => $response['0']['address_components']['1']['long_name'],
                              'city' => $response['0']['address_components']['2']['long_name'],
                              'country' => $response['0']['address_components']['3']['long_name'],
                              'country_to_zip' => $response['0']['formatted_address']                         );
             }
    }

    function makerequest($url) {
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $url);
        $response = curl_exec($c);
        curl_close($c);

        return json_decode($response, TRUE);
    }
}
