<?php

$lang['required']			= " %s 欄位為必填.";
$lang['isset']				= " %s 欄位必須有值.";
$lang['valid_email']		= " %s 欄位 並不是有效的email.";
$lang['valid_emails']		= " %s 欄位 包含無效的email.";
$lang['valid_url']			= " %s 欄位 must contain a valid URL.";
$lang['valid_ip']			= " %s 欄位 must contain a valid IP.";
$lang['min_length']			= " %s 欄位 至少為 %s 字元長度.";
$lang['max_length']			= " %s 欄位 最多為 %s 字元長度.";
$lang['exact_length']		= " %s 欄位 must be exactly %s characters in length.";
$lang['alpha']				= " %s 欄位 may only contain alphabetical characters.";
$lang['alpha_numeric']		= " %s 欄位 may only contain alpha-numeric characters.";
$lang['alpha_dash']			= " %s 欄位 may only contain alpha-numeric characters, underscores, and dashes.";
$lang['numeric']			= " %s 欄位只能輸入數字.";
$lang['is_numeric']			= " %s 欄位只能輸入數字.";
$lang['integer']			= " %s 欄位只能輸入整數.";
$lang['regex_match']		= " %s 欄位 is not in  correct format.";
$lang['matches']			= " %s 欄位 與  %s 欄位 內容不同.";
$lang['is_unique'] 			= " %s 已註冊過.";
$lang['is_natural']			= " %s 欄位 必須為整數.";
$lang['is_natural_no_zero']	= " %s 欄位 must contain a number greater than zero.";
$lang['decimal']			= " %s 欄位 must contain a decimal number.";
$lang['less_than']			= " %s 欄位 must contain a number less than %s.";
$lang['greater_than']		= " %s 欄位 must contain a number greater than %s.";

// 以下為原本英文版
// $lang['required']     = "The %s field is required.";
// $lang['isset']        = "The %s field must have a value.";
// $lang['valid_email']    = "The %s field must contain a valid email address.";
// $lang['valid_emails']   = "The %s field must contain all valid email addresses.";
// $lang['valid_url']      = "The %s field must contain a valid URL.";
// $lang['valid_ip']     = "The %s field must contain a valid IP.";
// $lang['min_length']     = "The %s field must be at least %s characters in length.";
// $lang['max_length']     = "The %s field can not exceed %s characters in length.";
// $lang['exact_length']   = "The %s field must be exactly %s characters in length.";
// $lang['alpha']        = "The %s field may only contain alphabetical characters.";
// $lang['alpha_numeric']    = "The %s field may only contain alpha-numeric characters.";
// $lang['alpha_dash']     = "The %s field may only contain alpha-numeric characters, underscores, and dashes.";
// $lang['numeric']      = "The %s field must contain only numbers.";
// $lang['is_numeric']     = "The %s field must contain only numeric characters.";
// $lang['integer']      = "The %s field must contain an integer.";
// $lang['regex_match']    = "The %s field is not in the correct format.";
// $lang['matches']      = "The %s field does not match the %s field.";
// $lang['is_unique']      = "The %s field must contain a unique value.";
// $lang['is_natural']     = "The %s field must contain only positive numbers.";
// $lang['is_natural_no_zero'] = "The %s field must contain a number greater than zero.";
// $lang['decimal']      = "The %s field must contain a decimal number.";
// $lang['less_than']      = "The %s field must contain a number less than %s.";
// $lang['greater_than']   = "The %s field must contain a number greater than %s.";

/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */
